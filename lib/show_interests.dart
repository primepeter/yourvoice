import 'dart:ui';

import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'LangMain.dart';
import 'app_config.dart';

class ShowInterests extends StatefulWidget {
  @override
  _ShowInterestsState createState() => _ShowInterestsState();
}

class _ShowInterestsState extends State<ShowInterests> {
  List allItems = appSettingsModel.getList(APP_INTERESTS);
  List interests = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    allItems.sort((m1, m2) => m1[OBJECT_ID].compareTo(m2[OBJECT_ID]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(Language.value(LangKey.yourInterests)),
      // ),
      backgroundColor: AppConfig.appColor,
      body: interestBody(),
    );
  }

  interestBody() {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Image.asset(
          'assets/images/interest_bg.png',
          fit: BoxFit.cover,
        ),
        BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 8.0,
            sigmaY: 8.0,
          ),
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      black.withOpacity(.09),
                      AppConfig.appColor.withOpacity(.2)
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.1, 1.0])),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 40,
          ),
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  color: white.withOpacity(.6),
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(10),
                  child: Icon(
                    Icons.close,
                    size: 18,
                  ),
                ),
              ),
              Text(
                Language.value(LangKey.yourInterests),
                style: textStyle(true, 20, black),
              ),
              Container(
                padding: 15.padAll(),
                margin: 10.padAll(),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: black.withOpacity(.02),
                    border:
                        Border.all(color: AppConfig.appColor.withOpacity(.5))),
                child: Text(
                  Language.value(LangKey.interestTitle),
                  style: textStyle(false, 16, black),
                ),
              ),
              Flexible(
                child: GridView.builder(
                  padding: EdgeInsets.zero,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, childAspectRatio: .85),
                  itemBuilder: (ctx, p) {
                    Map item = allItems[p];
                    BaseModel model = BaseModel(items: item);
                    Map translations = model.getMap(TRANSLATIONS);
                    String value =
                        translations[appLanguage] ?? "No Translation";
                    if (value.isEmpty) value = "No Translation";
                    bool selected = interests.contains(model.getObjectId());
                    return GestureDetector(
                      onTap: () {
                        print(model.getObjectId());
                        //return;
                        if (selected)
                          interests.remove(model.getObjectId());
                        else
                          interests.add(model.getObjectId());
                        setState(() {});
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Card(
                              color: selected ? AppConfig.appColor : white,
                              elevation: 12,
                              child: Container(
                                height: selected ? 100 : 90,
                                width: selected ? 100 : 90,
                                padding: EdgeInsets.all(15),
                                alignment: Alignment.center,
                                child: CachedNetworkImage(
                                  imageUrl: model.getString(ICON),
                                  height: 80,
                                  width: 80,
                                  fit: BoxFit.cover,
                                  alignment: Alignment.center,
                                ),
                              ),
                              shape: CircleBorder(),
                              clipBehavior: Clip.antiAlias,
                            ),
                            Text(
                              value,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: textStyle(false, 14, black),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: allItems.length,
                ),
              ),
              if (interests.isNotEmpty)
                Container(
                  padding: 15.padAll(),
                  margin: 15.padAll(),
                  child: FlatButton(
                    onPressed: () {
                      setState(() {
                        userModel
                          ..put(INTERESTS, interests)
                          ..put(INTEREST_SHOWN, true)
                          ..updateItems();
                        showMessage(
                            context,
                            Icons.check,
                            Colors.green,
                            Language.value(LangKey.successReceived),
                            Language.value(LangKey.interestUpdated),
                            clickYesText: Language.value(LangKey.okString),
                            cancellable: true, onClicked: (_) async {
                          Navigator.pop(context);
                        });
                      });
                    },
                    color: AppConfig.appColor,
                    padding: EdgeInsets.all(16),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: white.withOpacity(.5)),
                        borderRadius: BorderRadius.circular(25)),
                    child: Center(
                      child: Text(
                        Language.value(LangKey.letsStart),
                        style: textStyle(true, 16, white),
                      ),
                    ),
                  ),
                ),
            ],
          ),
        )
      ],
    );
  }
}
