import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_photo_picker/photo_picker.dart';

import 'LangMain.dart';
import 'app/app.dart';
import 'app_config.dart';
import 'app_index.dart';

class AddSuggestions extends StatefulWidget {
  @override
  _AddSuggestionsState createState() => _AddSuggestionsState();
}

class _AddSuggestionsState extends State<AddSuggestions> {
  final messageController = TextEditingController();
  bool imagesEdited = false;
  List<BaseModel> photos = [];
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: white,
      body: pageBody(),
    );
  }

  pageBody() {
    return Container(
      margin: 20.padAt(t: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.centerRight,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              shape: CircleBorder(),
              padding: EdgeInsets.all(20),
              child: Icon(Icons.close),
            ),
          ),
          Container(
            padding: EdgeInsets.all(14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        Language.value(LangKey.addSuggestions),
                        textAlign: TextAlign.start,
                        style: textStyle(true, 25, Colors.black),
                      ),
                      addSpace(4),
                      Text(
                        Language.value(LangKey.suggestOrRequest),
                        style:
                            textStyle(false, 14, Colors.black.withOpacity(.7)),
                      )
                    ],
                  ),
                ),
                Image.asset(
                  "assets/images/suggestions.png",
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                )
              ],
            ),
          ),
          //addSpace(15),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            width: double.infinity,
            height: errorText.isEmpty ? 0 : 40,
            color: red0,
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Center(
                child: Text(
              errorText,
              style: textStyle(true, 16, white),
            )),
          ),

          Flexible(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(5),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    textCapitalization: TextCapitalization.sentences,
                    textAlignVertical: TextAlignVertical.top,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        //counter: null,
                        //labelText: "title",
                        labelStyle: textStyle(false, 18, black.withOpacity(.3)),
                        hintText: Language.value(LangKey.writeSuggestion)),
                    style: textStyle(
                      false,
                      18,
                      black,
                    ),
                    //focusNode: focusNode,
                    controller: messageController,
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: 6,
                    keyboardType: TextInputType.multiline,
                    inputFormatters: [],
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                addSpace(10),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        Language.value(LangKey.addPhotosAndVideos),
                        style: textStyle(true, 18, black),
                      ),
                      Text(
                        Language.value(LangKey.optional),
                        style: textStyle(false, 13, black.withOpacity(.5)),
                      ),
                    ],
                  ),
                ),
                addSpace(10),
                Row(
                  children: [
                    Flexible(
                      child: SingleChildScrollView(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: List.generate(photos.length, (index) {
                            BaseModel model = photos[index];
                            String imageUrl = model.getString(IMAGE_PATH);
                            bool online = imageUrl.startsWith("http");
                            bool active = 1 == index;
                            return GestureDetector(
                              onTap: () {
                                setState(() {});
                              },
                              child: Container(
                                padding: EdgeInsets.all(1),
                                margin: EdgeInsets.all(5),
                                height: 160,
                                width: 160,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: black.withOpacity(.1)),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Stack(
                                      children: [
                                        if (online)
                                          CachedNetworkImage(
                                            imageUrl: imageUrl,
                                            height: 160,
                                            width: 160,
                                            fit: BoxFit.cover,
                                          )
                                        else
                                          Image.file(
                                            File(imageUrl),
                                            height: 160,
                                            width: 160,
                                            fit: BoxFit.cover,
                                          ),
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Flexible(
                                                child: FlatButton(
                                                  onPressed: () {},
                                                  child: Icon(
                                                    Icons.visibility,
                                                    color: white,
                                                    size: 15,
                                                  ),
                                                  color: AppConfig.appColor,
                                                  padding: EdgeInsets.all(0),
                                                  materialTapTargetSize:
                                                      MaterialTapTargetSize
                                                          .shrinkWrap,
                                                  /*shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),*/
                                                ),
                                              ),
                                              Flexible(
                                                child: FlatButton(
                                                  onPressed: () {
                                                    photos.remove(model);
                                                    setState(() {});
                                                  },
                                                  child: Icon(
                                                    Icons.delete,
                                                    color: white,
                                                    size: 15,
                                                  ),
                                                  color: red0,
                                                  padding: EdgeInsets.all(0),
                                                  materialTapTargetSize:
                                                      MaterialTapTargetSize
                                                          .shrinkWrap,
                                                  /*shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topRight: Radius
                                                                  .circular(5),
                                                              bottomRight:
                                                                  Radius
                                                                      .circular(
                                                                          5))),*/
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )),
                              ),
                            );
                          }),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        PhotoPicker.openPicker(
                          mediaType: 'image', // image | video | any
                          multiple: true,
                          limit: 6,
                        ).then((value) {
                          if (null == value) return;
                          photos = value.map((e) {
                            BaseModel model = BaseModel();
                            bool isVideo = e.type == "video";
                            if (isVideo) {
                              model.put(THUMBNAIL_PATH,
                                  e.thumbnailUrl.replaceAll("file://", ""));
                              model.put(VIDEO_PATH,
                                  e.thumbnailUrl.replaceAll("file://", ""));
                              model.put(IS_VIDEO, true);
                            } else {
                              model.put(
                                  IMAGE_PATH, e.url.replaceAll("file://", ""));
                            }
                            return model;
                          }).toList();
                          setState(() {});
                        }).catchError((e) {
                          logError(e.code, e.bookmarkList);
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.all(5),
                        height: photos.isEmpty ? 100 : 50,
                        width: photos.isEmpty ? 100 : 50,
                        decoration: BoxDecoration(
                            shape: photos.isEmpty
                                ? BoxShape.rectangle
                                : BoxShape.circle,
                            borderRadius: photos.isEmpty
                                ? BorderRadius.circular(10)
                                : null,
                            color: black.withOpacity(0.09)),
                        child: Center(
                            child: Icon(
                          Icons.add_circle_outline,
                          size: 20,
                        )),
                      ),
                    )
                  ],
                ),
                addSpace(10),
                Container(
                  padding: EdgeInsets.all(14),
                  child: FlatButton(
                    onPressed: () {
                      onSavePressed();
                    },
                    color: AppConfig.appColor,
                    padding: EdgeInsets.all(16),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: Center(
                      child: Text(
                        Language.value(LangKey.sendSuggestion),
                        style: textStyle(true, 16, white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  onSavePressed() async {
    bool hasInternet = await isConnected();

    if (!hasInternet) {
      showError(Language.value(LangKey.noInternet));
      return;
    }

    if (messageController.text.isEmpty) {
      showError(Language.value(LangKey.suggestionRequired));
      return;
    }

    String id = getRandomId();
    BaseModel model = BaseModel();
    model.put(OBJECT_ID, id);
    model.put(MESSAGE, messageController.text);
    model.put(STATUS, PENDING);
    model.put(IMAGES, photos.map((e) => e.items).toList());
    suggestionsController.add(model);

    showMessage(
        context,
        Icons.check,
        Colors.green,
        Language.value(LangKey.successReceived),
        Language.value(LangKey.requestSubSuccessful),
        clickYesText: Language.value(LangKey.okString),
        delayInMilli: 1000,
        cancellable: true, onClicked: (_) async {
      Navigator.pop(context, "");
    });
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
