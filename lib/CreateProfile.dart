import 'dart:io';

import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateProfile extends StatefulWidget {
  @override
  _CreateProfileState createState() => _CreateProfileState();
}

class _CreateProfileState extends State<CreateProfile> {
  String addressMeBy;
  List addressMeList = appSettingsModel.getList(ADDRESS_ME_BY);

  final aboutMe = TextEditingController();
  final whatIdo = TextEditingController();
  String userImage;

  String countrySelected;
  String countryFlag;
  String countryCode;

  final country =
      getCountries().singleWhere((e) => e.countryName == userModel.country);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    aboutMe.text = userModel.aboutMe;
    whatIdo.text = userModel.whatIdo;
    userImage = userModel.userImage;
    addressMeBy = userModel.addressMe;
    countrySelected = country.countryName;
    countryFlag = country.countryFlag;
    countryCode = country.countryCode;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "Hi ${userModel.name}",
                style: textStyle(true, 20, black),
              )),
            ],
          ),
        ),
        addSpace(10),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: showSuccess ? dark_green0 : red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Flexible(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 10, right: 15, left: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: appColor_light,
                      borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    "We welcome you into His Presence.Our visions is and will remain to expose you to different powerful dimensions of God.",
                    style: textStyle(false, 14, white),
                  ),
                ),
                addSpace(10),
                GestureDetector(
                  onTap: () async {
                    PickedFile file = await ImagePicker()
                        .getImage(source: ImageSource.gallery);
                    if (file == null) return;
                    File croppedFile = await ImageCropper.cropImage(
                        sourcePath: file.path,
                        maxWidth: 2500,
                        maxHeight: 2500,
                        cropStyle: CropStyle.circle,
                        compressFormat: ImageCompressFormat.png);
                    if (croppedFile != null) {
                      userImage = croppedFile.path;
                      setState(() {});
                    }
                  },
                  child: Center(
                    child: Container(
                      height: 130,
                      width: 130,
                      margin: EdgeInsets.all(10),
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            height: 130,
                            width: 130,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: appColor, width: 2),
                            ),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: Builder(
                                  builder: (c) {
                                    if (userImage.isEmpty)
                                      return Container(
                                        height: 120,
                                        width: 120,
                                        color: black.withOpacity(.05),
                                        alignment: Alignment.center,
                                        child: Icon(
                                          Icons.person,
                                          color: black.withOpacity(.4),
                                          size: 30,
                                        ),
                                      );

                                    if (!userImage.contains("http"))
                                      return Image.file(
                                        File(userImage),
                                        fit: BoxFit.cover,
                                      );

                                    return CachedNetworkImage(
                                      imageUrl: userImage,
                                      fit: BoxFit.cover,
                                      placeholder: (c, z) {
                                        return Container(
                                          height: 120,
                                          width: 120,
                                          color: black.withOpacity(.05),
                                          alignment: Alignment.center,
                                          child: Icon(
                                            Icons.person,
                                            color: black.withOpacity(.4),
                                            size: 30,
                                          ),
                                        );
                                      },
                                    );
                                  },
                                )),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 40,
                              width: 40,
                              child: Icon(
                                Icons.camera_alt,
                                color: white,
                                size: 20,
                              ),
                              decoration: BoxDecoration(
                                  color: appColor,
                                  border: Border.all(color: white, width: 2),
                                  shape: BoxShape.circle),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          showListDialog(
                            context,
                            addressMeList,
                            (_) {
                              print(_);
                              addressMeBy = _[0];
                              setState(() {});
                            },
                            singleSelection: true,
                            selections: [addressMeBy],
                          );
                        },
                        child: Container(
                          height: 50,
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: blue09,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: black.withOpacity(.1), width: .5)),
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(
                                (null == addressMeBy || addressMeBy.isEmpty)
                                    ? "How may we address you?"
                                    : addressMeBy,
                                style: textStyle(
                                    false,
                                    16,
                                    black.withOpacity((null == addressMeBy ||
                                            addressMeBy.isEmpty)
                                        ? 0.5
                                        : 1)),
                              )),
                              Icon(Icons.keyboard_arrow_down_sharp)
                            ],
                          ),
                        ),
                      ),
                    ),
                    addSpaceWidth(10),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          pickCountry(context, (_) {
                            countrySelected = _.name;
                            countryFlag =
                                CountryPickerUtils.getFlagImageAssetPath(
                                    _.isoCode);
                            defaultCountry = _.name;
                            countryFlag = getCountries()
                                .singleWhere(
                                    (e) => e.countryName == countrySelected)
                                .countryFlag;
                            countryCode = getCountries()
                                .singleWhere(
                                    (e) => e.countryName == countrySelected)
                                .countryCode;

                            SharedPreferences.getInstance().then((value) =>
                                value.setString(COUNTRY, defaultCountry));
                            setState(() {});
                          });
                        },
                        child: Container(
                          height: 50,
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: blue09,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: black.withOpacity(.1), width: .5)),
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              if (null != countryFlag && countryFlag.isNotEmpty)
                                Image.asset(
                                  countryFlag,
                                  height: 25,
                                  width: 40,
                                  fit: BoxFit.cover,
                                ),
                              if (null != countryFlag && countryFlag.isNotEmpty)
                                addSpaceWidth(15),
                              Expanded(
                                  child: Text(
                                (null == countrySelected ||
                                        countrySelected.isEmpty)
                                    ? "Country?"
                                    : countrySelected,
                                style: textStyle(
                                    false,
                                    16,
                                    black.withOpacity(
                                        (null == countrySelected ||
                                                countrySelected.isEmpty)
                                            ? 0.5
                                            : 1)),
                              )),
                              Icon(Icons.keyboard_arrow_down_sharp)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Text(
                  "What do you do for Christ?",
                  style: textStyle(true, 14, appColor),
                ),
                addSpace(10),
                Container(
                  //height: 45,
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(10),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    onSubmitted: (_) {
                      //post();
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        hintText:
                            "Don't be scared, just tell the community what you do to serve the Lord Jesus Christ",
                        hintStyle: textStyle(false, 16, black.withOpacity(.4))),
                    style: textStyle(
                      false,
                      16,
                      black,
                    ),
                    controller: whatIdo,
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: 5,
                    keyboardType: TextInputType.number,
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                Text(
                  "Tell us about you?",
                  style: textStyle(true, 14, appColor),
                ),
                addSpace(10),
                Container(
                  //height: 45,
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(10),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    onSubmitted: (_) {
                      //post();
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        hintText:
                            "Let the community know a little more about you",
                        hintStyle: textStyle(false, 16, black.withOpacity(.4))),
                    style: textStyle(
                      false,
                      16,
                      black,
                    ),
                    controller: aboutMe,
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: 5,
                    keyboardType: TextInputType.number,
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                addSpace(10),
                RaisedButton(
                  onPressed: () {
                    handleSave();
                  },
                  padding: EdgeInsets.all(14),
                  color: appColor_light,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: Text(
                      "PROCEED",
                      style: textStyle(true, 16, white),
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  savePhoto() {
    showProgress(true, context, msg: "Saving Photo");
    uploadFile(File(userImage), (res, error) {
      showProgress(false, context);
      Future.delayed(Duration(milliseconds: 500), () {
        if (error != null) {
          showError("Error");
          return;
        }
        userImage = res;
        userModel.put(USER_IMAGE, userImage);
        setState(() {});
        handleSave();
      });
    });
  }

  handleSave() {
    if (null == addressMeBy || addressMeBy.isEmpty) {
      showError("How may we address you?");
      return;
    }
    if (null == countrySelected || countrySelected.isEmpty) {
      showError("Choose your country?");
      return;
    }

    if (whatIdo.text.isEmpty) {
      showError("What do you do for christ?");
      return;
    }

    if (aboutMe.text.isEmpty) {
      showError("Tell us about you?");
      return;
    }

    if (!userImage.startsWith("http")) {
      savePhoto();
      return;
    }

    userModel
      ..put(ADDRESS_ME, addressMeBy)
      ..put(WHAT_I_DO, whatIdo.text)
      ..put(ABOUT_ME, aboutMe.text)
      ..put(COUNTRY, countrySelected)
      ..updateItems();
    popUpWidgetScreenUntil(context, AppIndex());
  }
}
