import 'package:acclaim/app_config.dart';
import 'package:flutter/material.dart';

import 'app/app.dart';

class EditPrivacyPolicy extends StatefulWidget {
  @override
  _EditPrivacyPolicyState createState() => _EditPrivacyPolicyState();
}

class _EditPrivacyPolicyState extends State<EditPrivacyPolicy> {
  BaseModel model = appSettingsModel.getModel(PRIVACY_POLICY);

  final translationController = TextEditingController();
  int currentIndex = 0;
  List<BaseModel> options = [];
  List<TextEditingController> languages = [];

  String selectedLanguage = '';
  String selectedKey;
  FocusNode focusNode = FocusNode();

  insertLanguages() {
    Map map = model.getMap(TRANSLATIONS);
    final languages = appSettingsModel.getList(APP_LANGUAGE);
    for (var s in languages) {
      map[s.toLowerCase()] = '';
    }
    model.put(TRANSLATIONS, map);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (model.getMap(TRANSLATIONS).isEmpty) {
      insertLanguages();
    }

    selectedLanguage = model.getMap(TRANSLATIONS).keys.toList()[0];
    selectedKey = model.getMap(TRANSLATIONS).keys.toList()[0].toLowerCase();
    translationController.text = model.getMap(TRANSLATIONS).values.toList()[0];
    translationController.addListener(() {
      final map = model.getMap(TRANSLATIONS);
      String value = translationController.text;
      map[selectedKey] = value;
      model.put(TRANSLATIONS, map);
      setState(() {});
    });
    focusNode.requestFocus();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    translationController.dispose();
  }

  int clickBack = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int now = DateTime.now().millisecondsSinceEpoch;
        if ((now - clickBack) > 5000) {
          clickBack = now;
          showError("Click back again to exit");
          return false;
        }
        Navigator.pop(context, "");
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding:
                  EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BackButton(
                    onPressed: () {
                      Navigator.pop(context, '');
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Text(
                      "Privacy Settings",
                      style: textStyle(true, 25, black),
                    ),
                  )
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: showSuccess ? dark_green0 : red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            page(),
            Container(
              padding: EdgeInsets.all(20),
              child: FlatButton(
                onPressed: () {
                  if (selectedLanguage.isEmpty) {
                    showError("Choose translation language");
                    return;
                  }

                  post();
                },
                color: AppConfig.appColor,
                padding: EdgeInsets.all(15),
                child: Center(
                    child: Text(
                  'UPDATE',
                  style: textStyle(false, 18, white),
                )),
              ),
            )
          ],
        ),
      ),
    );
  }

  page() {
    return Expanded(
      child: ListView(
        padding: EdgeInsets.all(10),
        children: [
          clickText("Choose Language", selectedLanguage, () {
            final map = model.getMap(TRANSLATIONS);
            List items =
                map.keys.map((e) => e.toString().toUpperCase()).toList();
            showListDialog(context, items,  (_) {
              selectedLanguage = items[_];
              selectedKey = map.keys.toList()[_];
              translationController.text = map[selectedKey];
              setState(() {});
            });
          }),
          inputTextView(
            "Privacy Policy",
            translationController,
            maxLine: 15,
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
                border: Border.all(color: black.withOpacity(.1), width: .5),
                color: blue09),
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(model.getMap(TRANSLATIONS).keys.length,
                  (index) {
                String key = model.getMap(TRANSLATIONS).keys.toList()[index];
                String value = model.getMap(TRANSLATIONS)[key];
                return nameItem(
                    key, value.isEmpty ? "No Translation Yet" : value);
              }),
            ),
          ),
        ],
      ),
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  inputTextView(String title, controller, {int maxLine = 1}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          //constraints: BoxConstraints(maxHeight: 250),
          margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
          decoration: BoxDecoration(
              color: blue09,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: black.withOpacity(.1), width: .5)),
          child: new TextField(
            textCapitalization: TextCapitalization.sentences,
            textAlignVertical: TextAlignVertical.top,
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                counter: null,
                labelText: title,
                labelStyle: textStyle(false, 18, black.withOpacity(.3)),
                hintText: "Write the Privacy Policy of the app"),
            style: textStyle(
              false,
              18,
              black,
            ),
            focusNode: focusNode,
            controller: controller,
            cursorColor: black,
            cursorWidth: 1,
            maxLines: 15,
            keyboardType: TextInputType.multiline,
            inputFormatters: [],
            scrollPadding: EdgeInsets.all(0),
          ),
        ),
      ],
    );
  }

  post() {
    appSettingsModel
      ..put(PRIVACY_POLICY, model.items)
      ..updateItems();
    showError("Updated!!!", success: true);
  }
}
