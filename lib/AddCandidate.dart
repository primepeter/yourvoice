import 'dart:io';

import 'package:acclaim/LangMain.dart';
import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'SelectRegion.dart';
import 'ShowDistricts.dart';

class AddCandidate extends StatefulWidget {
  final bool isPickup;
  final BaseModel model;
  const AddCandidate({Key key, this.isPickup = false, this.model})
      : super(key: key);
  @override
  _AddCandidateState createState() => _AddCandidateState();
}

class _AddCandidateState extends State<AddCandidate>
    with WidgetsBindingObserver {
  final nameController = TextEditingController();
  String state = '';
  String senatorialDistrict = '';
  BaseModel district;
  String position = '';
  String sit = '';
  String party = '';
  List localGovs = [];
  List electoralPosition = appSettingsModel.getList(POSITIONS);
  List sitType = appSettingsModel.getList(SIT_TYPE);
  List politicalParty = appSettingsModel.getList(PARTIES);
  String selfieImage = '';
  bool selfieError = false;
  bool presidentSelected = false;
  bool govSelected = false;
  bool senatorSelected = false;

  BaseModel model = BaseModel();
  String objectId = getRandomId();

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    if (widget.model != null) {
      model = widget.model;
      objectId = model.getObjectId();
      nameController.text = model.getString(NAME);
      selfieImage = model.getString(IMAGE_URL);
      state = model.getString(STATE);
      party = model.getString(POLITICAL_PARTY);
      position = electoralPosition[model.getInt(ELECTORAL_POSITION)];
      sit = sitType[model.getInt(SIT_TYPE)];
      presidentSelected = position == "President";
      govSelected = position == "Governor";
      senatorSelected = position == "Senator";
      localGovs = model.getList(STATE_LOCALS);
      district = model.getModel(DISTRICT);
      senatorialDistrict = model.getModel(DISTRICT).getString(NAME);
      setState(() {});
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  onError(e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0,
        Language.value(LangKey.errorReceived), e.bookmarkList,
        delayInMilli: 900, cancellable: true);
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    FocusScope.of(context).requestFocus(FocusNode());
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 30, right: 10, left: 10),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BackButton(),
                    Text(
                      "Add Candidate",
                      style: textStyle(true, 20, black),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 500),
                width: double.infinity,
                height: errorText.isEmpty ? 0 : 40,
                color: red0,
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Center(
                    child: Text(
                  errorText,
                  style: textStyle(true, 16, white),
                )),
              ),
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "All fields are mandatory",
                        style: textStyle(true, 15, appColor),
                      ),
                      addSpace(10),
                      InkWell(
                        onTap: () async {
                          FocusScope.of(context).requestFocus(FocusNode());
                          PickedFile file = await ImagePicker()
                              .getImage(source: ImageSource.gallery);
                          if (file == null) return;
                          File croppedFile = await ImageCropper.cropImage(
                              sourcePath: file.path,
                              maxWidth: 2500,
                              maxHeight: 2500,
                              cropStyle: CropStyle.circle,
                              compressFormat: ImageCompressFormat.png);
                          if (croppedFile != null) {
                            selfieImage = croppedFile.path;
                            setState(() {});
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 0, right: 10),
                                alignment: Alignment.center,
                                child: Container(
                                  width: 125,
                                  height: 125,
                                  child: Stack(
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(60),
                                        child: Container(
                                          width: 120,
                                          height: 120,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: appColor.withOpacity(.06),
                                              border: Border.all(
                                                  width: selfieError ? 3 : 1,
                                                  color: selfieError
                                                      ? red0
                                                      : black
                                                          .withOpacity(.02))),
                                          child: selfieImage.isEmpty
                                              ? Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Icon(
                                                      Icons.person_add,
                                                      size: 30,
                                                      color:
                                                          black.withOpacity(.5),
                                                    ),
                                                    Text("Add")
                                                  ],
                                                )
                                              : selfieImage.startsWith('http')
                                                  ? CachedNetworkImage(
                                                      imageUrl: selfieImage,
                                                      width: 120,
                                                      height: 120,
                                                      fit: BoxFit.cover,
                                                    )
                                                  : Image.file(
                                                      File(selfieImage),
                                                      width: 120,
                                                      height: 120,
                                                      fit: BoxFit.cover,
                                                    ),
                                        ),
                                      ),
                                      // Align(
                                      //   alignment: Alignment.topRight,
                                      //   child: GestureDetector(
                                      //     onTap: () {
                                      //       showMessage(
                                      //           context,
                                      //           Icons.person,
                                      //           appColor,
                                      //           'Selfie Image',
                                      //           "It's important that you take a selfie photo of yourself."
                                      //               " This is to prevent fraud your identity remains anonymous.");
                                      //     },
                                      //     child: Container(
                                      //       decoration: BoxDecoration(
                                      //           color: appColor,
                                      //           shape: BoxShape.circle,
                                      //           border: Border.all(color: white, width: 3)),
                                      //       height: 35,
                                      //       width: 35,
                                      //       padding: EdgeInsets.all(2),
                                      //       child: Center(
                                      //           child: Icon(
                                      //             Icons.info_outline,
                                      //             color: white,
                                      //             size: 20,
                                      //           )),
                                      //     ),
                                      //   ),
                                      // )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      clickField('Political Party', 'Choose Party', party, () {
                        FocusScope.of(context).requestFocus(FocusNode());

                        showListDialog(
                          context,
                          politicalParty,
                          (_) {
                            print(_);
                            party = _[0];

                            setState(() {});
                          },
                          singleSelection: true,
                          selections: [party],
                        );
                      }),
                      clickField(
                          'Electoral Position', 'Choose Position', position,
                          () {
                        FocusScope.of(context).requestFocus(FocusNode());

                        showListDialog(
                          context,
                          electoralPosition,
                          (_) {
                            print(_);
                            position = _[0];
                            presidentSelected = position == "President";
                            govSelected = position == "Governor";
                            senatorSelected = position == "Senator";
                            setState(() {});
                          },
                          singleSelection: true,
                          selections: [position],
                        );
                      }),
                      clickField('Sit Type', 'Choose Type', sit, () {
                        FocusScope.of(context).requestFocus(FocusNode());

                        showListDialog(
                          context,
                          sitType,
                          (_) {
                            print(_);
                            sit = _[0];
                            setState(() {});
                          },
                          singleSelection: true,
                          selections: [sit],
                        );
                      }),

                      textField("Name", 'Candidate name', nameController),
                      if (!presidentSelected)
                        clickField('State', 'Choose State', state, () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          pushAndResult(
                              context,
                              SelectRegion(
                                canSelectOnlyState: true,
                              ),
                              transitionBuilder: fadeTransition, result: (_) {
                            if (null == _ || !(_ is List)) return;
                            String pState = _[0];

                            if (pState != state) localGovs.clear();
                            state = pState;
                            if (mounted) setState(() {});
                          });
                        }),
                      // if (!presidentSelected)
                      //   if (!govSelected)
                      if (senatorSelected)
                        clickField(
                          'Senatorial District.',
                          'Choose a District',
                          senatorialDistrict,
                          () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            pushAndResult(
                                context,
                                ShowDistricts(
                                  popResult: true,
                                ),
                                transitionBuilder: fadeTransition, result: (_) {
                              if (null == _) return;
                              district = _;
                              senatorialDistrict = district.getString(NAME);
                              // String pState = _[0];
                              //
                              // if (pState != state) localGovs.clear();
                              // state = pState;
                              // //constituency = _[1];
                              // //stateConstituency = '$constituency in $state';
                              if (mounted) setState(() {});
                            });
                          },
                        ),
                      if (district != null && district.items.isNotEmpty)
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: Card(
                            color: default_white,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                // side: BorderSide(
                                //     color: appColor
                                //         .withOpacity(active ? 1 : 0),
                                //     width: active ? 2 : 1),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5))),
                            clipBehavior: Clip.antiAlias,
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Flexible(
                                        fit: FlexFit.tight,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Image.asset(
                                                  'assets/images/district.jpg',
                                                  width: 14,
                                                  height: 14,
                                                  color: light_green3,
                                                ),
                                                addSpaceWidth(5),
                                                Text(
                                                  senatorialDistrict +
                                                      ' - ' +
                                                      state,
                                                  style: textStyle(
                                                      true, 12, light_green3),
                                                ),
                                              ],
                                            ),
                                            addSpace(5),
                                            Text(
                                              "Local.Govs Constituencies",
                                              style: textStyle(true, 13, black),
                                            ),
                                            addSpace(5),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  addSpace(5),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: List.generate(
                                          district.getList(STATE_LOCALS).length,
                                          (index) {
                                        String item = district
                                            .getList(STATE_LOCALS)[index];
                                        return Card(
                                          color: white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Text(
                                                  item,
                                                  style: textStyle(false, 12,
                                                      black.withOpacity(.5)),
                                                ),
                                                addSpace(5),
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                  addSpace(5),
                                ],
                              ),
                            ),
                          ),
                        ),
                      addSpace(50),
                    ],
                  ),
                ),
              ),
              addSpace(15),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  onPressed: () async {
                    proceed();
                  },
                  color: appColor,
                  padding: EdgeInsets.all(14),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                      child: Text(
                    'Save',
                    style: textStyle(true, 16, white),
                  )),
                ),
              ),
              addSpace(15),
            ],
          ),
        ],
      ),
    );
  }

  savePhoto() {
    showProgress(true, context, msg: "Saving Photo");
    uploadFile(File(selfieImage), (res, error) {
      showProgress(false, context);
      Future.delayed(Duration(milliseconds: 500), () {
        if (error != null) {
          showError("Error");
          return;
        }
        selfieImage = res;
        model.put(IMAGE_URL, res);
        setState(() {});
        proceed();
      });
    });
  }

  proceed() async {
    String name = nameController.text;
    FocusScope.of(context).requestFocus(FocusNode());

    if (selfieImage.isEmpty) {
      showError("Add Photo!");
      return;
    }
    if (party.isEmpty) {
      showError("Choose Party!");
      return;
    }

    if (position.isEmpty) {
      showError("Choose Position!");
      return;
    }
    if (sit.isEmpty) {
      showError("Choose Sit!");
      return;
    }
    if (name.isEmpty) {
      showError("Enter Name!");
      return;
    }

    if (!presidentSelected && state.isEmpty) {
      showError("Choose a State!");
      return;
    }

    if (senatorSelected && district == null) {
      showError("Add Local.Gov Constituency!");
      return;
    }

    // return;

    if (!selfieImage.startsWith("http")) {
      savePhoto();
      return;
    }

    model.put(NAME, name);
    model.put(STATE, state);
    model.put(
        ELECTORAL_POSITION, electoralPosition.indexWhere((e) => e == position));

    model.put(SIT_TYPE, sitType.indexWhere((e) => e == sit));

    model.put(POLITICAL_PARTY, party);
    model.put(DISTRICT, district == null ? {} : district.items);
    model.saveItem(CANDIDATE_BASE, false, document: objectId, merged: true,
        onComplete: () {
      showMessage(context, Icons.check_circle, appColor, "Candidate Created!",
          "Electoral Candidate has been created Successfully.",
          cancellable: false,
          delayInMilli: 800,
          clickYesText: 'Go Back', onClicked: (_) {
        Navigator.pop(context, model);
      });
    });
  }
}

textField(String title, String hint, TextEditingController controller,
    {bool numb = false, int max = 1}) {
  return Container(
    padding: EdgeInsets.only(top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyle(false, 15, appColor),
        ),
        addSpace(10),
        Container(
          decoration: BoxDecoration(
              color: black.withOpacity(.02),
              border: Border.all(color: black.withOpacity(.02)),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: TextField(
            controller: controller,
            maxLines: max,
            keyboardType: numb ? TextInputType.number : null,
            decoration:
                InputDecoration(border: InputBorder.none, hintText: hint),
          ),
        )
      ],
    ),
  );
}

clickField(String title, String hint, String value, onTap,
    {bool list = false, List items = const [], removeAt}) {
  bool isEmpty = value.isEmpty;

  return Container(
    padding: EdgeInsets.only(top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyle(false, 15, appColor),
        ),
        addSpace(10),
        if (items.isNotEmpty)
          ...List.generate(
              items.length,
              (index) => Container(
                    decoration: BoxDecoration(
                        color: black.withOpacity(.02),
                        border: Border.all(color: black.withOpacity(.2)),
                        borderRadius: BorderRadius.circular(5)),
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(bottom: 10),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            items[index],
                            style: textStyle(true, 15, appColor),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            if (null != removeAt) removeAt(index);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: red0,
                              // border: Border.all(color: black.withOpacity(.2)),
                            ),
                            child: Icon(
                              Icons.clear,
                              size: 20,
                              color: white,
                            ),
                          ),
                        )
                      ],
                    ),
                  )),
        if (items.isNotEmpty) addSpace(10),
        InkWell(
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
                color: black.withOpacity(.02),
                border: Border.all(color: black.withOpacity(.02)),
                borderRadius: BorderRadius.circular(5)),
            padding: EdgeInsets.only(left: 10, right: 10),
            alignment: Alignment.centerLeft,
            height: 50,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    isEmpty ? hint : value,
                    style: textStyle(
                        false, 15, black.withOpacity(isEmpty ? 0.5 : 1)),
                  ),
                ),
                Icon(
                  list
                      ? Icons.add_circle_outline
                      : Icons.arrow_drop_down_circle_outlined,
                  size: 20,
                  color: black.withOpacity(.5),
                )
              ],
            ),
          ),
        )
      ],
    ),
  );
}
