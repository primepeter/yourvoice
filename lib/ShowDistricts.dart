import 'package:acclaim/AddDistrict.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'app/app.dart';

class ShowDistricts extends StatefulWidget {
  final bool popResult;

  const ShowDistricts({Key key, this.popResult = false}) : super(key: key);
  @override
  _ShowDistrictsState createState() => _ShowDistrictsState();
}

class _ShowDistrictsState extends State<ShowDistricts> {
  List items = [];
  List allItems = [];
  bool setup = false;
  String objectId = '';

  TextEditingController searchController = TextEditingController();
  bool showCancel = false;
  FocusNode focusSearch = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadItems();
  }

  loadItems() async {
    setup = false;
    setState(() {});
    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(DISTRICT_BASE)
        //.where(COUNTRY, isEqualTo: 'nigeria')
        .get();
    for (DocumentSnapshot shot in shots.docs) {
      BaseModel model = BaseModel(doc: shot);
      allItems.add(model);
    }
    reload();
  }

  reload() {
    String search = searchController.text.trim().toLowerCase();
    items.clear();
    for (BaseModel item in allItems) {
      if (search.isNotEmpty) {
        String state = item.getString(STATE);
        if (state.toLowerCase().contains(search)) {
          items.add(item);
        } else {
          List cities = List.from(item.getList(STATE_LOCALS));
          bool exist = false;
          for (var s in cities) {
            // print(s);
            bool exist = s.toLowerCase().contains(search.toLowerCase());
            if (!exist) continue;
            items.add(item);
            //selectedState = item.getString(STATE);
          }
        }
        continue;
      }
      items.add(item);
    }
    setup = true;
    if (mounted) setState(() {});
  }

  RefreshController refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(25),
        new Container(
          width: double.infinity,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: Text(
                  "Districts",
                  style: textStyle(true, 20, black),
                ),
              ),
              if (objectId.isNotEmpty)
                FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: appColor,
                    onPressed: () {
                      BaseModel model =
                          items.singleWhere((e) => e.getObjectId() == objectId);
                      Navigator.pop(context, model);
                    },
                    child: Text(
                      'Select',
                      style: textStyle(true, 16, white),
                    )),
              if (!widget.popResult)
                FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: appColor,
                    onPressed: () {
                      pushAndResult(context, AddDistrict(), result: (_) {
                        items.add(_);

                        setState(() {});
                      });
                    },
                    child: Text(
                      'Add',
                      style: textStyle(true, 16, white),
                    )),
              addSpaceWidth(10),
            ],
          ),
        ),
        if (setup)
          Container(
            height: 45,
            margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
            decoration: BoxDecoration(
                color: white.withOpacity(.8),
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: black.withOpacity(.1), width: 1)),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                addSpaceWidth(10),
                Icon(
                  Icons.search,
                  color: black.withOpacity(.09),
                  size: 17,
                ),
                addSpaceWidth(10),
                new Flexible(
                  flex: 1,
                  child: new TextField(
                    textInputAction: TextInputAction.search,
                    textCapitalization: TextCapitalization.sentences,
                    autofocus: false,
                    onSubmitted: (_) {
                      //reload();
                    },
                    decoration: InputDecoration(
                        hintText: "Search",
                        hintStyle: textStyle(
                          false,
                          18,
                          black.withOpacity(.4),
                        ),
                        border: InputBorder.none,
                        isDense: true),
                    style: textStyle(false, 16, black),
                    controller: searchController,
                    cursorColor: black,
                    cursorWidth: 1,
                    focusNode: focusSearch,
                    keyboardType: TextInputType.text,
                    onChanged: (s) {
                      showCancel = s.trim().isNotEmpty;
                      setState(() {});
                      reload();
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      focusSearch.unfocus();
                      showCancel = false;
                      searchController.text = "";
                    });
                    reload();
                  },
                  child: showCancel
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                          child: Icon(
                            Icons.close,
                            color: black,
                            size: 20,
                          ),
                        )
                      : new Container(),
                )
              ],
            ),
          ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: messageText.isEmpty ? 0 : 40,
          color: light_green3,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            messageText,
            style: textStyle(true, 14, white),
          )),
        ),
        Expanded(
            child: !setup
                ? loadingLayout()
                : items.isEmpty
                    ? emptyLayout(Icons.report, "No District Yet", "",
                        clickText: "Reload", click: () {
                        setup = false;
                        setState(() {});
                        loadItems();
                      })
                    : ListView.builder(
                        itemBuilder: (c, p) {
                          BaseModel model = items[p];
                          String name = model.getString(NAME);
                          String state = model.getString(STATE);
                          List states = model.getList(STATE_LOCALS);
                          bool active = objectId == model.getObjectId();
                          return GestureDetector(
                            onTap: () {
                              if (widget.popResult) {
                                objectId = model.getObjectId();
                                setState(() {});
                                return;
                              }

                              showListDialog(context, ['Edit', 'Delete'], (_) {
                                print(_);
                                if (_ == 'Edit') {
                                  pushAndResult(
                                      context,
                                      AddDistrict(
                                        model: model,
                                      ), result: (_) {
                                    items[p] = _;
                                    setState(() {});
                                  });
                                }

                                if (_ == 'Delete') {
                                  model.deleteItem();
                                  items.removeAt(p);
                                  setState(() {});
                                }
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Card(
                                color: default_white,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: appColor
                                            .withOpacity(active ? 1 : 0),
                                        width: active ? 2 : 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                clipBehavior: Clip.antiAlias,
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Flexible(
                                            fit: FlexFit.tight,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Image.asset(
                                                      'assets/images/district.jpg',
                                                      width: 14,
                                                      height: 14,
                                                      color: light_green3,
                                                    ),
                                                    addSpaceWidth(5),
                                                    Text(
                                                      name + ' - ' + state,
                                                      style: textStyle(true, 12,
                                                          light_green3),
                                                    ),
                                                  ],
                                                ),
                                                addSpace(5),
                                                Text(
                                                  "Local.Govs Constituencies",
                                                  style: textStyle(
                                                      true, 13, black),
                                                ),
                                                addSpace(5),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      addSpace(5),
                                      SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: List.generate(states.length,
                                              (index) {
                                            String item = states[index];
                                            return Card(
                                              color: white,
                                              elevation: 0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      item,
                                                      style: textStyle(
                                                          false,
                                                          12,
                                                          black
                                                              .withOpacity(.5)),
                                                    ),
                                                    addSpace(5),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                                      addSpace(5),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                        shrinkWrap: true,
                        itemCount: items.length,
                        padding: EdgeInsets.zero,
                      ))
      ],
    );
  }

  String messageText = "";
  popMessage(String text) {
    messageText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      messageText = "";
      setState(() {});
    });
  }
}
