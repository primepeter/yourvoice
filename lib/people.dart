import 'dart:ui';

import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'LangMain.dart';
import 'app_config.dart';
import 'app_index.dart';

class People extends StatefulWidget {
  final List selections;

  const People({Key key, this.selections = const []}) : super(key: key);
  @override
  _PeopleState createState() => _PeopleState();
}

class _PeopleState extends State<People> {
  TextEditingController searchController = TextEditingController();
  bool setup = false;
  bool showCancel = false;
  FocusNode focusSearch = FocusNode();

  List allItems = [];
  List items = [];
  List selections = [];

  @override
  initState() {
    super.initState();
    selections = widget.selections;
    allItems = random100Users.map((e) => e.items).toList();
    allItems.shuffle();
    reload();
  }

  reload() {
    String search = searchController.text.trim().toLowerCase();
    items.clear();
    for (Map item in allItems) {
      if (search.isNotEmpty) {
        List values = item.values.toList();
        bool exist = false;
        for (var a in values) {
          if (a.toString().toLowerCase().contains(search)) {
            exist = true;
            break;
          }
        }
        if (!exist) {
          loadOnline(search);
          continue;
        }
      }
      items.add(item);
    }

    if (mounted) setState(() {});
  }

  loadOnline(String search) async {
    Firestore.instance
        .collection(USER_BASE)
        .where(SEARCH_PARAM, arrayContains: search)
        .getDocuments()
        .then((value) {
      for (var doc in value.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        int p = items.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
        if (p == -1)
          items.add(doc.data);
        else
          items[p] = doc.data;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppConfig.appColor,
      body: page(),
    );
  }

  page() {
    return Stack(
      fit: StackFit.expand,
      children: [
        Image.asset(
          'assets/images/auth_bg.png',
          fit: BoxFit.cover,
        ),
        Image.asset(
          'assets/images/pattern_bg.png',
          fit: BoxFit.cover,
        ),
        BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 8.0,
            sigmaY: 8.0,
          ),
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      black.withOpacity(.09),
                      AppConfig.appColor.withOpacity(.5)
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.1, 1.0])),
          ),
        ),
        Column(
          children: [
            addSpace(30),
            Padding(
              padding: const EdgeInsets.only(left: 0),
              child: Row(
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        child: Center(
                            child: Icon(
                          Icons.keyboard_backspace,
                          color: white,
                          size: 25,
                        )),
                      )),
                  Expanded(
                      child: Text(
                    LangKey.people.toAppLanguage,
                    style: textStyle(true, 20, white),
                  )),
                  if (selections.isNotEmpty)
                    FlatButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        color: dark_green5,
                        onPressed: () {
                          Navigator.pop(context, selections);
                        },
                        child: Text(
                          LangKey.choose.toAppLanguage,
                          style: textStyle(true, 14, white),
                        )),
                  addSpaceWidth(10)
                  /*InkWell(
                      onTap: () {},
                      child: Container(
                        width: 50,
                        height: 50,
                        child: Center(
                            child: Icon(
                          Icons.search,
                          color: white,
                          size: 25,
                        )),
                      )),*/
                ],
              ),
            ),
            Container(
              height: 45,
              margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
              decoration: BoxDecoration(
                  color: white.withOpacity(.8),
                  borderRadius: BorderRadius.circular(25),
                  border: Border.all(color: black.withOpacity(.1), width: 1)),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  addSpaceWidth(10),
                  Icon(
                    Icons.search,
                    color: AppConfig.appColor.withOpacity(.8),
                    size: 17,
                  ),
                  addSpaceWidth(10),
                  new Flexible(
                    flex: 1,
                    child: new TextField(
                      textInputAction: TextInputAction.search,
                      textCapitalization: TextCapitalization.sentences,
                      autofocus: false,
                      onSubmitted: (_) {
                        //reload();
                      },
                      decoration: InputDecoration(
                          hintText: LangKey.search.toAppLanguage,
                          hintStyle: textStyle(
                            false,
                            18,
                            AppConfig.appColor.withOpacity(.8),
                          ),
                          border: InputBorder.none,
                          isDense: true),
                      style: textStyle(false, 16, black),
                      controller: searchController,
                      cursorColor: black,
                      cursorWidth: 1,
                      focusNode: focusSearch,
                      keyboardType: TextInputType.text,
                      onChanged: (s) {
                        showCancel = s.trim().isNotEmpty;
                        setState(() {});
                        reload();
                      },
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        focusSearch.unfocus();
                        showCancel = false;
                        searchController.text = "";
                      });
                      reload();
                    },
                    child: showCancel
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                            child: Icon(
                              Icons.close,
                              color: black,
                              size: 20,
                            ),
                          )
                        : new Container(),
                  ),
                  addSpaceWidth(10),
                ],
              ),
            ),
            pageList()
          ],
        )
      ],
    );
  }

  pageList() {
    return Expanded(
      child: Builder(
        builder: (c) {
          return SingleChildScrollView(
            child: Container(
              width: double.infinity,
              child: Wrap(
                //alignment: WrapAlignment.start,
                children: List.generate(items.length, (index) {
                  Map item = items[index];
                  BaseModel model = BaseModel(items: item);
                  String username = model.getString(NICKNAME);
                  String name = model.getString(NAME);
                  name = name.replaceAll(" ", ".");
                  if (name.length > 15) name = name.substring(0, 14) + "..";

                  bool selected = selections.contains(item);

                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        if (selected) {
                          selections.remove(item);
                        } else {
                          selections.add(item);
                        }
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: selected
                              ? AppConfig.appColor
                              : black.withOpacity(.6),
                          border: Border.all(
                              color: white.withOpacity(selected ? 1 : 0)),
                          borderRadius: BorderRadius.circular(25)),
                      padding:
                          EdgeInsets.only(left: 5, right: 5, top: 3, bottom: 3),
                      margin: EdgeInsets.all(3),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: white),
                              shape: BoxShape.circle,
                              // borderRadius: BorderRadius.circular(10),
                            ),
                            padding: EdgeInsets.all(1),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: CachedNetworkImage(
                                imageUrl: userModel.userImage,
                                height: 25,
                                width: 25,
                                fit: BoxFit.cover,
                                placeholder: (c, x) {
                                  return Container(
                                    height: 25,
                                    width: 25,
                                    child: Icon(
                                      Icons.person,
                                      size: 18,
                                      color: white.withOpacity(.5),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          addSpaceWidth(5),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                name,
                                style:
                                    textStyle(false, selected ? 14 : 13, white),
                              ),
                              Text(
                                "@$username",
                                style: textStyle(false, selected ? 13 : 12,
                                    white.withOpacity(.7)),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                }),
              ),
            ),
          );
        },
      ),
    );
  }

  String names(int p) {
    if (p.isEven) return "Maugost King";
    return "Esther";
  }
}
