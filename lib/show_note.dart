import 'dart:io';

import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'app_config.dart';

class ShowNote extends StatefulWidget {
  final BaseModel note;

  const ShowNote({Key key, this.note}) : super(key: key);
  @override
  _ShowNoteState createState() => _ShowNoteState();
}

class _ShowNoteState extends State<ShowNote> {
  BaseModel myNote;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      myNote = widget.note;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(myNote.getString(TITLE)),
        centerTitle: true,
        actions: <Widget>[
//          IconButton(
//            onPressed: () {},
//            icon: Image.asset(
//              "assets/images/share.png",
//              color: white,
//            ),
//          ),
//          IconButton(
//            onPressed: () {},
//            icon: Image.asset(
//              "assets/images/delete.png",
//              color: white,
//            ),
//          )
        ],
      ),
      backgroundColor: AppConfig.appColor,
      body: page(),
    );
  }

  page() {
    return Stack(
      children: <Widget>[
        Image.asset(
          'assets/images/interest_bg.png',
          fit: BoxFit.cover,
          height: context.screenHeight(),
          width: context.screenWidth(),
          colorBlendMode: BlendMode.color,
        ),
        ListView(
          children: <Widget>[
            Container(
              padding: 15.padAll(),
              margin: 10.padAll(),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: black.withOpacity(.02),
                  border:
                      Border.all(color: AppConfig.appColor.withOpacity(.5))),
              child: Text(
                myNote.getString(DESCRIPTION),
                style: textStyle(false, 16, black),
              ),
            ),
            if (myNote.getPhotos.isNotEmpty)
              Container(
                height: 240,
                child: LayoutBuilder(
                  builder: (ctx, b) {
                    bool isEmpty = myNote.getPhotos.isEmpty;
                    int photoLength = myNote.getPhotos.length;

                    return Column(
                      children: <Widget>[
                        Flexible(
                          child: ListView.builder(
                              itemCount: photoLength,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (ctx, p) {
                                BaseModel photo = myNote.getPhotos[p];
                                bool isLocal = photo.isLocal;

                                return Stack(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(8),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: isLocal
                                            ? Image.file(
                                                File(photo.urlPath),
                                                height: 200,
                                                width: 150,
                                                fit: BoxFit.cover,
                                              )
                                            : CachedNetworkImage(
                                                imageUrl: photo.urlPath,
                                                height: 200,
                                                width: 150,
                                                fit: BoxFit.cover,
                                                placeholder: (ctx, s) {
                                                  return placeHolder(200,
                                                      width: 120);
                                                },
                                              ),
                                      ),
                                    ),
                                  ],
                                );
                              }),
                        ),
                      ],
                    );
                  },
                ),
              ),
          ],
        )
      ],
    );
  }
}
