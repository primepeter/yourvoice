import 'dart:async';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'app/app.dart';
import 'app_config.dart';

enum PlayerState { stopped, playing, paused }
enum PlayingRouteState { speakers, earpiece }

class PlayAudioBook extends StatefulWidget {
  final BaseModel audioModel;

  const PlayAudioBook({Key key, this.audioModel}) : super(key: key);
  @override
  _PlayAudioBookState createState() => _PlayAudioBookState();
}

class _PlayAudioBookState extends State<PlayAudioBook> {
  BaseModel audioModel;
  List<BaseModel> audioBooks = [];
  final vp = PageController();
  int currentPage = 0;
  String coverPhoto;

  AudioPlayer audioPlayer;
  AudioPlayerState playerState;
  Duration duration;
  Duration position;

  PlayerState playingState = PlayerState.stopped;
  PlayingRouteState playingRouteState = PlayingRouteState.speakers;
  StreamSubscription durationSubscription;
  StreamSubscription positionSubscription;
  StreamSubscription playerCompleteSubscription;
  StreamSubscription playerErrorSubscription;
  StreamSubscription playerStateSubscription;

  get isPlaying => playingState == PlayerState.playing;
  get isPaused => playingState == PlayerState.paused;
  get durationText => duration?.toString()?.split('.')?.first ?? '';
  get positionText => position?.toString()?.split('.')?.first ?? '';
  get isPlayingThroughEarpiece =>
      playingRouteState == PlayingRouteState.earpiece;

  @override
  initState() {
    super.initState();
    audioModel = widget.audioModel;
    audioBooks = audioModel.getAudioBooks;
    coverPhoto = audioModel.getString(COVER_PHOTO);
    initAudioPlayer();
  }

  @override
  void dispose() {
    audioPlayer.dispose();
    durationSubscription?.cancel();
    positionSubscription?.cancel();
    playerCompleteSubscription?.cancel();
    playerErrorSubscription?.cancel();
    playerStateSubscription?.cancel();
    super.dispose();
  }

  void initAudioPlayer() async {
    audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);

    durationSubscription = audioPlayer.onDurationChanged.listen((d) {
      setState(() => duration = d);

      // TODO implemented for iOS, waiting for android impl
      if (Theme.of(context).platform == TargetPlatform.iOS) {
        // (Optional) listen for notification updates in the background
        audioPlayer.startHeadlessService();

        // set at least title to see the notification bar on ios.
        audioPlayer.setNotification(
            title: 'App Name',
            artist: 'Artist or blank',
            albumTitle: 'Name or blank',
            imageUrl: 'url or blank',
            forwardSkipInterval: const Duration(seconds: 30), // default is 30s
            backwardSkipInterval: const Duration(seconds: 30), // default is 30s
            duration: d,
            elapsedTime: Duration(seconds: 0));
      }
    });

    positionSubscription = audioPlayer.onAudioPositionChanged.listen((p) {
      setState(() {
        position = p;
      });
    });

    playerCompleteSubscription = audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        position = duration;
      });
    });

    playerErrorSubscription = audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        playingState = PlayerState.stopped;
        duration = Duration(seconds: 0);
        position = Duration(seconds: 0);
      });
    });

    audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        playerState = state;
      });
    });

    audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() => playerState = state);
    });

    playingRouteState = PlayingRouteState.speakers;
  }

  Future<int> play() async {
    final playPosition = (position != null &&
            duration != null &&
            position.inMilliseconds > 0 &&
            position.inMilliseconds < duration.inMilliseconds)
        ? position
        : null;
    final result = await audioPlayer.play(audioBooks[currentPage].urlPath,
        position: playPosition);
    if (result == 1) setState(() => playingState = PlayerState.playing);

    audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Future<int> pause() async {
    final result = await audioPlayer.pause();
    if (result == 1) setState(() => playingState = PlayerState.paused);
    return result;
  }

  Future<int> earpieceOrSpeakersToggle() async {
    final result = await audioPlayer.earpieceOrSpeakersToggle();
    if (result == 1)
      setState(() => playingRouteState =
          playingRouteState == PlayingRouteState.speakers
              ? PlayingRouteState.earpiece
              : PlayingRouteState.speakers);
    return result;
  }

  Future<int> stop() async {
    final result = await audioPlayer.stop();
    if (result == 1) {
      setState(() {
        playingState = PlayerState.stopped;
        position = Duration();
      });
    }
    return result;
  }

  void _onComplete() {
    setState(() => playingState = PlayerState.stopped);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await stop();
        await audioPlayer.release();
        return true;
      },
      child: Scaffold(
        backgroundColor: AppConfig.appColor,
        body: page(),
      ),
    );
  }

  page() {
    return Stack(
      children: [
        if (fromOnline(coverPhoto))
          CachedNetworkImage(
            imageUrl: coverPhoto,
            fit: BoxFit.cover,
            height: screenH(context),
          )
        else
          Image.file(
            File(coverPhoto),
            fit: BoxFit.cover,
            height: screenH(context),
          ),
        Container(
          color: black.withOpacity(0.4),
        ),
        Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  BackButton(
                    color: white,
                    onPressed: () async {
                      await stop();
                      await audioPlayer.release();
                      Navigator.pop(context);
                    },
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.share,
                      color: white,
                    ),
                  )
                ],
              ),
            ),
            Flexible(
              child: PageView.builder(
                controller: vp,
                onPageChanged: (p) {
                  setState(() {
                    currentPage = p;
                  });
                },
                itemCount: audioBooks.length,
                itemBuilder: (ctx, p) {
                  return pageItem(p);
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  pageItem(int p) {
    BaseModel audio = audioBooks[p];
    String url = audio.urlPath;
    int duration = audio.getInt(DURATION);
    String title = audio.getString(TITLE);
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Flexible(child: Container()),
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: AppConfig.appColor,
                border: Border.all(color: black.withOpacity(.05)),
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: FlatButton(
                        shape: CircleBorder(),
                        //onPressed: isPlaying ? null : () => play(),
                        child: Icon(
                          Icons.skip_next,
                          color: white,
                        ),
                        color: black.withOpacity(.2),
                        padding: EdgeInsets.all(15),
                      ),
                    ),
                    addSpaceWidth(10),
                    Flexible(
                      child: FlatButton(
                        shape: CircleBorder(),

                        //onPressed: isPlaying || isPaused ? () => stop() : null,
                        child: Icon(
                          Icons.fast_rewind,
                          color: white,
                        ),
                        color: black.withOpacity(.2),
                        //iconSize: 30.0,
                        padding: EdgeInsets.all(5),
                      ),
                    ),
                    addSpaceWidth(10),
                    Flexible(
                      child: FlatButton(
                        shape: CircleBorder(),

                        onPressed: isPlaying ? () => pause() : () => play(),
                        child: Icon(
                          isPlaying ? Icons.pause : Icons.play_arrow,
                          color: white,
                        ),
                        color: black.withOpacity(.2),
                        //iconSize: 30.0,
                        padding: EdgeInsets.all(15),
                      ),
                    ),
                    addSpaceWidth(10),
                    Flexible(
                      child: FlatButton(
                        shape: CircleBorder(),
                        //onPressed: isPlaying || isPaused ? () => stop() : null,
                        child: Icon(
                          Icons.fast_forward,
                          color: white,
                        ),
                        color: black.withOpacity(.2),
                        //iconSize: 30.0,
                        padding: EdgeInsets.all(5),
                      ),
                    ),
                    addSpaceWidth(10),
                    Flexible(
                      child: FlatButton(
                        shape: CircleBorder(),

                        //onPressed: isPlaying || isPaused ? () => stop() : null,
                        child: Icon(
                          Icons.skip_previous,
                          color: white,
                        ),
                        color: black.withOpacity(.2),
                        //iconSize: 30.0,
                        padding: EdgeInsets.all(5),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      position != null
                          ? '${positionText ?? ''}'
                          : duration != null ? durationText : '',
                      style: TextStyle(fontSize: 13.0),
                    ),
                    addSpaceWidth(10),
                    Flexible(
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            height: 24,
                            //width: double.infinity,
                            decoration: BoxDecoration(
                                color: white,
                                border:
                                    Border.all(color: black.withOpacity(.05)),
                                borderRadius: BorderRadius.circular(10)),
                          ),
                          Slider(
                            activeColor: AppConfig.appColor,
                            inactiveColor: AppConfig.appColor.withOpacity(.2),
                            onChanged: (v) {
                              final position = v * duration;
                              audioPlayer.seek(
                                  Duration(milliseconds: position.round()));
                            },
                            value: (position != null &&
                                    duration != null &&
                                    position.inMilliseconds > 0 &&
                                    position.inMilliseconds < duration)
                                ? position.inMilliseconds / duration
                                : 0.0,
                          ),
                        ],
                      ),
                    ),
                    addSpaceWidth(10),
                    Text(
                      position != null
                          ? '${durationText ?? ''}'
                          : duration != null ? durationText : '',
                      style: TextStyle(fontSize: 13.0),
                    )
                  ],
                ),
                //Text('State: $_audioPlayerState')
              ],
            ),
          ),
        ],
      ),
    );
  }
}
