import 'dart:io';

import 'package:acclaim/VideoController.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/tapped.dart';
import 'package:acclaim/video_trimmer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_photo_picker/photo_picker.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:video_trimmer/video_trimmer.dart';

import 'app_index.dart';

typedef void PreviewCallBack(
    List<BaseModel> photos, List<PhotoPickerAsset> selectedPhotos);

class PreviewPostImages extends StatefulWidget {
  final List<BaseModel> photos;
  final List<PhotoPickerAsset> selectedPhotos;
  final int indexOf;
  final bool edittable;
  final String heroTag;
  final bool storyMode;
  PreviewPostImages(this.photos, this.selectedPhotos,
      {this.indexOf = 0,
      this.edittable = false,
      this.heroTag = "heroTag",
      this.storyMode = false});

  @override
  _PreviewPostImagesState createState() => _PreviewPostImagesState();
}

class _PreviewPostImagesState extends State<PreviewPostImages> {
  int currentPage;
  PageController controller;
  //CachedVideoPlayerController videoController;
  List<BaseModel> photos = [];
  List<PhotoPickerAsset> selectedPhotos = [];
  ScrollController imageScroll = ScrollController();
  CachedVideoPlayerController playerController;
  bool keyboardVisible = false;
  final messageController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = PageController(initialPage: widget.indexOf);
    currentPage = widget.indexOf;
    photos = widget.photos;
    selectedPhotos = widget.selectedPhotos;
    //if (!widget.edittable) loadVideo();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      try {
        imageScroll.animateTo((widget.indexOf * 80).toDouble(),
            duration: Duration(milliseconds: 500), curve: Curves.ease);
        pauseAllVideo();
      } catch (e) {}
    });

    KeyboardVisibility.onChange.listen((bool visible) {
      keyboardVisible = visible;
      print("Keyboard Visible $visible");
      if (mounted) setState(() {});
    });

    if (widget.storyMode) {
      messageController.addListener(() {
        String text = messageController.text;
        if (text.isEmpty) return;
        BaseModel model = photos[currentPage];
        model.put(MESSAGE, text);
        photos[currentPage] = model;
        if (mounted) setState(() {});
      });
    }
    Future.delayed(Duration(seconds: 2), () {
      BaseModel model = photos[widget.indexOf];
      bool isVideo = model.isVideo;
      if (isVideo) {
        VideoController.playVideoWithId(model, (c) {
          //videoController = c;
          if (mounted) setState(() {});
        }, loop: true);
      }
    });
  }

  @override
  dispose() {
    super.dispose();
    videoController?.dispose();
  }

  loadVideo() async {
    for (int p = 0; p < photos.length; p++) {
      BaseModel bm = photos[p];
      if (!bm.isVideo) continue;
      String videoUrl = bm.getString(IMAGE_URL);
      if (videoUrl.isEmpty) continue;
      if (!videoUrl.startsWith("http")) continue;
      CachedVideoPlayerController controller = bm.get(VIDEO_CONTROLLER);
      if (null != controller) {
        if (controller.value.initialized) {
          if (widget.indexOf == p && mounted) controller.play();
          bm.put(VIDEO_LENGTH, controller.value.duration.inMilliseconds);
          photos[p] = bm;
          if (mounted) setState(() {});
        } else
          controller.initialize().then((value) {
            if (widget.indexOf == p && mounted) controller.play();
            controller.setLooping(true);
            bm.put(VIDEO_LENGTH, controller.value.duration.inMilliseconds);
            photos[p] = bm;
            if (mounted) setState(() {});
          });

        continue;
      }
      controller = CachedVideoPlayerController.network(videoUrl)
        ..initialize().then((value) {
          if (widget.indexOf == p && mounted) controller.play();
          controller.setLooping(true);
          bm.put(VIDEO_LENGTH, controller.value.duration.inMilliseconds);
          photos[p] = bm;
          if (mounted) setState(() {});
        });
      bm.put(VIDEO_CONTROLLER, controller);
      photos[p] = bm;
    }
  }

  pauseAllVideo() {
    print("Pause videos....");

    VideoController.stopAllVideos(() {
      if (mounted) setState(() {});
    });

/*    for (int p = 0; p < photos.length; p++) {
      BaseModel bm = photos[p];
      if (!bm.isVideo) continue;
      String videoUrl = bm.getString(IMAGE_URL);
      if (videoUrl.isEmpty) continue;
      if (!videoUrl.startsWith("http")) continue;
      CachedVideoPlayerController controller = bm.get(VIDEO_CONTROLLER);
      if (null == controller) continue;
      controller.pause();
    }
    print("videos paused....");*/
  }

  pauseAtVideo(int p) async {
    BaseModel bm = photos[p];
    if (!bm.isVideo) return;
    String videoUrl = bm.getString(IMAGE_URL);
    if (videoUrl.isEmpty) return;
    if (!videoUrl.startsWith("http")) return;

    VideoController.pauseVideoWithId(bm, (c) {
      //videoController = c;
      if (mounted) setState(() {});
    });
/*    CachedVideoPlayerController controller = bm.get(VIDEO_CONTROLLER);
    if (null == controller) return;
    controller.pause();*/
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (!widget.edittable) pauseAllVideo();
        // Navigator.of(context).pop(photos);
        Navigator.of(context).pop();
        return false;
      },
      child: Scaffold(
          backgroundColor: black,
          body: Column(children: [
            addSpace(30),
            Padding(
              padding: const EdgeInsets.only(left: 0),
              child: Row(
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        if (!widget.edittable) pauseAllVideo();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        child: Center(
                            child: Icon(
                          Icons.keyboard_backspace,
                          color: white,
                          size: 25,
                        )),
                      )),
                  Expanded(
                      child: Text(
                    'Preview',
                    style: textStyle(true, 20, white),
                  )),
                  if (widget.edittable &&
                      !photos[currentPage]
                          .getString(IMAGE_URL)
                          .startsWith("http"))
                    IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: white,
                        ),
                        onPressed: () async {
                          BaseModel bm = photos[currentPage];
                          String imageUrl = bm.getString(IMAGE_URL);
                          String imagePath = bm.getString(IMAGE_PATH);
                          String thumbPath = bm.getString(THUMBNAIL_PATH);
                          String thumbUrl = bm.getString(THUMBNAIL_URL);
                          bool onlineImage = imageUrl.startsWith("http");
                          bool isVideo = bm.isVideo;

                          if (isVideo) {
                            final Trimmer _trimmer = Trimmer();
                            await _trimmer.loadVideo(
                                videoFile: File(imagePath));
                            pushAndResult(context, TrimmerView(_trimmer),
                                result: (_) async {
                              if (null == _) return;
                              String thumbnailPath = await getVideoThumbnail(_);
                              bm.put(THUMBNAIL_PATH, thumbnailPath);
                              photos[currentPage] = bm;
                              setState(() {});
                            });
                          } else {
                            if (imagePath.startsWith("http")) return;
                            final crop = await ImageCropper.cropImage(
                                sourcePath: imagePath);
                            if (null == crop) return;
                            bm.put(IMAGE_PATH, crop.path);
                            photos[currentPage] = bm;
                            setState(() {});
                          }
                        }),
                  if (widget.edittable)
                    IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: white,
                        ),
                        onPressed: () {
                          photos.removeAt(currentPage);
                          if (photos.isEmpty) Navigator.pop(context);
                          setState(() {});
                        }),
                  if (!widget.edittable ||
                      photos[currentPage]
                          .getString(IMAGE_URL)
                          .startsWith("http"))
                    IconButton(
                        icon: Icon(
                          Icons.cloud_download_outlined,
                          color: white,
                        ),
                        onPressed: () {
                          showError("Downloading...", success: true);
                          BaseModel bm = photos[currentPage];
                          bool isVideo = bm.isVideo;
                          String imageUrl = bm.getString(IMAGE_URL);
                          if (isVideo)
                            GallerySaver.saveVideo(imageUrl)
                                .then((bool success) {
                              showError("Video is saved!...", success: true);
                            });
                          else
                            GallerySaver.saveImage(imageUrl)
                                .then((bool success) {
                              showError("Image is saved!...", success: true);
                            });
                        })
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: showSuccess ? dark_green0 : red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            pages()
          ])),
    );
  }

  pages() {
    return Expanded(
      child: Stack(
        children: [
          PageView.builder(
            controller: controller,
            itemCount: photos.length,
            onPageChanged: (p) {
              currentPage = p;

              if (widget.storyMode) {
                BaseModel model = photos[p];
                String text = model.getString(MESSAGE);
                messageController.text = text;
              }

              try {
                imageScroll.animateTo((p * 80).toDouble(),
                    duration: Duration(milliseconds: 500), curve: Curves.ease);
                BaseModel bm = photos[(p - 1).clamp(0, photos.length - 1)];
                if (bm.isVideo) {
                  pauseAllVideo();

                  Future.delayed(Duration(seconds: 2), () {
                    BaseModel model = photos[p];
                    bool isVideo = model.isVideo;
                    if (isVideo) {
                      VideoController.playVideoWithId(model, (c) {
                        if (mounted) setState(() {});
                      }, loop: true);
                    }
                  });
                }
              } catch (e) {}
              setState(() {});
            },
            itemBuilder: (c, p) {
              BaseModel bm = photos[p];
              String objectId = bm.getObjectId();
              String imageUrl = bm.getString(IMAGE_URL);
              if (objectId.isEmpty) objectId = imageUrl;
              String imagePath = bm.getString(IMAGE_PATH);
              String thumbPath = bm.getString(THUMBNAIL_PATH);
              String thumbUrl = bm.getString(THUMBNAIL_URL);
              bool onlineImage = imageUrl.startsWith("http");
              bool isVideo = bm.isVideo;

              if (isVideo && !widget.edittable) {
                int vp =
                    allVideos.indexWhere((e) => e.getObjectId() == objectId);
                BaseModel videoModel;
                CachedVideoPlayerController videoController;

                if (vp != -1) {
                  videoModel = allVideos[vp];
                  videoController = videoModel.get(VIDEO_CONTROLLER);
                }

                if (null == videoController || vp == -1) return videoLoading;
                return Stack(
                  alignment: Alignment.center,
                  children: [
                    AspectRatio(
                      aspectRatio: videoController.value.aspectRatio,
                      child: CachedVideoPlayer(videoController),
                    ),
                    Tapped(
                      showIcon: !videoController.value.isPlaying,
                      child: Container(
                        color: transparent,
                        alignment: Alignment.center,
                        child: AnimatedContainer(
                            duration: Duration(milliseconds: 400),
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                border: Border.all(color: white, width: 1),
                                color: black.withOpacity(.5),
                                shape: BoxShape.circle),
                            child: Icon(
                              videoController.value.isPlaying
                                  ? Icons.pause
                                  : Icons.play_arrow,
                              color: white,
                              size: 20,
                            )),
                      ),
                      onTap: () async {
                        final playerPosition = videoController.value.position;
                        final videoDuration = videoController.value.duration;
                        bool hasEnded = playerPosition == videoDuration;
                        if (videoController.value.isPlaying)
                          videoController.pause();
                        else {
                          if (hasEnded)
                            videoController.seekTo(Duration(seconds: 0));
                          videoController.play();
                        }
                        setState(() {});
                      },
                    ),
                  ],
                );
              }

              return GestureDetector(
                onTap: () async {
                  if (!widget.edittable) return;
                  if (!isVideo) return;
                  final Trimmer _trimmer = Trimmer();
                  await _trimmer.loadVideo(videoFile: File(imagePath));
                  pushAndResult(context, TrimmerView(_trimmer),
                      result: (_) async {
                    if (null == _) return;
                    String thumbnailPath = await getVideoThumbnail(_[0]);
                    bm.put(THUMBNAIL_PATH, thumbnailPath);
                    bm.put(IMAGE_PATH, _[0]);
                    bm.put(VIDEO_LENGTH, _[1]);

                    print("url $_");

                    photos[p] = bm;
                    setState(() {});
                  });
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    if (onlineImage)
                      CachedNetworkImage(
                        imageUrl: isVideo ? thumbUrl : imageUrl,
                        fit: BoxFit.cover,
                        //height: MediaQuery.of(context).size.height,
                        width: double.infinity,
                        placeholder: (_, s) {
                          return placeHolder(250, width: double.infinity);
                        },
                      )
                    else
                      Container(
                        margin: EdgeInsets.all(1.2),
                        child: Image.file(
                          File(isVideo ? thumbPath : imagePath),
                          fit: BoxFit.cover,
                          //height: MediaQuery.of(context).size.height,
                          width: double.infinity,
                        ),
                      ),
                    if (isVideo)
                      Container(
                        child: Center(
                          child: Container(
                            height: 60,
                            width: 60,
                            padding: 3.padAll(),
                            margin: 6.padAll(),
                            decoration: BoxDecoration(
                              color: black.withOpacity(.5),
                              border: Border.all(color: white, width: 2),
                              shape: BoxShape.circle,
                            ),
                            alignment: Alignment.center,
                            child: Icon(
                              Icons.play_arrow,
                              color: white,
                              size: 20,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              );
            },
          ),
          if (photos.length > 0)
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                color: black.withOpacity(.8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    addSpace(10),
                    Row(
                      children: [
                        if (widget.storyMode)
                          Expanded(
                              child: Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(10)),
                            child: TextField(
                              controller: messageController,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Add caption"),
                            ),
                          ))
                        else
                          Expanded(
                            child: Container(),
                          ),
                        //addSpaceWidth(10),
                        if (widget.edittable)
                          Container(
                            margin: EdgeInsets.all(20),
                            child: FloatingActionButton(
                              onPressed: () {
                                print(photos[0].items);
                                Navigator.pop(context, photos);
                              },
                              heroTag: "p33",
                              clipBehavior: Clip.antiAlias,
                              backgroundColor: light_green3,
                              shape: CircleBorder(),
                              child: Icon(
                                Icons.check,
                                color: white,
                                size: 30,
                              ),
                            ),
                          ),
                      ],
                    ),
                    if (!keyboardVisible)
                      new Container(
                        height: 70,
                        width: double.infinity,
                        child: ListView.builder(
                          itemBuilder: (c, p) {
                            BaseModel bm = photos[p];
                            String imageUrl = bm.getString(IMAGE_URL);
                            String imagePath = bm.getString(IMAGE_PATH);
                            String thumbPath = bm.getString(THUMBNAIL_PATH);
                            String thumbUrl = bm.getString(THUMBNAIL_URL);
                            bool onlineImage = imageUrl.startsWith("http");
                            bool isVideo = bm.isVideo;
                            return GestureDetector(
                              onTap: () {
                                controller.animateToPage(p,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.ease);
                              },
                              child: Row(
                                children: [
                                  Container(
                                    width: 70,
                                    height: 70,
                                    margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                    child: Card(
                                      clipBehavior: Clip.antiAlias,
                                      shape: CircleBorder(
                                          side: BorderSide(
                                              color: p == currentPage
                                                  ? light_green3
                                                  : transparent,
                                              width: 3)),
                                      child: onlineImage
                                          ? CachedNetworkImage(
                                              imageUrl:
                                                  isVideo ? thumbUrl : imageUrl,
                                              height: MediaQuery.of(context)
                                                  .size
                                                  .height,
                                              fit: BoxFit.cover,
                                            )
                                          : Image.file(
                                              File(isVideo
                                                  ? thumbPath
                                                  : imagePath),
                                              fit: BoxFit.cover,
                                              width: double.infinity,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          shrinkWrap: true,
                          padding: EdgeInsets.all(0),
                          itemCount: photos.length,
                          scrollDirection: Axis.horizontal,
                          controller: imageScroll,
                        ),
                      ),
                    addSpace(10),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
