import 'dart:io';

import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'LangMain.dart';
import 'app_config.dart';
import 'date_picker/flutter_datetime_picker.dart';
import 'date_picker/i18n_model.dart';

class ProfileSettings extends StatefulWidget {
  @override
  _ProfileSettingsState createState() => _ProfileSettingsState();
}

class _ProfileSettingsState extends State<ProfileSettings> {
  var nameController = TextEditingController();
  final nickNameController = TextEditingController();
  final professionController = TextEditingController();
  var aboutMeController = TextEditingController();
  String dateOfBirth;
  String selectedGender;
  String photoUrl;
  bool isFile = false;
  bool showPassword = true;

  String countrySelected;
  String countryFlag;
  String countryCode;

  final scaffoldKey = GlobalKey<ScaffoldState>();

  bool checkingAvailability = false;
  bool usernameAvailable = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameController.text = userModel.fullName;
    nickNameController.text = userModel.nickName;
    photoUrl = userModel.userImage;
    professionController.text = userModel.profession;
    selectedGender = userModel.gender;
    aboutMeController.text = userModel.aboutMe;
    dateOfBirth = userModel.dateOfBirth;
    countrySelected = getCountries()
        .singleWhere((e) => e.countryName == userModel.country)
        .countryName;
    countryFlag = getCountries()
        .singleWhere((e) => e.countryName == userModel.country)
        .countryFlag;
    countryCode = getCountries()
        .singleWhere((e) => e.countryName == userModel.country)
        .countryCode;
    setState(() {});

    nickNameController.addListener(() async {
      final nickName = nickNameController.text;
      if (nickName.isEmpty) return;
      bool available = await checkUsernameAvailability(nickName);
      if (mounted)
        setState(() {
          checkingAvailability = false;
          usernameAvailable = available;
        });
    });
  }

  checkUsernameAvailability(String nickName) async {
    var doc = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(NICKNAME, isEqualTo: nickName)
        .limit(1)
        .getDocuments();
    if (doc.documents.isEmpty) {
      return true;
    }
    return false;
  }

  getAvailabilityTxt() {
    if (checkingAvailability) return "Checking nickname avaiability";
    if (usernameAvailable) return "Nickname can be used";
    return "Nickname can't be used";
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, !fromOnline(photoUrl));
        return true;
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: white,
        body: profileBody(),
      ),
    );
  }

  profileBody() {
    print(userModel.gender);
    return Stack(
      children: <Widget>[
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 35),
              child: Row(
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        Navigator.pop(context, "");
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        child: Center(
                            child: Icon(
                          Icons.keyboard_backspace,
                          color: black,
                          size: 25,
                        )),
                      )),
                  Expanded(
                      child: Text(
                    LangKey.profileSettings.toAppLanguage,
                    style: textStyle(true, 20, black),
                  )),
                  RaisedButton(
                    color: AppConfig.appColor,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: white.withOpacity(.5)),
                        borderRadius: BorderRadius.circular(25)),
                    child: Text(
                      Language.value(LangKey.save),
                      style: textStyle(true, 16, white),
                    ),
                    onPressed: handleSave,
                  ),
                  addSpaceWidth(10)
                ],
              ),
            ),
            addSpace(10),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: showSuccess ? dark_green0 : red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            Flexible(
              child: SingleChildScrollView(
                padding: EdgeInsets.only(top: 10),
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        pickSingleImage();
                      },
                      child: Container(
                        height: 130,
                        width: 130,
                        margin: EdgeInsets.all(10),
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              height: 130,
                              width: 130,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: AppConfig.appColor, width: 2),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: CachedNetworkImage(
                                  imageUrl: photoUrl,
                                  placeholder: (c, z) {
                                    if (photoUrl.isNotEmpty &&
                                        !photoUrl.contains("http"))
                                      return Image.file(
                                        File(photoUrl),
                                        fit: BoxFit.cover,
                                      );

                                    return Container(
                                      height: 120,
                                      width: 120,
                                      color: black.withOpacity(.05),
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.person,
                                        color: black.withOpacity(.4),
                                        size: 30,
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                height: 40,
                                width: 40,
                                child: Icon(
                                  Icons.camera_alt,
                                  color: white,
                                  size: 20,
                                ),
                                decoration: BoxDecoration(
                                    color: AppConfig.appColor,
                                    border: Border.all(color: white, width: 2),
                                    shape: BoxShape.circle),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    textBoxItem(
                        title: "Full Name",
                        hint: 'Your Name',
                        controller: nameController,
                        focusNode: null),
                    textBoxItem(
                        title: "Username",
                        hint: 'Your Username',
                        controller: nickNameController,
                        focusNode: null),
                    textBoxItem(
                        title: "Profession",
                        hint: 'Your Profession',
                        controller: professionController,
                        focusNode: null),
                    clickGenderItem(),
                    clickCountryItem(),
                    clickDobItem(),
                    textBoxItem(
                        title: "About You",
                        hint: 'Tell us more about you',
                        controller: aboutMeController,
                        maxLines: 4,
                        focusNode: null),
                    interestBoxItem(),
                    addSpace(40)
                  ],
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  textBoxItem(
      {@required String title,
      @required String hint,
      @required TextEditingController controller,
      @required FocusNode focusNode,
      int maxLines = 1}) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            title,
            style: textStyle(false, 12, black.withOpacity(.7)),
          ),
          addSpace(5),
          Container(
            decoration: BoxDecoration(
                color: black.withOpacity(.04),
                borderRadius: BorderRadius.circular(10)),
            child: TextField(
              focusNode: focusNode,
              controller: controller,
              textInputAction: TextInputAction.done,
              style: textStyle(false, 18, black),
              maxLines: maxLines,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20),
                border: InputBorder.none,
                hintText: hint,
                //hintStyle: textStyle(false, 18, black.withOpacity(.5)),
              ),
            ),
          )
        ],
      ),
    );
  }

  clickGenderItem() {
    bool active = selectedGender != null && selectedGender.isNotEmpty;
    return Container(
      //padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(left: 10, right: 10, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            Language.value(LangKey.gender),
            style: textStyle(false, 12, black.withOpacity(.7)),
          ),
          addSpace(5),
          FlatButton(
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              localTranslation = localTranslation;
              setState(() {});
              pushAndResult(
                  context,
                  listDialog(
                    [
                      Language.value(LangKey.male),
                      Language.value(LangKey.female)
                    ],
                    //usePosition: false,
                  ), result: (_) {
                if (mounted)
                  setState(() {
                    selectedGender = _;
                  });
              });
            },
            padding: EdgeInsets.all(20),
            color: active ? default_white : black.withOpacity(0.2),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: white.withOpacity(.5))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  active
                      ? selectedGender.toUpperCase()
                      : Language.value(LangKey.chooseGender),
                  style: textStyle(
                      false, 18, active ? black : white.withOpacity(0.7)),
                ),
                Icon(
                  Icons.arrow_drop_down,
                  color: (active ? black : white).withOpacity(.5),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  clickDobItem() {
    bool active = dateOfBirth != null && dateOfBirth.isNotEmpty;
    return Container(
      //padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(left: 10, right: 10, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            Language.value(LangKey.chooseDateOfBirth),
            style: textStyle(false, 12, black.withOpacity(.7)),
          ),
          addSpace(5),
          FlatButton(
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              int year;
              int month;
              int day;
              if (null != dateOfBirth) {
                var birthDay = dateOfBirth.split("-");
                year = num.parse(birthDay[0]);
                month = num.parse(birthDay[1]);
                day = num.parse(birthDay[2]);
              }
              final localType = LocaleType.values.singleWhere((e) =>
                  e.toString().replaceAll("LocaleType.", "") ==
                  appLocale.languageCode);
              DatePicker.showDatePicker(context,
                  showTitleActions: true,
                  minTime: DateTime(1930, 12, 31),
                  maxTime: DateTime(2040, 12, 31),
                  onChanged: (date) {}, onConfirm: (date) {
                setState(() {
                  int year = date.year;
                  int month = date.month;
                  int day = date.day;
                  dateOfBirth = "$year-${formatDOB(month)}-${formatDOB(day)}";
                });
              },
                  currentTime:
                      null == dateOfBirth ? null : DateTime(year, month, day),
                  locale: localType);
            },
            padding: EdgeInsets.all(20),
            color: active ? default_white : black.withOpacity(0.2),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: white.withOpacity(.5))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  active
                      ? Language.value(LangKey.chooseDateOfBirth)
                      : dateOfBirth,
                  style: textStyle(
                      false, 18, active ? black : white.withOpacity(0.7)),
                ),
                Icon(
                  Icons.arrow_drop_down,
                  color: (active ? black : white).withOpacity(.5),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  clickCountryItem() {
    bool active = countrySelected != null && countrySelected.isNotEmpty;
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            Language.value(LangKey.country),
            style: textStyle(false, 12, black.withOpacity(.7)),
          ),
          addSpace(5),
          FlatButton(
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              pickCountry(context, (_) {
                countrySelected = _.name;
                countryFlag =
                    CountryPickerUtils.getFlagImageAssetPath(_.isoCode);
                defaultCountry = _.name;
                countryFlag = getCountries()
                    .singleWhere((e) => e.countryName == countrySelected)
                    .countryFlag;
                countryCode = getCountries()
                    .singleWhere((e) => e.countryName == countrySelected)
                    .countryCode;

                SharedPreferences.getInstance()
                    .then((value) => value.setString(COUNTRY, defaultCountry));
                setState(() {});
              });
            },
            padding: EdgeInsets.all(20),
            color: active ? default_white : black.withOpacity(0.2),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: white.withOpacity(.5))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  child: Row(
                    children: <Widget>[
                      if (null != countryFlag) ...[
                        Image.asset(
                          countryFlag,
                          height: 25,
                          width: 40,
                          fit: BoxFit.cover,
                        ),
                        15.spaceWidth()
                      ],
                      Flexible(
                        child: Text(
                          active
                              ? countrySelected
                              : Language.value(LangKey.chooseCountry),
                          style: textStyle(false, 18,
                              active ? black : white.withOpacity(0.7)),
                        ),
                      ),
                    ],
                  ),
                ),
                Icon(
                  Icons.arrow_drop_down,
                  color: (active ? black : white).withOpacity(.5),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  List allItems = appSettingsModel.getList(APP_INTERESTS);

  interestBoxItem() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            Language.value(LangKey.yourInterests),
            style: textStyle(false, 12, black.withOpacity(.7)),
          ),
          addSpace(5),
          Container(
            decoration: BoxDecoration(
                color: black.withOpacity(.04),
                borderRadius: BorderRadius.circular(10)),
            child: GridView.builder(
              padding: EdgeInsets.zero,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4, childAspectRatio: .8),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (ctx, p) {
                Map item = allItems[p];
                BaseModel model = BaseModel(items: item);
                Map translations = model.getMap(TRANSLATIONS);
                String value = translations
                    .toAppLanguage; //translations[appLanguage] ?? "No Translation";
                if (value.isEmpty) value = "No Translation";
                //List interests = userModel.getList(INTERESTS);

                bool selected =
                    userModel.getList(INTERESTS).contains(model.getObjectId());

                return GestureDetector(
                  onTap: () {
                    print(model.getObjectId());
                    userModel
                      ..putInList(INTERESTS, model.getObjectId(), !selected)
                      ..put(INTEREST_SHOWN, true)
                      ..updateItems();
                    setState(() {});
                  },
                  child: Container(
                    padding: EdgeInsets.all(8),
                    alignment: Alignment.center,
                    //color: Colors.red,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Card(
                            color: selected ? AppConfig.appColor : white,
                            elevation: 10,
                            child: Container(
                              height: 55,
                              width: 55,
                              padding: EdgeInsets.all(6),
                              alignment: Alignment.center,
                              child: CachedNetworkImage(
                                imageUrl: model.getString(ICON),
                                height: 40,
                                width: 40,
                                fit: BoxFit.cover,
                                alignment: Alignment.center,
                              ),
                            ),
                            shape: CircleBorder(),
                            clipBehavior: Clip.antiAlias,
                          ),
                          addSpace(2),
                          Flexible(
                            child: Text(
                              value,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: textStyle(false, 12, black),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: allItems.length,
            ),
          )
        ],
      ),
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  pickSingleImage() async {
    PickedFile file = await ImagePicker().getImage(source: ImageSource.gallery);
    if (file == null) return;
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: file.path,
        maxWidth: 2500,
        maxHeight: 2500,
        cropStyle: CropStyle.circle,
        compressFormat: ImageCompressFormat.png);
    if (croppedFile != null) {
      photoUrl = croppedFile.path;
      setState(() {});
    }
  }

  savePhoto() {
    showProgress(true, context, msg: "Saving Profile Photo");
    uploadFile(File(photoUrl), (res, error) {
      showProgress(false, context);
      Future.delayed(Duration(milliseconds: 500), () {
        if (error != null) {
          showError("Error");
          return;
        }
        photoUrl = res;
        userModel
          ..put(USER_IMAGE, res)
          ..updateItems();
        setState(() {});
        handleSave();
      });
    });
  }

  handleSave() {
    if (nameController.text.isEmpty) {
      showError(Language.value(LangKey.enterFullNameRequired));
      return;
    }

    if (nickNameController.text.isEmpty) {
      showError(Language.value(LangKey.enterNickNameRequired));
      return;
    }

    if (!usernameAvailable) {
      showError("Nickname not available");
      return;
    }

    if (professionController.text.isEmpty) {
      showError(Language.value(LangKey.enterProfessionRequired));
      return;
    }

    if (null == selectedGender) {
      showError(Language.value(LangKey.enterGenderRequired));
      return;
    }

    if (aboutMeController.text.isEmpty) {
      showError(Language.value(LangKey.enterAboutRequired));
      return;
    }

    if (null == countrySelected) {
      showError(Language.value(LangKey.chooseCountryRequired));
      return;
    }

    if (null == dateOfBirth) {
      showError(Language.value(LangKey.chooseDateOfBirthRequired));
      return;
    }

    if (!photoUrl.startsWith("http")) {
      savePhoto();
      return;
    }

    userModel
      ..put(USER_IMAGE, photoUrl)
      ..put(FULL_NAME, nameController.text)
      ..put(PROFESSION, professionController.text)
      ..put(GENDER, selectedGender)
      ..put(ABOUT_ME, aboutMeController.text)
      ..put(COUNTRY, countrySelected)
      ..put(DATE_OF_BIRTH, dateOfBirth)
      ..updateItems();
    //setState(() {});
    Navigator.pop(context, true);
  }
}
