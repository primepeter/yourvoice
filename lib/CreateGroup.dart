import 'dart:io';

import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class CreateGroup extends StatefulWidget {
  final BaseModel model;

  const CreateGroup({Key key, this.model}) : super(key: key);
  @override
  _CreateGroupState createState() => _CreateGroupState();
}

class _CreateGroupState extends State<CreateGroup> {
  String groupCategory;
  String activeCategory;
  List addressMeList = appSettingsModel.getList(ADDRESS_ME_BY);

  final groupName = TextEditingController();
  final groupInfo = TextEditingController();
  final channelLink = TextEditingController();
  String groupImage;

  String countrySelected;
  String countryFlag;
  String countryCode;

  final country =
      getCountries().singleWhere((e) => e.countryName == userModel.country);

  List allCategories = appSettingsModel.getList(APP_CATEGORIES);
  List groupMembers = [
    personMap(userModel.items, isAdmin: true, isCreator: true)
  ];

  BaseModel model = BaseModel();
  String groupId = getRandomId();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.model != null) {
      model = widget.model;
      groupId = widget.model.getObjectId();
    }
    groupInfo.text = model.aboutMe;
    channelLink.text = model.whatIdo;
    groupImage = model.userImage;
    groupCategory = model.addressMe;
    countrySelected = country.countryName;
    countryFlag = country.countryFlag;
    countryCode = country.countryCode;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "Create Group",
                style: textStyle(true, 20, black),
              )),
            ],
          ),
        ),
        addSpace(10),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: showSuccess ? dark_green0 : red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        Flexible(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 10, right: 15, left: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: appColor_light,
                      borderRadius: BorderRadius.circular(10)),
                  child: Text.rich(
                    TextSpan(children: [
                      TextSpan(
                          text:
                              "Whatsoever ye do, do all to the glory of God.” 1 Corinthians 10:31. ",
                          style: textStyle(true, 14, white)),
                      TextSpan(
                        text:
                            "Create a community centered around the Gift of God in you, Let the Presence of God be Felt Online.",
                        //style: textStyle(true, 14, white)
                      )
                    ]),
                    style: textStyle(false, 14, white),
                  ),
                ),
                addSpace(20),
                GestureDetector(
                  onTap: () async {
                    PickedFile file = await ImagePicker()
                        .getImage(source: ImageSource.gallery);
                    if (file == null) return;
                    File croppedFile = await ImageCropper.cropImage(
                        sourcePath: file.path,

                        //maxWidth: 2500,
                        //maxHeight: 2500,
                        //cropStyle: CropStyle.circle,
                        compressFormat: ImageCompressFormat.png);
                    if (croppedFile != null) {
                      groupImage = croppedFile.path;
                      setState(() {});
                    }
                  },
                  child: Center(
                    child: Container(
                      height: 250,
                      width: double.infinity,
                      //margin: EdgeInsets.all(10),
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              //shape: BoxShape.circle,
                              border: Border.all(
                                  color: black.withOpacity(.1), width: 2),
                            ),
                            child: Builder(
                              builder: (c) {
                                if (groupImage.isEmpty)
                                  return Container(
                                    height: 250,
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    color: blue09,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        /* Opacity(
                                          opacity: .4,
                                          child: Image.asset(
                                            "assets/images/group_bg.jpg",
                                            fit: BoxFit.cover,
                                            width: double.infinity,
                                            height: 250,
                                          ),
                                        ),*/
                                        Icon(
                                          Icons.people_alt,
                                          color: black.withOpacity(.1),
                                          size: 50,
                                        ),
                                      ],
                                    ),
                                  );

                                if (!groupImage.contains("http"))
                                  return Image.file(
                                    File(groupImage),
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                  );

                                return CachedNetworkImage(
                                  imageUrl: groupImage,
                                  fit: BoxFit.cover,
                                  placeholder: (c, z) {
                                    return Container(
                                      width: double.infinity,
                                      color: black.withOpacity(.05),
                                      alignment: Alignment.center,
                                      child: Icon(
                                        Icons.people_alt,
                                        color: black.withOpacity(.4),
                                        size: 30,
                                      ),
                                    );
                                  },
                                );
                              },
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 40,
                              width: 40,
                              child: Icon(
                                Icons.camera_alt,
                                color: white,
                                size: 20,
                              ),
                              decoration: BoxDecoration(
                                  color: appColor,
                                  border: Border.all(color: white, width: 2),
                                  //shape: BoxShape.circle,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      bottomLeft: Radius.circular(15))),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                addSpace(20),
                GestureDetector(
                  onTap: () {
                    showListDialog(
                      context,
                      allCategories
                          .map((e) => BaseModel(items: e)
                              .getMap(TRANSLATIONS)
                              .toAppLanguage)
                          .toList(),
                      (_) {
                        print(_);
                        groupCategory = _[0];
                        int p = allCategories.indexWhere((e) =>
                            BaseModel(items: e)
                                .getMap(TRANSLATIONS)
                                .toAppLanguage ==
                            groupCategory);
                        if (p != -1) {
                          print(allCategories[p]);
                          activeCategory = allCategories[p][OBJECT_ID];
                        }

                        setState(() {});
                      },
                      singleSelection: true,
                      selections: [groupCategory],
                    );
                  },
                  child: Container(
                    height: 50,
                    margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: blue09,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                            color: black.withOpacity(.1), width: .5)),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          (null == groupCategory || groupCategory.isEmpty)
                              ? "Choose Group Category?"
                              : groupCategory,
                          style: textStyle(
                              false,
                              16,
                              black.withOpacity((null == groupCategory ||
                                      groupCategory.isEmpty)
                                  ? 0.5
                                  : 1)),
                        )),
                        Icon(Icons.keyboard_arrow_down_sharp)
                      ],
                    ),
                  ),
                ),
                addSpace(10),
                Text(
                  "Group Name",
                  style: textStyle(true, 14, appColor),
                ),
                addSpace(10),
                Container(
                  //height: 45,
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(10),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    onSubmitted: (_) {
                      //post();
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        hintText: "Enter Group Name",
                        hintStyle: textStyle(false, 16, black.withOpacity(.4))),
                    style: textStyle(
                      false,
                      16,
                      black,
                    ),
                    controller: groupName,
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                addSpace(10),
                Text(
                  "Do you have a YouTube Channel? (Optional)",
                  style: textStyle(true, 14, appColor),
                ),
                addSpace(10),
                Container(
                  //height: 45,
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(10),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    onSubmitted: (_) {
                      //post();
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        hintText: "YouTube Channel Link",
                        hintStyle: textStyle(false, 16, black.withOpacity(.4))),
                    style: textStyle(
                      false,
                      16,
                      black,
                    ),
                    controller: channelLink,
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: red0, borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    "Do you have a Presence on YouTube?, We would want you to "
                    "Connect your YouTube Channel with the community for "
                    "us to be able to watch you from here when you are Live.",
                    style: textStyle(false, 14, white),
                  ),
                ),
                addSpace(20),
                Text(
                  "About Group?",
                  style: textStyle(true, 14, appColor),
                ),
                addSpace(10),
                Container(
                  //height: 45,
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(10),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    onSubmitted: (_) {
                      //post();
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        hintText:
                            "Let the community know what this Group is about.",
                        hintStyle: textStyle(false, 16, black.withOpacity(.4))),
                    style: textStyle(
                      false,
                      16,
                      black,
                    ),
                    controller: groupInfo,
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: 5,
                    keyboardType: TextInputType.number,
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                addSpace(10),
                Text(
                  "Members",
                  style: textStyle(true, 14, appColor),
                ),
                addSpace(10),
                Column(
                  children: List.generate(groupMembers.length, (index) {
                    BaseModel model = BaseModel(items: groupMembers[index]);

                    final date =
                        DateTime.fromMillisecondsSinceEpoch(model.time);
                    String joinedAt =
                        new DateFormat("MMMM d,yyyy").format(date);

                    bool isAdmin = model.isAdmin;
                    bool isCreator = model.getBoolean(IS_CREATOR);
                    return Container(
                      decoration: BoxDecoration(
                          color: blue09,
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: black),
                              shape: BoxShape.circle,
                              // borderRadius: BorderRadius.circular(10),
                            ),
                            padding: EdgeInsets.all(1),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: CachedNetworkImage(
                                imageUrl: model.userImage,
                                height: 25,
                                width: 25,
                                fit: BoxFit.cover,
                                placeholder: (c, x) {
                                  return Container(
                                    height: 25,
                                    width: 25,
                                    child: Icon(
                                      Icons.person,
                                      size: 18,
                                      color: white.withOpacity(.5),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          addSpaceWidth(5),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                model.getString(NAME),
                                style: textStyle(false, 13, black),
                              ),
                              Text(
                                "Member Since $joinedAt",
                                style:
                                    textStyle(false, 12, black.withOpacity(.7)),
                              ),
                            ],
                          ),
                          Spacer(),
                          if (!isCreator)
                            RaisedButton(
                              onPressed: () {
                                model.put(IS_ADMIN, !isAdmin);
                                groupMembers[index] = model.items;
                                setState(() {});
                              },
                              padding: EdgeInsets.all(2),
                              color: isAdmin ? appColor_light : white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  side: BorderSide(color: appColor_light)),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Icon(
                                    Icons.check,
                                    color: isAdmin ? white : appColor_light,
                                    size: 12,
                                  ),
                                  addSpaceWidth(3),
                                  Text(
                                    "Admin",
                                    style: textStyle(true, 12,
                                        isAdmin ? white : appColor_light),
                                  ),
                                ],
                              ),
                            ),
                          addSpaceWidth(5),
                          if (!isCreator)
                            FlatButton(
                              onPressed: () {
                                groupMembers.removeAt(index);
                                setState(() {});
                              },
                              height: 18,
                              minWidth: 18,
                              shape: CircleBorder(),
                              color: Colors.red,
                              padding: EdgeInsets.all(5),
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              child: Icon(
                                Icons.clear,
                                size: 18,
                                color: white,
                              ),
                            ),
                        ],
                      ),
                    );
                  }),
                ),
                addSpace(10),
                RaisedButton(
                  onPressed: () {},
                  padding: EdgeInsets.all(12),
                  color: white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                      side: BorderSide(color: appColor_light)),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.add,
                        color: appColor_light,
                        size: 16,
                      ),
                      addSpaceWidth(5),
                      Text(
                        "Add Members",
                        style: textStyle(true, 13, appColor_light),
                      ),
                    ],
                  ),
                ),
                addSpace(10),
                RaisedButton(
                  onPressed: () {
                    handleSave();
                  },
                  padding: EdgeInsets.all(14),
                  color: appColor_light,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: Text(
                      "PROCEED",
                      style: textStyle(true, 16, white),
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  savePhoto() {
    showProgress(true, context, msg: "Saving Photo");
    uploadFile(File(groupImage), (res, error) {
      showProgress(false, context);
      Future.delayed(Duration(milliseconds: 500), () {
        if (error != null) {
          showError("Error");
          return;
        }
        groupImage = res;
        model.put(GROUP_IMAGE, groupImage);
        setState(() {});
        handleSave();
      });
    });
  }

  handleSave() {
    if (null == groupImage || groupImage.isEmpty) {
      showError("Add Group Image!");
      return;
    }

    if (null == groupCategory || groupCategory.isEmpty) {
      showError("Choose Group Category?");
      return;
    }

    if (groupInfo.text.isEmpty) {
      showError("Tell us about the Group?");
      return;
    }

    /*  if (channelLink.text.isEmpty) {
      showError("What do you do for christ?");
      return;
    }*/

    if (groupInfo.text.isEmpty) {
      showError("Tell us about the Group?");
      return;
    }

    if (!groupImage.startsWith("http")) {
      savePhoto();
      return;
    }

    List category = [activeCategory];

    for (var item in allCategories) {
      BaseModel model = BaseModel(items: item);
      if (!model.getBoolean(IS_DEFAULT)) continue;
      int p = category.indexWhere((e) => e == model.getObjectId());
      if (p == -1) category.add(item[OBJECT_ID]);
    }

    model
      ..put(OBJECT_ID, groupId)
      ..put(GROUP_NAME, groupName.text)
      ..put(GROUP_CHANNEL_ID, channelLink.text)
      ..put(GROUP_CATEGORY, category)
      ..put(GROUP_INFO, groupInfo.text)
      ..put(GROUP_MEMBERS, groupMembers)
      ..put(PARTIES,
          groupMembers.map((e) => BaseModel(items: e).getObjectId()).toList())
      ..saveItem(GROUP_BASE, true, document: groupId, merged: true);
    showMessage(context, Icons.check, dark_green0, "Group Created!",
        "Group was created successfully.Remember Whatsoever ye do, do all to the glory of God.” 1 Corinthians 10:31.",
        onClicked: (_) {
      Navigator.pop(context);
    }, cancellable: false);
  }
}
