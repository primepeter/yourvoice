import 'package:acclaim/app/app.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'main_screens/home.dart';
import 'main_screens/search.dart';

class Bookmarks extends StatefulWidget {
  @override
  _BookmarksState createState() => _BookmarksState();
}

class _BookmarksState extends State<Bookmarks> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadItems(false);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];
  bool setup = false;

  loadItems(bool isNew) async {
    print(userModel.getUserId());
    QuerySnapshot query = await Firestore.instance
        .collection(POST_BASE)
        .where(BOOKMARKS, arrayContains: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ]).getDocuments();

    print(query.documents.length);

    for (var doc in query.documents) {
      BaseModel model = BaseModel(doc: doc);
      int p = items.indexWhere(
          (e) => BaseModel(items: e).getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model.items);
      else
        items[p] = model.items;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.documents.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context, "");
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "Bookmarks",
                style: textStyle(true, 20, black),
              )),
              InkWell(
                  onTap: () {
                    pushAndResult(context, Search());
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.search,
                      color: black,
                      size: 25,
                    )),
                  )),
            ],
          ),
        ),
        Expanded(
          child: Builder(
            builder: (c) {
              if (!setup) return loadingLayout();

              if (items.isEmpty)
                return emptyLayout(
                    Icons.bookmark, "No Bookmark", "Your bookmarks has nothing",
                    clickText: "Refresh", click: () {
                  setup = false;
                  setState(() {});
                  loadItems(true);
                });

              return SmartRefresher(
                controller: refreshController,
                enablePullDown: true,
                enablePullUp: true,
                header: WaterDropHeader(),
                footer: ClassicFooter(
                  noDataText: "Nothing more for now, check later...",
                  textStyle: textStyle(false, 12, black.withOpacity(.7)),
                ),
                onLoading: () {
                  loadItems(false);
                },
                onRefresh: () {
                  loadItems(true);
                },
                child: ListView.builder(
                  //controller: scrollController,
                  padding: EdgeInsets.only(
                    top: 10,
                  ),
                  itemCount: items.length,
                  itemBuilder: (c, p) {
                    Map item = items[p];
                    BaseModel model = BaseModel(items: item);

                    return Column(
                      children: [
                        // if (p == 0) storiesLayout(),
                        PostItem(
                            p: p,
                            heroTag: getRandomId(),
                            model: model,
                            stateCallBack: (_) {
                              items[p] = _;
                              BaseModel model = BaseModel(items: _);
                              print(model.getList(BOOKMARKS));
                              if (!model
                                  .getList(BOOKMARKS)
                                  .contains(userModel.getUserId()))
                                items.removeAt(p);
                              setState(() {});
                            }),
                      ],
                    );
                  },
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
