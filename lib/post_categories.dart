import 'package:flutter/material.dart';

import 'LangMain.dart';
import 'app/app.dart';
import 'app_config.dart';

class PostCategories extends StatefulWidget {
  final List selectedCategories;

  const PostCategories({Key key, this.selectedCategories = const []})
      : super(key: key);
  @override
  _PostCategoriesState createState() => _PostCategoriesState();
}

class _PostCategoriesState extends State<PostCategories> {
  List selectedCategories = [];
  List allItems = appSettingsModel
      .getList(APP_CATEGORIES)
      .where((e) => !BaseModel(items: e).getBoolean(IS_DEFAULT))
      .toList();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedCategories = widget.selectedCategories;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        color: black.withOpacity(.5),
        child: Stack(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                color: black.withOpacity(.5),
              ),
            ),
            Container(
                alignment: Alignment.bottomCenter,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  ),
                  child: Container(
                    color: white,
                    padding: EdgeInsets.all(15),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: 100.0,
                          height: 10.0,
                          decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                        ),
                        addSpace(10),
                        Container(
                          padding: EdgeInsets.only(left: 15, right: 15),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(
                                LangKey.chooseCategory.toAppLanguage,
                                style: textStyle(false, 16, black),
                              )),
                              Image.asset(
                                "assets/images/clip_categories.png",
                                height: 25,
                                width: 25,
                                fit: BoxFit.cover,
                              )
                            ],
                          ),
                        ),
                        addSpace(10),
                        Container(
                          height: 2.0,
                          margin: EdgeInsets.only(left: 20, right: 20),
                          color: Colors.grey[300],
                        ),
                        addSpace(10),
                        Flexible(
                          child: GridView.count(
                            padding: 0.padAll(),
                            crossAxisCount: 3,
                            childAspectRatio: 2.5,
                            crossAxisSpacing: 4,
                            mainAxisSpacing: 4,
                            shrinkWrap: true,
                            children: allItems.map((e) {
                              BaseModel model = BaseModel(items: e);
                              Map translations = model.getMap(TRANSLATIONS);
                              bool selected = selectedCategories.contains(e);
                              bool isDefault = model.getBoolean(IS_DEFAULT);

                              return GestureDetector(
                                onTap: () {
                                  if (isDefault) return;
                                  if (mounted)
                                    setState(() {
                                      if (selected) {
                                        selectedCategories.remove(e);
                                      } else {
                                        selectedCategories.add(e);
                                      }
                                    });
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: AppConfig.appColor
                                          .withOpacity(selected ? 1 : .1),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Text(
                                    translations.toAppLanguage,
                                    style: textStyle(
                                        selected, 14, selected ? white : black),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        if (selectedCategories.isNotEmpty)
                          RaisedButton(
                            onPressed: () {
                              Navigator.pop(context, selectedCategories);
                            },
                            color: AppConfig.appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25)),
                            child: Center(
                                child: Text(
                              LangKey.choose.toAppLanguage,
                              style: textStyle(true, 14, white),
                            )),
                          )
                      ],
                    ),
                  ),
                ))
          ],
        ));
  }
}
