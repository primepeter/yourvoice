import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:latlong/latlong.dart';

import 'LangMain.dart';
import 'app/app.dart';
import 'app_config.dart';

class SearchPlace extends StatefulWidget {
  @override
  _SearchPlaceState createState() => _SearchPlaceState();
}

class _SearchPlaceState extends State<SearchPlace> {
  TextEditingController searchController = TextEditingController();
  List searchResults = [];
  bool _progressLoading = false;
  bool _showCancel = false;
//  bool _noResultFound = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(fit: StackFit.expand, children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
              child: Container(
                color: black.withOpacity(.7),
              )),
        ),
        page()
      ]),
      backgroundColor: Colors.transparent,
    );
  }

  page() {
    return new Stack(
      children: <Widget>[
        _progressLoading
            ? loadingLayout(color: transparent)
            : Container(
                padding: EdgeInsets.only(top: 80),
                child: ListView.builder(
                  itemBuilder: (context, position) {
                    BaseModel bm = searchResults[position];
                    return GestureDetector(
                      onTap: () {
                        decodeAndSelectPlace(
                            bm.getString(PLACE_NAME), bm.getObjectId());
                      },
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 10, 10),
                        child: Card(
                          color: white,
                          child: Padding(
                            padding: const EdgeInsets.all(15),
                            child: Text(
                              bm.getString(PLACE_NAME),
                              style: textStyle(false, 20, black),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                  shrinkWrap: true,
                  itemCount: searchResults.length,
                ),
              ),
        new Container(
          padding: EdgeInsets.only(top: 50, left: 20),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              BackButton(
                color: white,
              ),
              Flexible(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 8,
                  ),
                  height: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: white,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 20.0,
                        color: Colors.white.withOpacity(0.4),
                      )
                    ],
                  ),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.search,
                        color: black.withOpacity(.5),
                        size: 20,
                      ),
                      addSpaceWidth(8),
                      new Expanded(
                        child: new TextField(
                          textInputAction: TextInputAction.search,
                          textCapitalization: TextCapitalization.sentences,
                          autofocus: true,
                          onSubmitted: (_) {
                            //search();
                          },
                          onChanged: (_) {
                            if (_.trim().isEmpty) {
                              setState(() {
                                _progressLoading = false;
                                searchResults.clear();
                              });
                              return;
                            }
                            createSearchHandler();
                          },
                          decoration: InputDecoration(
                            hintText: LangKey.searchPlace.toAppLanguage,
                            hintStyle: textStyle(
                              false,
                              18,
                              black.withOpacity(.5),
                            ),
                            isDense: true,
                            border: InputBorder.none,
                          ),
                          style: textStyle(false, 18, black),
                          controller: searchController,
                          cursorColor: black,
                          cursorWidth: 1,
                          keyboardType: TextInputType.text,
                        ),
                      ),
                      addSpaceWidth(8),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            searchResults.clear();
//                               _noResultFound = false;
                            searchController.text = "";
                          });
                        },
                        child: _showCancel
                            ? Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                                child: Icon(
                                  Icons.close,
                                  color: AppConfig.appColor,
                                  size: 20,
                                ),
                              )
                            : new Container(),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  void decodeAndSelectPlace(String name, String placeId) {
    showProgress(true, context);
    String endpoint =
        "https://maps.googleapis.com/maps/api/place/details/json?key=${apiKey}" +
            "&placeid=$placeId";

    var endP =
        "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/details/json?key=${apiKey}" +
            "&placeid=$placeId";

    http.get(endpoint).then((response) {
      if (response.statusCode == 200) {
        Future.delayed(Duration(seconds: 1), () {
          showProgress(false, context);
          Map<String, dynamic> location =
              jsonDecode(response.body)['result']['geometry']['location'];

          LatLng latLng = LatLng(location['lat'], location['lng']);

          BaseModel model = BaseModel();
          model.put(PLACE_NAME, name);
          model.put(OBJECT_ID, placeId);
          model.put(LATITUDE, latLng.latitude);
          model.put(LONGITUDE, latLng.longitude);
          Future.delayed(Duration(seconds: 2), () {
            Navigator.pop(context, model);
          });
        });
      }
    }).catchError((error) {
      print(error);
      showProgress(false, context);
    });
  }

  bool handlerSet = false;
  void createSearchHandler() {
    _showCancel = true;
    _progressLoading = true;
    setState(() {});
    if (handlerSet) return;
    handlerSet = true;

    Future.delayed(Duration(seconds: 2), () {
      runSearch();
      handlerSet = false;
    });
  }

  final String apiKey = "AIzaSyAdBH8Jt-ZIHs9iZdgBe5czWIK4lwRZRak";
  // final String apiKey = "AIzaSyDXqqm4xYxWk6mTPgKg6GXsJIG2Ah8MT1U";
  void runSearch() async {
    searchResults.clear();
    String place = searchController.text.trim().toLowerCase();
    String sessionToken = getRandomId();
    if (place.isEmpty) {
      _progressLoading = false;
      return;
    }

    var endpoint =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?" +
            "key=${apiKey}&" +
            "input={$place}&sessiontoken=${sessionToken}";

    var endP =
        "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/autocomplete/json?" +
            "key=${apiKey}&" +
            "input={$place}&sessiontoken=${sessionToken}";

    http.get(endpoint, headers: {"Access-Control-Allow-Origin": "*"}).then(
        (response) {
      _progressLoading = false;

      if (response.statusCode == 200) {
        Map<String, dynamic> data = jsonDecode(response.body);
        List<dynamic> predictions = data['predictions'];

        for (dynamic t in predictions) {
          BaseModel model = BaseModel();
          model.put(PLACE_NAME, t['description']);
          model.put(OBJECT_ID, t['place_id']);
          List terms = List.from(t['terms']);
          String country = terms[terms.length - 1]["value"];
          model.put(COUNTRY, country);

          searchResults.add(model);
        }
//        _noResultFound = searchResults.isEmpty;
        setState(() {});
      }
    }).catchError((error) {
      _progressLoading = false;
      print(error);
    });
  }
}
