import 'dart:ui';

import 'package:acclaim/app/app.dart';
import 'package:flutter/material.dart';

import 'LangMain.dart';
import 'app_config.dart';

class PrivacyPolicy extends StatefulWidget {
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppConfig.appColor,
      body: interestBody(),
    );
  }

  interestBody() {
    String translation =
        appSettingsModel.getMap(PRIVACY_POLICY)[TRANSLATIONS][appLanguage];

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Image.asset(
          'assets/images/interest_bg.png',
          fit: BoxFit.cover,
        ),
        BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 8.0,
            sigmaY: 8.0,
          ),
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      black.withOpacity(.09),
                      AppConfig.appColor.withOpacity(.2)
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.1, 1.0])),
          ),
        ),
        Column(
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 35),
              child: FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                color: white.withOpacity(.6),
                shape: CircleBorder(),
                padding: EdgeInsets.all(10),
                child: Icon(
                  Icons.close,
                  size: 18,
                ),
              ),
            ),
            Text(
              Language.value(LangKey.privacyPolicy),
              style: textStyle(true, 20, black),
            ),
            addSpace(10),
            Flexible(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  Container(
                    padding: 15.padAll(),
                    margin: 10.padAll(),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: white.withOpacity(.5),
                        border: Border.all(
                            color: AppConfig.appColor.withOpacity(.5))),
                    child: Text(
                      translation,
                      style: textStyle(false, 16, black),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
