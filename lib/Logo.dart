import 'package:acclaim/app/app.dart';
import 'package:flutter/material.dart';

class Logo extends StatefulWidget {
  final String appName, assetPath;

  const Logo({Key key, @required this.appName, @required this.assetPath})
      : super(key: key);
  @override
  _LogoState createState() => _LogoState();
}

class _LogoState extends State<Logo> with TickerProviderStateMixin {
  String appName, assetPath;
  bool reset = false;

  AnimationController animation;
  Animation<double> _fadeInFadeOut;

  AnimationController animation1;
  Animation<double> _fadeInFadeOut1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    appName = widget.appName;
    assetPath = widget.assetPath;
    loadAnimation();
  }

  loadAnimation() {
    animation = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    );

    animation1 = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );

    _fadeInFadeOut = Tween<double>(begin: 0.0, end: 1.0).animate(animation);
    _fadeInFadeOut1 = Tween<double>(begin: 0.0, end: 1.0).animate(animation1);

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        reset = true;
        animation1.forward();
        setState(() {});
      }
    });

    animation1.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        print("ready to proceed");
      }
    });
    animation.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Center(
        child: AnimatedContainer(
          duration: Duration(seconds: 500),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (reset)
                ScaleTransition(
                  scale: _fadeInFadeOut1,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        assetPath,
                        height: 60,
                        width: 60,
                        fit: BoxFit.cover,
                      ),
                      addSpaceWidth(5),
                      Text(
                        "OurVoice",
                        style: textStyle(true, 30, black),
                      )
                    ],
                  ),
                )
              else
                FadeTransition(
                  opacity: _fadeInFadeOut,
                  child: Image.asset(
                    assetPath,
                    height: 130,
                    width: 130,
                    fit: BoxFit.cover,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
