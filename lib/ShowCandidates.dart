import 'dart:math';

import 'package:acclaim/AddCandidate.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'app/app.dart';

class ShowCandidates extends StatefulWidget {
  final bool popResult;

  const ShowCandidates({Key key, this.popResult = false}) : super(key: key);
  @override
  _ShowCandidatesState createState() => _ShowCandidatesState();
}

class _ShowCandidatesState extends State<ShowCandidates> {
  List items = [];
  List allItems = [];
  bool setup = false;
  String objectId = '';
  List electoralPosition = appSettingsModel.getList(POSITIONS);
  List sitType = appSettingsModel.getList(SIT_TYPE);

  TextEditingController searchController = TextEditingController();
  bool showCancel = false;
  FocusNode focusSearch = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadItems();
  }

  int maxTime = 0;
  loadItems() async {
    QuerySnapshot shots = await Firestore.instance
        .collection(CANDIDATE_BASE)
        // .where(
        //   STATUS,
        //   isEqualTo: PENDING,
        // )
        // .orderBy(TIME, descending: false)
        //.startAt([maxTime])
        .get()
        .catchError((e) {
      print("Error John $e");
      checkError(context, e);
      setup = true;
      setState(() {});
    });

    for (DocumentSnapshot doc in shots.docs) {
      BaseModel model = BaseModel(doc: doc);
      maxTime = max(model.getTime(), maxTime);
      int index = items.indexWhere(
          (element) => element.getObjectId() == model.getObjectId());
      if (index == -1)
        items.add(model);
      else
        items[index] = model;
    }

    setup = true;
    try {
      refreshController.loadComplete();
    } catch (e) {}
    ;
    if (mounted) setState(() {});
  }

  RefreshController refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(25),
        new Container(
          width: double.infinity,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: Text(
                  "Candidates",
                  style: textStyle(true, 20, black),
                ),
              ),
              if (objectId.isNotEmpty)
                FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: appColor,
                    onPressed: () {
                      BaseModel model =
                          items.singleWhere((e) => e.getObjectId() == objectId);
                      Navigator.pop(context, model);
                    },
                    child: Text(
                      'Select',
                      style: textStyle(true, 16, white),
                    )),
              if (!widget.popResult)
                FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: appColor,
                    onPressed: () {
                      pushAndResult(context, AddCandidate(), result: (_) {
                        items.add(_);
                        setState(() {});
                      });
                    },
                    child: Text(
                      'Add',
                      style: textStyle(true, 16, white),
                    )),
              addSpaceWidth(10),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: messageText.isEmpty ? 0 : 40,
          color: light_green3,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            messageText,
            style: textStyle(true, 14, white),
          )),
        ),
        Expanded(
            child: !setup
                ? loadingLayout()
                : items.isEmpty
                    ? emptyLayout(Icons.report, "No Candidates Yet", "",
                        clickText: "Reload", click: () {
                        setup = false;
                        setState(() {});
                        loadItems();
                      })
                    : ListView.builder(
                        itemBuilder: (c, p) {
                          BaseModel model = items[p];
                          String name = model.getString(NAME);
                          String party = model.getString(POLITICAL_PARTY);
                          String state = model.getString(STATE);
                          int eP = model.getInt(ELECTORAL_POSITION);
                          int sP = model.getInt(SIT_TYPE);
                          String position = electoralPosition[eP];
                          String sit = sitType[sP];
                          List states = [];
                          BaseModel district = model.getModel(DISTRICT);
                          if (district != null)
                            states = district.getList(STATE_LOCALS);

                          bool active = objectId == model.getObjectId();
                          return GestureDetector(
                            onTap: () {
                              if (widget.popResult) {
                                objectId = model.getObjectId();
                                setState(() {});
                                return;
                              }

                              showListDialog(context, ['Edit', 'Delete'], (_) {
                                print(_);
                                if (_ == 'Edit') {
                                  pushAndResult(
                                      context,
                                      AddCandidate(
                                        model: model,
                                      ), result: (_) {
                                    items[p] = _;
                                    setState(() {});
                                  });
                                }

                                if (_ == 'Delete') {
                                  model.deleteItem();
                                  items.removeAt(p);
                                  setState(() {});
                                }
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Card(
                                color: default_white,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: appColor
                                            .withOpacity(active ? 1 : 0),
                                        width: active ? 2 : 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                clipBehavior: Clip.antiAlias,
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            //fit: FlexFit.tight,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Image.asset(
                                                      'assets/images/district.jpg',
                                                      width: 14,
                                                      height: 14,
                                                      color: light_green3,
                                                    ),
                                                    addSpaceWidth(5),
                                                    Flexible(
                                                      child: Text(
                                                        name + ' - ' + position,
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: textStyle(true,
                                                            12, light_green3),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                addSpace(5),
                                                nameItem(
                                                    'Electoral Party', party,
                                                    paddBottom: false),
                                                addSpace(5),
                                                nameItem('Sit Type', sit,
                                                    paddBottom: false),
                                                if (eP != 0) ...[
                                                  addSpace(5),
                                                  nameItem('State', state,
                                                      paddBottom: false),
                                                  addSpace(5),
                                                  Text(
                                                    "Electoral District",
                                                    style: textStyle(
                                                        true, 13, black),
                                                  ),
                                                ],
                                                addSpace(5),
                                              ],
                                            ),
                                          ),
                                          ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5)),
                                            child: CachedNetworkImage(
                                              imageUrl:
                                                  model.getString(IMAGE_URL),
                                              width: 60,
                                              height: 60,
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        ],
                                      ),
                                      addSpace(5),
                                      SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: List.generate(states.length,
                                              (index) {
                                            String item = states[index];
                                            return Card(
                                              color: white,
                                              elevation: 0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      item,
                                                      style: textStyle(
                                                          false,
                                                          12,
                                                          black
                                                              .withOpacity(.5)),
                                                    ),
                                                    addSpace(5),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                                      addSpace(5),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                        shrinkWrap: true,
                        itemCount: items.length,
                        padding: EdgeInsets.zero,
                      ))
      ],
    );
  }

  String messageText = "";
  popMessage(String text) {
    messageText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      messageText = "";
      setState(() {});
    });
  }
}
