import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app/app.dart';
import 'app_config.dart';
import 'play_audio_book.dart';

class AddBook extends StatefulWidget {
  final BaseModel post;

  const AddBook({Key key, this.post}) : super(key: key);

  @override
  _AddBookState createState() => _AddBookState();
}

class _AddBookState extends State<AddBook> {
  List<BaseModel> photos = [];
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final postController = new TextEditingController();
  final tagController = new TextEditingController();

  final descriptionController = new TextEditingController();
  final searchTagsController = new TextEditingController();
  final writerController = new TextEditingController();
  final publisherController = new TextEditingController();
  final studioController = new TextEditingController();
  final assistedController = new TextEditingController();
  final otherDetailsController = new TextEditingController();
  final narratorsController = new TextEditingController();

  bool isSearching = false;
  List<BaseModel> searchResponse = [];
  List<BaseModel> mentionedUsers = [];
  String str = '';
  List<String> words = [];

  String mentionedHashTagStr = "General";
  List<String> mentionedHashTag = ['post_type_0'];

  String mentionedPlace;
  double mentionedPlaceLat = 0.0;
  double mentionedPlaceLong = 0.0;

  BaseModel audioBook = BaseModel();
  String coverPhoto = "";
  List<BookStructure> structure = [
    BookStructure(
      controller: TextEditingController(),
    )
  ];
  List<AudioPlayer> players = [];

  bookItem(TextEditingController controller,
      {GlobalKey key,
      String file = "",
      onTap,
      onAddMore,
      onRemove,
      onPlay,
      bool isPlaying = false,
      bool canRemove = false,
      bool last = false}) {
    bool hasPath = null != file && file.isNotEmpty;
    return Container(
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: black.withOpacity(.05),
                border: Border.all(color: black.withOpacity(.05)),
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Chapter Title"),
                addSpace(5),
                TextField(
                  controller: controller,
                  decoration: InputDecoration(
                    fillColor: white,
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: black.withOpacity(.1))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: black.withOpacity(.1))),
                  ),
                ),
                addSpace(10),
                Text("Add Audio File"),
                Row(
                  children: [
                    Flexible(
                      child: FlatButton(
                          onPressed: onTap,
                          color: AppConfig.appColor,
                          child: Center(
                              child: Text(
                            "MP3/WAV",
                            style: textStyle(true, 13, white),
                          ))),
                    ),
                    if (canRemove) ...[
                      addSpaceWidth(10),
                      FlatButton(
                          onPressed: onRemove,
                          color: red0,
                          //shape: CircleBorder(),
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          padding: EdgeInsets.all(0),
                          child: Text(
                            "Remove",
                            style: textStyle(true, 13, white),
                          )),
                    ]
                  ],
                ),
                if (hasPath) ...[
                  addSpace(5),
                  Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: black.withOpacity(.05),
                          border: Border.all(color: black.withOpacity(.05)),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(file)),
                  addSpace(10),
                  Flexible(
                    child: FlatButton(
                        onPressed: onPlay,
                        color: AppConfig.appColor,
                        child: Center(
                            child: Text(
                          "PREVIEW",
                          style: textStyle(true, 13, white),
                        ))),
                  ),
                ],
              ],
            ),
          ),
          if (last) ...[
            addSpace(10),
            Row(
              children: [
                FlatButton(
                    onPressed: onAddMore,
                    color: AppConfig.appColor,
                    child: Center(
                        child: Text(
                      "Add File",
                      style: textStyle(true, 13, white),
                    ))),
              ],
            )
          ]
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      if (null != widget.post) {
        audioBook = widget.post;
      }
    });
  }

  loadPost() {
    photos = audioBook.getPhotos;
    mentionedUsers = audioBook.getList(TAGGED_PERSONS_IDS);
    mentionedHashTag = audioBook.getList(TAGGED_CATEGORY);
    mentionedPlace = audioBook.getString(TAGGED_PLACE);
    mentionedPlaceLat = audioBook.getDouble(TAGGED_PLACE_LAT);
    mentionedPlaceLong = audioBook.getDouble(TAGGED_PLACE_LONG);
    postController.text = audioBook.getString(MESSAGE);
  }

  @override
  void dispose() {
    // for (var e in structure) {
    //   if (e.key.currentState == null) continue;
    //   e.key.currentState.stop();
    // }
    // for (var d in players) {
    //   d?.stop();
    // }
    super.dispose();
  }

  appBar() {
    return new AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Upload a new book", //translate(context, LangKey.create_a_post),
        style: TextStyle(color: Colors.black),
      ),
      centerTitle: false,
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
            color: AppConfig.appColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: Text(
              translate(context, LangsKey.post),
              style: TextStyle(color: Colors.white),
            ),
            onPressed: makePost,
          ),
        ),
      ],
    );
  }

  makePost() async {
    String description = descriptionController.text;
    String searchTag = searchTagsController.text;
    String writers = writerController.text;
    String publishers = publisherController.text;
    String narrators = narratorsController.text;
    String production = studioController.text;
    String assisted = assistedController.text;
    String otherDetails = otherDetailsController.text;

    final books = structure;
    //Validate the structure model
    for (int p = 0; p < books.length; p++) {
      final book = books[p];
      final title = book.controller.text;
      final path = book.audioPath;

      if (title.isEmpty) {
        toast(scaffoldKey, "Please enter a title at the $p audio attached!");
        break;
      }
      if (path.isEmpty) {
        toast(scaffoldKey, "Please attach an audio at the $p!");
        break;
      }
    }
    final list =
        books.where((element) => element.audioDuration == null).toList();

    //if (list.isNotEmpty) return;

    final validatedAudio = books.map((e) {
      return BaseModel()
        ..put(TITLE, e.controller.text)
        ..put(DURATION, e.audioDuration)
        ..put(URL_PATH, e.audioPath);
    }).toList();

    if (coverPhoto.isEmpty) {
      toast(scaffoldKey, "Please add a cover photo of this book!");
      return;
    }

    if (description.isEmpty) {
      toast(scaffoldKey, "Please add a description of this book!");
      return;
    }

    if (searchTag.isEmpty) {
      toast(scaffoldKey, "Please add a search tags for this book!");
      return;
    }

    if (writers.isEmpty) {
      toast(scaffoldKey, "Please add writers of this book!");
      return;
    }
    if (publishers.isEmpty) {
      toast(scaffoldKey, "Please add publishers of this book");
      return;
    }

    if (narrators.isEmpty) {
      toast(scaffoldKey, "Please add narrators of this book");
      return;
    }

    if (production.isEmpty) {
      toast(scaffoldKey, "Please add studio of production of this book");
      return;
    }

    if (assisted.isEmpty) {
      toast(
          scaffoldKey, "Please memebers who assisted in creation of this book");
      return;
    }

    if (otherDetails.isEmpty) {
      toast(scaffoldKey, "Please add any other detail of this book");
      return;
    }

    String id = getRandomId();
    audioBook
      ..put(OBJECT_ID, id)
      ..put(AUDIO_BOOKS, validatedAudio.map((e) => e.items).toList())
      ..put(COVER_PHOTO, coverPhoto)
      ..put(DESCRIPTION, description)
      ..put(SEARCH_PARAM, getSearchString(searchTag))
      ..put(WRITER, writers)
      ..put(PUBLISHER, publishers)
      ..put(NARRATORS, narrators)
      ..put(STUDIO, production)
      ..put(ASSISTED, assisted)
      ..put(OTHER_DETAILS, otherDetails);
    audioBook.saveItem(LIBRARY_BASE, true, document: id, onComplete: () {
      bookUploadController.add(audioBook);
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: white,
        appBar: appBar(),
        body: libraryBody(),
      ),
    );
  }

  libraryBody() {
    return ListView(
      children: [
        GestureDetector(
          onTap: () {
            getSingleCroppedImage(context, onPicked: (_) {
              setState(() {
                coverPhoto = _.urlPath;
              });
            });
          },
          child: Container(
            height: 200,
            width: double.infinity,
            decoration: BoxDecoration(
              color: black.withOpacity(.05),
              border: Border.all(color: black.withOpacity(.05)),
              //borderRadius: BorderRadius.circular(10),
            ),
            child: Stack(
              children: [
                if (!fromOnline(coverPhoto) || coverPhoto.isEmpty)
                  Container(
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.center,
                    child: coverPhoto.isEmpty
                        ? Text(
                            "Tap to Add a cover picture",
                            style: textStyle(false, 20, black),
                          )
                        : Image.file(
                            File(coverPhoto),
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  )
                else
                  CachedNetworkImage(
                    imageUrl: coverPhoto,
                    alignment: Alignment.topCenter,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    placeholder: (c, s) {
                      return placeHolder(
                        200,
                        width: double.infinity,
                      );
                    },
                  )
              ],
            ),
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: List.generate(structure.length, (p) {
            final book = structure[p];
            bool last = p == structure.length - 1;
            int stopAt = p == 0 ? 0 : p - 1;

            return bookItem(book.controller,
                file: book.audioPath,
                last: last,
                canRemove: p != 0, onAddMore: () {
              if (structure[stopAt].audioPath.isNotEmpty) {}

              setState(() {
                final bk = BookStructure(
                  controller: TextEditingController(),
                );
                structure.add(bk);
              });
            }, onRemove: () {
              setState(() {
                structure.removeAt(p);
              });
            }, onTap: () async {
              try {
                /* String path = await FilePicker.getFilePath(
                    //type: FileType.audio,
                    //allowedExtensions: ["mp3", "wav"]
                    );
                if (null == path) return;
                structure[p].audioPath = path;*/

                setState(() {});
              } on PlatformException catch (e) {
                print("Unsupported operation" + e.toString());
              }
            }, onPlay: () {
              final validatedAudio = structure.map((e) {
                return BaseModel()
                  ..put(TITLE, e.controller.text)
                  ..put(DURATION, e.audioDuration)
                  ..put(URL_PATH, e.audioPath);
              }).toList();
              final model = BaseModel();
              model
                ..put(COVER_PHOTO, coverPhoto)
                ..put(AUDIO_BOOKS, validatedAudio.map((e) => e.items).toList());

              pushAndResult(
                  context,
                  PlayAudioBook(
                    audioModel: model,
                  ),
                  result: (_) {});
            });
          }),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              textField("Add Description", descriptionController, maxLines: 4),
              textField("Add Search tags", searchTagsController),
              textField("Add Writers", writerController),
              textField("Add Publishers", publisherController),
              textField("Add Narrator(s)", narratorsController, maxLines: 4),
              textField(
                "Add Production",
                studioController,
              ),
              textField("Add Assisted in production", assistedController),
              textField("Add other details", otherDetailsController)
            ],
          ),
        ),
        addSpace(30),
      ],
    );
  }

  textField(String hint, TextEditingController controller, {int maxLines}) {
    return Column(
      children: [
        addSpace(20),
        Container(
          decoration: BoxDecoration(
              color: black.withOpacity(.05),
              border: Border.all(color: black.withOpacity(.05)),
              borderRadius: BorderRadius.circular(10)),
          padding: EdgeInsets.all(8),
          child: TextField(
            controller: controller,
            style: textStyle(false, 20, black),
            textAlignVertical: TextAlignVertical.center,
            maxLines: maxLines,
            decoration: InputDecoration(
                suffixIcon: SizedBox(
                    child: Center(widthFactor: 0.0, child: Icon(Icons.info))),
                hintText: hint,
                border: InputBorder.none),
          ),
        ),
      ],
    );
  }
}

class BookStructure {
  TextEditingController controller;
  String audioPath;
  int audioDuration;
  BookStructure({
    this.controller,
    this.audioPath = '',
    this.audioDuration,
  });
}
