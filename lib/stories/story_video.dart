import 'dart:io';

import 'package:acclaim/app/app.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class StoryVideo extends StatefulWidget {
  final BaseModel thisVideo;
  final bool play;
  final Function(VideoPlayerController controller) controller;
  const StoryVideo({Key key, this.thisVideo, this.play, this.controller})
      : super(key: key);
  @override
  _VideoPopState createState() => _VideoPopState();
}

class _VideoPopState extends State<StoryVideo> {
  VideoPlayerController videoController;
  bool showPlayIcon = true;
  bool hasSetUp = false;
  bool caching = false;
  BaseModel thisVideo;
  bool isPlaying = false;

  @override
  void initState() {
    super.initState();
    //vp.addListener(listener);
    if (mounted)
      setState(() {
        thisVideo = widget.thisVideo;
      });
    loadCached();
    if (widget.play) videoController?.play();
  }

  loadCached() async {
    String fileName =
        "${thisVideo.getObjectId()}${thisVideo.urlPath.hashCode}.mp4";
    File file = await getLocalFile(fileName);
    bool exist = await file.exists();
    if (!exist) {
      downloadFile(file, thisVideo.urlPath);
      return;
    }

    videoController = VideoPlayerController.file(file)
      ..initialize().then((value) {
        bool isMuted = userModel.videoAutoMute;
        videoController?.setVolume(isMuted ? 0 : 1);
        videoController.play();
        if (mounted)
          setState(() {
            hasSetUp = true;
          });
      });
    videoController.addListener(() {
      if (null != widget.controller) widget.controller(videoController);

      isPlaying = videoController.value.isPlaying;
      if (mounted)
        setState(() {
          showPlayIcon = !isPlaying;
        });
    });
  }

  void downloadFile(File file, String urlLink) async {
    QuerySnapshot shots = await Firestore.instance
        .collection(AppDataBase.referenceBase)
        .where(FILE_URL, isEqualTo: urlLink)
        .limit(1)
        .getDocuments();
    if (shots.documents.isEmpty) return;
    for (DocumentSnapshot doc in shots.documents) {
      if (!doc.exists || doc.data().isEmpty) continue;
      print("OKORE >>>> DOWNLOADING....<<<<");
      String ref = doc.data()[REFERENCE];
       Reference storageReference =
          FirebaseStorage.instance.ref().child(ref);
      storageReference.writeToFile(file).then((_) {
        if (mounted) setState(() {});
        loadCached();
      }, onError: (error) {}).catchError((error) {});

      break;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: transparent,
      child: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              color: black.withOpacity(.5),
            ),
          ),
          Container(
            //padding: const EdgeInsets.fromLTRB(25, 45, 25, 25),
            alignment: Alignment.center,
            child: Builder(
              builder: (ctx) {
                bool isMuted = userModel.videoAutoMute;

                if (!hasSetUp) {
                  return Container(
                    width: double.infinity,
                    margin: 0.padAt(t: 5, b: 5),
                    color: black,
                    child: Center(
                      child: SizedBox(
                        height: 20,
                        width: 20,
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor: AlwaysStoppedAnimation(white),
                        ),
                      ),
                    ),
                  );
                }

                return GestureDetector(
                  onTap: () {
                    if (isPlaying) {
                      videoController.pause();
                    } else {
                      videoController.play();
                    }
                    isPlaying = !isPlaying;
                    if (mounted) setState(() {});
                    print("object");
                  },
                  child: Container(
                    margin: 0.padAt(t: 5, b: 5),
                    width: double.infinity,
                    child: AspectRatio(
                      aspectRatio: videoController.value.aspectRatio,
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            color: black,
                          ),
                          VideoPlayer(videoController)
                          //Container()
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
