import 'package:cached_video_player/cached_video_player.dart';
import 'package:dio/dio.dart';

import 'app/app.dart';
import 'app_index.dart';

class VideoController {
  static loadVideo(BaseModel model, VoidCallback setTheState,
      {bool isPost = false, bool idIsUrl = false, String playId = ""}) async {
    if (isPost) {
      final photos = model.getListModel(IMAGES);
      for (int p = 0; p < photos.length; p++) {
        BaseModel bm = photos[p];
        if (!bm.isVideo) continue;
        String objectId = bm.getObjectId();
        String videoUrl = bm.getString(IMAGE_URL);
        if (objectId.isEmpty) objectId = videoUrl;

        if (videoUrl.isEmpty) continue;
        if (!videoUrl.startsWith("http")) continue;
        int vp = allVideos.indexWhere((e) => e.getObjectId() == objectId);
        if (vp != -1) return;
        CachedVideoPlayerController controller;
        controller = CachedVideoPlayerController.network(videoUrl)
          ..initialize().then((value) {
            String db = model.getString(DATABASE_NAME).toLowerCase();
            print("Initialized post $db @ $objectId");
            bm.put(VIDEO_CONTROLLER, controller);
            bm.put(OBJECT_ID, objectId);
            if (vp != -1)
              allVideos[vp] = bm;
            else
              allVideos.add(bm);
            setTheState();
          });
      }
      setTheState();
      return;
    }
    String objectId = model.getObjectId();
    String videoUrl = model.getString(IMAGE_URL);
    if (objectId.isEmpty) objectId = videoUrl;
    int p = allVideos.indexWhere((e) => e.getObjectId() == objectId);
    if (p != -1) return;
    CachedVideoPlayerController controller;
    controller = CachedVideoPlayerController.network(videoUrl)
      ..initialize().then((value) {
        if (playId == objectId) controller.play();
        String db = model.getString(DATABASE_NAME).toLowerCase();
        print("Initialized $db @ $objectId");
        model.put(VIDEO_CONTROLLER, controller);
        model.put(OBJECT_ID, objectId);
        if (p != -1)
          allVideos[p] = model;
        else
          allVideos.add(model);
        setTheState();
      });
    setTheState();
  }

  static stopAllVideos(VoidCallback setTheState) async {
    for (int p = 0; p < allVideos.length; p++) {
      BaseModel model = allVideos[p];
      CachedVideoPlayerController control = model.get(VIDEO_CONTROLLER);
      if (null == control) continue;
      if (!control.value.isPlaying) continue;
      await control.seekTo(Duration(seconds: 0));
      await control.pause();
      model.put(VIDEO_CONTROLLER, control);
      allVideos[p] = model;
      setTheState();
    }
  }

  static playVideoWithId(BaseModel model,
      Function(CachedVideoPlayerController controller) setTheState,
      {bool loop = false}) async {
    String objectId = model.getObjectId();
    String videoUrl = model.getString(IMAGE_URL);
    if (objectId.isEmpty) objectId = videoUrl;
    int p = allVideos.indexWhere((e) => e.getObjectId() == objectId);
    if (p == -1) return;
    BaseModel videoModel = allVideos[p];
    CachedVideoPlayerController control = videoModel.get(VIDEO_CONTROLLER);
    if (null == control) return;
    //control.setLooping(loop);
    control.play();
    videoModel.put(VIDEO_CONTROLLER, control);
    allVideos[p] = videoModel;
    setTheState(control);
  }

  static pauseVideoWithId(BaseModel model,
      Function(CachedVideoPlayerController controller) setTheState) async {
    String objectId = model.getObjectId();
    String videoUrl = model.getString(IMAGE_URL);
    if (objectId.isEmpty) objectId = videoUrl;

    int p = allVideos.indexWhere((e) => e.getObjectId() == objectId);
    if (p == -1) return;
    BaseModel videoModel = allVideos[p];
    CachedVideoPlayerController control = videoModel.get(VIDEO_CONTROLLER);
    if (null == control) return;
    control.pause();
    videoModel.put(VIDEO_CONTROLLER, control);
    allVideos[p] = videoModel;
    setTheState(control);
  }
}
