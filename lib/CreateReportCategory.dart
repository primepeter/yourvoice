import 'dart:io';

import 'package:acclaim/app_config.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'app/app.dart';

class CreateReportCategory extends StatefulWidget {
  BaseModel model;

  CreateReportCategory({Key key, this.model}) : super(key: key);
  @override
  _CreateReportCategoryState createState() => _CreateReportCategoryState();
}

class _CreateReportCategoryState extends State<CreateReportCategory> {
  BaseModel model = BaseModel();
  final translationController = TextEditingController();
  final subController = TextEditingController();
  int currentIndex = 0;
  List<BaseModel> options = [];
  List<TextEditingController> languages = [];

  String selectedLanguage = '';
  String selectedKey;

  String imageIcon;

  insertLanguages() {
    Map map = model.getMap(TRANSLATIONS);
    final languages = appSettingsModel.getList(APP_LANGUAGE);
    for (var s in languages) {
      map[s.toLowerCase()] = '';
    }
    model.put(TRANSLATIONS, map);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.model != null) {
      model = widget.model;
      selectedLanguage = model.getMap(TRANSLATIONS).keys.toList()[0];
      selectedKey = model.getMap(TRANSLATIONS).keys.toList()[0].toLowerCase();
      translationController.text =
          model.getMap(TRANSLATIONS).values.toList()[0];
      imageIcon = model.getString(ICON);
      print(model.getMap(TRANSLATIONS));
    } else {
      model.put(OBJECT_ID, getRandomId());
      insertLanguages();
    }
    translationController.addListener(() {
      final map = model.getMap(TRANSLATIONS);
      String value = translationController.text;
      map[selectedKey] = value;
      model.put(TRANSLATIONS, map);
      setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    translationController.dispose();
  }

  int clickBack = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int now = DateTime.now().millisecondsSinceEpoch;
        if ((now - clickBack) > 5000) {
          clickBack = now;
          showError("Click back again to exit");
          return false;
        }
        Navigator.pop(context, "");
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding:
                  EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BackButton(
                    onPressed: () {
                      Navigator.pop(context, '');
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Text(
                      "${null != widget.model ? 'Update' : "Create"} Report Category",
                      style: textStyle(true, 25, black),
                    ),
                  )
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: showSuccess ? dark_green0 : red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            page(),
            Container(
              padding: EdgeInsets.all(20),
              child: FlatButton(
                onPressed: () {
                  if (selectedLanguage.isEmpty) {
                    showError("Choose translation language");
                    return;
                  }

                  post();
                },
                color: AppConfig.appColor,
                padding: EdgeInsets.all(15),
                child: Center(
                    child: Text(
                  null != widget.model ? "UPDATE" : "CREATE",
                  style: textStyle(false, 18, white),
                )),
              ),
            )
          ],
        ),
      ),
    );
  }

  page() {
    return Expanded(
      child: ListView(
        padding: EdgeInsets.all(10),
        children: [
          clickText("Choose Language", selectedLanguage, () {
            final map = model.getMap(TRANSLATIONS);
            List items =
                map.keys.map((e) => e.toString().toUpperCase()).toList();
            showListDialog(context, items, (_) {
              selectedLanguage = items[_];
              selectedKey = map.keys.toList()[_];
              translationController.text = map[selectedKey];
              setState(() {});
            });
          }),
          //addSpace(10),
          inputTextView("Title", translationController, isNum: false),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
                border: Border.all(color: black.withOpacity(.1), width: .5),
                color: blue09),
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(model.getMap(TRANSLATIONS).keys.length,
                  (index) {
                String key = model.getMap(TRANSLATIONS).keys.toList()[index];
                String value = model.getMap(TRANSLATIONS)[key];
                return nameItem(
                    key, value.isEmpty ? "No Translation Yet" : value);
              }),
            ),
          ),
        ],
      ),
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  pickSingleImage() async {
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: file.path,
        maxWidth: 2500,
        maxHeight: 2500,
        compressFormat: ImageCompressFormat.png);
    if (croppedFile != null) {
      imageIcon = croppedFile.path;
      setState(() {});
    }
  }

  saveIcon() {
    showProgress(true, context, msg: "Saving Icon");
    uploadFile(File(imageIcon), (res, error) {
      showProgress(false, context);
      Future.delayed(Duration(milliseconds: 500), () {
        if (error != null) {
          showError("Error");
          return;
        }
        imageIcon = res;
        model.put(ICON, res);
        setState(() {});
        post();
      });
    });
  }

  post() {
    final appInterests = appSettingsModel.getList(APP_REPORT_CATEGORIES);
    int p = appInterests.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
    if (p != -1)
      appInterests[p] = model.items;
    else
      appInterests.add(model.items);

    setState(() {});
    // return;
    appSettingsModel
      ..put(APP_REPORT_CATEGORIES, appInterests)
      ..updateItems();
    showError("Updated!!!", success: true);
  }
}
