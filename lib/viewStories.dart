import 'dart:ui';

import 'package:acclaim/VideoController.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/localization/app_translations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cached_video_player/cached_video_player.dart';
import 'package:flutter/material.dart';

import 'app_index.dart';

class viewStories extends StatefulWidget {
  final List<List<BaseModel>> stories;
  final int position;
  final playIndex;
  viewStories(this.stories, {this.position = 0, this.playIndex = 0});
  @override
  _viewStoriesState createState() => _viewStoriesState();
}

class _viewStoriesState extends State<viewStories>
    with TickerProviderStateMixin {
  PageController pageController;
  List<List<BaseModel>> stories;
  List<List<BaseModel>> indicator;
  int currentPage = 0;
  double progressValue = 0;
  BaseModel oldStory;
  // CachedVideoPlayerController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    stories = widget.stories;
    indicator = widget.stories;
    pageItemPosition = widget.playIndex;
    currentPage = widget.position;
    pageController = PageController(initialPage: widget.position);
    startStories();
  }

  startStories() {
    hasStarted = true;
    Future.delayed(Duration(milliseconds: 500), () {
      print("cp $currentPage tp ${widget.position}");
      //pageController?.jumpToPage(currentPage);
      createTimer(initPlay: true);
    });
    if (mounted) setState(() {});
  }

  stopAllVideos() async {
    VideoController.stopAllVideos(() {
      if (mounted) setState(() {});
    });
    /* for (int p = 0; p < allVideos.length; p++) {
      BaseModel model = allVideos[p];
      CachedVideoPlayerController control = model.get(VIDEO_CONTROLLER);
      if (null == control) return;
      control.seekTo(Duration(seconds: 0));
      await control.pause();
      // control.value.copyWith(duration: Duration(seconds: 0));
      model.put(VIDEO_CONTROLLER, control);
      allVideos[p] = model;
      if (mounted) setState(() {});
    }*/
  }

  playVideoAt(int currentPage, int pageItemPosition) async {
    List<BaseModel> storiesAtIndex = stories[currentPage];
    BaseModel bm = storiesAtIndex[pageItemPosition];
    if (!bm.isVideo) return;
    VideoController.playVideoWithId(bm, (c) {
      //controller = c;
      if (mounted) setState(() {});
    });
    /*  int p = allVideos.indexWhere((e) => e.getObjectId() == bm.getObjectId());
    if (p == -1) return;
    BaseModel videoModel = allVideos[p];
    CachedVideoPlayerController control = videoModel.get(VIDEO_CONTROLLER);
    if (null == control) return;
    control.play();
    videoModel.put(VIDEO_CONTROLLER, control);
    allVideos[p] = videoModel;
    if (mounted) setState(() {});*/
  }

  pauseVideoAt(int currentPage, int pageItemPosition) async {
    List<BaseModel> storiesAtIndex = stories[currentPage];
    BaseModel bm = storiesAtIndex[pageItemPosition];
    if (!bm.isVideo) return;

    VideoController.pauseVideoWithId(bm, (c) {
      //controller = c;
      if (mounted) setState(() {});
    });

/*    int p = allVideos.indexWhere((e) => e.getObjectId() == bm.getObjectId());
    if (p == -1) return;
    BaseModel videoModel = allVideos[p];
    CachedVideoPlayerController control = videoModel.get(VIDEO_CONTROLLER);
    if (null == control) return;
    control.pause();
    videoModel.put(VIDEO_CONTROLLER, control);
    allVideos[p] = videoModel;
    if (mounted) setState(() {});*/
  }

  bool hasStarted = false;

  @override
  Widget build(BuildContext context) {
    return !setup
        ? loadingLayout()
        : MaterialApp(
            debugShowCheckedModeBanner: false,
            locale: Locale("en"),
            color: black,
            home: Scaffold(
              body: page(),
              backgroundColor: black,
            ),
          );
  }

  disposeEverything() {
    try {
      animController?.stop(canceled: true);
    } catch (e) {}
    try {
      animController?.dispose();
    } catch (e) {}
    stopAllVideos();
  }

  page() {
    return Stack(
      alignment: Alignment.center,
      children: [
        GestureDetector(
          onTapDown: (td) {
            print("tapping down");
            if (tappedDown) return;
            tappedDown = true;
            int now = DateTime.now().millisecondsSinceEpoch;
            Future.delayed(Duration(milliseconds: 200), () {
              int diff = now - tappedUpTime;
              if (diff < 200) {
                bool toLeft = td.globalPosition.dx <= 100;
                print("Changing Page");
                oldStory = stories[currentPage][pageItemPosition];
                changePage(toLeft);
              } else {
                pausePage();
              }
              tappedDown = false;
            });
          },
          onTapUp: (td) {
            print("tapping up");
            tappedUpTime = DateTime.now().millisecondsSinceEpoch;
            if (pagePaused) resumePage();
          },
          child: PageView(
              controller: pageController,
              scrollDirection: Axis.vertical,
              onPageChanged: (p) {
                setState(() {
                  currentPage = p;
                  pageItemPosition = 0;
                  progressValue = 0;
                  oldStory = null;
                  createTimer(payChanged: true);
                });
              },
              children: List.generate(stories.length, (p) => pageItem(p))),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            margin: EdgeInsets.only(
              top: 60,
            ),
            child: FlatButton(
              color: Colors.red,
              onPressed: () {
                AppTranslations.load(Locale("ar"));
                disposeEverything();
                Navigator.pop(context);
              },
              child: Icon(
                Icons.close,
                color: white,
              ),
              shape: CircleBorder(),
            ),
          ),
        )
      ],
    );
  }

  storyProgressIndicator(
    int page,
  ) {
    BaseModel story = stories[page][0];
    String fullName = story.fullName;
    String nickName = story.nickName;
    String imageUrl = story.userImage;
    bool myPost = story.myItem();
    String time =
        getTimeAgo(stories[page][pageItemPosition].getInt(TIME_UPDATED));
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        padding: 30.padAt(t: 45, r: 10, l: 10),
        //color: red0,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: List.generate(indicator[page].length, (p) {
                /*BaseModel progress = indicator[page][p];
                double value = progress.getDouble(STORY_PROGRESS);
                return Flexible(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      height: 8,
                      padding: 2.padAll(),
                      child: LinearProgressIndicator(
                        value: value,
                        backgroundColor: white.withOpacity(0.4),
                        valueColor: AlwaysStoppedAnimation<Color>(white),
                      ),
                    ),
                  ),
                );*/

                double value = progressValue;
                if (pageItemPosition > p) value = 1.0;
                if (pageItemPosition < p) value = 0.0;

                return Flexible(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      height: 8,
                      padding: 2.padAll(),
                      child: LinearProgressIndicator(
                        value: value,
                        backgroundColor: white.withOpacity(0.4),
                        valueColor: AlwaysStoppedAnimation<Color>(white),
                      ),
                    ),
                  ),
                );
              }),
            ),
            10.spaceHeight(),
            Row(
              children: [
                Flexible(
                  child: Row(
                    children: [
                      imageHolder(50, imageUrl, stroke: 2, strokeColor: white),
                      10.spaceWidth(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(myPost ? "Your Story" : fullName,
                              style: textStyle(true, 14, white)),
                          5.spaceHeight(),
                          Text(time, style: textStyle(false, 12, white)),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  int pageItemPosition = 0;
  bool tappedDown = false;
  int tappedUpTime = 0;

  pageItem(int index) {
    List<BaseModel> story = stories[index];
    if (pageItemPosition < 0 || pageItemPosition >= story.length)
      return Container();

    print("story $index playIndex $pageItemPosition");
    BaseModel bm = story[pageItemPosition];
    String id = bm.getObjectId();
    String imageUrl = bm.getString(IMAGE_URL);
    String thumbUrl = bm.getString(THUMBNAIL_URL);
    bool isVideo = bm.isVideo;
    final taggedPersons = bm.getList(TAGGED_PERSONS);

    return Container(
      color: black, // AppConfig.appColor,
      width: double.infinity,

      child: Stack(
        alignment: Alignment.center,
        children: [
          BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
              child: Container(
                color: black.withOpacity(.4),
              )),
          if (isVideo)
            Center(
              child: Builder(
                builder: (c) {
                  int vp = allVideos.indexWhere((e) => e.getObjectId() == id);
                  BaseModel videoModel;
                  CachedVideoPlayerController controller;

                  if (vp != -1) {
                    videoModel = allVideos[vp];
                    controller = videoModel.get(VIDEO_CONTROLLER);
                  }

                  if (null == controller || vp == -1) return videoLoading;
                  print("isVideo ${controller.value.initialized}");

                  return Center(
                    child: AspectRatio(
                      aspectRatio: controller.value.aspectRatio,
                      child: CachedVideoPlayer(controller),
                    ),
                  );
                },
              ),
            )
          else
            CachedNetworkImage(
              imageUrl: isVideo ? thumbUrl : imageUrl,
              fit: BoxFit.cover,
            ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 100,
              width: double.infinity,
              //color: AppConfig.appColor,
              child: gradientLine(reverse: true, alpha: .4),
            ),
          ),
          storyProgressIndicator(index),
          if (bm.myItem())
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 70,
                child: Row(
                  children: [
                    addSpaceWidth(10),
                    GestureDetector(
                      onTap: () {
                        clickTagged(context, bm);
                      },
                      child: Container(
                        width: 35.0 * taggedPersons.length.clamp(0, 8),
                        height: 50,
                        child: Stack(
                          children: List.generate(
                              taggedPersons.length.clamp(0, 8), (index) {
                            int max = taggedPersons.length.clamp(0, 8);
                            BaseModel model =
                                BaseModel(items: taggedPersons[index]);
                            return Container(
                              height: 40,
                              width: 40,
                              margin: EdgeInsets.only(left: index * 30.0),
                              padding: EdgeInsets.all(1),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: .5, color: black.withOpacity(.1)),
                                  color: white,
                                  shape: BoxShape.circle),
                              child: Stack(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: Stack(
                                      children: [
                                        Container(
                                          height: 40,
                                          width: 40,
                                          color: appColor.withOpacity(.6),
                                        ),
                                        if (model.userImage.isNotEmpty)
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            child: CachedNetworkImage(
                                              imageUrl: model.userImage,
                                              height: 40,
                                              width: 40,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                  if (max - 1 > 3)
                                    Container(
                                      height: 40,
                                      width: 40,
                                      child: Center(
                                          child: Text(
                                        "${max - index}+",
                                        style: textStyle(true, 10, white),
                                      )),
                                      decoration: BoxDecoration(
                                          color: black.withOpacity(.4),
                                          shape: BoxShape.circle),
                                    )
                                ],
                              ),
                            );
                          }),
                        ),
                      ),
                    ),
                    Spacer(),
                    RaisedButton(
                      onPressed: () {
                        bm.deleteItem();
                        allStories.removeWhere(
                            (e) => e.getObjectId() == bm.getObjectId());
                        stories[index].remove(bm);
                        setState(() {});
                      },
                      shape: CircleBorder(),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      child: Icon(
                        Icons.delete,
                        size: 18,
                        color: Colors.red,
                      ),
                    )
                  ],
                ),
              ),
            )
        ],
      ),
    );
  }

  changePage(bool toLeft) {
    bool canCreateTimer = true;
    progressValue = 0;
    if (!mounted) return;
    if (toLeft) {
      pageItemPosition--;
    } else {
      pageItemPosition++;
    }
    if (pageItemPosition < 0) {
      canCreateTimer = false;
      if (currentPage == 0) {
        stopAllVideos();
        Navigator.pop(context);
      } else {
        pageItemPosition = stories[currentPage - 1].length - 1;
        //indicator[currentPage][pageItemPosition].put(STORY_PROGRESS, 0.0);
        pageController?.previousPage(
            duration: Duration(milliseconds: 500), curve: Curves.ease);
      }
    } else if (pageItemPosition > stories[currentPage].length - 1) {
      canCreateTimer = false;
      if (currentPage == stories.length - 1) {
        stopAllVideos();
        Navigator.pop(context);
      } else {
        pageItemPosition = 0;
        //indicator[currentPage][pageItemPosition].put(STORY_PROGRESS, 1.0);
        pageController?.nextPage(
            duration: Duration(milliseconds: 500), curve: Curves.ease);
      }
    }
    if (mounted && canCreateTimer) createTimer();
    if (mounted) setState(() {});
  }

  bool pagePaused = false;
  pausePage() {
    print("Pausing page at $progressValue");
    pagePaused = true;
    animController?.stop();
    pauseVideoAt(currentPage, pageItemPosition);
  }

  resumePage() {
    print("Resuming page at $progressValue");
    pagePaused = false;
    animController?.forward(from: progressValue);
    playVideoAt(currentPage, pageItemPosition);
  }

  AnimationController animController;

  createTimer(
      {double resumeSecond = 0,
      bool initPlay = false,
      bool payChanged = false}) async {
    if ((null != oldStory && oldStory.isVideo) || payChanged) {
      stopAllVideos();
      await Future.delayed(Duration(seconds: 1));
    }

    if (animController != null) {
      animController.stop(canceled: true);
    }
    int seconds = 6;
    progressValue = 0;
    BaseModel story = stories[currentPage][pageItemPosition];
    BaseModel progress = indicator[currentPage][pageItemPosition];

    if (story.isVideo) {
      VideoController.playVideoWithId(story, (c) {
        //controller = c;
        //seconds = c.value.duration.inSeconds;
        seconds = 25;
        if (mounted) setState(() {});
      });
    }

    if (resumeSecond != 0) {
      seconds = seconds - (resumeSecond * 5).toInt();
      progressValue = resumeSecond;
      progress.put(STORY_PROGRESS, resumeSecond);
    }
    animController =
        AnimationController(duration: Duration(seconds: seconds), vsync: this);
    Animation<double> animation =
        Tween(begin: resumeSecond, end: 1.0).animate(animController);
    animation.addListener(() async {
      bool completed = animation.status == AnimationStatus.completed;
      progressValue = animation.value;
      progress.put(STORY_PROGRESS, animation.value);
      if (completed) {
        animController?.stop();
        oldStory = stories[currentPage][pageItemPosition];
        changePage(false);
      }
      if (mounted) setState(() {});
    });
    animController?.forward();
    if (!story.myItem()) {
      List persons = story.getList(TAGGED_PERSONS);
      int p =
          persons.indexWhere((e) => e[OBJECT_ID] == userModel.getObjectId());
      if (p == -1) {
        persons.add(personMap(userModel.items));
        story
          ..put(TAGGED_PERSONS, persons)
          ..updateItems(delaySeconds: 2);
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    pageController?.dispose();
  }
}
