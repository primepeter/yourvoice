import 'package:acclaim/LangMain.dart';
import 'package:acclaim/app/app.dart';
import 'package:flutter/material.dart';

import 'SelectRegion.dart';

class AddDistrict extends StatefulWidget {
  final bool isPickup;
  final BaseModel model;
  const AddDistrict({Key key, this.isPickup = false, this.model})
      : super(key: key);
  @override
  _AddDistrictState createState() => _AddDistrictState();
}

class _AddDistrictState extends State<AddDistrict> with WidgetsBindingObserver {
  final nameController = TextEditingController();
  String state = '';
  String addressLocation = '';
  List localGovs = [];
  // final cNameController = TextEditingController();
  BaseModel model = BaseModel();
  String objectId = getRandomId();

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    if (widget.model != null) {
      model = widget.model;
      objectId = model.getObjectId();
      nameController.text = model.getString(NAME);
      state = model.getString(STATE);
      localGovs = model.getList(STATE_LOCALS);
      setState(() {});
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  onError(e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0,
        Language.value(LangKey.errorReceived), e.bookmarkList,
        delayInMilli: 900, cancellable: true);
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    FocusScope.of(context).requestFocus(FocusNode());
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 30, right: 10, left: 10),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BackButton(),
                    Text(
                      "Add District",
                      style: textStyle(true, 20, black),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 500),
                width: double.infinity,
                height: errorText.isEmpty ? 0 : 40,
                color: red0,
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Center(
                    child: Text(
                  errorText,
                  style: textStyle(true, 16, white),
                )),
              ),
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "All fields are mandatory",
                        style: textStyle(true, 15, appColor),
                      ),
                      addSpace(10),
                      textField("Name", 'Name Of Senatorial District',
                          nameController),
                      clickField('State', 'Choose State', state, () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        pushAndResult(
                            context,
                            SelectRegion(
                              canSelectOnlyState: true,
                            ),
                            transitionBuilder: fadeTransition, result: (_) {
                          if (null == _ || !(_ is List)) return;
                          String pState = _[0];

                          if (pState != state) localGovs.clear();
                          state = pState;
                          //constituency = _[1];
                          //stateConstituency = '$constituency in $state';
                          if (mounted) setState(() {});
                        });
                      }),
                      clickField(
                          'Local Govs.',
                          'Add Local Govs.Constituency',
                          '',
                          () {
                            if (state.isEmpty) {
                              showError('Choose a state!');
                              return;
                            }

                            FocusScope.of(context).requestFocus(FocusNode());
                            pushAndResult(context, SelectRegion(),
                                transitionBuilder: fadeTransition, result: (_) {
                              if (null == _ || !(_ is List)) return;
                              String lState = _[0];
                              String constituency = _[1];
                              if (lState != state) {
                                showError('Constituency not in State Selected');
                                return;
                              }

                              int p = localGovs
                                  .indexWhere((e) => e == constituency);
                              if (p == -1)
                                localGovs.add(constituency);
                              else
                                localGovs[p] = constituency;
                              if (mounted) setState(() {});
                            });
                          },
                          list: true,
                          items: localGovs,
                          removeAt: (p) {
                            localGovs.removeAt(p);
                            if (mounted) setState(() {});
                          }),
                      addSpace(50),
                    ],
                  ),
                ),
              ),
              addSpace(15),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  onPressed: () async {
                    proceed();
                  },
                  color: appColor,
                  padding: EdgeInsets.all(14),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                      child: Text(
                    'Save',
                    style: textStyle(true, 16, white),
                  )),
                ),
              ),
              addSpace(15),
            ],
          ),
        ],
      ),
    );
  }

  proceed() async {
    String name = nameController.text;
    FocusScope.of(context).requestFocus(FocusNode());

    if (name.isEmpty) {
      showError("Enter Name!");
      return;
    }

    if (state.isEmpty) {
      showError("Choose a State!");
      return;
    }

    if (localGovs.isEmpty) {
      showError("Add Local.Gov Constituency!");
      return;
    }

    model.put(OBJECT_ID, objectId);
    model.put(NAME, name);
    model.put(STATE, state);
    model.put(STATE_LOCALS, localGovs);
    model.saveItem(DISTRICT_BASE, false, document: objectId, merged: true,
        onComplete: () {
      showMessage(context, Icons.check_circle, appColor, "Candidate Created!",
          "Electoral Candidate has been created Successfully.",
          cancellable: false,
          delayInMilli: 800,
          clickYesText: 'Go Back', onClicked: (_) {
        Navigator.pop(context, model);
      });
    });
  }
}

textField(String title, String hint, TextEditingController controller,
    {bool numb = false, int max = 1}) {
  return Container(
    padding: EdgeInsets.only(top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyle(false, 15, appColor),
        ),
        addSpace(10),
        Container(
          decoration: BoxDecoration(
              color: black.withOpacity(.02),
              border: Border.all(color: black.withOpacity(.02)),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: TextField(
            controller: controller,
            maxLines: max,
            keyboardType: numb ? TextInputType.number : null,
            decoration:
                InputDecoration(border: InputBorder.none, hintText: hint),
          ),
        )
      ],
    ),
  );
}

clickField(String title, String hint, String value, onTap,
    {bool list = false, List items = const [], removeAt}) {
  bool isEmpty = value.isEmpty;

  return Container(
    padding: EdgeInsets.only(top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyle(false, 15, appColor),
        ),
        addSpace(10),
        if (items.isNotEmpty)
          ...List.generate(
              items.length,
              (index) => Container(
                    decoration: BoxDecoration(
                        color: black.withOpacity(.02),
                        border: Border.all(color: black.withOpacity(.2)),
                        borderRadius: BorderRadius.circular(5)),
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(bottom: 10),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            items[index],
                            style: textStyle(true, 15, appColor),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            if (null != removeAt) removeAt(index);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: red0,
                              // border: Border.all(color: black.withOpacity(.2)),
                            ),
                            child: Icon(
                              Icons.clear,
                              size: 20,
                              color: white,
                            ),
                          ),
                        )
                      ],
                    ),
                  )),
        if (items.isNotEmpty) addSpace(10),
        InkWell(
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
                color: black.withOpacity(.02),
                border: Border.all(color: black.withOpacity(.02)),
                borderRadius: BorderRadius.circular(5)),
            padding: EdgeInsets.only(left: 10, right: 10),
            alignment: Alignment.centerLeft,
            height: 50,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    isEmpty ? hint : value,
                    style: textStyle(
                        false, 15, black.withOpacity(isEmpty ? 0.5 : 1)),
                  ),
                ),
                Icon(
                  list
                      ? Icons.add_circle_outline
                      : Icons.arrow_drop_down_circle_outlined,
                  size: 20,
                  color: black.withOpacity(.5),
                )
              ],
            ),
          ),
        )
      ],
    ),
  );
}
