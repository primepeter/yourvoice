import 'dart:convert';
import 'dart:io';

import 'package:acclaim/SearchPlace.dart';
import 'package:acclaim/ShowCandidates.dart';
import 'package:acclaim/people.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_photo_picker/photo_picker.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:snapping_sheet/snapping_sheet.dart';
import 'package:video_trimmer/video_trimmer.dart';

import 'AddCandidate.dart';
import 'CameraMain.dart';
import 'app/app.dart';
import 'app_config.dart';
import 'post_categories.dart';
import 'preview_post_images.dart';
import 'video_trimmer.dart';

class AddPost extends StatefulWidget {
  final BaseModel model;

  const AddPost({Key key, this.model}) : super(key: key);

  @override
  _AddPostState createState() => _AddPostState();
}

class _AddPostState extends State<AddPost> with SingleTickerProviderStateMixin {
  List<BaseModel> photos = [];
  List<PhotoPickerAsset> selectedPhotos = [];
  final titleController = new TextEditingController();
  final messageController = new TextEditingController();
  FocusNode postFocusNode = FocusNode();

  List taggedPersons = [];
  List taggedCat = [];

  String taggedPlace = "";
  double taggedPlaceLat = 0.0;
  double taggedPlaceLong = 0.0;

  BaseModel post = BaseModel();
  String objectId = getRandomId();

  String selectedCandidate = '';
  BaseModel candidate;
  List electoralPosition = appSettingsModel.getList(POSITIONS);
  List sitType = appSettingsModel.getList(SIT_TYPE);

  String selectedPostType = 'General';
  List postType = ['General', 'Candidate'];

  var snapController = SnappingSheetController();
  AnimationController _arrowIconAnimationController;
  Animation<double> _arrowIconAnimation;

  List allCategories = appSettingsModel.getList(APP_CATEGORIES);

  loadPostCategories() {
    if (widget.model != null) {
      final catIds = widget.model.getList(TAGGED_CATEGORY);
      for (var item in allCategories) {
        BaseModel model = BaseModel(items: item);
        if (!catIds.contains(model.getObjectId())) continue;
        if (model.getBoolean(IS_DEFAULT)) continue;
        taggedCat.add(item);
      }
    }
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (null != widget.model) {
      objectId = widget.model.getObjectId();
      loadPost();
    }
    loadPostCategories();
    _arrowIconAnimationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _arrowIconAnimation = Tween(begin: 0.0, end: 0.5).animate(CurvedAnimation(
        curve: Curves.elasticOut,
        reverseCurve: Curves.elasticIn,
        parent: _arrowIconAnimationController));
    postFocusNode.addListener(() {
      if (postFocusNode.hasFocus) {
        snapController.snapToPosition(snapController.snapPositions.first);
      }
    });
  }

  loadPost() {
    post = widget.model;
    photos = post.getListModel(IMAGES);
    taggedPersons = post.getList(TAGGED_PERSONS);
    taggedPlace = post.getString(TAGGED_PLACE);
    taggedPlaceLat = post.getDouble(TAGGED_PLACE_LAT);
    taggedPlaceLong = post.getDouble(TAGGED_PLACE_LONG);
    messageController.text = post.getString(MESSAGE);
    candidate = post.getModel(CANDIDATE);
    selectedCandidate = post.getModel(CANDIDATE).getString(NAME);
    selectedPostType = postType[post.getInt(POST_TYPE)];
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  int activeIndex = 0;
  List items = ['Post', 'Story'];

  int clickBack = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int now = DateTime.now().millisecondsSinceEpoch;
        if ((now - clickBack) > 5000) {
          clickBack = now;
          showError('Click back again to exit!');
          return false;
        }
        Navigator.pop(context, "");
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Column(
          children: [
            addSpace(30),
            Padding(
              padding: const EdgeInsets.only(left: 0),
              child: Row(
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        int now = DateTime.now().millisecondsSinceEpoch;
                        if ((now - clickBack) > 5000) {
                          clickBack = now;
                          showError('Click back again to exit!');
                          return;
                        }
                        Navigator.pop(context, "");
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        child: Center(
                            child: Icon(
                          Icons.keyboard_backspace,
                          color: black,
                          size: 25,
                        )),
                      )),
                  Expanded(
                      child: Text(
                    'Share',
                    style: textStyle(true, 20, black),
                  )),
                  FlatButton(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25)),
                      color: appColor,
                      onPressed: handlePost,
                      child: Text(
                        'Post',
                        style: textStyle(true, 14, white),
                      )),
                  addSpaceWidth(10),
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: showSuccess ? dark_green0 : red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            Flexible(
              child: ListView(
                padding: EdgeInsets.only(bottom: 200),
                children: <Widget>[
                  imagesItem(),
                  textField("Title", 'Add a Title', titleController),

                  clickField(
                    'Type',
                    'Choose a Post Type',
                    selectedPostType,
                    () {
                      FocusScope.of(context).requestFocus(FocusNode());

                      showListDialog(
                        context,
                        postType,
                        (_) {
                          print(_);
                          selectedPostType = _[0];

                          setState(() {});
                        },
                        singleSelection: true,
                        selections: [selectedPostType],
                      );
                    },
                  ),

                  //if (senatorSelected)
                  if (selectedPostType != 'General')
                    clickField(
                      'Candidate',
                      'Choose a Candidate',
                      selectedCandidate,
                      () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        pushAndResult(
                            context,
                            ShowCandidates(
                              popResult: true,
                            ),
                            transitionBuilder: fadeTransition, result: (_) {
                          if (null == _) return;
                          candidate = _;
                          selectedCandidate = candidate.getString(NAME);
                          if (mounted) setState(() {});
                        });
                      },
                    ),
                  if (candidate != null && candidate.items.isNotEmpty)
                    Builder(
                      builder: (c) {
                        String name = candidate.getString(NAME);
                        String party = candidate.getString(POLITICAL_PARTY);
                        String state = candidate.getString(STATE);
                        int eP = candidate.getInt(ELECTORAL_POSITION);
                        int sP = candidate.getInt(SIT_TYPE);
                        String position = electoralPosition[eP];
                        String sit = sitType[sP];
                        List states = [];
                        BaseModel district = candidate.getModel(DISTRICT);
                        if (district != null)
                          states = district.getList(STATE_LOCALS);

                        // bool active = objectId == model.getObjectId();

                        return Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: Card(
                            color: default_white,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                // side: BorderSide(
                                //     color: appColor
                                //         .withOpacity(active ? 1 : 0),
                                //     width: active ? 2 : 1),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5))),
                            clipBehavior: Clip.antiAlias,
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        //fit: FlexFit.tight,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Image.asset(
                                                  'assets/images/district.jpg',
                                                  width: 14,
                                                  height: 14,
                                                  color: light_green3,
                                                ),
                                                addSpaceWidth(5),
                                                Flexible(
                                                  child: Text(
                                                    name + ' - ' + position,
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: textStyle(
                                                        true, 12, light_green3),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            addSpace(5),
                                            nameItem('Electoral Party', party,
                                                paddBottom: false),
                                            addSpace(5),
                                            nameItem('Sit Type', sit,
                                                paddBottom: false),
                                            if (eP != 0) ...[
                                              addSpace(5),
                                              nameItem('State', state,
                                                  paddBottom: false),
                                              addSpace(5),
                                              Text(
                                                "Electoral District",
                                                style:
                                                    textStyle(true, 13, black),
                                              ),
                                            ],
                                            addSpace(5),
                                          ],
                                        ),
                                      ),
                                      ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        child: CachedNetworkImage(
                                          imageUrl:
                                              candidate.getString(IMAGE_URL),
                                          width: 60,
                                          height: 60,
                                          fit: BoxFit.cover,
                                        ),
                                      )
                                    ],
                                  ),
                                  addSpace(5),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children:
                                          List.generate(states.length, (index) {
                                        String item = states[index];
                                        return Card(
                                          color: white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Text(
                                                  item,
                                                  style: textStyle(false, 12,
                                                      black.withOpacity(.5)),
                                                ),
                                                addSpace(5),
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                  addSpace(5),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  Container(
                    padding: 10.padAll(),
                    child: TextField(
                      textCapitalization: TextCapitalization.sentences,
                      //autofocus: true,
                      maxLength: 10000,
                      decoration: InputDecoration(
                          hintText: "Write something to share...",
                          counter: Container(),
                          hintStyle: textStyle(
                            false,
                            24,
                            black.withOpacity(.5),
                          ),
                          border: InputBorder.none),
                      style: textStyle(false, 24, black),
                      controller: messageController,
                      focusNode: postFocusNode,
                      cursorColor: black,
                      cursorWidth: 1,
                      maxLines: null,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  imagesLayout() {
    //CollageType.LeftBig 3
    //CollageType.VSplit 2

    int size = photos.length;
    CollageType type1 = size == 1
        ? CollageType.FourLeftBig
        : size == 2
            ? CollageType.VSplit
            : CollageType.LeftBig;
    CollageType type2 = size == 1
        ? CollageType.NineSquare
        : size == 2
            ? CollageType.VSplit
            : CollageType.LeftBig;
    return StaggeredGridView.countBuilder(
        shrinkWrap: true,
        itemCount: size > 3 ? 3 : size,
        padding: EdgeInsets.only(top: 10, bottom: 10),
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: getCrossAxisCount(type1),
        //primary: true,
        itemBuilder: (BuildContext context, int p) {
          BaseModel bm = photos[p];
          String imageUrl = bm.getString(IMAGE_URL);
          String imagePath = bm.getString(IMAGE_PATH);
          String thumbPath = bm.getString(THUMBNAIL_PATH);
          String thumbUrl = bm.getString(THUMBNAIL_URL);
          bool onlineImage = imageUrl.startsWith("http");
          bool isVideo = bm.isVideo;
          String heroTag = "image$p";

          return GestureDetector(
            onTap: () {
              pushAndResult(
                  context,
                  PreviewPostImages(
                    photos,
                    selectedPhotos,
                    edittable: true,
                    indexOf: p,
                    heroTag: heroTag,
                  ), result: (_) {
                photos = _;
                setState(() {});
              });
            },
            child: Stack(
              fit: StackFit.passthrough,
              children: <Widget>[
                if (onlineImage)
                  Container(
                    margin: EdgeInsets.all(1.2),
                    child: CachedNetworkImage(
                      imageUrl: isVideo ? thumbUrl : imageUrl,
                      fit: BoxFit.cover,
                      placeholder: (_, s) {
                        return placeHolder(250, width: double.infinity);
                      },
                    ),
                  )
                else
                  Container(
                    margin: EdgeInsets.all(1.2),
                    child: Image.file(
                      File(isVideo ? thumbPath : imagePath),
                      fit: BoxFit.cover,
                    ),
                  ),
                if (isVideo)
                  Container(
                    height: 250,
                    child: Center(
                      child: Container(
                        height: 30,
                        width: 30,
                        padding: 3.padAll(),
                        margin: 6.padAll(),
                        decoration: BoxDecoration(
                          color: black.withOpacity(.5),
                          border: Border.all(color: white, width: 2),
                          shape: BoxShape.circle,
                        ),
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.play_arrow,
                          color: white,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                if (size > 3 && p == 2)
                  Container(
                    color: AppConfig.appColor.withOpacity(.7),
                    margin: EdgeInsets.all(1.2),
                    alignment: Alignment.center,
                    child: Text(
                      "+${size - 3}",
                      style: textStyle(false, 18, white),
                    ),
                  ),
              ],
            ),
          );
        },
        staggeredTileBuilder: (int index) => StaggeredTile.count(
            getCellCount(index: index, isForCrossAxis: true, type: type2),
            getCellCount(index: index, isForCrossAxis: false, type: type2)));
  }

  handleImagePicking() {
    PhotoPicker.openPicker(
      mediaType: 'image', // image | video | any
      multiple: true,
      //selectedAssets: selectedPhotos,
    ).then((value) {
      if (null == value) return;
      //selectedPhotos.addAll(value);
      photos = value.map((e) {
        BaseModel model = BaseModel();
        model.put(IMAGE_PATH, e.url.replaceAll("file://", ""));
        return model;
      }).toList();
      snapController.snapToPosition(snapController.snapPositions.first);
      setState(() {});
    }).catchError((e) {
      logError(e.code, e.bookmarkList);
    });
  }

  handleCameraPicking() async {
    pushAndResult(context, CameraMain(), result: (List<BaseModel> _) {
      if (_ == null) return;
      photos.addAll(_);
      snapController.snapToPosition(snapController.snapPositions.first);
      setState(() {});
    });
  }

  handleVideoPicking() async {
    PickedFile videoFile =
        await ImagePicker().getVideo(source: ImageSource.gallery);
    if (null == videoFile) return;
    final Trimmer _trimmer = Trimmer();
    await _trimmer.loadVideo(videoFile: File(videoFile.path));
    pushAndResult(context, TrimmerView(_trimmer), result: (_) async {
      if (null == _) return;
      String path = _[0];
      int length = _[1];
      String thumbnailPath = await getVideoThumbnail(path);
      BaseModel model = BaseModel();
      model.put(IS_VIDEO, true);
      model.put(THUMBNAIL_PATH, thumbnailPath);
      model.put(IMAGE_PATH, path);
      model.put(VIDEO_LENGTH, length);
      photos.add(model);
      snapController.snapToPosition(snapController.snapPositions.first);
      setState(() {});
    });
  }

  handleCategoryPicking() {
    snapController.snapToPosition(snapController.snapPositions.first);
    pushAndResult(
        context,
        PostCategories(
          selectedCategories: taggedCat,
        ), result: (_) {
      taggedCat = _;
      setState(() {});
    });
  }

  handleTagLocation() {
    snapController.snapToPosition(snapController.snapPositions.first);
    pushAndResult(context, SearchPlace(), result: (BaseModel _) {
      if (_ == null) return;
      taggedPlace = _.getString(PLACE_NAME);
      taggedPlaceLat = _.getDouble(LATITUDE);
      taggedPlaceLong = _.getDouble(LONGITUDE);
      if (mounted) setState(() {});
      // setState(() {
      //   taggedPlace = _.name;
      //   taggedPlaceLat = _.latLng.latitude;
      //   taggedPlaceLong = _.latLng.longitude;
      // });
    });
  }

  handleTagFriends() {
    snapController.snapToPosition(snapController.snapPositions.first);
    pushAndResult(
        context,
        People(
          selections: taggedPersons,
        ), result: (_) {
      taggedPersons = _;
      setState(() {});
    });
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  handlePost() async {
    String title = titleController.text;
    String text = messageController.text;
    bool emptyText = text.trim().isEmpty;
    bool emptyList = photos.isEmpty;

    if (title.isEmpty) {
      showError('Add a title for Post!.');
      return;
    }

    if (selectedPostType != 'General' && candidate == null ||
        candidate.items.isEmpty) {
      showError('Choose a Candidate!.');
      return;
    }

    if (emptyList) {
      showError('Add at least one Photo or Video.');
      return;
    }

    if (emptyText) {
      showError('Add a write-up.');
      return;
    }

    //print(candidate.getObjectId());
    //return;

    if (candidate != null && candidate.items.isNotEmpty)
      post.put(POST_ID, candidate.getObjectId());

    post
      ..put(OBJECT_ID, objectId)
      ..put(DATABASE_NAME, POST_BASE)
      ..put(EDITED, widget.model != null)
      ..put(IMAGES, photos.map((e) => e.items).toList())
      ..put(TITLE, title)
      ..put(MESSAGE, text)
      ..put(SEARCH_PARAM, getSearchString(text));

    // post.saveItem(POST_BASE, true, document: objectId, merged: true);
    // print(post.items);
    // return;
    postUploadController.add(post);

    Navigator.pop(context);
  }

  sendOneSignal(Map<String, dynamic> data) async {
    http
        .post('https://onesignal.com/api/v1/notifications',
            headers: {
              "Content-Type": "application/json; charset=utf-8",
              "Authorization":
                  "Basic ${appSettingsModel.getString(ONE_SIGNAL_API_KEY)}"
            },
            body: jsonEncode(data))
        .then((value) {
      print(value.body);
    });
  }

  imagesItem() {
    return Container(
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Flexible(
                      child: Text(
                        'This Post would be reviewed before published. Ensure that Items to be shared follow our Community Guidelines.Do not share Personal Information to Strangers.',
                        style: textStyle(false, 14, white),
                      ),
                    ),
                    addSpaceWidth(5),
                    Icon(
                      Icons.info_outline,
                      size: 20,
                      color: white.withOpacity(.5),
                    )
                  ],
                ),
              ],
            ),
            padding: EdgeInsets.all(10),
            //margin: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: red0,
                //color: black.withOpacity(.02),
                border: Border.all(color: black.withOpacity(.06)),
                borderRadius: BorderRadius.circular(8)),
          ),
          addSpace(10),
          Container(
            height: 200,
            // margin: EdgeInsets.all(10),
            child: Stack(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      if (photos.isEmpty)
                        GestureDetector(
                          onTap: () {
                            pickAssets();
                          },
                          child: Container(
                            width: 250,
                            height: 160,
                            decoration: BoxDecoration(
                                color: appColor_light.withOpacity(.05),
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(
                                    width: .5, color: black.withOpacity(.05))),
                            child: Center(
                              child: Image.asset(
                                GetAsset.images('camera_clip.png'),
                                height: 50,
                                width: 50,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      Row(
                        children: List.generate(photos.length, (index) {
                          BaseModel model = photos[index];
                          String imageUrl = model.getString(IMAGE_URL);
                          String imagePath = model.getString(IMAGE_PATH);
                          String thumbPath = model.getString(THUMBNAIL_PATH);
                          String thumbUrl = model.getString(THUMBNAIL_URL);
                          bool online = fromOnline(imageUrl);
                          bool onlineThumb = fromOnline(thumbUrl);
                          bool isVideo = model.isVideo;
                          String heroTag = "image$index";

                          String aUrl = online ? imageUrl : imagePath;
                          String aThumbUrl = onlineThumb ? thumbUrl : thumbPath;

                          return GestureDetector(
                            onTap: () {
                              pushAndResult(
                                  context,
                                  PreviewPostImages(
                                    photos,
                                    [],
                                    edittable: true,
                                    indexOf: index,
                                    heroTag: heroTag,
                                  ),
                                  transitionBuilder: fadeTransition,
                                  result: (_) {
                                photos = _;
                                setState(() {});
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.all(5),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Stack(
                                  children: [
                                    if (online)
                                      Container(
                                        margin: EdgeInsets.all(1.2),
                                        child: CachedNetworkImage(
                                          imageUrl:
                                              isVideo ? thumbUrl : imageUrl,
                                          fit: BoxFit.cover,
                                          width: 250,
                                          height: 160,
                                        ),
                                      )
                                    else
                                      Image.file(
                                        File(isVideo ? thumbPath : imagePath),
                                        fit: BoxFit.cover,
                                        alignment: Alignment.topCenter,
                                        width: 250,
                                        height: 160,
                                      ),
                                    Container(
                                      width: 250,
                                      height: 160,
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              colors: [
                                            /*appColor.withOpacity(.6),
                                                appColor_light.withOpacity(.6),*/
                                            black.withOpacity(.6),
                                            black.withOpacity(.2),
                                          ],
                                              begin: Alignment.bottomCenter,
                                              end: Alignment.topCenter)),
                                    ),
                                    if (isVideo)
                                      Container(
                                        width: 250,
                                        height: 160,
                                        child: Center(
                                          child: Container(
                                            height: 30,
                                            width: 30,
                                            padding: 3.padAll(),
                                            margin: 6.padAll(),
                                            decoration: BoxDecoration(
                                              color: black.withOpacity(.5),
                                              border: Border.all(
                                                  color: white, width: 2),
                                              shape: BoxShape.circle,
                                            ),
                                            alignment: Alignment.center,
                                            child: Icon(
                                              Icons.play_arrow,
                                              color: white,
                                              size: 15,
                                            ),
                                          ),
                                        ),
                                      ),
                                    FlatButton(
                                      onPressed: () {
                                        photos.remove(index);
                                        setState(() {});
                                      },
                                      minWidth: 25,
                                      height: 25,
                                      shape: CircleBorder(),
                                      color: red0,
                                      child: Icon(
                                        Icons.close,
                                        color: white,
                                        size: 20,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: MaterialButton(
                    shape: CircleBorder(),
                    onPressed: () {
                      pickAssets();
                    },
                    color: white,
                    elevation: 6,
                    padding: EdgeInsets.all(10),
                    minWidth: 50,
                    height: 50,
                    child: Icon(
                      Icons.add_a_photo_outlined,
                      color: appColor_light,
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  pickAssets() async {
    PhotoPicker.openPicker(
      //mediaType: 'image', // image | video | any
      multiple: true,
      limit: 6,
    ).then((value) async {
      if (null == value) return;
      List<BaseModel> ph = [];
      for (var e in value) {
        BaseModel model = BaseModel();
        bool isVideo = e.type == "video";

        String path = e.url.replaceAll("file://", "");
        String thumbPath = e.thumbnailUrl.replaceAll("file://", "");

        if (isVideo) {
          final Trimmer _trimmer = Trimmer();
          await _trimmer.loadVideo(videoFile: File(path));
          pushAndResult(context, TrimmerView(_trimmer), result: (_) async {
            if (null == _) return;
            String path = _[0];
            int length = _[1];
            String thumbnailPath = await getVideoThumbnail(path);
            BaseModel model = BaseModel();
            model.put(OBJECT_ID, '${path.hashCode}');
            model.put(IS_VIDEO, true);
            model.put(THUMBNAIL_PATH, thumbnailPath);
            model.put(IMAGE_PATH, path);
            model.put(VIDEO_LENGTH, length);
            model.put(THUMBNAIL_PATH, thumbPath);
            model.put(VIDEO_PATH, path);
            model.put(IS_VIDEO, true);
            int p = photos
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p == -1)
              photos.add(model);
            else
              photos[p] = model;

            setState(() {});
          });
        } else {
          model.put(OBJECT_ID, '${path.hashCode}');
          model.put(IMAGE_PATH, path);
          int p =
              photos.indexWhere((e) => e.getObjectId() == model.getObjectId());
          if (p == -1)
            photos.add(model);
          else
            photos[p] = model;
          setState(() {});
        }
      }
      setState(() {});
    }).catchError((e) {
      logError(e.code, e.bookmarkList);
    });
  }

  groupField(String title, List items, int activeIndex, onTap,
      {bool hasError = false}) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: textStyle(false, 15, appColor),
          ),
          addSpace(10),
          Container(
            decoration: BoxDecoration(
                //color: black.withOpacity(.02),
                border: Border.all(
                    width: hasError ? 3 : 0,
                    color: hasError ? red0 : black.withOpacity(.02))),
            child: Row(
              children: List.generate(items.length, (index) {
                bool active = activeIndex == index;
                return Expanded(
                  child: InkWell(
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      onTap(index);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color:
                              active ? appColor_light : black.withOpacity(.02),
                          border: Border.all(color: black.withOpacity(.02)),
                          borderRadius: BorderRadius.circular(active ? 25 : 5)),
                      padding: EdgeInsets.only(left: 5, right: 5),
                      margin: EdgeInsets.all(5),
                      alignment: Alignment.centerLeft,
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Text(
                              items[index],
                              style: textStyle(active, 14,
                                  active ? white : black.withOpacity(0.6)),
                            ),
                          ),
                          if (active)
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              decoration: BoxDecoration(
                                  color: white,
                                  border:
                                      Border.all(color: black.withOpacity(.02)),
                                  borderRadius: BorderRadius.circular(25)),
                              width: 30,
                              child: Icon(
                                Icons.check,
                                color: appColor_light,
                                size: 15,
                              ),
                            )
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}
