import 'dart:async';

import 'package:acclaim/app/app.dart';
import 'package:acclaim/main_screens/ShowCandidateProfile.dart';
import 'package:acclaim/show_replies.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'CreateReport.dart';
import 'app_index.dart';
import 'like_animation.dart';
import 'main_screens/show_person.dart';

class ShowComments extends StatefulWidget {
  final BaseModel theCandidate;
  final String heroTag;
  final String candidateId;

  const ShowComments(
      {Key key, this.heroTag, this.theCandidate, this.candidateId})
      : super(key: key);
  @override
  _ShowCommentsState createState() => _ShowCommentsState();
}

class _ShowCommentsState extends State<ShowComments> {
  bool hasLoaded = false;
  List<BaseModel> commentList = [];
  BaseModel candidate;

  List electoralPosition = appSettingsModel.getList(POSITIONS);
  List sitType = appSettingsModel.getList(SIT_TYPE);

  final scrollController = ScrollController();
  String exitValue = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    candidate = widget.theCandidate;
    commentList = candidate.getComments;
    if (commentList.isNotEmpty) hasLoaded = true;
    loadComments(false);
    listenToCandidate();
  }

  @override
  dispose() {
    for (var s in commentShot) s.cancel();
    super.dispose();
  }

  listenToCandidate() async {
    var sub = FirebaseFirestore.instance
        .collection(CANDIDATE_BASE)
        .doc(widget.theCandidate.getObjectId())
        .snapshots()
        .listen((query) {
      candidate = BaseModel(doc: query);
      //setup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List<StreamSubscription<QuerySnapshot>> commentShot = [];

  loadComments(bool isNew) async {
    var sub = FirebaseFirestore.instance
        .collection(COMMENT_BASE)
        .where(POST_ID, isEqualTo: widget.theCandidate.getObjectId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          !isNew
              ? (commentList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : commentList[commentList.length - 1].time)
              : (commentList.isEmpty ? 0 : commentList[0].time)
        ])
        .snapshots()
        .listen((value) {
          List local = value.docs;
          for (var doc in value.docs) {
            BaseModel model = BaseModel(doc: doc);
            if (model.hidden.contains(userModel.getUserId())) continue;

            int p = commentList
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p == -1)
              commentList.add(model);
            else
              commentList[p] = model;
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = commentList.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              canRefresh = false;
            } else {
              refreshController.loadComplete();
            }
          }
          Future.delayed(Duration(seconds: 1), () {
            scrollTotallyDown(scrollController);
          });

          hasLoaded = true;
          if (mounted) setState(() {});
        });
    commentShot.add(sub);
  }

  messageItem() {
    return Container(
      margin: EdgeInsets.only(bottom: 20, right: 10, left: 10),
      child: Material(
        elevation: 5,
        color: white,
        shadowColor: black.withOpacity(.5),
        borderRadius: BorderRadius.circular(20),
        child: Container(
          padding: EdgeInsets.all(2),
          decoration: BoxDecoration(
              color: appColor,
              border: Border.all(color: black.withOpacity(.09)),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: <Widget>[
              Flexible(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: TextField(
                    focusNode: focus,
                    controller: messageController,
                    onSubmitted: (s) {
                      if (messageController.text.isEmpty) {
                        FocusScope.of(context).requestFocus(FocusNode());
                        return;
                      }
                      sendComment();
                    },
                    decoration: InputDecoration(
                        contentPadding: 10.padAll(),
                        border: InputBorder.none,
                        fillColor: white,
                        filled: true,
                        hintText: "Send Comment..."),
                  ),
                ),
              ),
              FlatButton(
                child: Text(
                  'Send',
                  style: textStyle(
                    false,
                    15,
                    white.withOpacity(
                        messageController.text.isNotEmpty ? 1 : .6),
                  ),
                ),
                onPressed: () {
                  if (messageController.text.isEmpty) return;
                  sendComment();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  final messageController = TextEditingController();
  final focus = FocusNode();
  BaseModel commentModel = BaseModel();

  sendComment() async {
    List taggedPersons = [/*theUser.items*/];
    //taggedPersons.addAll(widget.thisPost.getList(TAGGED_PERSONS));

    //personMap(items)

    String text = messageController.text;
    String id = getRandomId();
    messageController.clear();
    //BaseModel post = BaseModel();
    commentModel
      ..put(MESSAGE, text)
      ..put(OBJECT_ID, id)
      ..put(POST_ID, candidate.getObjectId())
      ..put(TAGGED_PERSONS, taggedPersons)
      ..saveItem(COMMENT_BASE, true);

    int p = commentList.indexWhere((e) => e.getObjectId() == id);
    if (p == -1)
      commentList.insert(0, commentModel);
    else
      commentList[p] = commentModel;

    setState(() {});

    commentModel = BaseModel();

    List parties = [];
    for (var bm in commentList) {
      if (bm.myItem()) continue;
      parties.add(bm.getUserId());
    }

    BaseModel model = BaseModel();
    model.put(COMMENT_ID, id);
    model.put(CANDIDATE, {
      NAME: candidate.getString(NAME),
      OBJECT_ID: candidate.getObjectId(),
      USER_ID: candidate.getObjectId(),
      IMAGE_URL: candidate.getString(IMAGE_URL),
    });
    model.put(TITLE, userModel.getString(USERNAME) + ": New Comment");
    model.put(TYPE, NOTIFY_COMMENT);
    model.put(MESSAGE, text);
    model.put(PARTIES, parties);

    List playerIds = [];
    for (var bm in commentList) {
      if (bm.getString(ONE_SIGNAL_ID).isEmpty) continue;
      if (bm.myItem()) continue;
      String id = bm.getString(ONE_SIGNAL_ID);
      if (playerIds.contains(id)) continue;
      playerIds.add(id);
    }

    var notification = OSCreateNotification(
        playerIds: playerIds,
        content: text,
        heading: userModel.getString(USERNAME) + ": New Comment",
        additionalData: model.items);

    OneSignal.shared.postNotification(notification).then((value) {
      model.saveItem(NOTIFY_BASE, true);
    });

    candidate
      ..put(COMMENTS, commentList.map((e) => e.userId).toList())
      ..updateItems();

    scrollTotallyDown(scrollController);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, candidate);
        return true;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Stack(
          children: [
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Row(
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            Navigator.pop(context, candidate);
                          },
                          child: Container(
                            width: 50,
                            height: 50,
                            child: Center(
                                child: Icon(
                              Icons.keyboard_backspace,
                              color: black,
                              size: 25,
                            )),
                          )),
                      Expanded(
                          child: Text(
                        "Discussion Thread",
                        style: textStyle(true, 20, black),
                      )),
                      GestureDetector(
                        onTap: () {
                          pushAndResult(
                              context,
                              ShowCandidateProfile(
                                model: candidate,
                                heroTag: 'asjfdfh3487dyu@343',
                              ),
                              replace: true);
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          child: CachedNetworkImage(
                            imageUrl: candidate.getString(IMAGE_URL),
                            width: 35,
                            height: 35,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      addSpaceWidth(10)
                    ],
                  ),
                ),
                Flexible(
                  child: SmartRefresher(
                    controller: refreshController,
                    enablePullDown: true,
                    enablePullUp: commentList.isNotEmpty,
                    header: WaterDropHeader(),
                    footer: ClassicFooter(
                      noDataText: "Nothing more for now, check later...",
                      textStyle: textStyle(false, 12, black.withOpacity(.7)),
                    ),
                    onLoading: () {
                      loadComments(false);
                    },
                    onRefresh: () {
                      loadComments(true);
                    },
                    child: ListView(
                      controller: scrollController,
                      children: <Widget>[
                        /*   Builder(
                          builder: (c) {
                            String name = candidate.getString(NAME);
                            String party = candidate.getString(POLITICAL_PARTY);
                            String state = candidate.getString(STATE);
                            int eP = candidate.getInt(ELECTORAL_POSITION);
                            int sP = candidate.getInt(SIT_TYPE);
                            String position = electoralPosition[eP];
                            String sit = sitType[sP];
                            List states = [];
                            BaseModel district = candidate.getModel(DISTRICT);
                            if (district != null)
                              states = district.getList(STATE_LOCALS);

                            // bool active = objectId == model.getObjectId();

                            return Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Card(
                                color: default_white,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    // side: BorderSide(
                                    //     color: appColor
                                    //         .withOpacity(active ? 1 : 0),
                                    //     width: active ? 2 : 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                clipBehavior: Clip.antiAlias,
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            //fit: FlexFit.tight,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Image.asset(
                                                      'assets/images/district.jpg',
                                                      width: 14,
                                                      height: 14,
                                                      color: light_green3,
                                                    ),
                                                    addSpaceWidth(5),
                                                    Flexible(
                                                      child: Text(
                                                        name + ' - ' + position,
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: textStyle(true,
                                                            12, light_green3),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                addSpace(5),
                                                nameItem(
                                                    'Electoral Party', party,
                                                    paddBottom: false),
                                                addSpace(5),
                                                nameItem('Sit Type', sit,
                                                    paddBottom: false),
                                                if (eP != 0) ...[
                                                  addSpace(5),
                                                  nameItem('State', state,
                                                      paddBottom: false),
                                                  addSpace(5),
                                                  Text(
                                                    "Electoral District",
                                                    style: textStyle(
                                                        true, 13, black),
                                                  ),
                                                ],
                                                addSpace(5),
                                              ],
                                            ),
                                          ),
                                          ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5)),
                                            child: CachedNetworkImage(
                                              imageUrl: candidate
                                                  .getString(IMAGE_URL),
                                              width: 60,
                                              height: 60,
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        ],
                                      ),
                                      addSpace(5),
                                      SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: List.generate(states.length,
                                              (index) {
                                            String item = states[index];
                                            return Card(
                                              color: white,
                                              elevation: 0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      item,
                                                      style: textStyle(
                                                          false,
                                                          12,
                                                          black
                                                              .withOpacity(.5)),
                                                    ),
                                                    addSpace(5),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                                      addSpace(5),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),*/
                        Builder(
                          builder: (ctx) {
                            if (!hasLoaded)
                              return Container(
                                height: 70,
                                alignment: Alignment.center,
                                child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 2,
                                    valueColor:
                                        AlwaysStoppedAnimation(appColor),
                                  ),
                                ),
                              );

                            if (commentList.isEmpty)
                              return Container(
                                height: 100,
                              );

                            return ListView.builder(
                                itemCount: commentList.length,
                                shrinkWrap: true,
                                reverse: true,
                                padding: EdgeInsets.only(top: 5, bottom: 80),
                                physics: NeverScrollableScrollPhysics(),
                                itemBuilder: (ctx, p) {
                                  return commentTiles(p);
                                });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                messageItem()
              ],
            ),
            LikeAnimation()
          ],
        ),
      ),
    );
  }

  commentTiles(int p) {
    BaseModel model = commentList[p];
    String message = model.getString(MESSAGE);
    String username = model.getString(USERNAME);
    int time = model.getTime();

    return GestureDetector(
      onLongPress: () {
        if (!isAdmin || !model.myItem()) return;

        showListDialog(context, ['Edit', 'Delete'], (_) {
          print(_);

          if (_ == 'Edit') {
            messageController.text = message;
            commentModel = model;
            setState(() {});
            return;
          }

          if (_ == "Delete") {
            model.deleteItem();
            commentList
                .removeWhere((e) => e.getObjectId() == model.getObjectId());
            setState(() {});
          }
        });
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 8),
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(35, 0, 40, 15),
              decoration: BoxDecoration(
                  color: black.withOpacity(.05),
                  border: Border.all(width: .5, color: black.withOpacity(.05)),
                  borderRadius: BorderRadius.circular(25)),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          username,
                          maxLines: 1,
                          style: textStyle(true, 12, black),
                        ),
                        addSpaceWidth(5),
                        Text(
                          getTimeAgo(time),
                          style: textStyle(false, 12, black.withOpacity(.6)),
                        ),
                      ],
                    ),
                    addSpace(5),
                    Text(
                      message,
                      style: textStyle(false, 17, black),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 10),
              child: Card(
                shape: CircleBorder(),
                child: CachedNetworkImage(
                  width: 30,
                  height: 30,
                  imageUrl: model.userImage,
                  fit: BoxFit.cover,
                  placeholder: (c, s) {
                    return Container(
                      child: Icon(
                        Icons.person,
                        size: 18,
                        color: black.withOpacity(.2),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CommentItem extends StatelessWidget {
  const CommentItem({
    Key key,
    @required this.p,
    @required this.heroTag,
    @required this.model,
    @required this.stateCallBack,
    @required this.onEditting,
    this.commentEnabled = true,
  }) : super(key: key);

  final int p;
  final bool commentEnabled;
  final String heroTag;
  final BaseModel model;
  final Function onEditting;
  final Function(Map map) stateCallBack;

  @override
  Widget build(BuildContext context) {
    String image = model.userImage;
    String name = model.name;
    String username = model.getString(USERNAME);
    String time = getTimeAgo(model.getTime());
    String message = model.getString(MESSAGE);
    final taggedPersons = model.getList(TAGGED_PERSONS);
    final likes = model.getList(LIKES);
    final comments = model.getList(REPLIES);
    bool liked = likes.contains(userModel.getUserId());
    String heroTag = getRandomId();

    //bool haveLiked = post.getList(LIKED_BY).contains(userModel.userId);
    //String time = getTimeAgo(post.getTime());

    Color iconColor = black.withOpacity(.7);
    double iconSize = 20;

    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      child: Material(
        color: white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
            side: BorderSide(color: black.withOpacity(.03))),
        elevation: 2,
        shadowColor: black.withOpacity(.2),
        child: Container(
          //margin: 5.padAll(),
          padding: 10.padAll(),
          decoration: BoxDecoration(
              //color: white,
              //borderRadius: BorderRadius.circular(5),
              border:
                  Border(bottom: BorderSide(color: black.withOpacity(.03)))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      pushAndResult(
                          context,
                          ShowPerson(
                            model: model,
                          ));
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      padding: EdgeInsets.all(1),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: .5, color: black.withOpacity(.1)),
                          color: white,
                          shape: BoxShape.circle),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(25),
                        child: Stack(
                          children: [
                            Container(
                              height: 40,
                              width: 40,
                              color: appColor.withOpacity(.6),
                              child: Center(
                                  child: Icon(
                                Icons.person,
                                size: 18,
                                color: white,
                              )),
                            ),
                            if (image.isNotEmpty)
                              ClipRRect(
                                borderRadius: BorderRadius.circular(25),
                                child: CachedNetworkImage(
                                  imageUrl: image,
                                  fit: BoxFit.cover,
                                  height: 40,
                                  width: 40,
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  addSpaceWidth(10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          username,
                          style: textStyle(true, 14, black),
                        ),
                        Text(
                          time,
                          style: textStyle(false, 11, black.withOpacity(.5)),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                      onTap: () {
                        showListDialog(context, [
                          if (model.myItem() || isAdmin) "Edit",
                          if (model.myItem() || isAdmin) "Delete",
                          if (!model.myItem()) "Report"
                        ], (_) {
                          if (_ == "Delete") {
                            model.deleteItem();
                            stateCallBack(null);
                          }
                          if (_ == "Edit") {
                            if (null != onEditting) onEditting();
                          }
                          if (_ == "Report") {
                            pushAndResult(
                                context,
                                CreateReport(
                                  item: model,
                                ), result: (_) {
                              if (null != _ || !!_) return;
                              stateCallBack(null);
                            });
                          }
                        });
                      },
                      child: Icon(Icons.more_horiz))
                ],
              ),
              addSpace(10),
              Text(
                message,
                style: textStyle(false, 16, black),
              ),
              addSpace(10),
              Row(
                children: [
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      if (!commentEnabled) return;
                      pushAndResult(
                          context,
                          ShowReplies(
                            thisPost: model,
                            heroTag: heroTag,
                          ), result: (_) {
                        if (null == _) return;
                        stateCallBack(_.postItems);
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: black.withOpacity(.0),
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          addSpaceWidth(4),
                          Text(
                            comments.length > 0
                                ? comments.length.toString()
                                : "",
                            style: textStyle(false, 14, black),
                          ),
                          addSpaceWidth(6),
                          Image.asset(
                            ic_comment,
                            height: iconSize,
                            width: iconSize,
                            fit: BoxFit.cover,
                            color: iconColor,
                          ),
                          addSpaceWidth(4),
                        ],
                      ),
                    ),
                  ),
                  addSpaceWidth(10),
                  GestureDetector(
                    onTap: () {
                      model
                        ..putInList(LIKES, userModel.userId, !liked)
                        ..updateItems();
                      if (!liked) flareController.add(model);
                      stateCallBack(model.items);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: liked ? red0 : black.withOpacity(.05),
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          addSpaceWidth(4),
                          Text(
                            likes.length > 0 ? likes.length.toString() : "",
                            style: textStyle(liked, 14, liked ? white : black),
                          ),
                          addSpaceWidth(6),
                          Image.asset(
                            ic_like,
                            height: iconSize,
                            width: iconSize,
                            fit: BoxFit.cover,
                            color: liked ? white : iconColor,
                          ),
                          addSpaceWidth(4),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
