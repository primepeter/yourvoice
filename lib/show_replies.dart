import 'dart:async';

import 'package:acclaim/LangMain.dart';
import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'ShowComments.dart';
import 'like_animation.dart';

class ShowReplies extends StatefulWidget {
  final BaseModel thisPost;
  final String heroTag;

  const ShowReplies({Key key, this.thisPost, this.heroTag}) : super(key: key);
  @override
  _ShowRepliesState createState() => _ShowRepliesState();
}

class _ShowRepliesState extends State<ShowReplies> {
  bool hasLoaded = false;
  List<BaseModel> commentList = [];
  BaseModel thisPost;
  BaseModel mainPost;
  BaseModel theUser;
  bool mainLoaded = false;
  final scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    thisPost = widget.thisPost;
    commentList = thisPost.getComments;
    if (commentList.isNotEmpty) hasLoaded = true;
    loadComments(false);
    loadUser();
    loadPost();
    print(thisPost.getObjectId());
  }

  @override
  dispose() {
    super.dispose();
    for (var sub in commentShot) sub.cancel();
  }

  loadPost() async {
    Firestore.instance
        .collection(POST_BASE)
        .document(widget.thisPost.getString(POST_ID))
        .get()
        .then((value) {
      mainPost = BaseModel(doc: value);
      print(mainPost.items);
      mainLoaded = true;
      if (mounted) setState(() {});
    });
  }

  loadUser() async {
    Firestore.instance
        .collection(USER_BASE)
        .document(widget.thisPost.getUserId())
        .get()
        .then((value) {
      theUser = BaseModel(doc: value);
      thisPost
        ..put(FULL_NAME, theUser.getString(FULL_NAME))
        ..put(NICKNAME, theUser.getString(NICKNAME))
        ..put(USER_IMAGE, theUser.userImage)
        //..put(USER_ID, model.userImage.items)
        ..updateItems();
      if (mounted) setState(() {});
    });
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List<StreamSubscription<QuerySnapshot>> commentShot = [];

  loadComments(bool isNew) async {
    var sub = Firestore.instance
        .collection(REPLY_BASE)
        .where(COMMENT_ID, isEqualTo: widget.thisPost.getObjectId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          !isNew
              ? (commentList.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : commentList[commentList.length - 1].time)
              : (commentList.isEmpty ? 0 : commentList[0].time)
        ])
        .snapshots()
        .listen(
          (value) {
            List local = value.documents;
            for (var doc in value.documents) {
              BaseModel model = BaseModel(doc: doc);
              if (model.hidden.contains(userModel.getUserId())) continue;
              int p = commentList
                  .indexWhere((e) => e.getObjectId() == model.getObjectId());
              if (p == -1)
                commentList.add(model);
              else
                commentList[p] = model;
            }

            if (isNew) {
              refreshController.refreshCompleted();
            } else {
              int oldLength = commentList.length;
              int newLength = local.length;
              if (newLength <= oldLength) {
                refreshController.loadNoData();
                canRefresh = false;
              } else {
                refreshController.loadComplete();
              }
            }

            hasLoaded = true;
            if (mounted) setState(() {});
          },
        );
    commentShot.add(sub);
  }

  messageItem() {
    return Container(
      margin: EdgeInsets.only(bottom: 20, right: 10, left: 10),
      child: Material(
        elevation: 5,
        color: white,
        shadowColor: black.withOpacity(.5),
        borderRadius: BorderRadius.circular(20),
        child: Container(
          padding: EdgeInsets.all(2),
          decoration: BoxDecoration(
              color: appColor,
              border: Border.all(color: black.withOpacity(.09)),
              borderRadius: BorderRadius.circular(20)),
          child: Row(
            children: <Widget>[
              FlatButton(
                child: Text(
                  LangKey.send.toAppLanguage,
                  style: textStyle(
                    false,
                    15,
                    white.withOpacity(
                        messageController.text.isNotEmpty ? 1 : .6),
                  ),
                ),
                onPressed: () {
                  if (messageController.text.isEmpty) return;
                  sendComment();
                },
              ),
              Flexible(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: TextField(
                    focusNode: focus,
                    controller: messageController,
                    onSubmitted: (s) {
                      if (messageController.text.isEmpty) {
                        FocusScope.of(context).requestFocus(FocusNode());
                        return;
                      }
                      sendComment();
                    },
                    decoration: InputDecoration(
                        contentPadding: 10.padAll(),
                        border: InputBorder.none,
                        fillColor: white,
                        filled: true,
                        hintText: "Reply Comment..."),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final messageController = TextEditingController();
  final focus = FocusNode();
  BaseModel commentModel = BaseModel();

  sendComment() async {
    // List parties = [theUser.items];
    List taggedPersons = [/*theUser.items*/];
    taggedPersons.addAll(mainPost.getList(TAGGED_PERSONS));

    String text = messageController.text;
    String id = getRandomId();
    messageController.clear();
    commentModel
      ..put(MESSAGE, text)
      ..put(OBJECT_ID, id)
      ..put(COMMENT_ID, thisPost.getObjectId())
      ..put(TAGGED_PERSONS, taggedPersons)
      ..saveItem(REPLY_BASE, true);

    int p = commentList
        .indexWhere((e) => e.getObjectId() == commentModel.getObjectId());
    if (p == -1)
      commentList.add(commentModel);
    else
      commentList[p] = commentModel;

/*    BaseModel theModel = widget.thisPost;
    theModel.put(THE_COMMENT, text);
    theModel.put(REPLY_ID, id);

    NotificationService.pushToUsers(
        title: userModel.nickName,
        type: NOTIFY_REPLY,
        message: "Replied your comment",
        parties: [theUser.items],
        theModel: theModel);*/

    BaseModel theModel = widget.thisPost;
    theModel.put(THE_COMMENT, text);
    theModel.put(POST_USER_ID, mainPost.getUserId());
    theModel.put(COMMENT_USER_ID, thisPost.getUserId());
    theModel.put(COMMENT_ID, thisPost.getObjectId());
    theModel.put(REPLY_ID, id);

    NotificationService.pushToUsers(
        title: userModel.nickName,
        type: NOTIFY_COMMENT,
        message: "Replied on comment",
        parties: taggedPersons,
        theModel: theModel);

    thisPost
      ..put(REPLIES, commentList.map((e) => e.userId).toList())
      ..updateItems();

    scrollTotallyDown(scrollController);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, thisPost);
        return true;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Stack(
          children: [
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Row(
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            Navigator.pop(context, thisPost);
                          },
                          child: Container(
                            width: 50,
                            height: 50,
                            child: Center(
                                child: Icon(
                              Icons.keyboard_backspace,
                              color: black,
                              size: 25,
                            )),
                          )),
                      Expanded(
                          child: Text(
                        "Replies (${widget.thisPost.nickName})",
                        style: textStyle(true, 20, black),
                      )),
                    ],
                  ),
                ),
                Flexible(
                  child: SmartRefresher(
                    controller: refreshController,
                    enablePullDown: true,
                    enablePullUp: commentList.isNotEmpty,
                    header: WaterDropHeader(),
                    footer: ClassicFooter(
                      noDataText: "Nothing more for now, check later...",
                      textStyle: textStyle(false, 12, black.withOpacity(.7)),
                    ),
                    onLoading: () {
                      loadComments(false);
                    },
                    onRefresh: () {
                      loadComments(true);
                    },
                    child: ListView(
                      controller: scrollController,
                      children: <Widget>[
                        if (mainLoaded)
                          Builder(
                            builder: (c) {
                              BaseModel model = mainPost;

                              String image = model.userImage;
                              String name = model.name;
                              String username = model.nickName;
                              String time = getTimeAgo(model.getTime());
                              String message = model.getString(MESSAGE);
                              //BaseModel theModel = model.getModel(THE_MODEL);
                              final photos = model.getListModel(IMAGES);
                              bool isVideo = photos.first.isVideo;
                              String first = photos.first.getString(
                                  isVideo ? THUMBNAIL_URL : IMAGE_URL);

                              return GestureDetector(
                                onTap: () {
                                  // pushReplaceAndResult(
                                  //   context,
                                  //   ShowComments(
                                  //     thisPost: model,
                                  //     heroTag: getRandomId(),
                                  //   ),
                                  // );
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, bottom: 5),
                                  child: Material(
                                    color: white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(15),
                                        side: BorderSide(
                                            color: black.withOpacity(.03))),
                                    elevation: 2,
                                    shadowColor: black.withOpacity(.3),
                                    child: Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        children: [
                                          Container(
                                            height: 40,
                                            width: 40,
                                            padding: EdgeInsets.all(1),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: .5,
                                                    color:
                                                        black.withOpacity(.1)),
                                                color: white,
                                                shape: BoxShape.circle),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              child: Stack(
                                                children: [
                                                  Container(
                                                    height: 40,
                                                    width: 40,
                                                    color: appColor
                                                        .withOpacity(.6),
                                                  ),
                                                  if (image.isNotEmpty)
                                                    ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              25),
                                                      child: CachedNetworkImage(
                                                        imageUrl: image,
                                                        height: 40,
                                                        width: 40,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          addSpaceWidth(10),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text(
                                                      username,
                                                      style: textStyle(
                                                          true, 14, black),
                                                    ),
                                                    addSpaceWidth(10),
                                                    Text(
                                                      "($time)",
                                                      style: textStyle(
                                                          false,
                                                          11,
                                                          black
                                                              .withOpacity(.5)),
                                                    ),
                                                  ],
                                                ),
                                                Text(
                                                  message,
                                                  style: textStyle(false, 14,
                                                      black.withOpacity(.5)),
                                                ),
                                              ],
                                            ),
                                          ),
                                          if (first.isNotEmpty)
                                            Container(
                                              height: 70,
                                              width: 70,
                                              child: Stack(
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                    child: CachedNetworkImage(
                                                      imageUrl: first,
                                                      height: 70,
                                                      width: 70,
                                                      fit: BoxFit.cover,
                                                      placeholder: (c, s) {
                                                        return placeHolder(70);
                                                      },
                                                    ),
                                                  ),
                                                  //if (isVideo)
                                                  Container(
                                                    height: 70,
                                                    width: 70,
                                                    child: Center(
                                                      child: Container(
                                                        height: 30,
                                                        width: 30,
                                                        padding: 3.padAll(),
                                                        margin: 6.padAll(),
                                                        decoration:
                                                            BoxDecoration(
                                                          color: black
                                                              .withOpacity(.8),
                                                          border: Border.all(
                                                              color: white,
                                                              width: 1),
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                        alignment:
                                                            Alignment.center,
                                                        child: Icon(
                                                          Icons.play_arrow,
                                                          color: white,
                                                          size: 16,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        Builder(
                          builder: (ctx) {
                            if (!hasLoaded)
                              return Container(
                                height: (screenH(context) - 200),
                                alignment: Alignment.center,
                                child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 2,
                                    valueColor:
                                        AlwaysStoppedAnimation(appColor),
                                  ),
                                ),
                              );

                            if (commentList.isEmpty)
                              return Container(
                                height: (screenH(context) - 200),
                                child: emptyLayout(ic_comment, "No Comment",
                                    "No reply on this comment yet"),
                              );

                            return ListView.builder(
                                itemCount: commentList.length,
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemBuilder: (ctx, p) {
                                  BaseModel model = commentList[p];
                                  return CommentItem(
                                      p: p,
                                      heroTag: getRandomId(),
                                      commentEnabled: false,
                                      model: model,
                                      onEditting: () {
                                        messageController.text = model.message;
                                        commentModel = model;
                                        setState(() {});
                                      },
                                      stateCallBack: (_) {
                                        if (_ == null) {
                                          commentList.remove(model);
                                          setState(() {});
                                          return;
                                        }
                                        commentList[p] = BaseModel(items: _);
                                        setState(() {});
                                      });
                                });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                messageItem()
              ],
            ),
            LikeAnimation()
          ],
        ),
      ),
    );
  }
}
