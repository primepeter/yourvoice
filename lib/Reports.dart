import 'dart:io';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'app/app.dart';

class Reports extends StatefulWidget {
  @override
  _ReportsState createState() => _ReportsState();
}

class _ReportsState extends State<Reports> {
  List items = [];
  bool setup = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadItems();
  }

  int maxTime = 0;
  loadItems() async {
    QuerySnapshot shots = await Firestore.instance
        .collection(REPORT_BASE)
        .where(
          STATUS,
          isEqualTo: PENDING,
        )
        .orderBy(TIME, descending: false)
        .startAt([maxTime])
        .getDocuments()
        .catchError((e) {
          print("Error John $e");
          checkError(context, e);
          setup = true;
          setState(() {});
        });

    if (shots == null) {
      return;
    }

    for (DocumentSnapshot doc in shots.docs) {
      BaseModel model = BaseModel(doc: doc);
      maxTime = max(model.getTime(), maxTime);
      int index = items.indexWhere(
          (element) => element.getObjectId() == model.getObjectId());
      if (index == -1) items.add(model);
    }

    setup = true;
    try {
      refreshController.loadComplete();
    } catch (e) {}
    ;
    if (mounted) setState(() {});
  }

  RefreshController refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(25),
        new Container(
          width: double.infinity,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: Text(
                  "Reports",
                  style: textStyle(true, 20, black),
                ),
              ),
              addSpaceWidth(15),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: messageText.isEmpty ? 0 : 40,
          color: light_green3,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            messageText,
            style: textStyle(true, 14, white),
          )),
        ),
        Expanded(
            child: !setup
                ? loadingLayout()
                : items.isEmpty
                    ? emptyLayout(Icons.report, "No Reports Yet", "",
                        clickText: "Reload", click: () {
                        setup = false;
                        setState(() {});
                        loadItems();
                      })
                    : SmartRefresher(
                        controller: refreshController,
                        enablePullDown: false,
                        enablePullUp: true,
                        header: Platform.isIOS
                            ? WaterDropHeader()
                            : WaterDropMaterialHeader(
                                color: white,
                                backgroundColor: dark_green0,
                              ),
                        footer: ClassicFooter(
                          idleText: "",
                          idleIcon:
                              Icon(Icons.arrow_drop_down, color: transparent),
                        ),
                        onLoading: () {
                          loadItems();
                        },
                        child: ListView.builder(
                          itemBuilder: (c, p) {
                            BaseModel model = items[p];
                            bool isPost = model.getString(ITEM_DB) == POST_BASE;
                            List reports = model.getList(REPORTS);
                            reports.sort((a, b) => b[TIME].compareTo(a[TIME]));
                            return Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Card(
                                color: default_white,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                clipBehavior: Clip.antiAlias,
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Flexible(
                                            fit: FlexFit.tight,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    //if(isPost)Image.asset(ic_farm,width: 14,height: 14,color: light_green3,),
                                                    //if(!isPost)Icon(Icons.category,color: light_green3,size: 14,),
                                                    addSpaceWidth(5),
                                                    Text(
                                                      isPost
                                                          ? "Farm Report"
                                                          : "Produce Report",
                                                      style: textStyle(true, 12,
                                                          light_green3),
                                                    ),
                                                  ],
                                                ),
                                                addSpace(5),
                                                nameItem("Item",
                                                    model.getString(ITEM_NAME),
                                                    paddBottom: false),
                                                addSpace(5),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 35,
                                            child: FlatButton(
                                              onPressed: () {
                                                model.put(STATUS, APPROVED);
                                                model.updateItems(
                                                    db: REPORT_BASE,
                                                    objectId: model
                                                        .getString(ITEM_ID));
                                                items.removeAt(p);
                                                setState(() {});
                                              },
                                              padding: EdgeInsets.fromLTRB(
                                                  10, 5, 10, 5),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "Seen",
                                                    style: textStyle(
                                                        true, 14, white),
                                                  ),
                                                  addSpaceWidth(5),
                                                  Icon(
                                                    Icons.check,
                                                    color: white,
                                                    size: 16,
                                                  )
                                                ],
                                              ),
                                              color: light_green3,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(25))),
                                            ),
                                          )
                                        ],
                                      ),
                                      Text(
                                        "${reports.length} Report${reports.length > 1 ? "s" : ""}",
                                        style: textStyle(
                                            false, 12, black.withOpacity(.5)),
                                      ),
                                      addSpace(5),
                                      SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: List.generate(
                                              reports.length, (index) {
                                            Map item = reports[index];
                                            String message =
                                                item[MESSAGE] ?? "";
                                            return Card(
                                              color: white,
                                              elevation: 0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      "Reported by: ${item[NAME]}",
                                                      style: textStyle(
                                                          false,
                                                          12,
                                                          black
                                                              .withOpacity(.5)),
                                                    ),
                                                    addSpace(5),
                                                    Text(
                                                      item[TITLE] ?? "",
                                                      style: textStyle(
                                                          true, 14, black),
                                                    ),
                                                    if (message.isNotEmpty)
                                                      addSpace(5),
                                                    if (message.isNotEmpty)
                                                      ReadMoreText(message),
                                                    addSpace(5),
                                                    Text(
                                                      getChatDate(item[TIME]),
                                                      style: textStyle(
                                                          false, 12, blue0),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                                      addSpace(5),
                                      getDefaultLine(),
                                      Container(
                                        height: 35,
                                        width: double.infinity,
                                        child: FlatButton(
                                          onPressed: () {
                                            // String itemId = model.getString(ITEM_ID);
                                            // print(itemId);
                                            // pushAndResult(context, isPost?FarmMain(id:itemId,):
                                            //                       ProductMain(id: itemId,),
                                            // );
                                          },
                                          padding:
                                              EdgeInsets.fromLTRB(10, 5, 10, 5),
                                          child: Row(
//                              mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Flexible(
                                                  fit: FlexFit.tight,
                                                  child: Text(
                                                    "View Item",
                                                    style: textStyle(
                                                        true, 14, light_green3),
                                                  )),
                                              addSpaceWidth(5),
                                              Icon(
                                                Icons.navigate_next,
                                                color: light_green3,
                                                size: 22,
                                              )
                                            ],
                                          ),
//                            color: light_green3,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                          shrinkWrap: true,
                          itemCount: items.length,
                        ),
                      ))
      ],
    );
  }

  String messageText = "";
  popMessage(String text) {
    messageText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      messageText = "";
      setState(() {});
    });
  }
}
