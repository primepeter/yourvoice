import 'package:acclaim/app_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'LangMain.dart';
import 'app/app.dart';

class EditTranslation extends StatefulWidget {
  final BaseModel model;

  const EditTranslation({Key key, this.model}) : super(key: key);
  @override
  _EditTranslationState createState() => _EditTranslationState();
}

class _EditTranslationState extends State<EditTranslation> {
  BaseModel model = BaseModel();
  final translationController = TextEditingController();
  final subController = TextEditingController();
  int currentIndex = 0;
  List<BaseModel> options = [];
  List<TextEditingController> languages = [];

  String selectedLanguage = '';
  String selectedKey;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.model != null) {
      model = widget.model;
      translationController.text = model.getString(TITLE);
    }
    translationController.addListener(() {
      final map = model.getMap(TRANSLATIONS);
      String value = translationController.text;
      map[selectedKey] = value;
      model.put(TRANSLATIONS, map);
      setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    translationController.dispose();
  }

  int clickBack = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        int now = DateTime.now().millisecondsSinceEpoch;
        if ((now - clickBack) > 5000) {
          clickBack = now;
          showError("Click back again to exit");
          return false;
        }
        Navigator.pop(context, model.items);
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding:
                  EdgeInsets.only(top: 30, right: 10, left: 10, bottom: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BackButton(),
                  Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Text(
                      "Edit Translation",
                      style: textStyle(true, 25, black),
                    ),
                  )
                ],
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              height: errorText.isEmpty ? 0 : 40,
              color: showSuccess ? dark_green0 : red0,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                  child: Text(
                errorText,
                style: textStyle(true, 16, white),
              )),
            ),
            page(),
            Container(
              padding: EdgeInsets.all(20),
              child: FlatButton(
                onPressed: () {
                  if (selectedLanguage.isEmpty) {
                    showError("Choose translation language");
                    return;
                  }
                  model.updateItems();
                  Language.addToRawTranslations(model);
                  appSettingsModel
                    ..put(LANGUAGE_VERSION,
                        DateTime.now().millisecondsSinceEpoch.toString())
                    ..updateItems();
                  showError("Updated!!!", success: true);
                },
                color: AppConfig.appColor,
                padding: EdgeInsets.all(15),
                child: Center(
                    child: Text(
                  "UPDATE",
                  style: textStyle(false, 18, white),
                )),
              ),
            )
          ],
        ),
      ),
    );
  }

  page() {
    return Flexible(
      child: Container(
        child: ListView(
          padding: EdgeInsets.all(10),
          children: [
            clickText("Choose Language", selectedLanguage, () {
              final translations = model.getMap(TRANSLATIONS);
              final languages = appSettingsModel.getList(APP_LANGUAGE);

              showListDialog(context,
                  languages.map((e) => e.toString().toUpperCase()).toList(),
                   (_) {
                print(
                    "dff $_ $languages @key ${languages[_]} map $translations");

                selectedLanguage = languages[_];
                selectedKey = languages[_];
                translationController.text = translations[selectedKey] ?? "";
                setState(() {});
              });
            }),
            if (selectedLanguage.isNotEmpty)
              inputTextView("Translation", translationController, isNum: false),
            if (selectedLanguage.isNotEmpty) addSpace(10),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                  border: Border.all(color: black.withOpacity(.1), width: .5),
                  color: blue09),
              margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: List.generate(model.getMap(TRANSLATIONS).keys.length,
                    (index) {
                  String key = model.getMap(TRANSLATIONS).keys.toList()[index];
                  String value = model.getMap(TRANSLATIONS)[key];
                  return nameItem(
                      key, value.isEmpty ? "No Translation Yet" : value);
                }),
              ),
            ),
            addSpace(10),
            if (selectedLanguage.isNotEmpty)
              Row(
                mainAxisSize: MainAxisSize.min,
                children: List.generate(
                  2,
                  (p) {
                    var icon = Icons.g_translate;
                    String title = "Google Translate";
                    Color color = Color(0XFF3cba54);

                    if (p == 0) {
                      icon = Icons.copy;
                      title = "Copy to Clipboard";
                      color = Color(0XFF555555);
                    }

                    return Expanded(
                      child: Container(
                          margin: EdgeInsets.all(5),
                          height: 40,
                          //width: 120,
                          child: FlatButton(
                            onPressed: () async {
                              final map = model.getMap(TRANSLATIONS);
                              String value = map[selectedKey] ?? "";

                              print(value);

                              if (p == 0) {
                                Clipboard.setData(ClipboardData(text: value));
                                showError("Copied to Clipboard!",
                                    success: true);
                              }
                            },
                            color: color,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              // side: BorderSide(color: blue3,width: 2)
                            ),
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Icon(
                                  icon,
                                  size: 14,
                                  color: white,
                                ),
                                addSpaceWidth(5),
                                Flexible(
                                  child: new Text(
                                    title,
                                    style: textStyle(true, 12, white),
                                    maxLines: 1,
                                  ),
                                ),
                              ],
                            ),
                          )),
                    );
                  },
                ),
              ),
            addSpace(10),
          ],
        ),
      ),
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  // handleSave() {
  //   String name = categoryName.text;
  //   String subText = subController.text;
  //   List subs = convertStringToList(",", subText);
  //
  //   if (name.isEmpty) {
  //     showError("Enter Category Name");
  //     return;
  //   }
  //
  //   if (imagesUrl.isEmpty) {
  //     showError("Enter Category Image");
  //     return;
  //   }
  //
  //   String categoryId = getRandomId();
  //   if (widget.model != null) categoryId = widget.model.getObjectId();
  //
  //   Map map = Map();
  //   map[NAME] = name;
  //   subs.sort((a, b) => a.compareTo(b));
  //   List subList = List.generate(subs.length, (index) {
  //     return {
  //       NAME: subs[index],
  //       CATEGORY: name,
  //       CATEGORY_ID: categoryId,
  //       OPTIONS: options.map((e) => e.items).toList()
  //     };
  //   });
  //
  //   showProgress(true, context,
  //       msg: "${widget.model != null ? "Saving" : "Adding"} Category...");
  //   List<BaseModel> modelsUploaded = [];
  //   List<BaseModel> images = this.imagesUrl;
  //   List categoryItems = appSettingsModel.getList(CATEGORIES);
  //
  //   saveCategoryImages(images, modelsUploaded, (_) {
  //     model.put(OBJECT_ID, categoryId);
  //     model.put(TITLE, name);
  //     model.put(SUB_CATEGORY, subList);
  //     model.put(IMAGES, _.map((e) => e.items).toList());
  //     if (categoryItems.isEmpty) {
  //       categoryItems.add(model.items);
  //     } else {
  //       int p = categoryItems.indexWhere((e) => e[OBJECT_ID] == categoryId);
  //       if (p != -1) {
  //         categoryItems[p] = model.items;
  //       } else {
  //         categoryItems.add(model.items);
  //       }
  //     }
  //     categoryItems.sort((m1, m2) => m1[TITLE].compareTo(m2[TITLE]));
  //     appSettingsModel
  //       ..put(CATEGORIES, categoryItems)
  //       ..updateItems();
  //     showProgress(false, context);
  //     // if (widget.model == null) {
  //     //   categoryName.clear();
  //     //   subController.clear();
  //     //   imagesUrl.clear();
  //     //   model = BaseModel();
  //     // }
  //     categoryName.clear();
  //     subController.clear();
  //     imagesUrl.clear();
  //     model = BaseModel();
  //     if (mounted) setState(() {});
  //     showMessage(context, Icons.check, green_dark, "Successful",
  //         'Category Successfully ${widget.model != null ? "Updated" : "Added"}!',
  //         cancellable: true, onClicked: (_) {}, delayInMilli: 1200);
  //   });
  // }

}
