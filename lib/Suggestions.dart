import 'dart:io';
import 'dart:math';

import 'package:acclaim/app_config.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'app/app.dart';

class Suggestions extends StatefulWidget {
  @override
  _SuggestionsState createState() => _SuggestionsState();
}

class _SuggestionsState extends State<Suggestions> {
  List items = [];
  bool setup = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadItems();
  }

  int maxTime = 0;
  loadItems() async {
    QuerySnapshot shots = await Firestore.instance
        .collection(SUGGESTION_BASE)
        .where(
          STATUS,
          isEqualTo: PENDING,
        )
        .orderBy(TIME, descending: false)
        .startAt([maxTime])
        .getDocuments()
        .catchError((e) {
          print("Error John $e");
          checkError(context, e);
          setup = true;
          setState(() {});
        });

    if (shots == null) return;

    for (DocumentSnapshot doc in shots.documents) {
      BaseModel model = BaseModel(doc: doc);
      maxTime = max(model.getTime(), maxTime);
      int index = items.indexWhere(
          (element) => element.getObjectId() == model.getObjectId());
      if (index == -1) items.add(model);
    }

    setup = true;
    try {
      refreshController.loadComplete();
    } catch (e) {}
    ;
    if (mounted) setState(() {});
  }

  RefreshController refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addSpace(25),
        new Container(
          width: double.infinity,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: Text(
                  "Suggestions",
                  style: textStyle(true, 20, black),
                ),
              ),
              addSpaceWidth(15),
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: messageText.isEmpty ? 0 : 40,
          color: light_green3,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            messageText,
            style: textStyle(true, 14, white),
          )),
        ),
        Expanded(
            child: !setup
                ? loadingLayout()
                : items.isEmpty
                    ? emptyLayout(Icons.report, "No Suggestions Yet", "",
                        clickText: "Reload", click: () {
                        setup = false;
                        setState(() {});
                        loadItems();
                      })
                    : SmartRefresher(
                        controller: refreshController,
                        enablePullDown: false,
                        enablePullUp: true,
                        header: Platform.isIOS
                            ? WaterDropHeader()
                            : WaterDropMaterialHeader(
                                color: white,
                                backgroundColor: dark_green0,
                              ),
                        footer: ClassicFooter(
                          idleText: "",
                          idleIcon:
                              Icon(Icons.arrow_drop_down, color: transparent),
                        ),
                        onLoading: () {
                          loadItems();
                        },
                        child: ListView.builder(
                          itemBuilder: (c, p) {
                            BaseModel model = items[p];
                            final photos = model.getListModel(IMAGES);
                            final message = model.getString(MESSAGE);
                            return Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Card(
                                color: default_white,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                clipBehavior: Clip.antiAlias,
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Flexible(
                                            fit: FlexFit.tight,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    //if(isPost)Image.asset(ic_farm,width: 14,height: 14,color: light_green3,),
                                                    //if(!isPost)Icon(Icons.category,color: light_green3,size: 14,),
                                                    addSpaceWidth(5),
                                                    Text(
                                                      "New Suggestion",
                                                      style: textStyle(true, 12,
                                                          AppConfig.appColor),
                                                    ),
                                                  ],
                                                ),
                                                addSpace(5),
                                                nameItem("Sender",
                                                    model.getString(NAME),
                                                    paddBottom: false),
                                                addSpace(5),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            height: 35,
                                            child: FlatButton(
                                              onPressed: () {
                                                model.put(STATUS, APPROVED);
                                                model.updateItems();
                                                items.removeAt(p);
                                                setState(() {});
                                              },
                                              padding: EdgeInsets.fromLTRB(
                                                  10, 5, 10, 5),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "Seen",
                                                    style: textStyle(
                                                        true, 14, white),
                                                  ),
                                                  addSpaceWidth(5),
                                                  Icon(
                                                    Icons.check,
                                                    color: white,
                                                    size: 16,
                                                  )
                                                ],
                                              ),
                                              color: light_green5,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(25))),
                                            ),
                                          )
                                        ],
                                      ),
                                      addSpace(5),
                                      Text(
                                        message,
                                        style: textStyle(false, 16, black),
                                      ),
                                      addSpace(5),
                                      getDefaultLine(),
                                      addSpace(10),
                                      SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: List.generate(photos.length,
                                              (index) {
                                            BaseModel model = photos[index];
                                            String imageUrl =
                                                model.getString(IMAGE_URL);
                                            return GestureDetector(
                                              onTap: () {},
                                              child: Container(
                                                padding: EdgeInsets.all(1),
                                                margin: EdgeInsets.all(5),
                                                height: 160,
                                                width: 160,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    color:
                                                        black.withOpacity(.1)),
                                                child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: CachedNetworkImage(
                                                      imageUrl: imageUrl,
                                                      height: 160,
                                                      width: 160,
                                                      fit: BoxFit.cover,
                                                    )),
                                              ),
                                            );
                                          }),
                                        ),
                                      ),
                                      addSpace(5),
                                      getDefaultLine(),
                                      Container(
                                        height: 35,
                                        width: double.infinity,
                                        child: FlatButton(
                                          onPressed: () {
                                            clickChat(context, model);
                                          },
                                          padding:
                                              EdgeInsets.fromLTRB(10, 5, 10, 5),
                                          child: Row(
                                            children: [
                                              Flexible(
                                                  fit: FlexFit.tight,
                                                  child: Text(
                                                    "Send Message",
                                                    style: textStyle(
                                                        true, 14, light_green3),
                                                  )),
                                              addSpaceWidth(5),
                                              Icon(
                                                Icons.insert_comment_outlined,
                                                color: light_green3,
                                                size: 22,
                                              )
                                            ],
                                          ),
//                            color: light_green3,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                          shrinkWrap: true,
                          itemCount: items.length,
                        ),
                      ))
      ],
    );
  }

  String messageText = "";
  popMessage(String text) {
    messageText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      messageText = "";
      setState(() {});
    });
  }
}
