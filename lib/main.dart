import 'dart:async';
import 'dart:io';

import 'package:acclaim/AppLogoStyle.dart';
import 'package:acclaim/Logo.dart';
import 'package:acclaim/auth/login.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_better_camera/camera.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:twilio_flutter/twilio_flutter.dart';
import 'package:twilio_phone_verify/twilio_phone_verify.dart';

import 'app/app.dart';
import 'app_index.dart';

/// Requires that a Firestore emulator is running locally.
/// See https://firebase.flutter.dev/docs/firestore/usage#emulator-usage
bool USE_FIRESTORE_EMULATOR = false;

TwilioPhoneVerify twilioPhoneVerify;
TwilioFlutter twilioFlutter;

void main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    if (USE_FIRESTORE_EMULATOR) {
      FirebaseFirestore.instance.settings = Settings(
          host: 'localhost:8080', sslEnabled: false, persistenceEnabled: false);
    }
    cameras = await availableCameras();
  } on CameraException catch (e) {
    logError(e.code, e.description);
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "OurVoice",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            fontFamily: 'Nunito',
            primarySwatch: Colors.green,
            primaryColor: appColor,
            indicatorColor: appColor),
        home: AppSetter());
  }
}

class AppSetter extends StatefulWidget {
  @override
  _AppSetterState createState() => _AppSetterState();
}

class _AppSetterState extends State<AppSetter>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;

  Timer _timer;

  AppLogoStyle _logoStyle = AppLogoStyle.markOnly;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _timer = new Timer(const Duration(seconds: 2), () {
      setState(() {
        _logoStyle = AppLogoStyle.horizontal;
      });
    });
    loadPermissions();
    checkUser(source: Source.serverAndCache);
    loadSettings();
    setUpAnimation();
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  setUpAnimation() {
    _animationController = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 800));
    _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.easeInCirc));
    _animationController.forward();
  }

  checkUser({Source source = Source.server}) async {
    // FirebaseAuth.instance.signOut();
    // return;
    User user = FirebaseAuth.instance.currentUser;
    bool noUser = user == null;

    Future.delayed(Duration(seconds: 2), () async {
      if (noUser) {
        pushAndResult(context, Login(), clear: true);
      } else {
        FirebaseFirestore.instance
            .collection(USER_BASE)
            .doc(user.uid)
            .get(GetOptions(source: source))
            .then((doc) {
          // pushAndResult(context, Login(), clear: true);
          // return;

          if (doc.exists) {
            userModel = BaseModel(doc: doc);

            pushAndResult(context, AppIndex(), clear: true);
          } else {
            pushAndResult(context, Login(), clear: true);
          }
        }).catchError((e) {
          if (e.toString().contains("exits")) {
            pushAndResult(context, Login(), clear: true);
          }
        });
      }
    });
  }

  loadPermissions() async {
    if (Platform.isIOS) return;
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
      Permission.camera,
      Permission.photos,
      Permission.mediaLibrary,
    ].request();
    print(statuses);

    return statuses;
  }

  loadSettings() {
    print("fe.....");
    Firestore.instance
        .collection(APP_SETTINGS_BASE)
        .document(APP_SETTINGS)
        .get(/*source: Source.cache*/)
        .then((doc) {
      if (!doc.exists) {
        appSettingsModel = new BaseModel();
        appSettingsModel.saveItem(APP_SETTINGS_BASE, false,
            document: APP_SETTINGS);
        return;
      }

      appSettingsModel = BaseModel(doc: doc);

      twilioFlutter = TwilioFlutter(
          accountSid: appSettingsModel.getString(TWILIO_ACCOUNT_SID),
          authToken: appSettingsModel.getString(TWILIO_AUTH_TOKEN),
          twilioNumber: appSettingsModel.getString(TWILIO_NUMBER) //
          );

      twilioPhoneVerify = new TwilioPhoneVerify(
          accountSid: appSettingsModel
              .getString(TWILIO_ACCOUNT_SID), // replace with Account SID
          authToken: appSettingsModel
              .getString(TWILIO_AUTH_TOKEN), // replace with Auth Token
          serviceSid: appSettingsModel
              .getString(TWILIO_SERVICE_SID) // replace with Service SID
          );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: loadingLayout(load: appColor),
    );

    return Logo(
        appName: 'OurVoice', assetPath: 'assets/images/ic_launcher.png');
  }
}
