import 'dart:async';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';

import 'app/app.dart';
import 'app_index.dart';

class LikeAnimation extends StatefulWidget {
  @override
  _LikeAnimationState createState() => _LikeAnimationState();
}

class _LikeAnimationState extends State<LikeAnimation> {
  List<StreamSubscription> subs = List();

  bool flareShowing = false;
  final FlareControls flareControls = FlareControls();

  @override
  void dispose() {
    super.dispose();
    for (var sub in subs) sub.cancel();
  }

  @override
  initState() {
    super.initState();

    var flareSub = flareController.stream.listen((model) {
      int type = 2;
      String message = "Liked your post";
      if (model.getString(DATABASE_NAME) == COMMENT_BASE) {
        type = NOTIFY_COMMENT_LIKE;
        message = "Liked your comment";
      }
      if (model.getString(DATABASE_NAME) == POST_BASE) {
        type = NOTIFY_LIKES;
        message = "Liked your post";
      }
      if (model.getString(DATABASE_NAME) == REPLY_BASE) {
        type = NOTIFY_REPLY_LIKE;
        message = "Liked your reply";
      }

      NotificationService.pushToUsers(
          title: userModel.nickName,
          type: type,
          message: message,
          //parties: [model.items],
          parties: model.getList(TAGGED_PERSONS),
          theModel: model);

      flareControls.play("like");
      setState(() {
        flareShowing = true;
      });
      Future.delayed(Duration(milliseconds: 500), () {
        flareShowing = false;
        if (mounted) setState(() {});
      });
    });
    subs.add(flareSub);
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Stack(
        fit: StackFit.expand,
        children: [
          if (flareShowing)
            Container(
              // curve: Curves.easeInOut,
              // duration: Duration(milliseconds: 500),
              color: black.withOpacity(.2),
            ),
          Align(
            alignment: Alignment.center,
            child: IgnorePointer(
              child: Container(
                width: double.infinity,
                height: 250,
                child: Center(
                  child: SizedBox(
                    width: 80,
                    height: 80,
                    child: FlareActor(
                      'assets/images/instagram_like.flr',
                      controller: flareControls,
                      animation: 'idle',
                      color: Colors.red,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
