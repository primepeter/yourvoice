import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:onesignal_flutter/onesignal_flutter.dart';

import 'app/app.dart';

//import 'package:ABM/photo_picker/photo.dart';

class GeneralMessage extends StatefulWidget {
  @override
  _GeneralMessageState createState() => _GeneralMessageState();
}

class _GeneralMessageState extends State<GeneralMessage> {
  final String progressId = getRandomId();

  TextEditingController titleController = new TextEditingController();
  TextEditingController messageController = new TextEditingController();

  bool mustUpdate = false;
  int clickBack = 0;
  BaseModel model = BaseModel();
  @override
  void initState() {
    // TODO: implement initState
    //codeController.text = appSettingsModel.getInt(VERSION_CODE).toString();
    //messageController.text = appSettingsModel.getString(NEW_FEATURE);
    //messageController.text = messageController.text.replaceAll("\n*", ",");
    //mustUpdate = appSettingsModel.getBoolean(MUST_UPDATE);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  BuildContext context;
  @override
  Widget build(BuildContext c) {
    context = c;
    return WillPopScope(
        onWillPop: () async {
          int now = DateTime.now().millisecondsSinceEpoch;
          if ((now - clickBack) > 5000) {
            clickBack = now;
            showError("Click back again to exit");
            return false;
          }
          return true;
        },
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: white,
            body: page()));
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  Builder page() {
    return Builder(builder: (context) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          addSpace(30),
          new Container(
            width: double.infinity,
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 1,
                  child: new Text(
                    "Push Message",
                    style: textStyle(true, 17, black),
                  ),
                ),
                addSpaceWidth(10),
                FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: appColor,
                    onPressed: () {
                      post();
                    },
                    child: Text(
                      "Send",
                      style: textStyle(true, 14, white),
                    )),
                addSpaceWidth(15)
              ],
            ),
          ),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            width: double.infinity,
            height: errorText.isEmpty ? 0 : 40,
            color: showSuccess ? dark_green0 : red0,
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Center(
                child: Text(
              errorText,
              style: textStyle(true, 16, white),
            )),
          ),
          Expanded(
            flex: 1,
            child: Scrollbar(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(0),
                child: new Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Title",
                              style: textStyle(true, 14, appColor),
                            ),
                            addSpace(10),
                            Container(
                              //height: 45,
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                              decoration: BoxDecoration(
                                  color: blue09,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: black.withOpacity(.1), width: .5)),
                              child: new TextField(
                                onSubmitted: (_) {
                                  //post();
                                },
                                textInputAction: TextInputAction.done,

                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    hintText: "Add Title",
                                    hintStyle: textStyle(
                                        false, 16, black.withOpacity(.2))),
                                style: textStyle(
                                  false,
                                  16,
                                  black,
                                ),
                                controller: titleController,
                                cursorColor: black,
                                cursorWidth: 1,
//                          maxLength: 50,
                                maxLines: 1,
                                keyboardType: TextInputType.text,
                                scrollPadding: EdgeInsets.all(0),
                              ),
                            ),
                            Text(
                              "Write message to push",
                              style: textStyle(true, 14, appColor),
                            ),
                            addSpace(10),
                            Container(
                              height: 120,
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                              decoration: BoxDecoration(
                                  color: blue09,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: black.withOpacity(.1), width: .5)),
                              width: double.infinity,
                              child: new TextField(
                                onSubmitted: (_) {
                                  //post();
                                },

                                // textInputAction: TextInputAction.newline,
                                textCapitalization:
                                    TextCapitalization.sentences,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    hintText: "",
                                    hintStyle: textStyle(
                                        false, 16, black.withOpacity(.2))),
                                style: textStyle(false, 16, black),
                                controller: messageController,
                                cursorColor: black,
                                cursorWidth: 1,
                                maxLines: null,
                                //maxLength: 50,
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            /*GestureDetector(
                              onTap: () {
                                setState(() {
                                  mustUpdate = !mustUpdate;
                                });
                              },
                              child: Container(
                                height: 35,
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: appColor, width: 2),
                                    color: transparent,
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Flexible(
                                        flex: 1,
                                        fit: FlexFit.tight,
                                        child: Text(
                                          "MUST UPDATE",
                                          style: textStyle(true, 16, appColor),
                                        )),
                                    addSpaceWidth(10),
                                    Container(
                                      width: 20,
                                      height: 20,
                                      decoration: BoxDecoration(
                                          color: mustUpdate ? blue0 : blue09,
                                          border: Border.all(
                                              color: blue0, width: 1),
                                          shape: BoxShape.circle),
                                      child: mustUpdate
                                          ? Center(
                                              child: Icon(
                                              Icons.check,
                                              color: white,
                                              size: 15,
                                            ))
                                          : Container(),
                                    )
                                  ],
                                ),
                              ),
                            ),*/
                            addSpace(50),
                          ]),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    });
  }

  void post() {
    String title = titleController.text.trim();
    String message = messageController.text.trim();
    //feature = feature.replaceAll(",", "\n*");

    if (title.isEmpty) {
      showError("Add title to message!");
      return;
    }

    if (message.isEmpty) {
      showError("Add a message to push!");
      return;
    }

    //BaseModel model = BaseModel();
    model.put(TITLE, title);
    model.put(TYPE, NOTIFY_ADMIN);
    model.put(MESSAGE, message);
    model.put(PARTIES, ["all"]);
    //model.saveItem(NOTIFY_BASE, false);

    String adminId = appSettingsModel.getString(ONE_SIGNAL_ID);
    String pushKey = appSettingsModel.getString(ONE_SIGNAL_KEY);

    List playerIds = [];
    for (var bm in usersTotal) {
      if (bm.getString(ONE_SIGNAL_ID).isEmpty) continue;
      playerIds.add(bm.getString(ONE_SIGNAL_ID));
    }

    var notification = OSCreateNotification(
        playerIds: [],
        content: message,
        heading: title,
        additionalData: model.items);

    var notifyJson = notification.mapRepresentation();
    notifyJson['app_id'] = pushKey;
    notifyJson['included_segments'] = ["Subscribed Users"];
    sendOneSignal(notifyJson);
    return;
  }

  sendOneSignal(Map<String, dynamic> data) async {
    http
        .post('https://onesignal.com/api/v1/notifications',
            headers: {
              "Content-Type": "application/json; charset=utf-8",
              "Authorization":
                  "Basic ${appSettingsModel.getString(ONE_SIGNAL_API_KEY)}"
            },
            body: jsonEncode(data))
        .then((value) {
      print(value.body);
      model.saveItem(NOTIFY_BASE, false);
      showMessage(context, Icons.check, appColor, "Sent!",
          "Notification has been sent to all users.", cancellable: false,
          onClicked: (_) {
        Navigator.pop(context);
      });
    }).catchError((e) {
      showMessage(
        context,
        Icons.error_outline,
        red0,
        "Error!",
        e.toString(),
      );
    });
    ;
  }
}
