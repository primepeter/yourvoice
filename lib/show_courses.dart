import 'package:acclaim/app/app.dart';
import 'package:acclaim/show_profile.dart';
import 'package:flutter/material.dart';

import 'app_config.dart';
import 'main_screens/search.dart';

class ShowCourses extends StatefulWidget {
  @override
  _ShowCoursesState createState() => _ShowCoursesState();
}

class _ShowCoursesState extends State<ShowCourses> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context, "");
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "Courses",
                style: textStyle(true, 20, black),
              )),
              InkWell(
                  onTap: () {
                    pushAndResult(context, Search());
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.search,
                      color: black,
                      size: 25,
                    )),
                  )),
            ],
          ),
        ),
        Expanded(
          child: Builder(
            builder: (c) {
              //if (following.isEmpty)
              return emptyLayout(
                  ic_study, "No Courses", "Your courses has nothing");

              return ListView.builder(
                  itemCount: following.length,
                  padding: 0.padAll(),
                  itemBuilder: (ctx, p) {
                    BaseModel thisUser = following[p];
                    bool followed = following.contains(thisUser.getUserId());

                    return InkWell(
                      onTap: () {
                        //print("hello");
                        pushAndResult(
                            context,
                            ShowProfile(
                              thisUser: thisUser,
                            ));
                      },
                      child: Container(
                        padding: 8.padAll(),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom:
                                    BorderSide(color: black.withOpacity(.02)))),
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Row(
                                children: <Widget>[
                                  imageHolder(50, thisUser.userImage),
                                  10.spaceWidth(),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        thisUser.fullName,
                                        style: textStyle(true, 14, black),
                                      ),
                                      5.spaceHeight(),
                                      Text(
                                        '@${thisUser.nickName}',
                                        style: textStyle(
                                            false, 12, black.withOpacity(.7)),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            5.spaceWidth(),
                            FlatButton(
                              onPressed: () {
                                followThisUser(thisUser, followed,
                                    onComplete: () {
                                  setState(() {});
                                });
                              },
                              color: AppConfig.appColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                  side: BorderSide(color: AppConfig.appColor)),
                              child: Center(
                                  child: Text(
                                followed ? "Unfollow" : "Follow",
                                style: textStyle(true, 12, white),
                              )),
                            ),
                          ],
                        ),
                      ),
                    );
                  });
            },
          ),
        )
      ],
    );
  }
}
