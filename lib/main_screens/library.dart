import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_config.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Library extends StatefulWidget {
  @override
  _LibraryState createState() => _LibraryState();
}

class _LibraryState extends State<Library> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Builder(builder: (ctx) {
        if (!librarySetup) return loadingLayout();
        if (libraryList.isEmpty)
          return emptyLayoutX(
            "assets/images/library.png",
            "No Library",
            "No items in the library yet.",
          );

        return ListView.builder(
            // itemCount: libraryList.length,
            itemCount: 3,
            itemBuilder: (c, p) {
              return page(p);
            });
      }),
    );
  }

  page(int p) {
    BaseModel model = libraryList[0];
    return Container(
      height: 260,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            color: AppConfig.appColor.withOpacity(.09),
            child: Row(
              children: [
                Text(
                  "Most Listened",
                  style: textStyle(true, 14, black.withOpacity(.5)),
                ),
                Spacer(),
                Text(
                  "Show More",
                  style: textStyle(false, 14, black.withOpacity(.5)),
                ),
                Icon(Icons.navigate_next)
              ],
            ),
          ),
          addSpace(5),
          Flexible(
            child: ListView.builder(
                itemCount: 10,
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemBuilder: (c, p) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        //width: 160,
                        width: 120,
                        height: 165,
                        margin: EdgeInsets.all(5),
                        child: Stack(
                          alignment: Alignment.topCenter,
                          children: [
                            Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                decoration: BoxDecoration(
                                    //color: AppConfig.appColor,
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                        width: 2,
                                        color: black.withOpacity(.1))),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: CachedNetworkImage(
                                    imageUrl: model.getString(COVER_PHOTO),
                                    fit: BoxFit.cover,
                                    width: 120,
                                    height: 150,
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                //alignment: Alignment.bottomCenter,
                                padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                                decoration: BoxDecoration(
                                    color: AppConfig.appColor,
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(color: white, width: 2)),
                                child: StarRating(
                                  rating: 5,
                                  size: 15,
                                  color: yellow04,
                                ),
                              ),
                            )

                            // addSpace(5),
                            // Text(model.getString(PUBLISHER)),
                            // addSpace(5),

                            // addSpace(5),
                          ],
                        ),
                      ),
                      Text(
                        model.getAudioBooks[0].getString(TITLE),
                        style: textStyle(true, 14, black),
                      ),
                      Text(
                        model.getString(PUBLISHER),
                        style: textStyle(false, 12, black),
                      ),
                    ],
                  );
                }),
          ),
        ],
      ),
    );
  }
}
