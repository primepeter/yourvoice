import 'dart:async';

import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'home.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with AutomaticKeepAliveClientMixin {
  int currentPage = 0;
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;

  List<StreamSubscription> subs = [];

  @override
  initState() {
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 150) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
        print("ppp $value $appBarOpacity ${scrollController.position.pixels}");
      }
    });

    var stateStream = stateController.stream.listen((event) {
      for (var d in postList) {
        BaseModel bm = BaseModel(items: d);
        if (!bm.myItem() || !bm.myFollowers.contains(userModel.getUserId()))
          continue;
        int p = items.indexWhere(
            (e) => BaseModel(items: e).getObjectId() == bm.getObjectId());
        if (p == -1)
          items.insert(0, bm.items);
        else
          items[p] = bm.items;
      }
      setState(() {});
    });
    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 0) return;
      print("scrolling up $p");
      scrollTotallyUp(scrollController);
    });
    subs.add(stateStream);
    subs.add(scrollSub);

    loadItems(false);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];
  bool setup = false;

  loadItems(bool isNew) async {
    print(userModel.getUserId());
    QuerySnapshot query = await Firestore.instance
        .collection(POST_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ]).getDocuments();

    print(query.documents.length);

    for (var doc in query.documents) {
      BaseModel model = BaseModel(doc: doc);
      int p = items.indexWhere(
          (e) => BaseModel(items: e).getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model.items);
      else
        items[p] = model.items;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.documents.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    bool pulling = refreshController.headerStatus != RefreshStatus.idle;
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(top: pulling ? 35 : 0),
          child: SmartRefresher(
            controller: refreshController,
            scrollController: scrollController,
            enablePullDown: true,
            enablePullUp: true,
            header: WaterDropHeader(),
            footer: ClassicFooter(
              noDataText: "Nothing more for now, check later...",
              textStyle: textStyle(false, 12, black.withOpacity(.7)),
            ),
            onLoading: () {
              loadItems(false);
            },
            onRefresh: () {
              loadItems(true);
            },
            child: SingleChildScrollView(
              //controller: scrollController,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        top: uploadingText == null ? 40 : 10, bottom: 10),
                    child: Column(
                      children: [
                        Text(
                          "@${userModel.nickName}",
                          style: textStyle(true, 16, black.withOpacity(.6)),
                        ),
                        addSpace(10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Text(
                                  "23.6K",
                                  style: textStyle(true, 18, black),
                                ),
                                Text(
                                  "Followers",
                                  style: textStyle(
                                      true, 14, black.withOpacity(.4)),
                                ),
                              ],
                            ),
                            addSpaceWidth(20),
                            Container(
                              height: 120,
                              width: 100,
                              child: Stack(
                                children: [
                                  Container(
                                    height: 100,
                                    width: 100,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        //borderRadius: BorderRadius.circular(20),
                                        gradient: LinearGradient(colors: [
                                          pink0,
                                          blue3,
                                          Colors.purple
                                        ])),
                                    padding: EdgeInsets.all(2),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(50),
                                      child: CachedNetworkImage(
                                        imageUrl: userModel.userImage,
                                        fit: BoxFit.cover,
                                        height: 105,
                                        width: 105,
                                        placeholder: (c, s) {
                                          return Container(
                                            height: 105,
                                            width: 105,
                                            color: black.withOpacity(.05),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: white, width: 2),
                                          shape: BoxShape.circle,

                                          // borderRadius: BorderRadius.circular(5),
                                          gradient: LinearGradient(colors: [
                                            pink0,
                                            blue3,
                                            Colors.purple
                                          ])),
                                      padding: EdgeInsets.all(3),
                                      child: Icon(
                                        Icons.add,
                                        color: white,
                                        size: 15,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            addSpaceWidth(20),
                            Column(
                              children: [
                                Text(
                                  "488",
                                  style: textStyle(true, 18, black),
                                ),
                                Text(
                                  "Following",
                                  style: textStyle(
                                      true, 14, black.withOpacity(.4)),
                                ),
                              ],
                            ),
                          ],
                        ),
                        addSpace(10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                width: screenW(context) / 2,
                                child: Text(
                                  userModel.getString(NAME),
                                  style: textStyle(true, 18, black),
                                ),
                              ),
                            ),
                            Container(
                              height: 30,
                              width: 2,
                              color: black.withOpacity(.09),
                              margin: EdgeInsets.only(left: 5, right: 5),
                            ),
                            Flexible(
                              child: Container(
                                width: screenW(context) / 2,
                                alignment: Alignment.centerRight,
                                child: Text(
                                  userModel.profession,
                                  style: textStyle(true, 18, black),
                                ),
                              ),
                            ),
                          ],
                        ),
                        addSpace(10),
                        Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: black.withOpacity(.02),
                                borderRadius: BorderRadius.circular(15),
                                border:
                                    Border.all(color: black.withOpacity(.09))),
                            child: ReadMoreText(userModel.aboutMe))
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: black.withOpacity(.02),
                        border: Border.all(color: black.withOpacity(.09)),
                        borderRadius: BorderRadius.circular(8)),
                    margin: EdgeInsets.all(5),
                    child: Row(
                      children: List.generate(3, (p) {
                        var icon = Icons.grid_view;
                        if (p == 1) icon = Icons.list;
                        if (p == 2) icon = Icons.person_pin_outlined;

                        bool active = currentPage == p;

                        return Flexible(
                          child: GestureDetector(
                            onTap: () {
                              currentPage = p;
                              setState(() {});
                            },
                            child: Container(
                              color: transparent,
                              padding: EdgeInsets.all(10),
                              alignment: Alignment.center,
                              child: Icon(
                                icon,
                                color: black.withOpacity(active ? 1 : 0.3),
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                  ),
                  Stack(
                    children: List.generate(3, (p) {
                      if (!setup)
                        return Container(
                          height: 400,
                          child: loadingLayout(),
                        );

                      if (items.isEmpty)
                        return Container(
                          height: 400,
                          child: emptyLayout(
                              currentPage == 2
                                  ? Icons.people_outlined
                                  : Icons.add,
                              "No Post",
                              currentPage == 2
                                  ? "Follow users to view there content"
                                  : "You have not share any post yet"),
                        );

                      return Offstage(
                        offstage: p != currentPage,
                        child: AnimatedOpacity(
                          curve: Curves.easeInOut,
                          duration: Duration(milliseconds: 500),
                          opacity: p == currentPage ? 1 : 0.0,
                          child: p == 0 ? postInGrid() : postInList(),
                        ),
                      );
                    }),
                  )
                ],
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: AnimatedOpacity(
            opacity: appBarOpacity,
            curve: Curves.easeInOut,
            duration: Duration(milliseconds: 500),
            child: Material(
              color: white,
              //padding: const EdgeInsets.all(8.0),
              elevation: 12,
              child: Container(
                decoration: BoxDecoration(
                    color: black.withOpacity(.02),
                    border: Border.all(color: black.withOpacity(.09)),
                    borderRadius: BorderRadius.circular(8)),
                margin: EdgeInsets.only(
                    top: uploadingText == null ? 40 : 10,
                    right: 5,
                    left: 5,
                    bottom: 10),
                child: Row(
                  children: List.generate(3, (p) {
                    var icon = Icons.grid_view;
                    if (p == 1) icon = Icons.list;
                    if (p == 2) icon = Icons.person_pin_outlined;

                    bool active = currentPage == p;

                    return Flexible(
                      child: GestureDetector(
                        onTap: () {
                          currentPage = p;
                          if (p == 0) uploadingController.add("Posting...");
                          setState(() {});
                        },
                        child: Container(
                          height: 45,
                          color: transparent,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          child: Icon(
                            icon,
                            color: black.withOpacity(active ? 1 : 0.3),
                          ),
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  postInGrid() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3, crossAxisSpacing: 8, mainAxisSpacing: 8),
      shrinkWrap: true,
      //physics: NeverScrollableScrollPhysics(),
      itemCount: items.length,
      padding: EdgeInsets.zero,
      itemBuilder: (c, p) {
        Map item = items[p];
        BaseModel model = BaseModel(items: item);

        String image = model.userImage;
        String name = model.name;
        String username = model.nickName;
        String time = getTimeAgo(model.getTime());
        String message = model.getString(MESSAGE);
        final taggedPersons = model.getList(TAGGED_PERSONS);
        final photos = model.getListModel(IMAGES);
        BaseModel bm = photos[0];
        String imageUrl = bm.getString(IMAGE_URL);
        String imagePath = bm.getString(IMAGE_PATH);
        String thumbPath = bm.getString(THUMBNAIL_PATH);
        String thumbUrl = bm.getString(THUMBNAIL_URL);
        bool onlineImage = imageUrl.startsWith("http");
        bool isVideo = bm.isVideo;
        String heroTag = "profile_hero_image$p";

        return GestureDetector(
          onTap: () {
            /*pushAndResult(
                context,
                ShowComments(
                  thisPost: model,
                  heroTag: getRandomId(),
                ), result: (_) {
              if (null == _) return;
              items[p] = _.postItems;
              setState(() {});
            });*/
          },
          child: Stack(
            fit: StackFit.passthrough,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(1.2),
                child: CachedNetworkImage(
                  imageUrl: isVideo ? thumbUrl : imageUrl,
                  fit: BoxFit.cover,
                  placeholder: (_, s) {
                    return placeHolder(250, width: double.infinity);
                  },
                ),
              ),
              if (isVideo)
                Container(
                  height: 250,
                  child: Center(
                    child: Container(
                      height: 30,
                      width: 30,
                      padding: 3.padAll(),
                      margin: 6.padAll(),
                      decoration: BoxDecoration(
                        color: black.withOpacity(.5),
                        border: Border.all(color: white, width: 2),
                        shape: BoxShape.circle,
                      ),
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.play_arrow,
                        color: white,
                        size: 15,
                      ),
                    ),
                  ),
                ),
              /*Center(
                            child: Text(
                              p.toString(),
                              style: textStyle(true, 30, white),
                            ),
                          )*/
            ],
          ),
        );
      },
    );
  }

  postInList() {
    return ListView.builder(
      itemCount: items.length,
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (c, p) {
        Map item = items[p];
        BaseModel model = BaseModel(items: item);

        return PostItem(
            p: p,
            heroTag: getRandomId(),
            model: model,
            stateCallBack: (_) {
              items[p] = _;
              setState(() {});
            });
      },
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
