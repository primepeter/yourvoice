import 'package:acclaim/ShowComments.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_config.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/show_profile.dart';
import 'package:acclaim/show_replies.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'show_person.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications>
    with AutomaticKeepAliveClientMixin {
  final scrollController = ScrollController();
  final vp = PageController();
  int currentPage = 0;

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 1) return;
      scrollTotallyUp(scrollController);
    });
    subs.add(scrollSub);
    loadItems(false);
  }

  loadItems(bool isNew) async {
    List local = [];
    Firestore.instance
        .collection(NOTIFY_BASE)
        // .where(PARTIES, arrayContainsAny: [userModel.getObjectId(), "all"])
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          !isNew
              ? (myNotifications.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : myNotifications[myNotifications.length - 1].time)
              : (myNotifications.isEmpty ? 0 : myNotifications[0].time)
        ])
        .getDocuments()
        .then((value) {
          local = value.documents;
          for (var doc in value.documents) {
            BaseModel model = BaseModel(doc: doc);
            int p = myNotifications
                .indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p == -1)
              myNotifications.add(model);
            else
              myNotifications[p] = model;
          }

          if (isNew) {
            refreshController.refreshCompleted();
          } else {
            int oldLength = otherPosts.length;
            int newLength = local.length;
            if (newLength <= oldLength) {
              refreshController.loadNoData();
              canRefresh = false;
            } else {
              refreshController.loadComplete();
            }
          }
          setup = true;
          myNotifications.sort((a, b) => b.time.compareTo(a.time));
          if (mounted) setState(() {});
          changeToRead();
        });
  }

  changeToRead() {
    for (int p = 0; p < myNotifications.length; p++) {
      BaseModel model = myNotifications[p];
      model
        ..putInList(READ_BY, userModel.getUserId(), true)
        ..updateItems(delaySeconds: 1);
      myNotifications[p] = model;
    }
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Column(
      children: [
        Container(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(2, (p) {
                String title = "All activity";
                int count = 0;
                bool active = currentPage == p;

                List<BaseModel> listItem = myNotifications;

                /* if (p == 1) {
                  title = "Likes";
                  listItem = myNotifications
                      .where((e) =>
                          e.getInt(TYPE) == NOTIFY_LIKES ||
                          e.getInt(TYPE) == NOTIFY_COMMENT_LIKE ||
                          e.getInt(TYPE) == NOTIFY_REPLY_LIKE)
                      .toList();
                }*/

                if (p == 1) {
                  title = "Comments";
                  listItem = myNotifications
                      .where((e) => e.getInt(TYPE) == NOTIFY_COMMENT)
                      .toList();
                }

                /* if (p == 3) {
                  title = "Mentions";
                  listItem = myNotifications
                      .where((e) => e.getInt(TYPE) == NOTIFY_MENTIONS)
                      .toList();
                }*/

                count = listItem
                    .where(
                        (e) => !e.getList(READ_BY).contains(userModel.userId))
                    .toList()
                    .length;

                return GestureDetector(
                  onTap: () {
                    vp.jumpToPage(p);
                  },
                  child: Stack(
                    alignment: Alignment.topRight,
                    children: [
                      Container(
                        height: 35,
                        padding: EdgeInsets.only(
                            top: 5, bottom: 5, right: 8, left: 8),
                        margin: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: appColor.withOpacity(active ? 1 : .2)),
                        child: Center(
                            child: Text(
                          title,
                          style: textStyle(active, 15, active ? white : black),
                        )),
                      ),
                      if (count > 0 && p == 2)
                        Align(
                          //alignment: Alignment.topCenter,
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.red,
                                  border: Border.all(color: white, width: 2)),
                              padding: EdgeInsets.all(5),
                              width: 30,
                              child: Center(
                                child: Text(
                                  "$count",
                                  style: textStyle(true, 12, white),
                                ),
                              )),
                        )
                    ],
                  ),
                );
              }),
            ),
          ),
        ),
        Expanded(
          child: PageView.builder(
            controller: vp,
            onPageChanged: (p) {
              currentPage = p;
              setState(() {});
            },
            itemCount: 2,
            itemBuilder: (c, p) {
              List<BaseModel> listItem = myNotifications;
              String title = "activity";

              /* if (p == 1) {
                listItem = myNotifications
                    .where((e) =>
                        e.getInt(TYPE) == NOTIFY_LIKES ||
                        e.getInt(TYPE) == NOTIFY_COMMENT_LIKE ||
                        e.getInt(TYPE) == NOTIFY_REPLY_LIKE)
                    .toList();
                title = "likes";
              }*/

              if (p == 1) {
                listItem = myNotifications
                    .where((e) => e.getInt(TYPE) == NOTIFY_COMMENT)
                    .toList();
                title = "comments";
              }

              /*if (p == 3) {
                listItem = myNotifications
                    .where((e) => e.getInt(TYPE) == NOTIFY_MENTIONS)
                    .toList();
                title = "mentions";
              }*/

              if (!setup) return loadingLayout();

              if (listItem.isEmpty)
                return emptyLayout(Icons.notifications_off_sharp,
                    "No Notification", "You have no $title notifications yet",
                    clickText: "Refresh", click: () {
                  setup = false;
                  setState(() {});
                  loadItems(true);
                });

              return SmartRefresher(
                controller: refreshController,
                enablePullDown: true,
                enablePullUp: true,
                header: WaterDropHeader(),
                footer: ClassicFooter(
                  noDataText: "Nothing more for now, check later...",
                  textStyle: textStyle(false, 12, black.withOpacity(.7)),
                ),
                onLoading: () {
                  loadItems(false);
                },
                onRefresh: () {
                  loadItems(true);
                },
                child: ListView.builder(
                  itemCount: listItem.length,
                  controller: scrollController,
                  padding: EdgeInsets.only(top: 10),
                  itemBuilder: (c, p) {
                    BaseModel model = listItem[p];

                    String image = model.userImage;
                    String name = model.name;
                    String username = model.getString(USERNAME);
                    String time = getTimeAgo(model.getTime());
                    String title = model.getString(TITLE);
                    String message = model.getString(MESSAGE);
                    BaseModel theModel = model.getModel(THE_MODEL);
                    final photos = theModel.getListModel(IMAGES);
                    String first = "";
                    bool isVideo = false;
                    bool unRead =
                        !model.getList(READ_BY).contains(userModel.getUserId());

                    if (photos.isNotEmpty) {
                      bool isVideo = photos.first.isVideo;
                      first = photos.first
                          .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                    }

                    // if (model.getType() == NOTIFY_COMMENT ||
                    //     model.getType() == NOTIFY_REPLY)
                    //   message = theModel.getString(THE_COMMENT);

                    bool showImages = model.getType() == NOTIFY_COMMENT ||
                        model.getType() == NOTIFY_MENTIONS ||
                        model.getType() == NOTIFY_LIKES;

                    bool followed =
                        userModel.amFollowing.contains(theModel.getObjectId());

                    return GestureDetector(
                      onTap: () {
                        /*myNotifications.remove(model);
                        model
                          ..put(TYPE, NOTIFY_FOLLOW)
                          ..updateItems();

                        setState(() {});

                        return;*/

                        model
                          ..putInList(READ_BY, userModel.getUserId(), true)
                          ..updateItems();
                        int p = myNotifications.indexWhere(
                            (e) => e.getObjectId() == model.getObjectId());
                        if (p != -1) myNotifications[p] = model;
                        setState(() {});

                        if (model.getType() == NOTIFY_FOLLOW)
                          pushAndResult(
                              context,
                              ShowPerson(
                                model: theModel,
                              ));

                        if (model.getType() == NOTIFY_MENTIONS ||
                            model.getType() == NOTIFY_COMMENT ||
                            model.getType() == NOTIFY_LIKES ||
                            model.getType() == NOTIFY_COMMENT_LIKE)
                          pushAndResult(
                            context,
                            ShowComments(
                              theCandidate: model.getModel(CANDIDATE),
                              candidateId:
                                  model.getModel(CANDIDATE).getObjectId(),
                              heroTag: getRandomId(),
                            ),
                          );

                        if (model.getType() == NOTIFY_REPLY ||
                            model.getType() == NOTIFY_REPLY_LIKE)
                          pushAndResult(
                            context,
                            ShowReplies(
                              thisPost: theModel,
                              heroTag: getRandomId(),
                            ),
                          );
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10, bottom: 5),
                        child: Material(
                          color: white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                              side: BorderSide(color: black.withOpacity(.03))),
                          elevation: 2,
                          shadowColor: black.withOpacity(.3),
                          child: Container(
                            decoration: BoxDecoration(
                              color: appColor.withOpacity(unRead ? 0.08 : 0),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            padding: EdgeInsets.all(8),
                            child: Row(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    // pushAndResult(
                                    //     context,
                                    //     ShowPerson(
                                    //       model: theModel,
                                    //     ));
                                  },
                                  child: Container(
                                    height: 40,
                                    width: 40,
                                    padding: EdgeInsets.all(1),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            width: .5,
                                            color: black.withOpacity(.1)),
                                        color: white,
                                        shape: BoxShape.circle),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(25),
                                      child: Stack(
                                        children: [
                                          Container(
                                            height: 40,
                                            width: 40,
                                            color: appColor.withOpacity(
                                                model.getType() == NOTIFY_ADMIN
                                                    ? 0
                                                    : .6),
                                          ),

                                          Center(
                                            child: Image.asset(
                                              GetAsset.images(
                                                model.getType() == NOTIFY_ADMIN
                                                    ? 'ic_launcher.png'
                                                    : 'comment.png',
                                              ),
                                              color: model.getType() !=
                                                      NOTIFY_ADMIN
                                                  ? white
                                                  : null,
                                              height: 20,
                                              width: 20,
                                            ),
                                          )

                                          // if (image.isNotEmpty)
                                          //   ClipRRect(
                                          //     borderRadius:
                                          //         BorderRadius.circular(25),
                                          //     child: CachedNetworkImage(
                                          //       imageUrl: image,
                                          //       height: 40,
                                          //       width: 40,
                                          //       fit: BoxFit.cover,
                                          //     ),
                                          //   ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                addSpaceWidth(10),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "$time",
                                        style: textStyle(
                                            false, 11, black.withOpacity(.5)),
                                      ),
                                      Text(
                                        model.getType() == NOTIFY_ADMIN
                                            ? "Admin"
                                            : username,
                                        style: textStyle(true, 14, black),
                                      ),
                                      Text(
                                        message,
                                        style: textStyle(
                                            false, 14, black.withOpacity(.5)),
                                      ),
                                    ],
                                  ),
                                ),
                                if (showImages && photos.isNotEmpty)
                                  Container(
                                    height: 70,
                                    width: 70,
                                    child: Stack(
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          child: CachedNetworkImage(
                                            imageUrl: first,
                                            height: 70,
                                            width: 70,
                                            fit: BoxFit.cover,
                                            placeholder: (c, s) {
                                              return placeHolder(70);
                                            },
                                          ),
                                        ),
                                        if (isVideo)
                                          Container(
                                            height: 70,
                                            width: 70,
                                            child: Center(
                                              child: Container(
                                                height: 30,
                                                width: 30,
                                                padding: 3.padAll(),
                                                margin: 6.padAll(),
                                                decoration: BoxDecoration(
                                                  color: black.withOpacity(.8),
                                                  border: Border.all(
                                                      color: white, width: 1),
                                                  shape: BoxShape.circle,
                                                ),
                                                alignment: Alignment.center,
                                                child: Icon(
                                                  Icons.play_arrow,
                                                  color: white,
                                                  size: 16,
                                                ),
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                if (model.getType() == NOTIFY_FOLLOW)
                                  FlatButton(
                                    onPressed: () {
                                      pushReplaceAndResult(
                                          context,
                                          ShowPerson(
                                            model: model,
                                          ));
                                      /*followThisUser(model, followed, onComplete: () {
                                          setState(() {});
                                        });*/
                                    },
                                    color: appColor,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        side: BorderSide(color: appColor)),
                                    child: Center(
                                        child: Text(
                                      followed ? "Unfollow" : "Follow",
                                      style: textStyle(true, 12, white),
                                    )),
                                  )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        )
      ],
    );
  }

  notifyViewBuilder(int p) {
    BaseModel notify = myNotifications[p];
    int type = notify.getType();
    String time = getTimeAgo(notify.getTime());
    String notifyId = notify.notifiedId;
    BaseModel notifyThis = notify.notifiedOn;
    bool muted = userModel.mutedItems.contains(notify.getObjectId());

    return InkWell(
      onLongPress: () {
        pushAndResult(context,
            listDialog(["Delete", "${muted ? "UnMute" : "Mute"} Notification"]),
            result: (_) {
          if (_ == 0) {
            //myNotifications.removeModel(notify.getObjectId());
            notify.deleteItem();
            myNotifications
                .removeWhere((e) => e.getObjectId() == notify.getObjectId());
            setState(() {});
            return;
          }

          if (_ == 0) {
            myNotifications
                .removeWhere((e) => e.getObjectId() == notify.getObjectId());

            userModel
              ..putInList(MUTED, notify.getObjectId(), true)
              ..updateItems();
            setState(() {});
            return;
          }
        });
      },
      onTap: () {
        if (type == NOTIFY_TYPE_FOLLOW) {
          pushAndResult(
              context,
              ShowProfile(
                thisUser: notifyThis,
              ));
          return;
        }
/*
        if (type == NOTIFY_TYPE_POST ||
            type == NOTIFY_TYPE_COMMENT ||
            type == NOTIFY_TYPE_REPLY) {
          pushAndResult(
              context,
              ShowComments(
                thisPost: notifyThis,
              ));
          return;
        }*/
      },
      child: Container(
        padding: 8.padAll(),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: black.withOpacity(.1), width: .5))),
        child: Row(
          children: <Widget>[
            Flexible(
              child: Row(
                children: <Widget>[
                  imageHolder(50, notify.userImage, onImageTap: () {
                    pushAndResult(
                        context,
                        ShowProfile(
                          thisUser: notifyThis,
                        ));
                  }),
                  10.spaceWidth(),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          notify.getString(TITLE),
                          style: textStyle(true, 14, black),
                        ),
                        5.spaceHeight(),
                        Text(
                          notify.getString(MESSAGE),
                          style: textStyle(false, 12, black.withOpacity(.7)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Builder(
                    builder: (ctx) {
                      if (type == NOTIFY_TYPE_POST ||
                          type == NOTIFY_TYPE_COMMENT ||
                          type == NOTIFY_TYPE_REPLY) {
                        final photos = notifyThis.getPhotos;
                        bool noPhoto = photos.isEmpty;

                        if (photos.isNotEmpty) {
                          BaseModel photo = photos[0];
                          return CachedNetworkImage(
                            imageUrl: photo.isVideo
                                ? photo.thumbnailPath
                                : photo.urlPath,
                            fit: BoxFit.cover,
                            height: 50,
                            width: 50,
                            placeholder: (s, c) {
                              return placeHolder(50, width: 50);
                            },
                          );
                        }

                        return Container(
                          //margin: 8.padAll(),
                          height: 50, width: 50,
                          color: AppConfig.appColor,
                          padding: 2.padAll(),
                          child: Text(
                            notifyThis.message,
                            //maxLines: 4,
                            //overflow: TextOverflow.ellipsis,
                            style: textStyle(false, 6, white),
                          ),
                        );
                      }

                      if (type == NOTIFY_TYPE_FOLLOW) {
                        bool following = userModel.amFollowing
                            .contains(notifyThis.getUserId());

                        if (following) return Container();

                        return Container(
                          height: 30,
                          width: 80,
                          child: FlatButton(
                            onPressed: () {
                              followThisUser(notifyThis, following,
                                  onComplete: () {
                                setState(() {});
                              });
                            },
                            padding: EdgeInsets.all(0),
                            color: AppConfig.appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                                side: BorderSide(color: AppConfig.appColor)),
                            child: Center(
                                child: Text(
                              following ? "Unfollow" : "Follow",
                              style: textStyle(true, 12, white),
                            )),
                          ),
                        );
                      }

                      return Container();
                    },
                  ),
                  4.spaceHeight(),
                  Text(
                    time,
                    style: textStyle(false, 11, black.withOpacity(.5)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
