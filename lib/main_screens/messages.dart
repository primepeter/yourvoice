import 'dart:ui';

import 'package:acclaim/ChatMain.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_config.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../app_index.dart';

class Messages extends StatefulWidget {
  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages>
    with AutomaticKeepAliveClientMixin {
  var sub;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sub = chatMessageController.stream.listen((_) {
//      toastInAndroid("New Chat");
      if (mounted) setState(() {});
    });

    //startRefreshingMessages();
    //loadMessages();
  }

  @override
  void dispose() {
    // TODO: implement dispose
//    chatMessageController.close();
    sub.cancel();
    super.dispose();
  }

  startRefreshingMessages() {
    Future.delayed(Duration(seconds: 2), () {
      if (mounted) setState(() {});
      startRefreshingMessages();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[page()],
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(30),
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                  child: Text(
                "Messages",
                style: textStyle(true, 25, black),
              )),
            ],
          ),
        ),
        addSpace(15),
        Expanded(
          flex: 1,
          child: !chatSetup
              ? loadingLayout()
              : lastMessages.isEmpty
                  ? emptyLayout(
                      ic_message, "No Conversation", "you have no messages yet")
                  : Container(
//            color: default_white,
                      child: ListView.builder(
                      itemBuilder: (c, p) {
                        BaseModel model = lastMessages[p];

                        String chatId = model.getString(CHAT_ID);
                        String otherPersonId =
                            getOtherPersonId(model.getList(PARTIES));
                        BaseModel otherPerson = otherPeronInfo[otherPersonId];
                        if (otherPerson == null) return Container();

                        String name = otherPerson.getString(FULL_NAME);
                        String image = otherPerson.getString(USER_IMAGE);

                        return chatItem(image, name, model,
                            p == lastMessages.length - 1, otherPerson);
                      },
                      shrinkWrap: true,
                      itemCount: lastMessages.length,
                      padding: EdgeInsets.all(0),
                    )),
        )
      ],
    );
  }

  chatItem(String image, String name, BaseModel chatModel, bool last,
      BaseModel otherPerson) {
    int type = chatModel.getInt(TYPE);
    String chatId = chatModel.getString(CHAT_ID);
    bool myItem = chatModel.myItem();

    String hisId = chatId.replaceAll(userModel.getObjectId(), "");
    bool read = chatModel.getList(READ_BY).contains(hisId);
    bool myRead = chatModel.getList(READ_BY).contains(userModel.getObjectId());
    List mutedList = userModel.getList(MUTED);
    int unread = unreadCounter[chatId] ?? 0;

    return new InkWell(
      onLongPress: () {
        pushAndResult(
            context,
            listDialog([
              mutedList.contains(chatId) ? "Unmute Chat" : "Mute Chat",
              "Delete Chat"
            ]), result: (_) {
          if (_ == "Mute Chat" || _ == "Unmute Chat") {
            if (mutedList.contains(chatId)) {
              mutedList.remove(chatId);
            } else {
              mutedList.add(chatId);
            }
            userModel.put(MUTED, mutedList);
            userModel.updateItems();
            setState(() {});
          }
          if (_ == "Delete Chat") {
            yesNoDialog(context, "Delete Chat?",
                "Are you sure you want to delete this chat?", () {
              deleteChat(chatId);
            });
          }
        });
      },
      onTap: () {
        BaseModel chat = BaseModel();
        chat.put(PARTIES, [userModel.getObjectId(), otherPerson.getObjectId()]);
        chat.saveItem(AppDataBase.chat_ids_base, false, document: chatId);

        chatModel.putInList(READ_BY, userModel.getObjectId(), true);
        chatModel.updateItems();
        showNewMessageDot.removeWhere((id) => id == chatId);
        setState(() {});
        pushAndResult(
            context,
            ChatMain(
              chatId,
              //otherPerson: otherPerson,
            ));
      },
      //margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Container(
        color: /*!myItem && !myRead ? black : */ null,
        padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 10, 10),
                  width: 70,
                  height: 70,
                  child: Card(
                    color: blue09,
                    elevation: 0,
                    clipBehavior: Clip.antiAlias,
                    shape: CircleBorder(
                        //borderRadius: BorderRadius.all(Radius.circular(10)),
                        side:
                            BorderSide(color: black.withOpacity(.1), width: 2)),
                    child: CachedNetworkImage(
                      imageUrl: otherPerson.userImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                addSpaceWidth(10),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Text(
                              //"Emeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                              name,
                              maxLines: 1, overflow: TextOverflow.ellipsis,
                              style: textStyle(false, 20, black),
                            ),
                          ),
                          addSpaceWidth(5),
                          !myItem && !myRead && unread > 0
                              ? /*Icon(
                                  Icons.new_releases,
                                  size: 20,
                                  color: black,
                                )*/
                              (Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                      color: red0,
                                      shape: BoxShape.circle,
                                      border:
                                          Border.all(color: white, width: 2)),
                                  child: Center(
                                      child: Text(
                                    "${unread > 9 ? "9+" : unread}",
                                    style: textStyle(true, 12, white),
                                  )),
                                ))
                              : Container(),
                          addSpaceWidth(5),
                          Text(
                            getChatTime(chatModel.getTime()),
                            style: textStyle(false, 12, AppConfig.appColor),
                            textAlign: TextAlign.end,
                          ),

                          //addSpaceWidth(5),
                        ],
                      ),
                      addSpace(5),
                      Row(
                        children: <Widget>[
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  child: Container(
                                    margin: EdgeInsets.only(left: 8, top: 5),
                                    decoration: BoxDecoration(
                                        color: default_white,
                                        borderRadius: BorderRadius.circular(25),
                                        border: Border.all(
                                            color: black.withOpacity(.1),
                                            width: .5)),
                                    child: new Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 5, 10, 5),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          !myItem /*|| isGroup*/
                                              ? Container()
                                              : Container(
                                                  width: 12,
                                                  height: 12,
                                                  margin: EdgeInsets.fromLTRB(
                                                      5, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                      color:
                                                          read ? blue4 : blue0,
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                          color: black,
                                                          width: .5)),
                                                ),
                                          Icon(
                                            type == CHAT_TYPE_TEXT
                                                ? Icons.message
                                                : type == CHAT_TYPE_IMAGE
                                                    ? Icons.camera_alt
                                                    : type == CHAT_TYPE_VIDEO
                                                        ? Icons.videocam
                                                        : type == CHAT_TYPE_REC
                                                            ? Icons.mic
                                                            : Icons
                                                                .library_books,
                                            color: black.withOpacity(.8),
                                            size: 12,
                                          ),
                                          addSpaceWidth(5),
                                          Flexible(
                                            flex: 1,
                                            child: Text(
                                              chatRemoved(chatModel)
                                                  ? "This message has been removed"
                                                  : type == CHAT_TYPE_TEXT
                                                      ? chatModel
                                                          .getString(MESSAGE)
                                                      : type == CHAT_TYPE_IMAGE
                                                          ? "Photo"
                                                          : type ==
                                                                  CHAT_TYPE_VIDEO
                                                              ? "Video"
                                                              : type ==
                                                                      CHAT_TYPE_REC
                                                                  ? "Voice Note (${chatModel.getString(AUDIO_LENGTH)})"
                                                                  : "Document",
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: textStyle(false, 12,
                                                  black.withOpacity(.5)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          if (mutedList.contains(chatId))
                            Image.asset(
                              ic_mute,
                              width: 18,
                              height: 18,
                              color: black.withOpacity(.5),
                            )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            addSpace(5),
            addLine(.5, black.withOpacity(.1), 0, 0, 0, 0)
          ],
        ),
      ),
    );
  }

  deleteChat(String chatId) {
    lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
    stopListening.add(chatId);
    userModel.putInList(DELETED_CHATS, chatId, true);
    userModel.updateItems();
    if (mounted) setState(() {});

    Firestore.instance
        .collection(CHAT_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .where(CHAT_ID, isEqualTo: chatId)
        .orderBy(TIME, descending: false)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chat = BaseModel(doc: doc);
        if (chat.myItem()) {
          chat.put(DELETED, true);
          chat.updateItems();
        } else {
          List hidden = List.from(chat.getList(HIDDEN));
          hidden.add(userModel.getObjectId());
          chat.put(HIDDEN, hidden);
          chat.updateItems();
        }
      }
    });
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
