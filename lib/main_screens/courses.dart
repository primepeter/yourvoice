import 'package:acclaim/app/app.dart';
import 'package:flutter/material.dart';

class Courses extends StatefulWidget {
  @override
  _CoursesState createState() => _CoursesState();
}

class _CoursesState extends State<Courses> {
  @override
  Widget build(BuildContext context) {
    return emptyLayoutX(
      "assets/images/courses.png",
      "No Courses",
      "No items in the courses yet.",
    );
  }
}
