import 'dart:async';

import 'package:acclaim/AddPost.dart';
import 'package:acclaim/CreateReport.dart';
import 'package:acclaim/ShowBookmark.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/show_archives.dart';
import 'package:acclaim/show_courses.dart';
import 'package:acclaim/show_favorites.dart';
import 'package:acclaim/show_followers.dart';
import 'package:acclaim/show_following.dart';
import 'package:acclaim/show_library.dart';
import 'package:acclaim/viewStories.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashed_circle/dashed_circle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'home.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with AutomaticKeepAliveClientMixin {
  int currentPage = 0;
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;

  List<StreamSubscription> subs = [];

  @override
  initState() {
    super.initState();
    items = postList.where((e) => e[USER_ID] == userModel.getUserId()).toList();
    setup = items.isNotEmpty;
    setState(() {});

    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 150) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
        print("ppp $value $appBarOpacity ${scrollController.position.pixels}");
      }
    });

    var stateStream = stateController.stream.listen((event) {
      for (var d in postList) {
        BaseModel bm = BaseModel(items: d);
        if (!bm.myItem()) continue;
        int p = items.indexWhere(
            (e) => BaseModel(items: e).getObjectId() == bm.getObjectId());
        if (p == -1)
          items.insert(0, bm.items);
        else
          items[p] = bm.items;
      }
      items.sort((p1, p2) => p2[TIME].compareTo(p1[TIME]));
      setState(() {});
    });
    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 0) return;
      print("scrolling up $p");
      scrollTotallyUp(scrollController);
    });
    subs.add(stateStream);
    subs.add(scrollSub);

    loadItems(true);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];
  bool setup = false;

  loadItems(bool isNew) async {
    print(userModel.getUserId());
    QuerySnapshot query = await Firestore.instance
        .collection(POST_BASE)
        .where(USER_ID, isEqualTo: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ]).getDocuments();

    print(query.documents.length);

    for (var doc in query.documents) {
      BaseModel model = BaseModel(doc: doc);
      int p = items.indexWhere(
          (e) => BaseModel(items: e).getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model.items);
      else
        items[p] = model.items;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.documents.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    items.sort((p1, p2) => p2[TIME].compareTo(p1[TIME]));

    //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  openStoryAt({
    @required int position,
  }) {
    List<List<BaseModel>> stories = [];
    final foldedOthers =
        allStories.where((e) => !e.myItem()).toList().groupBy((s) => s.userId);
    final foldedMine =
        allStories.where((e) => e.myItem()).toList().groupBy((s) => s.userId);
    stories.addAll(foldedMine.values.toList());
    pushAndResult(context, viewStories(stories, position: position));
  }

  page() {
    String followers =
        formatToK(userModel.getList(MY_FOLLOWER_IDS).length).toString();

    String followings =
        formatToK(userModel.getList(AM_FOLLOWING_IDS).length).toString();

    final myStories = allStories.where((e) => e.myItem()).toList();
    bool noStories = myStories.isEmpty;

    final foldedMine = myStories.groupBy((s) => s.userId);

    var keys;
    List<BaseModel> mStory = [];
    BaseModel story;
    bool noStory = foldedMine.isEmpty;
    if (storiesReady && myStories.isNotEmpty) {
      keys = foldedMine.keys.toList()[0];
      mStory = foldedMine[keys];
      story = mStory[0];
    }

    double storyBoxSize = 80;

    return Column(
      children: [
        Material(
          color: white,
          //padding: const EdgeInsets.all(8.0),
          elevation: 0,
          shadowColor: black.withOpacity(.2),
          child: Container(
            padding: EdgeInsets.only(
                top: uploadingText == null ? 40 : 10,
                right: 5,
                left: 5,
                bottom: 10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    addSpaceWidth(10),
                    Text(
                      "@${userModel.nickName}",
                      style: textStyle(true, 16, black.withOpacity(.6)),
                    ),
                    addSpaceWidth(10),
                  ],
                ),
                addSpace(10),
                SingleChildScrollView(
                  padding: EdgeInsets.zero,
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(5, (p) {
                      List items = [
                        {"icon": Icons.star, "title": "Favorites"},
                        {"icon": Icons.bookmark, "title": "Bookmarks"},
                        {"icon": ic_book, "title": "Library"},
                        {"icon": ic_study, "title": "Courses"},
                        {"icon": Icons.category, "title": "Archives"},
                      ];
                      return GestureDetector(
                        onTap: () {
                          if (p == 0) pushAndResult(context, ShowFavorites());
                          if (p == 1) pushAndResult(context, Bookmarks());
                          if (p == 2) pushAndResult(context, ShowLibrary());
                          if (p == 3) pushAndResult(context, ShowCourses());
                          if (p == 4) pushAndResult(context, ShowArchives());
                        },
                        child: Container(
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: appColor,
                              borderRadius: BorderRadius.circular(25)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              addSpaceWidth(5),
                              if (items[p]["icon"] is IconData)
                                Icon(
                                  items[p]["icon"],
                                  color: white,
                                  size: 20,
                                )
                              else
                                Image.asset(
                                  items[p]["icon"],
                                  color: white,
                                  height: 18,
                                  width: 18,
                                ),
                              addSpaceWidth(4),
                              Text(
                                items[p]["title"],
                                style: textStyle(false, 14, white),
                              ),
                              addSpaceWidth(5),
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                ),

                /*
                addSpace(10),
                Container(
                  decoration: BoxDecoration(
                      color: black.withOpacity(.02),
                      border: Border.all(color: black.withOpacity(.09)),
                      borderRadius: BorderRadius.circular(8)),
                  child: Row(
                    children: List.generate(3, (p) {
                      String assetPath = 'assets/images/';
                      String key;

                      if (p == 0) key = "library";
                      if (p == 1) key = "courses";
                      if (p == 2) key = "archive";

                      var icon = p == 0
                          ? ic_book
                          : p == 1
                              ? ic_study
                              : Icons.bookmark;

                      bool active = currentPage == p;

                      return Flexible(
                        child: GestureDetector(
                          onTap: () {
                            currentPage = p;
                            if (p == 0) uploadingController.add("Posting...");
                            setState(() {});
                          },
                          child: Container(
                              color: transparent,
                              padding: EdgeInsets.all(8),
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  (icon is String)
                                      ? Image.asset(
                                          icon,
                                          height: 18,
                                          width: 18,
                                          fit: BoxFit.cover,
                                          color: black,
                                        )
                                      : Icon(
                                          icon,
                                          size: 18,
                                          color: black,
                                        ),
                                  Text(key)
                                ],
                              )),
                        ),
                      );
                    }),
                  ),
                ),*/
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            ///padding: EdgeInsets.only(top: 20),
            child: SmartRefresher(
              controller: refreshController,
              scrollController: scrollController,
              enablePullDown: true,
              enablePullUp: true,
              header: WaterDropHeader(),
              footer: ClassicFooter(
                noDataText: "Nothing more for now, check later...",
                textStyle: textStyle(false, 12, black.withOpacity(.7)),
              ),
              onLoading: () {
                loadItems(false);
              },
              onRefresh: () {
                loadItems(true);
              },
              child: SingleChildScrollView(
                //controller: scrollController,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () {
                                  pushAndResult(context, ShowFollowers());
                                },
                                child: Column(
                                  children: [
                                    Text(
                                      followers,
                                      style: textStyle(true, 18, black),
                                    ),
                                    Text(
                                      "Followers",
                                      style: textStyle(
                                          true, 14, black.withOpacity(.4)),
                                    ),
                                  ],
                                ),
                              ),
                              addSpaceWidth(20),
                              Container(
                                height: 120,
                                width: 120,
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    DashedCircle(
                                      strokeWidth: 3,
                                      color: appColor,
                                      dashes: mStory.length,
                                      gapSize: 5,
                                      child: GestureDetector(
                                        onTap: () {
                                          if (noStory) {
                                            storyController.add(true);
                                          } else {
                                            openStoryAt(
                                              position: 0,
                                            );
                                          }
                                        },
                                        child: Container(
                                          height: 100,
                                          width: 100,
                                          margin: EdgeInsets.all(3),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              //borderRadius: BorderRadius.circular(20),
                                              gradient: LinearGradient(colors: [
                                                pink0,
                                                blue3,
                                                Colors.purple
                                              ])),
                                          padding: EdgeInsets.all(2),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            child: CachedNetworkImage(
                                              imageUrl: userModel.userImage,
                                              fit: BoxFit.cover,
                                              height: 100,
                                              width: 100,
                                              placeholder: (c, s) {
                                                return Container(
                                                  height: 100,
                                                  width: 100,
                                                  color: black.withOpacity(.05),
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.bottomRight,
                                      child: GestureDetector(
                                        onTap: () {
                                          storyController.add(true);
                                        },
                                        child: Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: white, width: 2),
                                              shape: BoxShape.circle,

                                              // borderRadius: BorderRadius.circular(5),
                                              gradient: LinearGradient(colors: [
                                                pink0,
                                                blue3,
                                                Colors.purple
                                              ])),
                                          padding: EdgeInsets.all(3),
                                          child: Icon(
                                            Icons.add,
                                            color: white,
                                            size: 15,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              addSpaceWidth(20),
                              InkWell(
                                onTap: () {
                                  pushAndResult(context, ShowFollowing());
                                },
                                child: Column(
                                  children: [
                                    Text(
                                      followings,
                                      style: textStyle(true, 18, black),
                                    ),
                                    Text(
                                      "Following",
                                      style: textStyle(
                                          true, 14, black.withOpacity(.4)),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          addSpace(10),
                          Text(
                            userModel.getString(NAME),
                            style: textStyle(true, 18, black),
                          ),
                          addSpace(5),
                          Text(
                            userModel.profession.isEmpty
                                ? "*******"
                                : userModel.profession,
                            style: textStyle(true, 16, black),
                          ),
                          addSpace(5),
                          Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: black.withOpacity(.02),
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(
                                      color: black.withOpacity(.09))),
                              child: ReadMoreText(userModel.aboutMe))
                        ],
                      ),
                    ),
                    addSpace(10),
                    Builder(
                      builder: (c) {
                        if (!setup)
                          return Container(
                            height: 400,
                            child: loadingLayout(),
                          );

                        if (items.isEmpty)
                          return Container(
                            height: 400,
                            child: emptyLayout(Icons.add, "No Post",
                                "You have not share any post yet"),
                          );

                        return postInGrid();
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  postInGrid() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3, crossAxisSpacing: 8, mainAxisSpacing: 8),
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: items.length,
      padding: EdgeInsets.zero,
      itemBuilder: (c, p) {
        Map item = items[p];
        BaseModel model = BaseModel(items: item);

        String image = model.userImage;
        String name = model.name;
        String username = model.nickName;
        String time = getTimeAgo(model.getTime());
        String message = model.getString(MESSAGE);
        final taggedPersons = model.getList(TAGGED_PERSONS);
        final photos = model.getListModel(IMAGES);
        BaseModel bm = photos[0];
        String imageUrl = bm.getString(IMAGE_URL);
        String imagePath = bm.getString(IMAGE_PATH);
        String thumbPath = bm.getString(THUMBNAIL_PATH);
        String thumbUrl = bm.getString(THUMBNAIL_URL);
        bool onlineImage = imageUrl.startsWith("http");
        bool isVideo = bm.isVideo;
        String heroTag = "profile_hero_image$p";

        return GestureDetector(
          onLongPress: () {
            showListDialog(context, [
              if (model.myItem() || isAdmin) "Edit",
              if (model.myItem() || isAdmin) "Delete",
              if (!model.myItem()) "Report"
            ], (_) {
              if (_ == "Delete") {
                model.deleteItem();
                items.remove(item);
                allPosts.remove(item);
                setState(() {});
              }
              if (_ == "Edit") {
                pushAndResult(
                    context,
                    AddPost(
                      model: model,
                    ));
              }
              if (_ == "Report") {
                pushAndResult(
                    context,
                    CreateReport(
                      item: model,
                    ), result: (_) {
                  if (null != _ || !!_) return;
                  items.remove(item);
                  setState(() {});
                });
              }
            });
          },
          onTap: () {
            /* pushAndResult(
                context,
                ShowComments(
                  thisPost: model,
                  heroTag: getRandomId(),
                ), result: (_) {
              if (null == _) return;

              if (_ == "deleted") {
                items.remove(item);
                setState(() {});
                return;
              }

              items[p] = _.postItems;
              setState(() {});
            });*/
          },
          child: Stack(
            fit: StackFit.passthrough,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(1.2),
                child: CachedNetworkImage(
                  imageUrl: isVideo ? thumbUrl : imageUrl,
                  fit: BoxFit.cover,
                  placeholder: (_, s) {
                    return placeHolder(250, width: double.infinity);
                  },
                ),
              ),
              if (isVideo)
                Container(
                  height: 250,
                  child: Center(
                    child: Container(
                      height: 30,
                      width: 30,
                      padding: 3.padAll(),
                      margin: 6.padAll(),
                      decoration: BoxDecoration(
                        color: black.withOpacity(.5),
                        border: Border.all(color: white, width: 2),
                        shape: BoxShape.circle,
                      ),
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.play_arrow,
                        color: white,
                        size: 15,
                      ),
                    ),
                  ),
                ),
              /*Center(
                            child: Text(
                              p.toString(),
                              style: textStyle(true, 30, white),
                            ),
                          )*/
            ],
          ),
        );
      },
    );
  }

  postInList() {
    return ListView.builder(
      itemCount: items.length,
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (c, p) {
        Map item = items[p];
        BaseModel model = BaseModel(items: item);

        return PostItem(
            p: p,
            heroTag: getRandomId(),
            model: model,
            stateCallBack: (_) {
              if (_ == null) {
                items.remove(item);
                setState(() {});
                return;
              }
              items[p] = _;
              setState(() {});
            });
      },
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
