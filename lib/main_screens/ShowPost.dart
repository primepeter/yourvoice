import 'dart:async';

import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/preview_post_images.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:color_thief_flutter/color_thief_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ShowPost extends StatefulWidget {
  final BaseModel model;
  final String heroTag;
  const ShowPost({Key key, this.model, this.heroTag}) : super(key: key);
  @override
  _ShowPostState createState() => _ShowPostState();
}

class _ShowPostState extends State<ShowPost>
    with AutomaticKeepAliveClientMixin {
  int currentPage = 0;
  final vp = PageController();
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;

  List<StreamSubscription> subs = [];

  BaseModel model = BaseModel();
  List groupMembers = [];
  List<BaseModel> photos = [];
  int currentPhoto = 0;
  String currentPhotoUrl = '';

  List<Color> photoColors = [];

  int mins = 0;

  @override
  initState() {
    super.initState();

    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 100) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
      }
    });

    model = widget.model;
    groupMembers = model.getList(GROUP_MEMBERS);
    if (widget.model != null) {
      photos = widget.model.getListModel(IMAGES);
      photoColors = List.generate(photos.length, (index) => appColor);
      if (photos.isNotEmpty) {
        currentPhotoUrl = photos[0].getString(IMAGE_URL);
        loadPhotoColor();
      }
      mins = model.getString(MESSAGE).length ~/ 200;
    }

    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 0) return;
      print("scrolling up $p");
      scrollTotallyUp(scrollController);
    });
    subs.add(scrollSub);
    //loadGroup();
    loadItems(true);
  }

  BaseModel theUser;
  List<BaseModel> followers = [];
  List<BaseModel> followings = [];

  loadPhotoColor() {
    for (int p = 0; p < photos.length; p++) {
      String url = photos[p].getString(IMAGE_URL);

      getColorFromUrl(url).then((color) {
        photoColors[p] = Color.fromRGBO(color[0], color[1], color[2], 1);
        if (mounted) setState(() {});
      });
    }
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];
  bool setup = false;

  loadItems(bool isNew) async {
    var sub = FirebaseFirestore.instance
        .collection(POST_BASE)
        .doc(widget.model.getObjectId())
        .snapshots()
        .listen((query) {
      model = BaseModel(doc: query);
      setup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    final likes = model.getList(LIKES);
    final disLikes = model.getList(DISLIKES);
    final bookmarks = model.getList(BOOKMARKS);
    bool liked = likes.contains(userModel.getUserId());
    bool disLiked = disLikes.contains(userModel.getUserId());
    bool bookmarked = bookmarks.contains(userModel.getUserId());

    return Stack(
      children: [
        SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: [
              Container(
                height: 320,
                width: double.infinity,
                child: Stack(
                  children: [
                    PageView(
                      controller: vp,
                      onPageChanged: (p) {
                        currentPage = p;
                        setState(() {});
                      },
                      scrollDirection: Axis.vertical,
                      children: List.generate(photos.length, (p) {
                        String url = photos[p].getString(IMAGE_URL);

                        return Stack(
                          children: [
                            GestureDetector(
                              onTap: () {
                                pushAndResult(
                                    context,
                                    PreviewPostImages(
                                      photos,
                                      [],
                                      indexOf: currentPhoto,
                                    ));
                              },
                              child: CachedNetworkImage(
                                imageUrl: url,
                                height: 300,
                                width: double.infinity,
                                fit: BoxFit.cover,
                              ),
                            ),
                            IgnorePointer(
                              child: Container(
                                height: 300,
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                      /*appColor.withOpacity(.6),
                                                      appColor_light.withOpacity(.6),*/
                                      black.withOpacity(.1),
                                      black.withOpacity(.07),
                                    ],
                                        begin: Alignment.bottomCenter,
                                        end: Alignment.topCenter)),
                              ),
                            ),
                          ],
                        );
                      }),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Material(
                        color: white,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Container(
                          padding: EdgeInsets.all(5),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              InkWell(
                                onTap: () {
                                  // model.remove(BOOKMARKS);
                                  // setState(() {});
                                  // return;

                                  model
                                    ..putInList(BOOKMARKS,
                                        userModel.getUserId(), !bookmarked)
                                    ..updateItems();

                                  int ps = bookmarkList.indexWhere((e) =>
                                      e.getObjectId() == model.getObjectId());

                                  if (bookmarked) {
                                    if (ps == -1)
                                      bookmarkList.add(model);
                                    else
                                      bookmarkList[ps] = model;
                                  } else {
                                    bookmarkList.removeWhere((e) =>
                                        e.getObjectId() == model.getObjectId());
                                  }
                                  stateController.add(true);
                                  setState(() {});
                                },
                                child: Container(
                                    width: 40,
                                    height: 40,
                                    child: Icon(
                                      bookmarked
                                          ? Icons.bookmark
                                          : Icons.bookmark_border,
                                      color: bookmarked
                                          ? appColor
                                          : black.withOpacity(.5),
                                    )),
                              ),
                              InkWell(
                                onTap: () {
                                  sharePost(model);
                                },
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  child: Center(
                                    child: Image.asset(
                                      GetAsset.images('share.png'),
                                      height: 20,
                                      width: 20,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  model
                                    ..putInList(
                                        DISLIKES, userModel.getUserId(), false)
                                    ..updateItems();
                                  model
                                    ..putInList(
                                        LIKES, userModel.getUserId(), !liked)
                                    ..updateItems();
                                  //items[p] = model.items;
                                  setState(() {});
                                },
                                child: Container(
                                  width: 40,
                                  height: 50,
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(5),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Image.asset(
                                        GetAsset.images(
                                            liked ? 'like1.png' : 'like.png'),
                                        height: 20,
                                        width: 20,
                                        fit: BoxFit.cover,
                                      ),
                                      addSpace(2),
                                      if (likes.length > 0)
                                        Text(
                                          formatToK(likes.length),
                                          style: textStyle(false, 13, black),
                                        ),
                                    ],
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  model
                                    ..putInList(DISLIKES, userModel.getUserId(),
                                        !disLiked)
                                    ..updateItems();
                                  model
                                    ..putInList(
                                        LIKES, userModel.getUserId(), false)
                                    ..updateItems();

                                  //items[p] = model.items;
                                  setState(() {});
                                },
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  width: 40,
                                  height: 50,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        GetAsset.images(disLiked
                                            ? 'dislike1.png'
                                            : 'dislike.png'),
                                        height: 20,
                                        width: 20,
                                        fit: BoxFit.cover,
                                      ),
                                      addSpace(2),
                                      if (disLikes.length > 0)
                                        Text(
                                          formatToK(disLikes.length),
                                          style: textStyle(false, 13, black),
                                        ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    if (photos.length > 1)
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          decoration: BoxDecoration(
                              color: black.withOpacity(.5),
                              borderRadius: BorderRadius.circular(15)),
                          padding: EdgeInsets.all(2),
                          margin: EdgeInsets.all(10),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: List.generate(photos.length, (index) {
                              bool active = currentPage == index;
                              return Container(
                                height: active ? 9 : 6,
                                width: active ? 9 : 6,
                                margin: EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: white.withOpacity(active ? 1 : 0.7)),
                              );
                            }),
                          ),
                        ),
                      )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      model.getString(TITLE),
                      textAlign: TextAlign.center,
                      style: textStyle(true, 20, black),
                    ),
                    addSpace(10),
                    /*Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child: Container(
                                child: CachedNetworkImage(
                                  imageUrl: userModel.userImage,
                                  height: 50,
                                  width: 50,
                                  fit: BoxFit.cover,
                                  placeholder: (c, x) {
                                    return Container(
                                      height: 50,
                                      width: 50,
                                      decoration: BoxDecoration(
                                          color: appColor.withOpacity(.1),
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: appColor, width: .5)),
                                      child: Icon(
                                        Icons.person,
                                        size: 18,
                                        color: appColor,
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                            addSpaceWidth(10),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    model.getString(USERNAME),
                                    style: textStyle(false, 16, black),
                                  ),
                                  addSpace(8),
                                  Row(
                                    children: [
                                      Text(
                                        '$mins min read',
                                        style: textStyle(
                                            false, 12, black.withOpacity(.8)),
                                      ),
                                      Container(
                                        height: 5,
                                        width: 5,
                                        margin: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: black,
                                        ),
                                      ),
                                      Text(
                                        getTimeAgo(model.getTime()),
                                        style: textStyle(
                                            false, 12, black.withOpacity(.8)),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      addSpace(10),*/
                    addLine(.5, black.withOpacity(.1), 10, 5, 10, 5),
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: black.withOpacity(.0),
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: black.withOpacity(.0))),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        model.getString(MESSAGE),
                        style: textStyle(false, 16, black),
                      ),
                    ),
                    addSpace(10),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.only(top: 25),
          color: photoColors[currentPage].withOpacity(appBarOpacity),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context, setup ? model.items : {});
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    // decoration: BoxDecoration(
                    //     color: red0,
                    //     borderRadius: BorderRadius.only(
                    //       topRight: Radius.circular(25),
                    //       bottomRight: Radius.circular(25),
                    //     )),
                    padding: EdgeInsets.all(6),
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: white,
                      size: 25,
                    )),
                  )),
              Spacer(),
            ],
          ),
        ),
        if (appBarOpacity == 1)
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              margin: EdgeInsets.only(bottom: 50, right: 5),
              child: RaisedButton(
                onPressed: () {
                  scrollTotallyUp(scrollController);
                },
                padding: EdgeInsets.all(15),
                elevation: 10,
                shape: CircleBorder(),
                child: Icon(
                  Icons.keyboard_arrow_up,
                  color: appColor,
                ),
                color: white,
              ),
            ),
          )
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

String formatToActualDate(int time) {
  Timestamp t = userModel.get(CREATED_AT);
  DateTime d = DateTime.fromMillisecondsSinceEpoch(time);
  final date = DateTime.parse(d.toString());
  return new DateFormat(
    "MMMM d,yyyy",
  ).format(date);
}
