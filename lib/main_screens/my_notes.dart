import 'package:acclaim/add_note.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyNotes extends StatefulWidget {
  @override
  _MyNotesState createState() => _MyNotesState();
}

class _MyNotesState extends State<MyNotes> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (selectedNote.isEmpty) {
          Navigator.pop(context);
        } else {
          setState(() {
            selectedNote.clear();
          });
        }

        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        /* appBar: AppBar(
          title: Text(
              "Notes ${selectedNote.isEmpty ? "" : '(${selectedNote.length})'}"),
          centerTitle: true,
          actions: <Widget>[
            if (selectedNote.isEmpty) ...[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.add_circle_outline),
                onPressed: () {
                  pushAndResult(context, AddNotes(), result: (_) {
                    setState(() {});
                  });
                },
              ),
            ] else
              IconButton(
                icon: Icon(
                  Icons.delete,
                ),
                onPressed: () {
                  showMessage(context, Icons.warning, Colors.red, "Delete?",
                      "Are you sure that you want delete these notes?",
                      cancellable: true,
                      clickYesText: "Delete", onClicked: (_) async {
                    if (_) {
                      for (var id in selectedNote) {
                        int p =
                            myNotes.indexWhere((e) => e.getObjectId() == id);
                        if (p != -1) {
                          final model = myNotes[p];
                          model.deleteItem();
                          myNotes.removeModel(id);
                        }
                      }
                      selectedNote.clear();
                      setState(() {});
                    }
                  });
                },
              ),
          ],
        ),*/
        body: page(),
      ),
    );
  }

  page() {
    return Column(
      children: [
        addSpace(30),
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "Notes ${selectedNote.isEmpty ? "" : '(${selectedNote.length})'}",
                style: textStyle(true, 25, black),
              )),
              if (selectedNote.isEmpty) ...[
                /* IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {},
                ),*/
                IconButton(
                  icon: Icon(Icons.add_circle_outline),
                  onPressed: () {
                    pushAndResult(context, AddNote(), result: (_) {
                      setState(() {});
                    });
                  },
                ),
              ] else
                IconButton(
                  icon: Icon(
                    Icons.delete,
                  ),
                  onPressed: () {
                    showMessage(context, Icons.warning, Colors.red, "Delete?",
                        "Are you sure that you want delete these notes?",
                        cancellable: true,
                        clickYesText: "Delete", onClicked: (_) async {
                      if (_) {
                        for (var id in selectedNote) {
                          int p =
                              myNotes.indexWhere((e) => e.getObjectId() == id);
                          if (p != -1) {
                            final model = myNotes[p];
                            model.deleteItem();
                            myNotes.removeModel(id);
                          }
                        }
                        selectedNote.clear();
                        setState(() {});
                      }
                    });
                  },
                ),
            ],
          ),
        ),
        pageBody()
      ],
    );
  }

  pageBody() {
    return Expanded(
      child: Builder(
        builder: (ctx) {
          if (!readyNotes) return loadingLayout();

          if (myNotes.isEmpty)
            return emptyLayout(
              Icons.note_add,
              "No Note",
              "You have not added any notes to your profile yet.",
            );

          return ListView.builder(
              itemCount: myNotes.length,
              padding: 0.padAtTop(),
              itemBuilder: (ctx, p) {
                return notesViewBuilder(p);
              });
        },
      ),
    );
  }

  List<String> selectedNote = [];

  notesViewBuilder(int p) {
    BaseModel model = myNotes[p];
    String title = model.getString(TITLE);
    String description = model.getString(MESSAGE);
    String time = getTimeAgo(model.time);
    bool selected = selectedNote.contains(model.getObjectId());

    return InkWell(
      onTap: () {
        if (selectedNote.isNotEmpty) {
          setState(() {
            if (selected) {
              setState(() {
                selectedNote.remove(model.getObjectId());
              });
            } else {
              selectedNote.add(model.getObjectId());
            }
          });
          return;
        }

        if (selected && selectedNote.isNotEmpty) {
          setState(() {
            selectedNote.remove(model.getObjectId());
          });
          return;
        }
        pushAndResult(
            context,
            AddNote(
              model: model,
            ), result: (_) {
          setState(() {});
        });
      },
      onLongPress: () {
        setState(() {
          if (selected) {
            selectedNote.remove(model.getObjectId());
          } else {
            selectedNote.add(model.getObjectId());
          }
        });
      },
      child: Container(
        padding: 10.padAll(),
        decoration: BoxDecoration(
            color: selected ? black.withOpacity(.1) : null,
            border: Border(bottom: BorderSide(color: black.withOpacity(.03)))),
        child: Row(
          children: <Widget>[
            Flexible(
              child: Row(
                children: <Widget>[
                  Image.asset(
                    "assets/images/notes.png",
                    height: 50,
                    width: 50,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          title,
                          style: textStyle(true, 16, black),
                        ),
                        5.spaceHeight(),
                        Text(
                          time,
                          style: textStyle(false, 11, black.withOpacity(.5)),
                        ),
                        5.spaceHeight(),
                        Text(
                          description,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: textStyle(false, 16, black),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            if (selected)
              Container(
                decoration: BoxDecoration(
                    color: AppConfig.appColor,
                    shape: BoxShape.circle,
                    border: Border.all(color: white, width: 2)),
                height: 50,
                width: 50,
                child: Icon(
                  Icons.check,
                  color: white,
                ),
              )
            else
              IconButton(
                onPressed: () {
                  pushAndResult(
                      context,
                      listDialog([
                        "Edit",
                        "Share",
                        "Delete",
                      ]), result: (_) {
                    if (_ == 0) {
                      pushAndResult(
                          context,
                          AddNote(
                            model: model,
                          ));
                      return;
                    }

                    if (_ == 2) {
                      model.deleteItem();
                      myNotes.removeModel(model.getObjectId());
                      setState(() {});
                      return;
                    }
                  });
                },
                icon: Icon(
                  Icons.more_vert,
                  color: black.withOpacity(.6),
                ),
              )
          ],
        ),
      ),
    );
  }
}
