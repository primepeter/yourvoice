import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_config.dart';
import 'package:acclaim/show_profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SearchX extends StatefulWidget {
  @override
  _SearchXState createState() => _SearchXState();
}

class _SearchXState extends State<SearchX> {
  final searchController = TextEditingController();
  bool isSearching = false;

  List<String> types = ["People", "Books", "Courses", "Posts"];
  int searchType = 0;

  List<BaseModel> searchResult = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //createDefSearchParam();
    initSearchParam();
  }

  @override
  void dispose() {
    searchController?.dispose();
    super.dispose();
  }

  initSearchParam() async {
    searchController.addListener(() {
      String param = searchController.text;
      if (mounted)
        setState(() {
          if (param.isEmpty) {
            isSearching = false;
          } else {
            isSearching = true;
            performSearch(param);
          }
        });
    });
  }

  createDefSearchParam() {
    String path;
    if (searchType == 0) path = "userBase";
    if (searchType == 1) path = "userBase";
    if (searchType == 2) path = "postBase";
    Firestore.instance
        .collection(path)
        //.document(userModel.userId)
        .getDocuments()
        .then((d) {
      for (var doc in d.documents) {
        BaseModel model = BaseModel(doc: doc);
        userModel
          ..put(SEARCH_PARAM, model.fullName)
          ..updateItems();
      }
    });
  }

  performSearch(String param) {
    String path;
    if (searchType == 0) path = "userBase";
    if (searchType == 1) path = "userBase";
    if (searchType == 2) path = "postBase";

    print("we are here...");

    Firestore.instance
        .collection(path)
        .where("searchParam", arrayContains: param.toLowerCase())
        .getDocuments()
        .then((d) {
      for (var doc in d.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (mounted)
          setState(() {
            searchResult.addModel(model);
            isSearching = false;
          });
      }
      if (mounted)
        setState(() {
          isSearching = false;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Column(
        children: <Widget>[searchAppBar(), searchBody()],
      ),
    );
  }

  searchAppBar() {
    final borderDecoration = OutlineInputBorder(
        borderRadius: BorderRadius.circular(25),
        borderSide: BorderSide(color: white.withOpacity(.3)));

    return Column(
      children: <Widget>[
        Container(
          color: AppConfig.appColor,
          padding: 30.padAt(t: 38, r: 10, l: 10, b: 15),
          child: Row(
            children: <Widget>[
              BackButton(
                color: white,
              ),
              Flexible(
                  child: TextField(
                controller: searchController,
                style: textStyle(false, 14, white),
                decoration: InputDecoration(
                    contentPadding: 0.padAt(l: 10, r: 10, t: 15, b: 15),
                    border: borderDecoration,
                    enabledBorder: borderDecoration,
                    focusedBorder: borderDecoration,
                    filled: true,
                    fillColor: black.withOpacity(.3),
                    hintText: 'Search ...',
                    hintStyle: textStyle(false, 14, white.withOpacity(.5))),
              ))
            ],
          ),
        ),
        5.spaceHeight(),
        Container(
          //padding: 12.padAtRight(),
          color: black.withOpacity(.02),
          child: Column(
            children: [
              Container(
                height: 25,
                alignment: Alignment.center,
                child: Text(
                  "Apply Filter",
                  style: textStyle(false, 12, black.withOpacity(.7)),
                ),
              ),
              5.spaceHeight(),
              ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Container(
                  color: AppConfig.appColor.withOpacity(.6),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: List.generate(types.length, (p) {
                      bool active = searchType == p;
                      return Flexible(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              searchType = p;
                              searchResult.clear();
                              searchController.clear();
                            });
                          },
                          child: Container(
                              width: context.screenWidthDivide(4),
                              height: 40,
                              padding: 10.padAll(),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(active ? 15 : 0),
                                  color: AppConfig.appColor
                                      .withOpacity(active ? 1 : .0),
                                  border: Border.all(
                                      color:
                                          black.withOpacity(active ? 1 : 0.0),
                                      width: active ? 1 : 0.0)),
                              child: Center(
                                child: Text(
                                  types[p].toUpperCase(),
                                  style: textStyle(active, 12,
                                      white.withOpacity(active ? 1 : .8)),
                                ),
                              )),
                        ),
                      );
                    }),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  searchBody() {
    return Flexible(
      child: Container(
        //color: white,
        child: LayoutBuilder(
          builder: (ctx, b) {
            if (isSearching) return loadingLayout();
            return ListView.builder(
                itemCount: searchResult.length,
                padding: 0.padAll(),
                itemBuilder: (ctx, p) {
                  final thisUser = searchResult[p];
                  bool following =
                      userModel.amFollowing.contains(thisUser.getUserId());

                  return InkWell(
                    onTap: () {
                      //print("hello");
                      pushAndResult(
                          context,
                          ShowProfile(
                            thisUser: thisUser,
                          ));
                    },
                    child: Container(
                      padding: 8.padAll(),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom:
                                  BorderSide(color: black.withOpacity(.02)))),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: Row(
                              children: <Widget>[
                                imageHolder(50, thisUser.userImage),
                                10.spaceWidth(),
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        thisUser.fullName,
                                        style: textStyle(true, 14, black),
                                      ),
                                      5.spaceHeight(),
                                      Text(
                                        '@${thisUser.nickName}',
                                        style: textStyle(
                                            false, 12, black.withOpacity(.7)),
                                      ),
                                      if (thisUser.aboutMe.isNotEmpty) ...[
                                        5.spaceHeight(),
                                        Text(
                                          '${thisUser.aboutMe}',
                                          maxLines: 1,
                                          style: textStyle(false, 13, black),
                                        ),
                                      ]
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          FlatButton(
                            onPressed: () {
                              followThisUser(thisUser, following,
                                  onComplete: () {
                                setState(() {});
                              });
                            },
                            color: AppConfig.appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                                side: BorderSide(color: AppConfig.appColor)),
                            child: Center(
                                child: Text(
                              following ? "Unfollow" : "Follow",
                              style: textStyle(true, 12, white),
                            )),
                          ),
                        ],
                      ),
                    ),
                  );
                });
          },
        ),
      ),
    );
  }
}
