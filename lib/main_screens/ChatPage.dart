import 'dart:ui';

import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../ChatMain.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  //bool setup = false;
  var sub;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sub = chatMessageController.stream.listen((_) {
//      toastInAndroid("New Chat");
      if (mounted) setState(() {});
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
//    chatMessageController.close();
    sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: white, body: page());
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(30),
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Flexible(
                  child: Text(
                "Messages",
                style: textStyle(true, 25, black),
              )),
            ],
          ),
        ),
        addSpace(15),
        Expanded(
          flex: 1,
          child: !chatSetup
              ? loadingLayout()
              : lastMessages.isEmpty
                  ? emptyLayout(
                      ic_message, "No Conversation", "you have no messages yet")
                  : Container(
//            color: default_white,
                      child: ListView.builder(
                      itemBuilder: (c, p) {
                        BaseModel model = lastMessages[p];

                        return chatItem(model);
                      },
                      shrinkWrap: true,
                      itemCount: lastMessages.length,
                      padding: EdgeInsets.all(0),
                    )),
        )
      ],
    );
  }

  chatItem(BaseModel model) {
    String chatId = model.getString(CHAT_ID);
    BaseModel chatSetup = chatSetupInfo[chatId];
    if (chatSetup == null) return Container();

    String otherPersonId = getTheOtherId(chatSetup);
    BaseModel otherPerson = otherPeronInfo[otherPersonId];
    String name = otherPerson.getString(NAME);
    String image = otherPerson.getString(USER_IMAGE);

    int type = model.getInt(TYPE);
    bool myItem = model.myItem();

    bool read = model.getList(READ_BY).contains(getTheOtherId(chatSetup));
    bool myRead = model.getList(READ_BY).contains(userModel.getObjectId());
    if (!myRead && lastSeenMessages.contains(model.getObjectId())) {
      myRead = true;
    }
    List mutedList = userModel.getList(MUTED);
    int unread = unreadCounter[chatId] ?? 0;
    bool splitMode = chatId.endsWith("split");
    bool supportMode = chatId.endsWith("support");
    int members = chatSetup.getList(PARTIES).length - 1;

    if (supportMode) {
      name = "Support Team";
    }
    bool muted = userModel.getList(MUTED).contains(chatId);
    return new InkWell(
      onLongPress: () {
        showListDialog(
          context,
          [muted ? "Unmute Chat" : "Mute Chat", "Delete Chat"],
            (_) {
            if (_ == 0) {
              userModel.putInList(MUTED, chatId, !muted);
//            subscribeToTopic(chatId, muted);
              setState(() {});
            }
            if (_ == 1) {
              yesNoDialog(context, "Delete Chat?",
                  "Are you sure you want to delete this chat?", () {
                deleteChat(chatId);
              });
            }
          }, /* returnIndex: true*/
        );
      },
      onTap: () {
        if (!lastSeenMessages.contains(model.getObjectId()))
          lastSeenMessages.add(model.getObjectId());
        newUnreadMessageIds.removeWhere((id) => id == chatId);
        setState(() {});
        mainController.add(true);
        pushAndResult(
            context,
            ChatMain(
              chatId,
            ));
      },
      child: Container(
        color: (!myItem && !myRead) ? default_white : white,
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 60,
                  height: 60,
                  child: Card(
                    color: supportMode ? appColor.withOpacity(.5) : white,
                    elevation: 0,
                    clipBehavior: Clip.antiAlias,
                    shape: CircleBorder(
                        side: BorderSide(
                            color: appColor.withOpacity(.5), width: 2)),
                    child: Stack(children: [
                      Center(
                        child: Icon(
                          splitMode ? Icons.group : Icons.person,
                          color: appColor.withOpacity(.5),
                        ),
                      ),
                      CachedNetworkImage(
                        imageUrl: image,
                        fit: BoxFit.cover,
                        width: 60,
                        height: 60,
                      )
                    ]),
                  ),
                ),
                addSpaceWidth(10),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Text(
                              //"Emeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                              name,
                              maxLines: 1, overflow: TextOverflow.ellipsis,
                              style: textStyle(true, 18, black),
                            ),
                          ),
                          addSpaceWidth(5),
                          if (!myItem && !myRead && unread > 0)
                            Container(
                              width: 27,
                              height: 27,
                              child: (Card(
                                margin: EdgeInsets.all(0),
                                color: red0,
                                shape: CircleBorder(
//                            borderRadius: BorderRadius.circular(25),
                                    side: BorderSide(color: white, width: 3)),
                                child: Center(
                                    child: Text(
                                  "${unread > 9 ? "9+" : "$unread"}",
                                  style: textStyle(true, 12, white),
                                )),
                              )),
                            ),
                          addSpaceWidth(5),
                          Text(
                            getChatTime(model.getTime()),
                            style: textStyle(false, 12, black.withOpacity(.8)),
                            textAlign: TextAlign.end,
                          ),

                          //addSpaceWidth(5),
                        ],
                      ),
                      addSpace(5),
                      Row(
                        children: <Widget>[
                          if (myItem && read)
                            Container(
                                margin: EdgeInsets.only(right: 5),
                                child: Icon(
                                  Icons.remove_red_eye,
                                  size: 12,
                                  color: appColor.withOpacity(.5),
                                )),
                          Icon(
                            type == CHAT_TYPE_TEXT
                                ? Icons.message
                                : type == CHAT_TYPE_IMAGE
                                    ? Icons.camera_alt
                                    : type == CHAT_TYPE_REC
                                        ? Icons.mic
                                        : type == CHAT_TYPE_VIDEO
                                            ? Icons.videocam
                                            : type == CHAT_TYPE_LOCATION
                                                ? Icons.location_on
                                                : Icons.library_books,
                            color: black.withOpacity(.4),
                            size: 12,
                          ),
                          addSpaceWidth(5),
                          Flexible(
                            flex: 1,
                            child: Text(
                              chatRemoved(model)
                                  ? "This message has been removed"
                                  : type == CHAT_TYPE_TEXT
                                      ? model.getString(MESSAGE)
                                      : type == CHAT_TYPE_IMAGE
                                          ? "Photo"
                                          : type == CHAT_TYPE_REC
                                              ? "Rec ${model.getString(AUDIO_LENGTH)}"
                                              : type == CHAT_TYPE_VIDEO
                                                  ? "Video"
                                                  : type == CHAT_TYPE_LOCATION
                                                      ? "Location"
                                                      : "Document",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  textStyle(false, 14, black.withOpacity(.5)),
                            ),
                          ),
                          if (mutedList.contains(chatId)) addSpaceWidth(5),
                          if (mutedList.contains(chatId))
                            Image.asset(
                              ic_mute,
                              width: 15,
                              height: 15,
                              color: black.withOpacity(.3),
                            )
                        ],
                      ),
                      /*if (myFarm && !splitMode)
                        Container(
                          margin: EdgeInsets.only(top: 5),
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25)),
                              border: Border.all(
                                  color: black.withOpacity(.1), width: .5),
                              color: default_white),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [

//                            CachedNetworkImage(imageUrl: farmLogo,width: 15,height: 15,),
                              addSpaceWidth(5),
                              Flexible(
                                child: Text(
                                  farmName,
                                  style: textStyle(true, 12, black),
                                  maxLines: 1,
                                ),
                              )
                            ],
                          ),
                        ),*/
                      if (splitMode)
                        Container(
                          color: red0,
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "SPLIT GROUP",
                                style: textStyle(true, 12, white),
                              ),
                              addSpaceWidth(5),
                              Flexible(
                                  child: Text(
                                "$members ${members > 1 ? "Members" : "Member"}",
                                style:
                                    textStyle(false, 12, white.withOpacity(.8)),
                              ))
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
            addSpace(10),
            addLine(.5, black.withOpacity(.1), 0, 0, 0, 0)
          ],
        ),
      ),
    );
  }

  deleteChat(String chatId) {
    lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
//    stopListening.add(chatId);
    Map deleted = {
      CHAT_ID: chatId,
      TIME: DateTime.now().millisecondsSinceEpoch
    };
    List deletedList = userModel.getList(DELETED_CHATS);
    int index = deletedList.indexWhere((element) => element[CHAT_ID] == chatId);
    if (index == -1) {
      deletedList.add(deleted);
    } else {
      Map deleted = deletedList[index];
      deleted[TIME] = DateTime.now().millisecondsSinceEpoch;
      deletedList[index] = deleted;
    }
    userModel.put(DELETED_CHATS, deletedList);

    List muted = userModel.getList(MUTED);
    muted.add(chatId);
    userModel.put(MUTED, muted);
    userModel.updateItems();

//    subscribeToTopic(chatId,false);

    if (mounted) setState(() {});
  }
}
