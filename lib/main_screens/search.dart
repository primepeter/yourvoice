import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

import 'home.dart';
import 'show_person.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final searchController = TextEditingController();
  ScrollController scrollController = ScrollController();
  bool setup = false;
  bool showCancel = false;
  bool searching = false;
  FocusNode focusSearch = FocusNode();
  List allItems = [];
  List items = [];
  List selections;
  int currentPage = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    allItems = recentSearches;
    allItems.sort((m1, m2) => m1[OBJECT_ID].compareTo(m2[OBJECT_ID]));

    reload();
  }

  reload() {
    String search = searchController.text.trim().toLowerCase();
    items.clear();
    searching = true;
    if (mounted) setState(() {});
    print("here....");

    for (var item in allItems) {
      print("here....  $item");
      if (search.isNotEmpty) {
        List values = item.values.toList();
        bool exist = false;
        print("here....  $exist");

        for (var a in values) {
          if (a.toString().toLowerCase().contains(search)) {
            exist = true;
            break;
          }
        }
        if (!exist) {
          loadOnline(search);
          continue;
        }
      }
      items.add(item);
    }
    if (allItems.isEmpty){
      loadOnline(search);
    }

    else {
      searching = false;
      if (mounted) setState(() {});
    }

    if (mounted) setState(() {});
  }

  loadOnline(String search) async {
    print("searching..");
    searching = true;
    if (mounted) setState(() {});

    var doc1 = await Firestore.instance
        .collection(USER_BASE)
        .where(SEARCH_PARAM, arrayContains: search)
        .getDocuments();

    for (var doc in doc1.documents) {
      BaseModel model = BaseModel(doc: doc);
      //if (model.myItem()) continue;

      // if (model.getString(DATABASE_NAME) == USER_BASE &&
      //     !model.name.startsWith(search)) continue;
      // if (model.getString(DATABASE_NAME) == POST_BASE &&
      //     !model.message.startsWith(search)) continue;
      int p = items.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
      if (p == -1)
        items.add(doc.data());
      else
        items[p] = doc.data();
    }

    var doc2 = await Firestore.instance
        .collection(POST_BASE)
        .where(SEARCH_PARAM, arrayContains: search)
        .getDocuments();

    for (var doc in doc2.documents) {
      BaseModel model = BaseModel(doc: doc);
      //if (model.myItem()) continue;

      // if (model.getString(DATABASE_NAME) == USER_BASE &&
      //     !model.name.startsWith(search)) continue;
      // if (model.getString(DATABASE_NAME) == POST_BASE &&
      //     !model.message.startsWith(search)) continue;
      int p = items.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
      if (p == -1)
        items.add(doc.data());
      else
        items[p] = doc.data();
    }
    searching = false;
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //print(items.toList());

    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, selections);
        return;
      },
      child: KeyboardDismisser(
        gestures: [
          GestureType.onTap,
          //GestureType.onPanUpdateDownDirection,
          GestureType.onPanUpdateAnyDirection,
        ],
        child: Scaffold(
          body: page(),
          backgroundColor: white,
          key: _scaffoldKey,
        ),
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(40),
        Row(
          children: <Widget>[
            InkWell(
                onTap: () {
                  Navigator.pop(context, selections);
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                      child: Icon(
                    Icons.keyboard_backspace,
                    color: black,
                    size: 25,
                  )),
                )),
            Text(
              "Search",
              style: textStyle(true, 20, black),
            ),
            addSpaceWidth(10),
            Flexible(
              child: Container(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(5, (p) {
                      String title = "All";
                      int count = 0;
                      bool active = currentPage == p;

                      List<BaseModel> listItem = myNotifications;

                      if (p == 1) {
                        title = "People";
                        listItem = myNotifications
                            .where((e) =>
                                e.getInt(TYPE) == NOTIFY_LIKES ||
                                e.getInt(TYPE) == NOTIFY_COMMENT_LIKE ||
                                e.getInt(TYPE) == NOTIFY_REPLY_LIKE)
                            .toList();
                      }

                      if (p == 2) {
                        title = "Posts";
                        listItem = myNotifications
                            .where((e) => e.getInt(TYPE) == NOTIFY_COMMENT)
                            .toList();
                        count = listItem.length;
                      }

                      if (p == 3) {
                        title = "Books";
                        listItem = myNotifications
                            .where((e) => e.getInt(TYPE) == NOTIFY_MENTIONS)
                            .toList();
                      }

                      if (p == 4) {
                        title = "Courses";
                        listItem = myNotifications
                            .where((e) => e.getInt(TYPE) == NOTIFY_MENTIONS)
                            .toList();
                      }

                      return GestureDetector(
                        onTap: () {
                          currentPage = p;
                          setState(() {});
                        },
                        child: Container(
                          height: 35,
                          padding: EdgeInsets.only(
                              top: 4, bottom: 4, right: 8, left: 8),
                          margin: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: appColor.withOpacity(active ? 1 : .2)),
                          child: Center(
                              child: Text(
                            title,
                            style:
                                textStyle(active, 14, active ? white : black),
                          )),
                        ),
                      );
                    }),
                  ),
                ),
              ),
            ),
          ],
        ),
        addSpace(5),
        Container(
          height: 45,
          margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
          decoration: BoxDecoration(
              color: white.withOpacity(.8),
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: black.withOpacity(.1), width: 1)),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              addSpaceWidth(10),
              Icon(
                Icons.search,
                color: appColor.withOpacity(.7),
                size: 17,
              ),
              addSpaceWidth(10),
              new Flexible(
                flex: 1,
                child: new TextField(
                  textInputAction: TextInputAction.search,
                  textCapitalization: TextCapitalization.sentences,
                  autofocus: false,
                  onSubmitted: (_) {
                    //reload();
                  },
                  decoration: InputDecoration(
                      hintText: "Search",
                      hintStyle: textStyle(
                        false,
                        18,
                        appColor.withOpacity(.7),
                      ),
                      border: InputBorder.none,
                      isDense: true),
                  style: textStyle(false, 16, black),
                  controller: searchController,
                  cursorColor: black,
                  cursorWidth: 1,
                  focusNode: focusSearch,
                  keyboardType: TextInputType.text,
                  onChanged: (s) {
                    showCancel = s.trim().isNotEmpty;
                    setState(() {});
                    reload();
                  },
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    focusSearch.unfocus();
                    showCancel = false;
                    searchController.text = "";
                  });
                  reload();
                },
                child: showCancel
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                        child: Icon(
                          Icons.close,
                          color: black,
                          size: 20,
                        ),
                      )
                    : new Container(),
              ),
              addSpaceWidth(10),
              if (searching /* && items.isEmpty*/)
                Container(
                  height: 45,
                  margin: EdgeInsets.only(left: 10),
                  child: Center(
                    child: SizedBox(
                      height: 16,
                      width: 16,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        valueColor: AlwaysStoppedAnimation(appColor),
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
        Expanded(
            child: ListView.builder(
          controller: scrollController,
          itemBuilder: (ctx, p) {
            final item = currentList[p];
            BaseModel model = BaseModel(items: item);

            String database = model.getString(DATABASE_NAME);

            if (database == USER_BASE) {
              bool followed =
                  userModel.amFollowing.contains(model.getObjectId());
              return Container(
                  margin: EdgeInsets.only(left: 10, right: 10, bottom: 2),
                  //padding: EdgeInsets.all(10),
                  child: GestureDetector(
                    onTap: () {
                      addToRecent(model);
                      pushAndResult(
                          context,
                          ShowPerson(
                            model: model,
                          ));
                    },
                    child: Container(
                      color: transparent,
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Stack(
                              children: [
                                Container(
                                  height: 40,
                                  width: 40,
                                  color: appColor.withOpacity(.6),
                                ),
                                if (model.userImage.isNotEmpty)
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: CachedNetworkImage(
                                      imageUrl: model.userImage,
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          addSpaceWidth(10),
                          Column(
                            children: [Text(model.getString(NAME))],
                          ),
                          Spacer(),
                          FlatButton(
                            onPressed: () {
                              pushReplaceAndResult(
                                  context,
                                  ShowPerson(
                                    model: model,
                                  ));
                              /*followThisUser(model, followed, onComplete: () {
                                     setState(() {});
                                   });*/
                            },
                            color: appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: BorderSide(color: appColor)),
                            child: Center(
                                child: Text(
                              followed ? "Unfollow" : "Follow",
                              style: textStyle(true, 12, white),
                            )),
                          )
                        ],
                      ),
                    ),
                  ));
            }
            return PostItem(
                p: p,
                heroTag: getRandomId(),
                model: model,
                stateCallBack: (_) {
                  if (_ == null) {
                    items.remove(item);
                    setState(() {});
                    return;
                  }
                  currentList[p] = _;
                  setState(() {});
                });
          },
          itemCount: currentList.length,
          padding: 5.padAll(),
          //physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
        )),
      ],
    );
  }

  List get currentList {
    if (currentPage == 0) return items;
    if (currentPage == 1)
      return items.where((e) => e[DATABASE_NAME] == USER_BASE).toList();
    if (currentPage == 2)
      return items.where((e) => e[DATABASE_NAME] == POST_BASE).toList();
    //if(currentPage==1)
    return items.where((e) => e[DATABASE_NAME] == LIBRARY_BASE).toList();
  }

  addToRecent(BaseModel model) {
    Map data = {
      OBJECT_ID: model.getObjectId(),
      DATABASE_NAME: model.getString(DATABASE_NAME)
    };
    List recent = userModel.getList(RECENT_SEARCH);
    int p = recent.indexWhere((e) => e[OBJECT_ID] == data[OBJECT_ID]);
    if (p == -1)
      recent.add(data);
    else
      recent[p] = data;
    userModel
      ..put(RECENT_SEARCH, recent)
      ..updateItems();

    int p2 = allItems.indexWhere((e) => e[OBJECT_ID] == data[OBJECT_ID]);
    if (p2 == -1)
      allItems.add(model.items);
    else
      allItems[p2] = model.items;

    setState(() {});
  }
}
