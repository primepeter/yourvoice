import 'package:acclaim/app/app.dart';
import 'package:flutter/material.dart';

class Archives extends StatefulWidget {
  @override
  _ArchivesState createState() => _ArchivesState();
}

class _ArchivesState extends State<Archives> {
  @override
  Widget build(BuildContext context) {
    return emptyLayoutX(
      "assets/images/archive.png",
      "No Archive",
      "You have not saved any item yet.",
    );
  }
}
