import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_config.dart';
import 'package:flutter/material.dart';

import 'archives.dart';
import 'courses.dart';
import 'library.dart';

class Tutorials extends StatefulWidget {
  final int position;

  const Tutorials({Key key, this.position = 0}) : super(key: key);
  @override
  _TutorialsState createState() => _TutorialsState();
}

class _TutorialsState extends State<Tutorials> {
  int currentPage = 0;
  PageController vp;

  @override
  initState() {
    super.initState();
    currentPage = widget.position;
    vp = PageController(initialPage: widget.position);
  }

  @override
  dispose() {
    super.dispose();
  }

  appBar() {
    return Container(
      padding: 5.padAt(
        t: 30,
      ),
      color: AppConfig.appColor,
      alignment: Alignment.center,
      child: Row(
        children: <Widget>[
          BackButton(
            color: white,
          ),
          Flexible(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(3, (p) {
                String assetPath = 'assets/images/';
                String key;

                if (p == 0) key = "library";
                if (p == 1) key = "courses";
                if (p == 2) key = "archive";

                bool active = p == currentPage;
                String path = "$assetPath$key.png";
                String title = context.trans(key);

                return Flexible(
                  child: InkWell(
                    onTap: () {
                      vp.jumpToPage(p);
                      print(p);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      //height: 50,
                      padding: 5.padAll(),
                      width: context.screenWidthDivide(3),
                      color: black.withOpacity(active ? 0.5 : .0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            path,
                            height: 24,
                            width: 24,
                          ),
                          5.spaceHeight(),
                          Text(
                            title,
                            style: textStyle(true, 12, white),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  tutorialPages() {
    return Flexible(
      child: PageView(
        controller: vp,
        onPageChanged: (p) {
          setState(() {
            currentPage = p;
          });
        },
        children: [
          Library(),
          Courses(),
          Archives(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [appBar(), tutorialPages()],
      ),
    );
  }
}
