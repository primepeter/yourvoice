import 'dart:async';

import 'package:acclaim/AddPost.dart';
import 'package:acclaim/CreateReport.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/like_animation.dart';
import 'package:acclaim/preview_post_images.dart';
import 'package:acclaim/services/YouTubeService.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'ShowPost.dart';
import 'search.dart';
import 'show_person.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  int currentPage = 0;
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;
  bool isLiked = false;
  List<StreamSubscription> subs = [];
  ScrollController liveScroll = ScrollController();
  final liveVp = PageController();

  AnimationController _animationController;
  bool isPlaying = false;

  @override
  initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 150) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
      }
    });

    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 3) return;
      scrollTotallyUp(scrollController);
    });
    subs.add(scrollSub);

    loadItems(false);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];
  List importantNews = [];
  List recentNews = [];

  bool setup = false;

  loadItems(bool isNew) async {
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection(POST_BASE)
        //.where(PARTIES, arrayContains: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ]).get();

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      if (model.hidden.contains(userModel.getUserId())) continue;
      int p = items.indexWhere(
          (e) => BaseModel(items: e).getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model.items);
      else
        items[p] = model.items;
    }

    importantNews =
        items.where((element) => element[IS_IMPORTANT] ?? false).toList();
    recentNews = items;

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.docs.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Stack(
      children: [listItems(), LikeAnimation()],
    );
  }

  listItems() {
    return Builder(
      builder: (c) {
        if (!setup) return loadingLayout();
        if (recentNews.isEmpty)
          return Column(
            children: [
              storiesItem(),
              addOns(),
              addSpace(10),
              Container(
                child: Row(
                  children: [
                    /*Icon(Icons.bar_chart),
                    addSpaceWidth(10),*/
                    Text(
                      'Popular Reads',
                      style: textStyle(true, 16, black),
                    ),
                  ],
                ),
                padding: EdgeInsets.all(10),
                //margin: EdgeInsets.all(10),
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    color: black.withOpacity(.0),
                    border: Border.all(color: black.withOpacity(.02)),
                    borderRadius: BorderRadius.circular(8)),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      GetAsset.images('blog.png'),
                      height: 60,
                      width: 60,
                      fit: BoxFit.cover,
                    ),
                    addSpace(10),
                    Text(
                      'No Blog Post.',
                      style: textStyle(false, 16, black.withOpacity(.5)),
                      textAlign: TextAlign.center,
                    ),
                    addSpace(10),
                    FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        color: black.withOpacity(.2),
                        onPressed: () {
                          setup = false;
                          setState(() {});
                          loadItems(true);
                        },
                        child: Text(
                          'Refresh',
                          style: textStyle(true, 16, black),
                        ))
                  ],
                ),
              ),
            ],
          );

        return SmartRefresher(
          controller: refreshController,
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropHeader(),
          footer: ClassicFooter(
            noDataText:
                items.length < 9 ? '' : "Nothing more for now, check later...",
            textStyle: textStyle(false, 12, black.withOpacity(.7)),
          ),
          onLoading: () {
            loadItems(false);
          },
          onRefresh: () {
            loadItems(true);
          },
          child: SingleChildScrollView(
            child: Column(
              children: [
                storiesItem(),
                addOns(),
                addSpace(10),
                Container(
                  child: Row(
                    children: [
                      /*Icon(Icons.bar_chart),
                    addSpaceWidth(10),*/
                      Text(
                        'Recent News',
                        style: textStyle(true, 18, black),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.all(10),
                  //margin: EdgeInsets.all(10),
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    color: black.withOpacity(.0),
                    border: Border.all(color: black.withOpacity(.02)),
                    //borderRadius: BorderRadius.circular(8)
                  ),
                ),
                Column(
                  children: List.generate(recentNews.length, (p) {
                    BaseModel model = BaseModel(items: recentNews[p]);
                    //int count = items['count'];
                    final photos = model.getListModel(IMAGES);
                    String icon = '';
                    if (photos.isNotEmpty)
                      icon = photos[0].getString(IMAGE_URL);
                    String body = model.getString(MESSAGE);
                    int mins = body.length ~/ 200;

                    final likes = model.getList(LIKES);
                    final bookmarks = model.getList(BOOKMARKS);
                    bool liked = likes.contains(userModel.getUserId());
                    bool bookmarked = bookmarks.contains(userModel.getUserId());

                    // List likes = model.getList(LIKES);
                    // bool liked = likes.contains(userModel.getUserId());

                    List disLikes = model.getList(DISLIKES);
                    bool disLiked = disLikes.contains(userModel.getUserId());

                    double performance =
                        likes.length / (likes.length + disLikes.length);
                    performance = performance.clamp(0.0, 1.0);

                    double rating = calculateRating(performance);

                    print(rating);

                    return GestureDetector(
                      onLongPress: () {
                        if (!isAdmin) return;
                        model
                          ..put(IS_IMPORTANT, true)
                          ..updateItems();
                        setState(() {});
                        showMessage(
                            context,
                            Icons.check,
                            appColor,
                            "Marked Important",
                            "This Post has been added to important messages..");
                      },
                      onTap: () {
                        pushAndResult(
                            context,
                            ShowPost(
                              model: model,
                              heroTag: "lol...sdjsf",
                            ));
                      },
                      child: Row(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: 150,
                                height: 120,
                                margin: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        width: .5,
                                        color: black.withOpacity(.05))),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Stack(
                                    children: [
                                      CachedNetworkImage(
                                        imageUrl: icon,
                                        width: 250,
                                        height: 140,
                                        fit: BoxFit.cover,
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                colors: [
                                              /*appColor.withOpacity(.6),
                                            appColor_light.withOpacity(.6),*/
                                              black.withOpacity(.1),
                                              black.withOpacity(.08),
                                            ],
                                                begin: Alignment.bottomCenter,
                                                end: Alignment.topCenter)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          addSpaceWidth(10),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  body,
                                  maxLines: 3,
                                  style: textStyle(true, 16, black),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      '$mins min read',
                                      style: textStyle(
                                          false, 12, black.withOpacity(.8)),
                                    ),
                                    Container(
                                      height: 5,
                                      width: 5,
                                      margin: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: black,
                                      ),
                                    ),
                                    Text(
                                      getTimeAgo(model.getTime()),
                                      style: textStyle(
                                          false, 12, black.withOpacity(.8)),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        bookmarked = !bookmarked;
                                        model
                                          ..putInList(BOOKMARKS,
                                              userModel.getUserId(), bookmarked)
                                          ..updateItems();
                                        items[p] = model.items;
                                        //if (!bookmarked) items.removeAt(p);

                                        int ps = bookmarkList.indexWhere((e) =>
                                        e.getObjectId() == model.getObjectId());

                                        if (bookmarked) {
                                          if (ps == -1)
                                            bookmarkList.add(model);
                                          else
                                            bookmarkList[ps] = model;
                                        } else {
                                          bookmarkList.removeWhere((e) =>
                                          e.getObjectId() == model.getObjectId());
                                        }
                                        stateController.add(true);
                                        setState(() {});


                                      },
                                      child: Container(
                                          width: 30,
                                          height: 30,
                                          child: Icon(
                                            bookmarked
                                                ? Icons.bookmark
                                                : Icons.bookmark_border,
                                            color: bookmarked
                                                ? appColor
                                                : black.withOpacity(.5),
                                          )),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        sharePost(model);
                                      },
                                      child: Image.asset(
                                        GetAsset.images('share.png'),
                                        height: 20,
                                        width: 20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        model
                                          ..putInList(DISLIKES,
                                              userModel.getUserId(), false)
                                          ..updateItems();
                                        model
                                          ..putInList(LIKES,
                                              userModel.getUserId(), !liked)
                                          ..updateItems();
                                        items[p] = model.items;
                                        setState(() {});
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Column(
                                          children: [
                                            Image.asset(
                                              GetAsset.images(liked
                                                  ? 'like1.png'
                                                  : 'like.png'),
                                              height: 20,
                                              width: 20,
                                              fit: BoxFit.cover,
                                            ),
                                            addSpace(2),
                                            if (likes.length > 0)
                                              Text(
                                                formatToK(likes.length),
                                                style:
                                                    textStyle(false, 14, black),
                                              ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        model
                                          ..putInList(DISLIKES,
                                              userModel.getUserId(), !disLiked)
                                          ..updateItems();
                                        model
                                          ..putInList(LIKES,
                                              userModel.getUserId(), false)
                                          ..updateItems();

                                        items[p] = model.items;
                                        setState(() {});
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Column(
                                          children: [
                                            Image.asset(
                                              GetAsset.images(disLiked
                                                  ? 'dislike1.png'
                                                  : 'dislike.png'),
                                              height: 20,
                                              width: 20,
                                              fit: BoxFit.cover,
                                            ),
                                            addSpace(2),
                                            if (disLikes.length > 0)
                                              Text(
                                                formatToK(disLikes.length),
                                                style:
                                                    textStyle(false, 14, black),
                                              ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }),
                ),
                addSpace(90)
              ],
            ),
          ),
          /*child: ListView.builder(
            controller: scrollController,
            padding: EdgeInsets.only(
              top: 10,
            ),
            itemCount: items.length,
            itemBuilder: (c, p) {
              Map item = items[p];
              BaseModel model = BaseModel(items: item);

              return Column(
                children: [
                  //if (p == 0) storiesLayout(),
                  PostItem(
                      p: p,
                      heroTag: getRandomId(),
                      model: model,
                      stateCallBack: (_) {
                        if (_ == null) {
                          items.remove(item);
                          setState(() {});
                          return;
                        }

                        items[p] = _;
                        setState(() {});
                      }),
                ],
              );
            },
          ),*/
        );
      },
    );
  }

  List get statistics {
    return [
      {
        'title': 'Members',
        'icon': GetAsset.images('members.png'),
        'count': usersTotal.length
      },
      {
        'title': 'Online',
        'icon': GetAsset.images('online.png'),
        'count': (usersOnline.length)
      },
    ];
  }

  storiesItem() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              children: [
                /*Icon(Icons.bar_chart),
                addSpaceWidth(10),*/
                Text(
                  'Important Messages',
                  style: textStyle(true, 18, black),
                ),
              ],
            ),
            padding: EdgeInsets.all(10),
            //margin: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: black.withOpacity(.00),
              border: Border.all(color: black.withOpacity(.02)),
              // borderRadius: BorderRadius.circular(8)
            ),
          ),
          //addSpace(10),
          if (importantNews.isNotEmpty)
            SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List.generate(importantNews.length, (p) {
                    Map items = importantNews[p];
                    BaseModel model = BaseModel(items: items);
                    final photos = model.getListModel(IMAGES);
                    String icon = '';
                    if (photos.isNotEmpty)
                      icon = photos[0].getString(IMAGE_URL);
                    String body = model.getString(MESSAGE);
                    int mins = body.length ~/ 200;

                    final likes = model.getList(LIKES);
                    final bookmarks = model.getList(BOOKMARKS);
                    bool liked = likes.contains(userModel.getUserId());
                    List disLikes = model.getList(DISLIKES);
                    bool disLiked = disLikes.contains(userModel.getUserId());

                    return GestureDetector(
                      onLongPress: () {
                        if (!isAdmin) return;
                        model
                          ..put(IS_IMPORTANT, false)
                          ..updateItems();
                        setState(() {});
                        showMessage(
                            context,
                            Icons.check,
                            appColor,
                            "UnMarked Important",
                            "This Post has been removed from important messages..");
                      },
                      onTap: () {
                        pushAndResult(
                            context,
                            ShowPost(
                              model: model,
                              heroTag: "lol...sdjsf",
                            ));
                      },
                      child: Container(
                        width: 250,
                        height: 180,
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Center(
                                child: Container(
                                  width: 250,
                                  height: 140,
                                  margin: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      border: Border.all(
                                          width: .5,
                                          color: black.withOpacity(.05))),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20),
                                    child: Stack(
                                      children: [
                                        // Image.asset(
                                        //   icon,
                                        //   width: 250,
                                        //   height: 140,
                                        //   fit: BoxFit.cover,
                                        // ),
                                        CachedNetworkImage(
                                          imageUrl: icon,
                                          width: 250,
                                          height: 140,
                                          fit: BoxFit.cover,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  colors: [
                                                /*appColor.withOpacity(.6),
                                                appColor_light.withOpacity(.6),*/
                                                black.withOpacity(.6),
                                                black.withOpacity(.2),
                                              ],
                                                  begin: Alignment.bottomCenter,
                                                  end: Alignment.topCenter)),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Container(
                                            padding: EdgeInsets.all(10),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  '$mins min read',
                                                  style: textStyle(false, 12,
                                                      white.withOpacity(.8)),
                                                ),
                                                Text(
                                                  body,
                                                  maxLines: 2,
                                                  style: textStyle(
                                                      true, 16, white),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: RaisedButton(
                                shape: CircleBorder(),
                                onPressed: () {
                                  model
                                    ..putInList(
                                        DISLIKES, userModel.getUserId(), false)
                                    ..updateItems();
                                  model
                                    ..putInList(
                                        LIKES, userModel.getUserId(), !liked)
                                    ..updateItems();
                                  importantNews[p] = model.items;
                                  setState(() {});
                                },
                                color: white,
                                elevation: 6,
                                child: Icon(
                                  liked
                                      ? Icons.favorite
                                      : Icons.favorite_border,
                                  color: liked ? red0 : black.withOpacity(.8),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
                )),
          addSpace(10),
        ],
      ),
    );
  }

  addOns() {
    return Container(
      child: Column(
        children: [
          /*Container(
            child: Row(
              children: [
                Icon(Icons.bar_chart),
                addSpaceWidth(10),
                Text(
                  'Statistics',
                  style: textStyle(true, 13, black),
                ),
              ],
            ),
            padding: EdgeInsets.all(10),
            //margin: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: black.withOpacity(.04),
                border: Border.all(color: black.withOpacity(.02)),
                borderRadius: BorderRadius.circular(8)),
          ),*/
          Container(
            margin: EdgeInsets.all(10),
            child: Row(
              children: List.generate(statistics.length, (index) {
                Map items = statistics[index];
                int count = items['count'];
                String title = items['title'];
                String icon = items['icon'];

                return Flexible(
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: .5, color: black.withOpacity(.05))),
                    child: Column(
                      children: [
                        Text(formatToK(count),
                            style: textStyle(true, 22, black)),
                        addLine(.5, black.withOpacity(.09), 0, 5, 0, 5),
                        Text(title,
                            style: textStyle(false, 12, black.withOpacity(.6)))
                      ],
                    ),
                  ),
                );
              }),
            ),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: black.withOpacity(.0),
                border: Border.all(color: black.withOpacity(.0)),
                borderRadius: BorderRadius.circular(8)),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class LiveItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: getScreenHeight(context) * .5,
      width: double.infinity,
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Stack(
          children: [
            CachedNetworkImage(
              imageUrl: userModel.userImage,
              height: getScreenHeight(context) * .5,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                appColor.withOpacity(.4),
                appColor.withOpacity(.9),
                appColor_light.withOpacity(.9),
              ], stops: [
                0,
                0.3,
                1.0
              ], begin: Alignment.center, end: Alignment.bottomCenter)),
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      FlatButton(
                        onPressed: () {
                          pushAndResult(context, Search());
                        },
                        height: 25,
                        minWidth: 25,
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Icon(
                          Icons.cloud_download_outlined,
                          color: white,
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          pushAndResult(context, Search());
                        },
                        height: 25,
                        minWidth: 25,
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Icon(
                          Icons.favorite_border,
                          color: white,
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: appColor_light,
                            borderRadius: BorderRadius.circular(25)),
                        padding: EdgeInsets.all(4),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            addSpaceWidth(5),
                            Image.asset(
                              ic_church,
                              fit: BoxFit.cover,
                              height: 18,
                              width: 18,
                              color: white,
                            ),
                            addSpaceWidth(8),
                            Text(
                              "Church",
                              style: textStyle(false, 14, white),
                            ),
                            addSpaceWidth(5),
                          ],
                        ),
                      ),
                      addSpaceWidth(10),
                      Container(
                        decoration: BoxDecoration(
                            color: red1,
                            borderRadius: BorderRadius.circular(25)),
                        padding: EdgeInsets.all(4),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            addSpaceWidth(5),
                            Container(
                              height: 12,
                              width: 12,
                              decoration: BoxDecoration(
                                  color: white, shape: BoxShape.circle),
                            ),
                            addSpaceWidth(10),
                            Text(
                              "Live",
                              style: textStyle(false, 14, white),
                            ),
                            addSpaceWidth(10),
                          ],
                        ),
                      ),
                      addSpaceWidth(10),
                    ],
                  ),
                  Text(
                    "Spur One Another",
                    style: textStyle(true, 25, white),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "What happens when you consistently intercede on behalf of another brethren in Prayer?",
                          style: textStyle(false, 14, white),
                        ),
                      ),
                      addSpaceWidth(30),
                      GestureDetector(
                        onTap: () {
                          YouTubeService.fetchLiveStream();
                        },
                        child: Container(
                          height: 50,
                          width: 50,
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.play_arrow,
                            color: appColor,
                          ),
                          decoration: BoxDecoration(
                              color: white, shape: BoxShape.circle),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PostItem extends StatelessWidget {
  const PostItem({
    Key key,
    @required this.p,
    @required this.heroTag,
    @required this.model,
    @required this.stateCallBack,
    this.commentEnabled = true,
  }) : super(key: key);

  final int p;
  final bool commentEnabled;
  final String heroTag;
  final BaseModel model;
  final Function(Map map) stateCallBack;

  @override
  Widget build(BuildContext context) {
    Color iconColor = black.withOpacity(.7);
    double iconSize = 20;

    String image = model.userImage;
    String name = model.name;
    String username = model.nickName;
    String time = getTimeAgo(model.getTime());
    String message = model.getString(MESSAGE);
    final taggedPersons = model.getList(TAGGED_PERSONS);
    final likes = model.getList(LIKES);
    final comments = model.getList(COMMENTS);
    final bookmarks = model.getList(BOOKMARKS);
    bool liked = likes.contains(userModel.getUserId());
    bool bookmarked = bookmarks.contains(userModel.getUserId());
    String heroTag = "${this.heroTag}_show_${p}post${model.getObjectId()}$p";

    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      child: Material(
        color: white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
            side: BorderSide(color: black.withOpacity(.03))),
        elevation: 5,
        shadowColor: black.withOpacity(.3),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      pushAndResult(
                          context,
                          ShowPerson(
                            model: model,
                          ));
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      padding: EdgeInsets.all(1),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: .5, color: black.withOpacity(.1)),
                          color: white,
                          shape: BoxShape.circle),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(25),
                        child: Stack(
                          children: [
                            Container(
                              height: 40,
                              width: 40,
                              color: appColor.withOpacity(.6),
                            ),
                            if (image.isNotEmpty)
                              ClipRRect(
                                borderRadius: BorderRadius.circular(25),
                                child: CachedNetworkImage(
                                  imageUrl: image,
                                  fit: BoxFit.cover,
                                  height: 40,
                                  width: 40,
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  addSpaceWidth(10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          username,
                          style: textStyle(true, 14, black),
                        ),
                        Text(
                          time,
                          style: textStyle(false, 11, black.withOpacity(.5)),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                      onTap: () {
                        showListDialog(context, [
                          if (model.myItem() || isAdmin) "Edit",
                          if (model.myItem() || isAdmin) "Delete",
                          if (!model.myItem()) "Report"
                        ], (_) {
                          if (_ == "Delete") {
                            model.deleteItem();
                            allPosts.remove(model.items);
                            stateCallBack(null);
                          }
                          if (_ == "Edit") {
                            pushAndResult(
                                context,
                                AddPost(
                                  model: model,
                                ));
                          }
                          if (_ == "Report") {
                            pushAndResult(
                                context,
                                CreateReport(
                                  item: model,
                                ), result: (_) {
                              if (null != _ || !!_) return;
                              stateCallBack(null);
                            });
                          }
                        });
                      },
                      child: Icon(Icons.more_horiz))
                ],
              ),
              addSpace(10),
              DisplayImages(
                key: ValueKey(model.getObjectId()),
                tag: heroTag,
                feedPosition: p,
                model: model,
                callBack: (_) {
                  stateCallBack(_);
                },
              ),
              addSpace(10),
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      model
                        ..putInList(BOOKMARKS, userModel.userId, !bookmarked)
                        ..updateItems();
                      stateCallBack(model.items);
                    },
                    child: Container(
                      padding: EdgeInsets.all(5),
                      color: transparent,
                      child: Icon(
                        bookmarked ? Icons.bookmark : Icons.bookmark_border,
                        color: bookmarked ? appColor : iconColor,
                        size: iconSize,
                      ),
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      if (!commentEnabled) return;
                      /* pushAndResult(
                          context,
                          ShowComments(
                            thisPost: model,
                            heroTag: heroTag,
                          ), result: (_) {
                        if (null == _) return;
                        if (_ == "deleted") {
                          stateCallBack(null);
                          return;
                        }
                        stateCallBack(_.postItems);
                      });*/
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: black.withOpacity(.0),
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          addSpaceWidth(4),
                          Text(
                            comments.length > 0
                                ? comments.length.toString()
                                : "",
                            style: textStyle(false, 14, black),
                          ),
                          addSpaceWidth(6),
                          Image.asset(
                            ic_comment,
                            height: iconSize,
                            width: iconSize,
                            fit: BoxFit.cover,
                            color: iconColor,
                          ),
                          addSpaceWidth(4),
                        ],
                      ),
                    ),
                  ),
                  addSpaceWidth(10),
                  GestureDetector(
                    onTap: () {
                      model
                        ..putInList(LIKES, userModel.userId, !liked)
                        ..updateItems();
                      if (!liked) flareController.add(model);
                      stateCallBack(model.items);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: liked ? red0 : black.withOpacity(.05),
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          addSpaceWidth(4),
                          Text(
                            likes.length > 0 ? likes.length.toString() : "",
                            style: textStyle(liked, 14, liked ? white : black),
                          ),
                          addSpaceWidth(6),
                          Image.asset(
                            ic_like,
                            height: iconSize,
                            width: iconSize,
                            fit: BoxFit.cover,
                            color: liked ? white : iconColor,
                          ),
                          addSpaceWidth(4),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              addSpace(10),
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      clickTagged(context, model);
                    },
                    child: Container(
                      width: 35.0 * taggedPersons.length.clamp(0, 3),
                      height: taggedPersons.isEmpty ? 0 : 50,
                      child: Stack(
                        children: List.generate(
                            taggedPersons.length.clamp(0, 3), (index) {
                          int max = taggedPersons.length.clamp(0, 3);
                          BaseModel model =
                              BaseModel(items: taggedPersons[index]);
                          return Container(
                            height: 40,
                            width: 40,
                            margin: EdgeInsets.only(right: index * 30.0),
                            padding: EdgeInsets.all(1),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: .5, color: black.withOpacity(.1)),
                                color: white,
                                shape: BoxShape.circle),
                            child: Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(25),
                                  child: Stack(
                                    children: [
                                      Container(
                                        height: 40,
                                        width: 40,
                                        color: appColor.withOpacity(.6),
                                      ),
                                      if (model.userImage.isNotEmpty)
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          child: CachedNetworkImage(
                                            imageUrl: model.userImage,
                                            height: 40,
                                            width: 40,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                    ],
                                  ),
                                ),
                                if (max - 1 > 3)
                                  Container(
                                    height: 40,
                                    width: 40,
                                    child: Center(
                                        child: Text(
                                      "${max - index}+",
                                      style: textStyle(true, 10, white),
                                    )),
                                    decoration: BoxDecoration(
                                        color: black.withOpacity(.4),
                                        shape: BoxShape.circle),
                                  )
                              ],
                            ),
                          );
                        }),
                      ),
                    ),
                  ),
                  addSpaceWidth(10),
                  if (message.isNotEmpty) Flexible(child: ReadMoreText(message))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class DisplayImages extends StatefulWidget {
  final BaseModel model;
  final String tag;
  final int feedPosition;
  final Function callBack;

  const DisplayImages(
      {Key key, this.model, this.feedPosition, this.callBack, this.tag})
      : super(key: key);
  @override
  _DisplayImagesState createState() => _DisplayImagesState();
}

class _DisplayImagesState extends State<DisplayImages> {
  int currentPage = 0;
  final vp = PageController();
  List images = [];
  BaseModel model;

  @override
  initState() {
    super.initState();
    model = widget.model;
    images = widget.model.getList(IMAGES);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 320,
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: PageView.builder(
              controller: vp,
              itemCount: images.length,
              scrollDirection: Axis.horizontal,
              onPageChanged: (p) {
                currentPage = p;
                setState(() {});
              },
              itemBuilder: (c, p) {
                Map items = images[p];
                BaseModel model = BaseModel(items: items);
                bool isVideo = model.isVideo;
                String imageUrl =
                    model.getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                String heroTag = "${widget.tag}image${widget.feedPosition}$p";
                bool liked =
                    this.model.getList(LIKES).contains(userModel.getUserId());

                return GestureDetector(
                  onDoubleTap: () {
                    this.model
                      ..putInList(LIKES, userModel.userId, !liked)
                      ..updateItems();
                    if (!liked) flareController.add(this.model);
                    widget.callBack(this.model.items);
                  },
                  onTap: () {
                    pushAndResult(
                        context,
                        PreviewPostImages(
                          images.map((e) => BaseModel(items: e)).toList(),
                          [],
                          indexOf: p,
                        ));
                  },
                  child: Hero(
                    tag: heroTag,
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: CachedNetworkImage(
                            imageUrl: imageUrl,
                            fit: BoxFit.cover,
                            height: 320,
                            width: double.infinity,
                            placeholder: (c, s) {
                              return placeHolder(320);
                            },
                          ),
                        ),
                        if (isVideo)
                          Container(
                            height: 300,
                            child: Center(
                              child: Container(
                                height: 50,
                                width: 50,
                                padding: 3.padAll(),
                                margin: 6.padAll(),
                                decoration: BoxDecoration(
                                  color: black.withOpacity(.8),
                                  border: Border.all(color: white, width: 3),
                                  shape: BoxShape.circle,
                                ),
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.play_arrow,
                                  color: white,
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          if (images.length > 1)
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.all(5),
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: black.withOpacity(.7),
                    borderRadius: BorderRadius.circular(25),
                    border: Border.all(color: white.withOpacity(.5))),
                child: DotsIndicator(
                  dotsCount: images.length,
                  position: currentPage,
                  decorator: DotsDecorator(
                      activeColor: white,
                      color: white.withOpacity(.7),
                      activeSize: Size(14, 8),
                      size: Size(6, 6),
                      activeShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      spacing: EdgeInsets.all(2)),
                ),
              ),
            )
        ],
      ),
    );
  }
}
