import 'dart:async';

import 'package:acclaim/CreateGroup.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/viewStories.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashed_circle/dashed_circle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'ChatPage.dart';
import 'ShowPost.dart';
import 'search.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories>
    with AutomaticKeepAliveClientMixin {
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;

  List allCategories = appSettingsModel.getList(APP_CATEGORIES);
  String activeCategory = "";
  ScrollController categoryScroll = ScrollController();
  List<StreamSubscription> subs = [];

  @override
  initState() {
    super.initState();
    allCategories.sort((a, b) {
      return BaseModel(items: b)
          .getBoolean(IS_DEFAULT)
          .toString()
          .compareTo(BaseModel(items: a).getBoolean(IS_DEFAULT).toString());
    });
    activeCategory = BaseModel(items: allCategories[0]).getObjectId();
    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 150) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
      }
    });

    /*var stateStream = stateController.stream.listen((event) {
      for (var d in postList) {
        BaseModel bm = BaseModel(items: d);
        if (!bm.myItem() || !bm.myFollowers.contains(userModel.getUserId()))
          continue;
        int p = items.indexWhere(
            (e) => BaseModel(items: e).getObjectId() == bm.getObjectId());
        if (p == -1)
          items.insert(0, bm.items);
        else
          items[p] = bm.items;
      }
      setState(() {});
    });*/
    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 2) return;
      scrollTotallyUp(scrollController);
      if (activeCategory != allCategories.first[OBJECT_ID]) {
        activeCategory = allCategories.first[OBJECT_ID];
        vp.jumpToPage(0);
        categoryScroll.animateTo((0 * 60).toDouble(),
            duration: Duration(milliseconds: 500), curve: Curves.ease);
        setState(() {});
        return;
      }

      loadItems(true);
    });
    //subs.add(stateStream);
    subs.add(scrollSub);

    loadItems(false);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];
  bool setup = false;

  loadItems(bool isNew) async {
    QuerySnapshot query = await Firestore.instance
        .collection(GROUP_BASE)
        .where(GROUP_CATEGORY, arrayContains: activeCategory)
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ]).get();

    print(query.docs.length);

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      if (model.hidden.contains(userModel.getUserId())) continue;

      int p = items.indexWhere(
          (e) => BaseModel(items: e).getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model.items);
      else
        items[p] = model.items;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.documents.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
    //loadVideo();
    if (mounted) setState(() {});
  }

/*  loadVideo() async {
    await Future.delayed(Duration(seconds: 2));
    for (int vp = 0; vp < items.length; vp++) {
      BaseModel model = BaseModel(items: items[vp]);
      final photos = model.getListModel(IMAGES);
      for (int p = 0; p < photos.length; p++) {
        BaseModel bm = photos[p];
        if (!bm.isVideo) continue;
        String videoUrl = bm.getString(IMAGE_URL);
        if (videoUrl.isEmpty) continue;
        if (!videoUrl.startsWith("http")) continue;
        CachedVideoPlayerController controller = bm.get(VIDEO_CONTROLLER);
        if (null != controller) continue;
        controller = CachedVideoPlayerController.network(videoUrl)
          ..initialize().then((value) {
            //controller.play();
            controller.setLooping(true);
            setState(() {});
          });
        bm.put(VIDEO_CONTROLLER, controller);
        photos[p] = bm;
        model.put(IMAGES, photos.map((e) => e.items).toList());
        items[vp] = model.items;
      }
    }
  }*/

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    Color iconColor = black.withOpacity(.7);

    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  top: uploadingText == null ? 40 : 10,
                  right: 10,
                  left: 10,
                  bottom: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text("ourVoice",
                          style: TextStyle(
                              fontFamily: 'Billabong',
                              fontSize: 35,
                              fontWeight: FontWeight.bold)
                          //style: textStyle(true, 20, black),
                          ),
                      Spacer(),
                      InkWell(
                        onTap: () {
                          pushAndResult(context, ChatPage());
                        },
                        child: Container(
                          width: 30,
                          //height: 25,
                          color: transparent,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Image.asset(
                                  ic_message,
                                  height: 24,
                                  width: 24,
                                  fit: BoxFit.cover,
                                  color: iconColor,
                                ),
                              ),
                              if (newUnreadMessageIds.length.clamp(0, 9) > 0)
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 20,
                                    width: 20,
                                    margin: EdgeInsets.only(right: 10),
                                    alignment: Alignment.center,
                                    child: Text(
                                      "${newUnreadMessageIds.length.clamp(0, 9)}+",
                                      style: textStyle(true, 8, white),
                                    ),
                                    decoration: BoxDecoration(
                                        border: Border.all(color: white),
                                        color: Colors.red[600],
                                        shape: BoxShape.circle),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                      addSpaceWidth(5),
                      /* FlatButton(
                        onPressed: () {
                          pushAndResult(context, MyNotes());
                        },
                        height: 25,
                        minWidth: 25,
                        padding: EdgeInsets.all(5),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: CircleBorder(),
                        child: Image.asset(
                          ic_note,
                          height: 25,
                          width: 25,
                          fit: BoxFit.cover,
                          color: iconColor,
                        ),
                      ),
                      addSpaceWidth(10),*/
                      FlatButton(
                        onPressed: () {
                          pushAndResult(context, Search());
                        },
                        height: 18,
                        minWidth: 18,
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Image.asset(
                          ic_search,
                          height: 18,
                          width: 18,
                          fit: BoxFit.cover,
                          color: iconColor,
                        ),
                      ),
                      addSpaceWidth(5),
                      GestureDetector(
                        onTap: () {
                          pushAndResult(context, CreateGroup());
                        },
                        child: Container(
                          height: 35,
                          padding: EdgeInsets.only(
                              left: 5, right: 5, top: 3, bottom: 3),
                          margin: EdgeInsets.all(4),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: appColor_light,
                              borderRadius: BorderRadius.circular(25)),
                          child: Row(
                            children: [
                              addSpaceWidth(4),
                              Text(
                                "Create Group",
                                style: textStyle(true, 12, white),
                              ),
                              addSpaceWidth(4),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SingleChildScrollView(
                    controller: categoryScroll,
                    scrollDirection: Axis.horizontal,
                    child: Row(
                        children: List.generate(allCategories.length, (p) {
                      Map items = allCategories[p];
                      BaseModel model = BaseModel(items: items);
                      Map translations = model.getMap(TRANSLATIONS);
                      bool selected = activeCategory == model.getObjectId();
                      bool isDefault = model.getBoolean(IS_DEFAULT);

                      return GestureDetector(
                        onTap: () {
                          activeCategory = model.getObjectId();

                          try {
                            if ((categoryScroll.position.pixels == 0 &&
                                    !categoryScroll.position
                                        .outOfRange) /*||
                                (categoryScroll.position.pixels <=
                                    categoryScroll.position.maxScrollExtent)*/
                                )
                              categoryScroll.animateTo((p * 60).toDouble(),
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.ease);
                            vp.jumpToPage(p);
                            loadItems(true);
                          } catch (e) {}
                          setState(() {});
                        },
                        child: Container(
                          height: 40,
                          padding: EdgeInsets.only(
                              left: 5, right: 5, top: 3, bottom: 3),
                          margin: EdgeInsets.all(4),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: appColor.withOpacity(selected ? 1 : .1),
                              borderRadius: BorderRadius.circular(25)),
                          child: Row(
                            children: [
                              addSpaceWidth(8),
                              Text(
                                translations.toAppLanguage,
                                style: textStyle(
                                    selected, 14, selected ? white : black),
                              ),
                              addSpaceWidth(8),
                            ],
                          ),
                        ),
                      );
                    })),
                  )
                ],
              ),
            ),
            listItems()
          ],
        ),
      ],
    );
  }

  storiesLayout() {
    final foldedOthers =
        allStories.where((e) => !e.myItem()).groupBy((s) => s.userId);
    final foldedMine =
        allStories.where((e) => e.myItem()).groupBy((s) => s.userId);

    var keys;
    List<BaseModel> mStory = [];
    BaseModel story;
    bool noStory = foldedMine.isEmpty;
    String imageUrl;
    int playIndex = 0;

    if (storiesReady && foldedMine.isNotEmpty) {
      keys = foldedMine.keys.toList()[0];
      mStory = foldedMine[keys];
      playIndex = (mStory.length - 1).clamp(0, mStory.length - 1);
      story = mStory[playIndex];
      imageUrl = story.getString(story.isVideo ? THUMBNAIL_URL : IMAGE_URL);
    }

    double storyBoxSize = 80;

    return Container(
      width: double.infinity,
      padding: 10.padAll(),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.zero,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: storyBoxSize,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  DashedCircle(
                    strokeWidth: 3,
                    color: appColor,
                    dashes: mStory.length,
                    gapSize: 5,
                    child: Container(
                      height: storyBoxSize - 10,
                      width: storyBoxSize - 10,
                      child: Stack(
                        children: [
                          Container(
                              padding: 3.padAll(),
                              alignment: Alignment.center,
                              child: imageHolder(storyBoxSize - 10,
                                  imageUrl ?? userModel.userImage,
                                  iconHolder: Icons.camera,
                                  iconHolderSize: 25,
                                  stroke: noStory ? 3 : 1,
                                  strokeColor: noStory
                                      ? appColor
                                      : Colors.blueGrey, onImageTap: () {
                                if (noStory) {
                                  storyController.add(true);
                                  return;
                                }
                                openStoryAt(
                                  position: 0,
                                  playIndex: playIndex,
                                );
                              })),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  border: Border.all(color: white),
                                  gradient: LinearGradient(
                                      colors: [pink0, blue3, Colors.purple]),
                                  shape: BoxShape.circle),
                              child: Center(
                                  child: Icon(
                                Icons.add_a_photo,
                                size: 15,
                                color: white,
                              )),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  5.spaceHeight(),
                  Flexible(
                    child: Text(
                      "Your Story",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: textStyle(false, 14, black),
                    ),
                  )
                ],
              ),
            ),
            Row(
              children: List.generate(foldedOthers.keys.length, (p) {
                final keys = foldedOthers.keys.toList()[p];
                final stories = foldedOthers[keys];
                final seenAll = stories
                        .where((e) => e.seenBy.contains(userModel.userId))
                        .toList()
                        .length ==
                    stories.length;

                final defPosition = stories
                    .indexWhere((e) => !e.seenBy.contains(userModel.userId));
                int playIndex =
                    (stories.length - 1).clamp(0, stories.length - 1);
                BaseModel story = stories[playIndex];
                //String name = story.fullName;
                bool isVideo = story.isVideo;
                String imageUrl =
                    story.getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                String name = story.fullName;
                String nickName = story.nickName;
                bool myStory = story.myItem();
                if (nickName.startsWith("@")) {
                  nickName = nickName.replaceAll("@", "");
                  if (myStory) nickName = "YOU";
                }
                return Container(
                  width: storyBoxSize,
                  padding: 3.padAll(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      DashedCircle(
                        strokeWidth: seenAll ? 1 : 3,
                        color:
                            seenAll ? black.withOpacity(.2) : Colors.blueGrey,
                        dashes: stories.length > 15 ? 15 : stories.length,
                        gapSize: 5,
                        child: Container(
                            padding: 3.padAll(),
                            height: storyBoxSize - 10,
                            width: storyBoxSize - 10,
                            child: imageHolder(storyBoxSize - 10, imageUrl,
                                iconHolder: Icons.camera,
                                iconHolderSize: 20,
                                stroke: 1,
                                strokeColor: appColor, onImageTap: () {
                              openStoryAt(
                                position: p,
                                playIndex: playIndex,
                              );
                              return;
                            })),
                      ),
                      5.spaceHeight(),
                      Flexible(
                        child: Text(
                          name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: textStyle(false, 14, black),
                        ),
                      )
                    ],
                  ),
                );
              }),
            )
          ],
        ),
      ),
    );
  }

  openStoryAt({
    @required int position,
    @required int playIndex,
  }) {
    List<List<BaseModel>> stories = [];
    final foldedOthers =
        allStories.where((e) => !e.myItem()).toList().groupBy((s) => s.userId);
    final foldedMine =
        allStories.where((e) => e.myItem()).toList().groupBy((s) => s.userId);
    stories.addAll(foldedMine.values.toList());
    stories.addAll(foldedOthers.values.toList());

    BaseModel story = stories[position][playIndex];
    print(story.isVideo);

    // return;

    pushAndResult(
        context,
        viewStories(
          stories,
          position: position,
          playIndex: playIndex,
        ));
  }

  final vp = PageController();
  int currentPage = 0;

  listItems() {
    Color iconColor = black.withOpacity(.7);
    double iconSize = 20;

    return Expanded(
      child: PageView.builder(
        controller: vp,
        onPageChanged: (p) {
          currentPage = p;
          activeCategory = allCategories[p][OBJECT_ID];
          categoryScroll.animateTo((p * 60).toDouble(),
              duration: Duration(milliseconds: 500), curve: Curves.ease);
          setState(() {});
        },
        itemCount: allCategories.length,
        itemBuilder: (c, p) {
          Map cat = allCategories[p];
          String catId = cat[OBJECT_ID];

          List list = items
              .where((e) =>
                  BaseModel(items: e).getList(GROUP_CATEGORY).contains(catId))
              .toList();

          if (!setup) return loadingLayout();
          if (list.isEmpty)
            return Container(
              height: 400,
              child: emptyLayout("assets/images/ic_compass.png", "No Content",
                  "No content has been shared on this category...",
                  clickText: "Refresh", click: () {
                setup = false;
                setState(() {});
                loadItems(true);
              }),
            );

          CollageType type = CollageType.FourLeftBig;

          return SmartRefresher(
            controller: refreshController,
            enablePullDown: true,
            enablePullUp: true,
            header: WaterDropHeader(),
            footer: ClassicFooter(
              noDataText: "Nothing more for now, check later...",
              textStyle: textStyle(false, 12, black.withOpacity(.7)),
            ),
            onLoading: () {
              loadItems(false);
            },
            onRefresh: () {
              loadItems(true);
            },
            child: ListView(
              controller: scrollController,
              padding: EdgeInsets.zero,
              children: [
                storiesLayout(),
                StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    itemCount: 10, //list.length,
                    padding:
                        EdgeInsets.only(top: 10, bottom: 10, right: 5, left: 5),
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 2, //getCrossAxisCount(type),
                    crossAxisSpacing: 4,
                    mainAxisSpacing: 4,
                    itemBuilder: (BuildContext context, int p) {
                      Map item = list[0];
                      BaseModel model = BaseModel(items: item);

                      String image = model.groupImage;
                      String name = model.groupName;
                      String info = model.groupInfo;
                      String username = model.nickName;
                      String time = getTimeAgo(model.getTime());
                      String message = model.getString(MESSAGE);
                      final taggedPersons = model.getList(TAGGED_PERSONS);
                      //final photos = model.getListModel(IMAGES);
                      //BaseModel bm = photos[0];

                      String heroTag = "image$p";
                      return GestureDetector(
                        onTap: () {
                          //print(model.getObjectId());
                          pushAndResult(
                              context,
                              ShowPost(
                                model: model,
                                heroTag: getRandomId(),
                              ), result: (_) {
                            /* if (null == _) return;

                            if (_ == "deleted") {
                              items.remove(item);
                              setState(() {});
                              return;
                            }

                            items[p] = _.items;
                            setState(() {});*/
                          });
                        },
                        child: Container(
                          height: 300,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: black.withOpacity(.1))),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Stack(
                              children: <Widget>[
                                Column(
                                  children: [
                                    Expanded(
                                      child: CachedNetworkImage(
                                        imageUrl: image,
                                        fit: BoxFit.cover,
                                        width: double.infinity,
                                        placeholder: (_, s) {
                                          return placeHolder(300,
                                              width: double.infinity);
                                        },
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                          color: white,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: black.withOpacity(.1))),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            model.getString(GROUP_NAME),
                                            style: textStyle(true, 16, black),
                                          ),
                                          Text(
                                            model.getString(GROUP_INFO),
                                            maxLines: 2,
                                            style: textStyle(false, 12, black),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                /*Container(
                                  height: 300,
                                  width: double.infinity,
                                  color: black.withOpacity(.3),
                                ),*/
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    staggeredTileBuilder: (int index) {
                      return StaggeredTile.extent(1, 300);
                    }),
              ],
            ),
          );
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
