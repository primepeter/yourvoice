import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'ShowPost.dart';

class Bookmarks extends StatefulWidget {
  @override
  _BookmarksState createState() => _BookmarksState();
}

class _BookmarksState extends State<Bookmarks>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var stateStream = stateController.stream.listen((event) {
      setState(() {});
    });
    loadItems(true);
    subs.add(stateStream);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  // List bookmarkList = [];
  bool setup = false;

  loadItems(bool isNew) async {
    print(userModel.getUserId());
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection(POST_BASE)
        .where(BOOKMARKS, arrayContains: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (bookmarkList.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : bookmarkList[bookmarkList.length - 1].getTime())
          : (bookmarkList.isEmpty ? 0 : bookmarkList[0].getTime())
    ]).get();

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      int p = bookmarkList
          .indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p == -1)
        bookmarkList.add(model);
      else
        bookmarkList[p] = model;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.docs.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        Expanded(
          child: Builder(
            builder: (c) {
              if (!setup) return loadingLayout();

              if (bookmarkList.isEmpty)
                return emptyLayout(
                    Icons.bookmark, "No Bookmark", "Your bookmarks has nothing",
                    clickText: "Refresh", click: () {
                  setup = false;
                  setState(() {});
                  loadItems(true);
                });

              return SmartRefresher(
                controller: refreshController,
                enablePullDown: true,
                enablePullUp: true,
                header: WaterDropHeader(),
                footer: ClassicFooter(
                  noDataText: "Nothing more for now, check later...",
                  textStyle: textStyle(false, 12, black.withOpacity(.7)),
                ),
                onLoading: () {
                  loadItems(false);
                },
                onRefresh: () {
                  loadItems(true);
                },
                child: ListView.builder(
                  //controller: scrollController,
                  padding: EdgeInsets.only(
                    top: 10,
                  ),
                  itemCount: bookmarkList.length,
                  itemBuilder: (c, p) {
                    BaseModel model = bookmarkList[p];
                    //int count = items['count'];
                    final photos = model.getListModel(IMAGES);
                    String icon = '';
                    if (photos.isNotEmpty)
                      icon = photos[0].getString(IMAGE_URL);
                    String body = model.getString(MESSAGE);
                    int mins = body.length ~/ 200;
                    final likes = model.getList(LIKES);
                    final bookmarks = model.getList(BOOKMARKS);
                    bool liked = likes.contains(userModel.getUserId());
                    bool bookmarked = bookmarks.contains(userModel.getUserId());

                    List disLikes = model.getList(DISLIKES);
                    bool disLiked = disLikes.contains(userModel.getUserId());

                    double performance =
                        likes.length / (likes.length + disLikes.length);
                    performance = performance.clamp(0.0, 1.0);

                    double rating = calculateRating(performance);

                    return GestureDetector(
                      onLongPress: () {
                        if (!isAdmin) return;
                        model
                          ..put(IS_IMPORTANT, true)
                          ..updateItems();
                        setState(() {});
                        showMessage(
                            context,
                            Icons.check,
                            appColor,
                            "Marked Important",
                            "This Post has been added to important messages..");
                      },
                      onTap: () {
                        pushAndResult(
                            context,
                            ShowPost(
                              model: model,
                              heroTag: "lol...sdjsf",
                            ));
                      },
                      child: Row(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: 150,
                                height: 120,
                                margin: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        width: .5,
                                        color: black.withOpacity(.05))),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Stack(
                                    children: [
                                      CachedNetworkImage(
                                        imageUrl: icon,
                                        width: 250,
                                        height: 140,
                                        fit: BoxFit.cover,
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                colors: [
                                              /*appColor.withOpacity(.6),
                                            appColor_light.withOpacity(.6),*/
                                              black.withOpacity(.1),
                                              black.withOpacity(.08),
                                            ],
                                                begin: Alignment.bottomCenter,
                                                end: Alignment.topCenter)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          addSpaceWidth(10),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  body,
                                  maxLines: 3,
                                  style: textStyle(true, 16, black),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      '$mins min read',
                                      style: textStyle(
                                          false, 12, black.withOpacity(.8)),
                                    ),
                                    Container(
                                      height: 5,
                                      width: 5,
                                      margin: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: black,
                                      ),
                                    ),
                                    Text(
                                      getTimeAgo(model.getTime()),
                                      style: textStyle(
                                          false, 12, black.withOpacity(.8)),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        bookmarked = !bookmarked;
                                        model
                                          ..putInList(BOOKMARKS,
                                              userModel.getUserId(), bookmarked)
                                          ..updateItems();
                                        bookmarkList[p] = model;
                                        if (!bookmarked)
                                          bookmarkList.removeWhere((e) =>
                                              e.getObjectId() ==
                                              model.getObjectId());

                                        setState(() {});
                                      },
                                      child: Container(
                                          width: 30,
                                          height: 30,
                                          child: Icon(
                                            bookmarked
                                                ? Icons.bookmark
                                                : Icons.bookmark_border,
                                            color: bookmarked
                                                ? appColor
                                                : black.withOpacity(.5),
                                          )),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        sharePost(model);
                                      },
                                      child: Image.asset(
                                        GetAsset.images('share.png'),
                                        height: 20,
                                        width: 20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        model
                                          ..putInList(DISLIKES,
                                              userModel.getUserId(), false)
                                          ..updateItems();
                                        model
                                          ..putInList(LIKES,
                                              userModel.getUserId(), !liked)
                                          ..updateItems();
                                        bookmarkList[p] = model;
                                        setState(() {});
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Column(
                                          children: [
                                            Image.asset(
                                              GetAsset.images(liked
                                                  ? 'like1.png'
                                                  : 'like.png'),
                                              height: 20,
                                              width: 20,
                                              fit: BoxFit.cover,
                                            ),
                                            addSpace(2),
                                            if (likes.length > 0)
                                              Text(
                                                formatToK(likes.length),
                                                style:
                                                    textStyle(false, 14, black),
                                              ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        model
                                          ..putInList(DISLIKES,
                                              userModel.getUserId(), !disLiked)
                                          ..updateItems();
                                        model
                                          ..putInList(LIKES,
                                              userModel.getUserId(), false)
                                          ..updateItems();

                                        bookmarkList[p] = model;
                                        setState(() {});
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Column(
                                          children: [
                                            Image.asset(
                                              GetAsset.images(disLiked
                                                  ? 'dislike1.png'
                                                  : 'dislike.png'),
                                              height: 20,
                                              width: 20,
                                              fit: BoxFit.cover,
                                            ),
                                            addSpace(2),
                                            if (disLikes.length > 0)
                                              Text(
                                                formatToK(disLikes.length),
                                                style:
                                                    textStyle(false, 14, black),
                                              ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              );
            },
          ),
        )
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
