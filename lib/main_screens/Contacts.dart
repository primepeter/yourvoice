import 'dart:async';

import 'package:acclaim/AddContact.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Contacts extends StatefulWidget {
  @override
  _ContactsState createState() => _ContactsState();
}

class _ContactsState extends State<Contacts>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;
  bool isLiked = false;
  List<StreamSubscription> subs = [];
  ScrollController liveScroll = ScrollController();

  final pageController = PageController();
  int currentPage = 0;

  AnimationController _animationController;
  bool isPlaying = false;
  List electoralPosition = appSettingsModel.getList(POSITIONS);
  List sitType = appSettingsModel.getList(SIT_TYPE);

  @override
  initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 150) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
      }
    });

    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 3) return;
      scrollTotallyUp(scrollController);
    });
    subs.add(scrollSub);

    loadItems(false);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List<BaseModel> items = [];
  List topStories = [];
  List recentNews = [];

  bool setup = false;

  loadItems(bool isNew) async {
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection(CONTACT_BASE)
        //.where(PARTIES, arrayContains: userModel.getUserId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1].getTime())
          : (items.isEmpty ? 0 : items[0].getTime())
    ]).get();

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      int p = items.indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model);
      else
        items[p] = model;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.docs.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: listItems(),
    );
  }

  page() {
    Color iconColor = black.withOpacity(.7);

    return Stack(
      children: [
        listItems(),
      ],
    );
  }

  listItems() {
    if (!setup) return loadingLayout();
    if (items.isEmpty)
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            GetAsset.images('candidate.png'),
            height: 60,
            width: 60,
            fit: BoxFit.cover,
          ),
          addSpace(10),
          Text(
            'No Contact Added.',
            style: textStyle(false, 16, black.withOpacity(.5)),
            textAlign: TextAlign.center,
          ),
          addSpace(10),
          FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              color: black.withOpacity(.2),
              onPressed: () {
                setup = false;
                setState(() {});
                loadItems(true);
              },
              child: Text(
                'Refresh',
                style: textStyle(true, 16, black),
              ))
        ],
      );

    return SmartRefresher(
      controller: refreshController,
      enablePullDown: true,
      enablePullUp: true,
      header: WaterDropHeader(),
      footer: ClassicFooter(
        noDataText:
            items.length < 9 ? '' : "Nothing more for now, check later...",
        textStyle: textStyle(false, 12, black.withOpacity(.7)),
      ),
      onLoading: () {
        loadItems(false);
      },
      onRefresh: () {
        loadItems(true);
      },
      child: ListView.builder(
        padding: EdgeInsets.only(bottom: 80),
        itemCount: items.length,
        itemBuilder: (c, p) {
          return contactTile(p);
        },
      ),
    );
  }

  contactTile(int p) {
    BaseModel model = items[p];

    return GestureDetector(
      onLongPress: () {
        showListDialog(context, ['Edit', 'Delete'], (_) {
          print(_);

          if (_ == 'Edit') {
            pushAndResult(
                context,
                AddContact(
                  item: model.items,
                ), result: (_) {
              items[p] = _;
              setState(() {});
            });
            return;
          }

          if (_ == "Delete") {
            model.deleteItem();
            items.removeWhere((e) => e.getObjectId() == model.getObjectId());
            setState(() {});
          }
        });
      },
      child: Container(
        // margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        decoration:
            BoxDecoration(border: Border.all(color: black.withOpacity(0.04))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.red,
                        border: Border.all(color: black.withOpacity(0.04))),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.help,
                          size: 16,
                          color: white,
                        ),
                        addSpaceWidth(3),
                        Text(
                          // "Domestic Violence",
                          model.getString(CONTACT_CATEGORY),
                          maxLines: 2,
                          style: textStyle(true, 12, white),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    model.getString(NAME),
                    maxLines: 2,
                    style: textStyle(true, 20, black),
                  ),
                  Text(
                    model.getEmail(),
                    maxLines: 2,
                    style: textStyle(false, 14, black),
                  ),
                ],
              ),
            ),
            FlatButton(
              onPressed: () {
                clickContact(model);
              },
              shape: CircleBorder(),
              color: appColor,
              minWidth: 40,
              height: 40,
              padding: EdgeInsets.all(8),
              child: Image.asset(
                'assets/images/ic_call.png',
                color: white,
                width: 18,
                height: 18,
              ),
            )
          ],
        ),
      ),
    );
  }

  clickContact(BaseModel model) {
    //List contacts = model.getList(CONTACTS);

    showModalBottomSheet(
        context: context,
        builder: (
          c,
        ) {
          return contactWidget(context, model.items);
        });
  }

  contactWidget(context, Map item, {String title = ""}) {
    String name = item[NAME];
    String phone = item[PHONE_NUMBER];
    String phonePref = item[PHONE_PREF];
    String email = item[EMAIL];
    String whatapp = item[WHATSAPP_NUMBER];
    String whatPref = item[WHATSAPP_PREF];

    return Container(
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      padding: const EdgeInsets.fromLTRB(0, 5, 8, 5),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Icon(
                Icons.person_pin,
                size: 30,
                color: dark_green0,
              ),
              addSpaceWidth(5),
              Flexible(
                child: Text(name, style: textStyle(true, 18, black)),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 14),
            padding: EdgeInsets.only(left: 10),
            decoration: BoxDecoration(
                border: Border(left: BorderSide(color: dark_green0, width: 2))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                addSpace(5),
                GestureDetector(
                  onTap: () {
                    placeCall(createPhoneNumer(phonePref, phone));
                  },
                  child: Row(
                    children: [
                      addSpaceWidth(10),
                      Flexible(
                          fit: FlexFit.tight,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Phone",
                                style: textStyle(true, 14, black),
                              ),
                              Text(
                                createPhoneNumer(phonePref, phone),
                                style:
                                    textStyle(false, 12, black.withOpacity(.5)),
                              ),
                            ],
                          )),
                      addSpaceWidth(10),
                      Container(
                        width: 40,
                        height: 40,
                        child: RaisedButton(
                          onPressed: () {
                            placeCall(createPhoneNumer(phonePref, phone));
                          },
                          color: blue0,
                          shape: CircleBorder(),
                          padding: EdgeInsets.all(0),
                          child: Image.asset(
                            'assets/images/ic_call.png',
                            color: white,
                            width: 18,
                            height: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                if (whatapp.isNotEmpty) addSpace(5),
                if (whatapp.isNotEmpty)
                  GestureDetector(
                    onTap: () {
                      openWhatsapp(
                          context,
                          createPhoneNumer(
                            whatPref,
                            whatapp,
                          ),
                          text: title.isEmpty
                              ? ""
                              : "Hello, am from OurVoice Please i have an issue in this \"$title\", Are you Available?");
                    },
                    child: Row(
                      children: [
                        addSpaceWidth(10),
                        Flexible(
                            fit: FlexFit.tight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Whatsapp",
                                  style: textStyle(true, 14, black),
                                ),
                                Text(
                                  createPhoneNumer(whatPref, whatapp),
                                  style: textStyle(
                                      false, 12, black.withOpacity(.5)),
                                ),
                              ],
                            )),
                        addSpaceWidth(10),
                        Container(
                          width: 40,
                          height: 40,
                          child: RaisedButton(
                            onPressed: () {
                              openWhatsapp(
                                  context,
                                  createPhoneNumer(
                                    whatPref,
                                    whatapp,
                                  ),
                                  text: title.isEmpty
                                      ? ""
                                      : "Hi, am interested in this \"$title\", it is available?");
                              //sendEmail(email);
                            },
                            color: light_green3,
                            shape: CircleBorder(),
                            padding: EdgeInsets.all(0),
                            child: Image.asset(
                              'assets/images/ic_whatsapp.png',
                              color: white,
                              width: 18,
                              height: 18,
                            ),
                            // Icon(Icons.email,color: white,size: 18,),
                          ),
                        ),
                      ],
                    ),
                  ),
                if (email.isNotEmpty) addSpace(5),
                if (email.isNotEmpty)
                  GestureDetector(
                    onTap: () {
                      sendEmail(email);
                    },
                    child: Row(
                      children: [
                        addSpaceWidth(10),
                        Flexible(
                            fit: FlexFit.tight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Email",
                                  style: textStyle(true, 14, black),
                                ),
                                Text(
                                  email,
                                  style: textStyle(
                                      false, 12, black.withOpacity(.5)),
                                ),
                              ],
                            )),

                        addSpaceWidth(10),
                        Container(
                          width: 40,
                          height: 40,
                          child: RaisedButton(
                            onPressed: () {
                              sendEmail(email);
                            },
                            color: red0,
                            shape: CircleBorder(),
                            padding: EdgeInsets.all(0),
                            child: Image.asset(
                              'assets/images/ic_mail.png',
                              color: white,
                              width: 18,
                              height: 18,
                            ),
                          ),
                        ),
//            addSpaceWidth(10),
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
    ;
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
