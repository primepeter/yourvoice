import 'dart:async';

import 'package:acclaim/ShowComments.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/preview_post_images.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:color_thief_flutter/color_thief_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'ShowPost.dart';

class ShowCandidateProfile extends StatefulWidget {
  final BaseModel model;
  final String heroTag;
  const ShowCandidateProfile({Key key, this.model, this.heroTag})
      : super(key: key);
  @override
  _ShowCandidateProfileState createState() => _ShowCandidateProfileState();
}

class _ShowCandidateProfileState extends State<ShowCandidateProfile>
    with AutomaticKeepAliveClientMixin {
  int currentPage = 0;
  final vp = PageController();
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;

  List<StreamSubscription> subs = [];

  BaseModel model = BaseModel();
  List groupMembers = [];
  List<BaseModel> photos = [];
  int currentPhoto = 0;
  String currentPhotoUrl = '';

  Color photoColor = appColor;

  int mins = 0;

  @override
  initState() {
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 100) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
      }
    });

    model = widget.model;
    groupMembers = model.getList(GROUP_MEMBERS);
    if (widget.model != null) {
      currentPhotoUrl = widget.model.getString(IMAGE_URL);
      if (currentPhotoUrl.isNotEmpty) {
        setState(() {});
        loadPhotoColor();
      }
      mins = model.getString(MESSAGE).length ~/ 200;
    }

    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 0) return;
      print("scrolling up $p");
      scrollTotallyUp(scrollController);
    });
    subs.add(scrollSub);

    listenToCandidate();
    //listenToPosts();
    loadItems(true);
    listenToComments();
  }

  BaseModel theUser;
  List<BaseModel> followers = [];
  List<BaseModel> followings = [];

  loadPhotoColor() {
    getColorFromUrl(model.getString(IMAGE_URL)).then((color) {
      photoColor = Color.fromRGBO(color[0], color[1], color[2], 1);
      if (mounted) setState(() {});
    });
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List<BaseModel> postItems = [];
  bool postSetup = false;

  listenToPosts() async {
    var sub = FirebaseFirestore.instance
        .collection(POST_BASE)
        .where(POST_ID, isEqualTo: widget.model.getObjectId())
        .limit(10)
        .orderBy(TIME, descending: true)
        .snapshots()
        .listen((event) {
      print(event.size);

      for (var doc in event.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p =
            postItems.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p == -1)
          postItems.add(model);
        else
          postItems[p] = model;
      }
      postSetup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  loadItems(bool isNew) async {
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection(POST_BASE)
        .where(POST_ID, isEqualTo: widget.model.getObjectId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (postItems.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : postItems[postItems.length - 1].getTime())
          : (postItems.isEmpty ? 0 : postItems[0].getTime())
    ]).get();

    print('query.size ${query.size}');

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      int p =
          postItems.indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p == -1)
        postItems.add(model);
      else
        postItems[p] = model;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.docs.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    postSetup = true;
    if (mounted) setState(() {});
  }

  List<BaseModel> commentItems = [];
  bool commentSetup = false;
  listenToComments() async {
    var sub = FirebaseFirestore.instance
        .collection(COMMENT_BASE)
        .where(POST_ID, isEqualTo: widget.model.getObjectId())
        .limit(10)
        .orderBy(TIME, descending: true)
        .snapshots()
        .listen((event) {
      for (var doc in event.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = commentItems
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p == -1)
          commentItems.add(model);
        else
          commentItems[p] = model;
      }
      commentSetup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  listenToCandidate() async {
    var sub = FirebaseFirestore.instance
        .collection(CANDIDATE_BASE)
        .doc(widget.model.getObjectId())
        .snapshots()
        .listen((query) {
      model = BaseModel(doc: query);
      //setup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    print(widget.model.getObjectId());

    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, model);
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: page(),
      ),
    );
  }

  List electoralPosition = appSettingsModel.getList(POSITIONS);
  List sitType = appSettingsModel.getList(SIT_TYPE);

  page() {
    String position = electoralPosition[model.getInt(ELECTORAL_POSITION)];
    bool senators = position == "Senator";
    BaseModel district = model.getModel(DISTRICT);
    String sit = sitType[model.getInt(SIT_TYPE)];
    List votes = model.getList(VOTES);
    bool voted = votes.contains(userModel.getUserId());

    List likes = model.getList(LIKES);
    bool liked = likes.contains(userModel.getUserId());

    List disLikes = model.getList(DISLIKES);
    bool disLiked = disLikes.contains(userModel.getUserId());

    double performance = likes.length / (likes.length + disLikes.length);
    performance = performance.clamp(0.0, 1.0);
    print(performance);

    return Stack(
      children: [
        Column(
          children: [
            Expanded(
                child: SmartRefresher(
              controller: refreshController,
              enablePullDown: true,
              enablePullUp: true,
              header: WaterDropHeader(),
              footer: ClassicFooter(
                noDataText: "",
                textStyle: textStyle(false, 12, black.withOpacity(.7)),
              ),
              onLoading: () {
                loadItems(false);
              },
              onRefresh: () {
                loadItems(true);
              },
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  children: [
                    Container(
                      height: 300,
                      width: double.infinity,
                      child: Stack(
                        children: [
                          GestureDetector(
                            onTap: () {
                              pushAndResult(
                                  context,
                                  PreviewPostImages(
                                    [model],
                                    [],
                                  ));
                            },
                            child: CachedNetworkImage(
                              imageUrl: model.getString(IMAGE_URL),
                              height: 300,
                              width: double.infinity,
                              fit: BoxFit.cover,
                            ),
                          ),
                          IgnorePointer(
                            child: Container(
                              height: 300,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      colors: [
                                    /*appColor.withOpacity(.6),
                                                            appColor_light.withOpacity(.6),*/
                                    black.withOpacity(.1),
                                    black.withOpacity(.07),
                                  ],
                                      begin: Alignment.bottomCenter,
                                      end: Alignment.topCenter)),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Opacity(
                              opacity: (1 - (appBarOpacity)).clamp(0.0, 1.0),
                              child: Container(
                                padding: EdgeInsets.all(10),
                                child: RaisedButton(
                                  onPressed: () {
                                    if (voted && !isAdmin) return;
                                    yesNoDialog(context, 'Important Message!',
                                        'You can only vote once. Ensure this is your Candidate.',
                                        () {
                                      model
                                        ..putInList(VOTES,
                                            userModel.getUserId(), !voted)
                                        ..updateItems();
                                      setState(() {});
                                    });
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        (votes.length > 0
                                                ? '(${formatToK(votes.length)}) '
                                                : '') +
                                            (voted ? 'Voted' : 'Vote'),
                                        style: textStyle(false, 14, white),
                                      ),
                                      addSpaceWidth(5),
                                      if (voted)
                                        Icon(
                                          Icons.check_circle_outline,
                                          color: white,
                                          size: 20,
                                        )
                                      else
                                        Image.asset(
                                          GetAsset.images('ic_archive.png'),
                                          height: 16,
                                          width: 16,
                                          fit: BoxFit.cover,
                                          color: white,
                                        ),
                                    ],
                                  ),
                                  color: appColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: black.withOpacity(.02),
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: black.withOpacity(.02))),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Text(
                                  model.getString(NAME),
                                  textAlign: TextAlign.start,
                                  style: textStyle(true, 20, black),
                                ),
                              ),
                              addSpaceWidth(10),
                              Container(
                                decoration: BoxDecoration(
                                    color: appColor,
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(
                                        color: black.withOpacity(.02))),
                                padding: EdgeInsets.all(3),
                                child: Text(
                                  sit,
                                  textAlign: TextAlign.center,
                                  style: textStyle(true, 12, white),
                                ),
                              ),
                            ],
                          ),
                          Text(
                            model.getString(POLITICAL_PARTY),
                            textAlign: TextAlign.center,
                            style: textStyle(false, 16, black.withOpacity(.8)),
                          ),
                          if (senators)
                            Text(
                              '${district.getString(NAME)}(${district.getString(STATE)})',
                              style:
                                  textStyle(false, 14, black.withOpacity(.8)),
                            ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 8),
                      height: 60,
                      alignment: Alignment.center,
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'How would you Rate this Candidate?',
                                  style: textStyle(
                                      false, 13, black.withOpacity(.7)),
                                ),
                                addSpace(4),
                                LinearPercentIndicator(
                                  backgroundColor: black.withOpacity(.05),
                                  alignment: MainAxisAlignment.start,
                                  progressColor: appColor,
                                  percent: performance,
                                  lineHeight: 10,
                                ),
                              ],
                            ),
                          ),
                          addSpaceWidth(5),
                          InkWell(
                            onTap: () {
                              model
                                ..putInList(
                                    DISLIKES, userModel.getUserId(), false)
                                ..updateItems();
                              model
                                ..putInList(
                                    LIKES, userModel.getUserId(), !liked)
                                ..updateItems();
                              setState(() {});
                            },
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Column(
                                children: [
                                  Icon(
                                    liked
                                        ? Icons.thumb_up
                                        : Icons.thumb_up_alt_outlined,
                                    color: liked
                                        ? appColor
                                        : black.withOpacity(.7),
                                    size: 20,
                                  ),
                                  addSpace(2),
                                  if (likes.length > 0)
                                    Text(
                                      formatToK(likes.length),
                                      style: textStyle(false, 14, black),
                                    ),
                                ],
                              ),
                            ),
                          ),
                          addSpaceWidth(5),
                          InkWell(
                            onTap: () {
                              model
                                ..putInList(
                                    DISLIKES, userModel.getUserId(), !disLiked)
                                ..updateItems();
                              model
                                ..putInList(LIKES, userModel.getUserId(), false)
                                ..updateItems();
                              setState(() {});
                            },
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Column(
                                children: [
                                  Icon(
                                    disLiked
                                        ? Icons.thumb_down
                                        : Icons.thumb_down_alt_outlined,
                                    color:
                                        disLiked ? red0 : black.withOpacity(.7),
                                    size: 20,
                                  ),
                                  addSpace(2),
                                  if (disLikes.length > 0)
                                    Text(
                                      formatToK(disLikes.length),
                                      style: textStyle(false, 14, black),
                                    ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    postPage(),
                    //pageComments(),
                    addSpace(100)
                  ],
                ),
              ),
            )),

            /*Expanded(
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  children: [
                    Container(
                      height: 300,
                      width: double.infinity,
                      child: Stack(
                        children: [
                          GestureDetector(
                            onTap: () {
                              pushAndResult(
                                  context,
                                  PreviewPostImages(
                                    [model],
                                    [],
                                  ));
                            },
                            child: CachedNetworkImage(
                              imageUrl: model.getString(IMAGE_URL),
                              height: 300,
                              width: double.infinity,
                              fit: BoxFit.cover,
                            ),
                          ),
                          IgnorePointer(
                            child: Container(
                              height: 300,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      colors: [
                                    */ /*appColor.withOpacity(.6),
                                                            appColor_light.withOpacity(.6),*/ /*
                                    black.withOpacity(.1),
                                    black.withOpacity(.07),
                                  ],
                                      begin: Alignment.bottomCenter,
                                      end: Alignment.topCenter)),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Opacity(
                              opacity: (1 - (appBarOpacity)).clamp(0.0, 1.0),
                              child: Container(
                                padding: EdgeInsets.all(10),
                                child: RaisedButton(
                                  onPressed: () {
                                    if (voted && !isAdmin) return;
                                    yesNoDialog(context, 'Important Message!',
                                        'You can only vote once. Ensure this is your Candidate.',
                                        () {
                                      model
                                        ..putInList(VOTES,
                                            userModel.getUserId(), !voted)
                                        ..updateItems();
                                      setState(() {});
                                    });
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        (votes.length > 0
                                                ? '(${formatToK(votes.length)}) '
                                                : '') +
                                            (voted ? 'Voted' : 'Vote'),
                                        style: textStyle(false, 14, white),
                                      ),
                                      addSpaceWidth(5),
                                      if (voted)
                                        Icon(
                                          Icons.check_circle_outline,
                                          color: white,
                                          size: 20,
                                        )
                                      else
                                        Image.asset(
                                          GetAsset.images('ic_archive.png'),
                                          height: 20,
                                          width: 20,
                                          fit: BoxFit.cover,
                                        ),
                                    ],
                                  ),
                                  color: appColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: black.withOpacity(.02),
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: black.withOpacity(.02))),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Text(
                                  model.getString(NAME),
                                  textAlign: TextAlign.start,
                                  style: textStyle(true, 20, black),
                                ),
                              ),
                              addSpaceWidth(10),
                              Container(
                                decoration: BoxDecoration(
                                    color: appColor,
                                    borderRadius: BorderRadius.circular(6),
                                    border: Border.all(
                                        color: black.withOpacity(.02))),
                                padding: EdgeInsets.all(3),
                                child: Text(
                                  sit,
                                  textAlign: TextAlign.center,
                                  style: textStyle(true, 12, white),
                                ),
                              ),
                            ],
                          ),
                          Text(
                            model.getString(POLITICAL_PARTY),
                            textAlign: TextAlign.center,
                            style: textStyle(false, 16, black.withOpacity(.8)),
                          ),
                          if (senators)
                            Text(
                              '${district.getString(NAME)}(${district.getString(STATE)})',
                              style:
                                  textStyle(false, 14, black.withOpacity(.8)),
                            ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 8),
                      height: 60,
                      alignment: Alignment.center,
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Performance Meter',
                                  style: textStyle(
                                      false, 12, black.withOpacity(.7)),
                                ),
                                addSpace(4),
                                LinearPercentIndicator(
                                  backgroundColor: black.withOpacity(.05),
                                  alignment: MainAxisAlignment.start,
                                  progressColor: appColor,
                                  percent: performance,
                                  lineHeight: 6,
                                ),
                              ],
                            ),
                          ),
                          addSpaceWidth(5),
                          InkWell(
                            onTap: () {
                              model
                                ..putInList(
                                    DISLIKES, userModel.getUserId(), false)
                                ..updateItems();
                              model
                                ..putInList(
                                    LIKES, userModel.getUserId(), !liked)
                                ..updateItems();
                              setState(() {});
                            },
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Column(
                                children: [
                                  Icon(
                                    liked
                                        ? Icons.thumb_up
                                        : Icons.thumb_up_alt_outlined,
                                    color: liked
                                        ? appColor
                                        : black.withOpacity(.7),
                                    size: 20,
                                  ),
                                  addSpace(2),
                                  if (likes.length > 0)
                                    Text(
                                      formatToK(likes.length),
                                      style: textStyle(false, 14, black),
                                    ),
                                ],
                              ),
                            ),
                          ),
                          addSpaceWidth(5),
                          InkWell(
                            onTap: () {
                              model
                                ..putInList(
                                    DISLIKES, userModel.getUserId(), !disLiked)
                                ..updateItems();
                              model
                                ..putInList(LIKES, userModel.getUserId(), false)
                                ..updateItems();
                              setState(() {});
                            },
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Column(
                                children: [
                                  Icon(
                                    disLiked
                                        ? Icons.thumb_down
                                        : Icons.thumb_down_alt_outlined,
                                    color:
                                        disLiked ? red0 : black.withOpacity(.7),
                                    size: 20,
                                  ),
                                  addSpace(2),
                                  if (disLikes.length > 0)
                                    Text(
                                      formatToK(disLikes.length),
                                      style: textStyle(false, 14, black),
                                    ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    postPage(),
                    //pageComments(),
                    addSpace(100)
                  ],
                ),
              ),
            ),*/
            Container(
              padding: EdgeInsets.all(12),
              child: RaisedButton(
                onPressed: () {
                  pushAndResult(
                      context,
                      ShowComments(
                        theCandidate: model,
                        candidateId: model.getObjectId(),
                        heroTag: "sddfgf@efdg",
                      ));
                },
                child: Center(
                  child: Text(
                    '${commentItems.length > 0 ? '(${commentItems.length})' : ''} Join Discussion',
                    style: textStyle(true, 16, white),
                  ),
                ),
                color: appColor,
                padding: EdgeInsets.all(12),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            )
          ],
        ),
        Container(
          padding: const EdgeInsets.only(top: 25),
          color: photoColor.withOpacity(appBarOpacity),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context, model);
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    // decoration: BoxDecoration(
                    //     color: red0,
                    //     borderRadius: BorderRadius.only(
                    //       topRight: Radius.circular(25),
                    //       bottomRight: Radius.circular(25),
                    //     )),
                    padding: EdgeInsets.all(6),
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: white,
                      size: 25,
                    )),
                  )),
              Spacer(),
              Opacity(
                opacity: appBarOpacity,
                child: Container(
                  //padding: EdgeInsets.all(10),
                  child: RaisedButton(
                    onPressed: () {
                      if (voted && !isAdmin) return;
                      yesNoDialog(context, 'Important Message!',
                          'You can only vote once. Ensure this is your Candidate.',
                          () {
                        model
                          ..putInList(VOTES, userModel.getUserId(), !voted)
                          ..updateItems();
                        setState(() {});
                      });
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          (votes.length > 0
                                  ? '(${formatToK(votes.length)}) '
                                  : '') +
                              (voted ? 'Voted' : 'Vote'),
                          style: textStyle(false, 14, white),
                        ),
                        addSpaceWidth(5),
                        if (voted)
                          Icon(
                            Icons.check_circle_outline,
                            color: white,
                            size: 20,
                          )
                        else
                          Image.asset(
                            GetAsset.images('ic_archive.png'),
                            height: 20,
                            width: 20,
                            fit: BoxFit.cover,
                          ),
                      ],
                    ),
                    color: appColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                  ),
                ),
              ),
              addSpaceWidth(10)
            ],
          ),
        ),
        if (appBarOpacity == 1)
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              margin: EdgeInsets.only(bottom: 70, right: 5),
              child: RaisedButton(
                onPressed: () {
                  scrollTotallyUp(scrollController);
                },
                padding: EdgeInsets.all(15),
                elevation: 10,
                shape: CircleBorder(),
                child: Icon(
                  Icons.keyboard_arrow_up,
                  color: appColor,
                ),
                color: white,
              ),
            ),
          )
      ],
    );
  }

  postPage() {
    if (!postSetup)
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              'Activities',
              style: textStyle(true, 18, black),
            ),
            padding: EdgeInsets.all(10),
            //margin: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: black.withOpacity(.0),
              border: Border.all(color: black.withOpacity(.02)),
              //borderRadius: BorderRadius.circular(8)
            ),
          ),
          Container(
            height: 50,
            child: Center(
              child: SizedBox(
                height: 16,
                width: 16,
                child: CircularProgressIndicator(
                  strokeWidth: 1,
                  valueColor: AlwaysStoppedAnimation(appColor),
                ),
              ),
            ),
          ),
        ],
      );

    // print(postItems.length);

    if (postItems.isEmpty)
      return Container(
        child: Text(
          'No Activities',
          style: textStyle(true, 18, black),
        ),
        padding: EdgeInsets.all(10),
        //margin: EdgeInsets.all(10),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: black.withOpacity(.0),
          border: Border.all(color: black.withOpacity(.02)),
          //borderRadius: BorderRadius.circular(8)
        ),
      );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: Row(
            children: [
              /*Icon(Icons.bar_chart),
                            addSpaceWidth(10),*/
              Text(
                'Activities',
                style: textStyle(true, 18, black),
              ),
            ],
          ),
          padding: EdgeInsets.all(10),
          //margin: EdgeInsets.all(10),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: black.withOpacity(.0),
            border: Border.all(color: black.withOpacity(.02)),
            //borderRadius: BorderRadius.circular(8)
          ),
        ),
        Column(
          children: List.generate(postItems.length, (p) {
            BaseModel model = postItems[p];
            //int count = items['count'];
            final photos = model.getListModel(IMAGES);
            String icon = '';
            if (photos.isNotEmpty) icon = photos[0].getString(IMAGE_URL);
            String body = model.getString(MESSAGE);
            int mins = body.length ~/ 200;
            final likes = model.getList(LIKES);
            final bookmarks = model.getList(BOOKMARKS);
            bool liked = likes.contains(userModel.getUserId());
            bool bookmarked = bookmarks.contains(userModel.getUserId());

            List disLikes = model.getList(DISLIKES);
            bool disLiked = disLikes.contains(userModel.getUserId());

            double performance =
                likes.length / (likes.length + disLikes.length);
            performance = performance.clamp(0.0, 1.0);

            double rating = calculateRating(performance);

            return GestureDetector(
              onLongPress: () {
                if (!isAdmin) return;
                model
                  ..put(IS_IMPORTANT, true)
                  ..updateItems();
                setState(() {});
                showMessage(context, Icons.check, appColor, "Marked Important",
                    "This Post has been added to important messages..");
              },
              onTap: () {
                pushAndResult(
                    context,
                    ShowPost(
                      model: model,
                      heroTag: "lol...sdjsf",
                    ));
              },
              child: Row(
                children: [
                  Stack(
                    children: [
                      Container(
                        width: 150,
                        height: 120,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                                width: .5, color: black.withOpacity(.05))),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Stack(
                            children: [
                              CachedNetworkImage(
                                imageUrl: icon,
                                width: 250,
                                height: 140,
                                fit: BoxFit.cover,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                      /*appColor.withOpacity(.6),
                                            appColor_light.withOpacity(.6),*/
                                      black.withOpacity(.1),
                                      black.withOpacity(.08),
                                    ],
                                        begin: Alignment.bottomCenter,
                                        end: Alignment.topCenter)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  addSpaceWidth(10),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          body,
                          maxLines: 3,
                          style: textStyle(true, 16, black),
                        ),
                        Row(
                          children: [
                            Text(
                              '$mins min read',
                              style:
                                  textStyle(false, 12, black.withOpacity(.8)),
                            ),
                            Container(
                              height: 5,
                              width: 5,
                              margin: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: black,
                              ),
                            ),
                            Text(
                              getTimeAgo(model.getTime()),
                              style:
                                  textStyle(false, 12, black.withOpacity(.8)),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InkWell(
                              onTap: () {
                                bookmarked = !bookmarked;
                                model
                                  ..putInList(BOOKMARKS, userModel.getUserId(),
                                      bookmarked)
                                  ..updateItems();
                                postItems[p] = model;

                                int ps = bookmarkList.indexWhere((e) =>
                                    e.getObjectId() == model.getObjectId());

                                if (bookmarked) {
                                  if (ps == -1)
                                    bookmarkList.add(model);
                                  else
                                    bookmarkList[ps] = model;
                                } else {
                                  bookmarkList.removeWhere((e) =>
                                      e.getObjectId() == model.getObjectId());
                                }
                                stateController.add(true);
                                setState(() {});
                              },
                              child: Container(
                                  width: 30,
                                  height: 30,
                                  child: Icon(
                                    bookmarked
                                        ? Icons.bookmark
                                        : Icons.bookmark_border,
                                    color: bookmarked
                                        ? appColor
                                        : black.withOpacity(.5),
                                  )),
                            ),
                            InkWell(
                              onTap: () {
                                sharePost(model);
                              },
                              child: Image.asset(
                                GetAsset.images('share.png'),
                                height: 20,
                                width: 20,
                                fit: BoxFit.cover,
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                model
                                  ..putInList(
                                      DISLIKES, userModel.getUserId(), false)
                                  ..updateItems();
                                model
                                  ..putInList(
                                      LIKES, userModel.getUserId(), !liked)
                                  ..updateItems();
                                postItems[p] = model;
                                setState(() {});
                              },
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: [
                                    Image.asset(
                                      GetAsset.images(
                                          liked ? 'like1.png' : 'like.png'),
                                      height: 20,
                                      width: 20,
                                      fit: BoxFit.cover,
                                    ),
                                    addSpace(2),
                                    if (likes.length > 0)
                                      Text(
                                        formatToK(likes.length),
                                        style: textStyle(false, 14, black),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                model
                                  ..putInList(DISLIKES, userModel.getUserId(),
                                      !disLiked)
                                  ..updateItems();
                                model
                                  ..putInList(
                                      LIKES, userModel.getUserId(), false)
                                  ..updateItems();

                                postItems[p] = model;
                                setState(() {});
                              },
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: [
                                    Image.asset(
                                      GetAsset.images(disLiked
                                          ? 'dislike1.png'
                                          : 'dislike.png'),
                                      height: 20,
                                      width: 20,
                                      fit: BoxFit.cover,
                                    ),
                                    addSpace(2),
                                    if (disLikes.length > 0)
                                      Text(
                                        formatToK(disLikes.length),
                                        style: textStyle(false, 14, black),
                                      ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            );
          }),
        ),
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

String formatToActualDate(int time) {
  Timestamp t = userModel.get(CREATED_AT);
  DateTime d = DateTime.fromMillisecondsSinceEpoch(time);
  final date = DateTime.parse(d.toString());
  return new DateFormat(
    "MMMM d,yyyy",
  ).format(date);
}
