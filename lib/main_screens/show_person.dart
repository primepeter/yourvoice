import 'dart:async';

import 'package:acclaim/AddPost.dart';
import 'package:acclaim/CreateReport.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/show_followers.dart';
import 'package:acclaim/show_following.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'home.dart';
import 'search.dart';

class ShowPerson extends StatefulWidget {
  final BaseModel model;

  const ShowPerson({Key key, this.model}) : super(key: key);
  @override
  _ShowPersonState createState() => _ShowPersonState();
}

class _ShowPersonState extends State<ShowPerson>
    with AutomaticKeepAliveClientMixin {
  int currentPage = 0;
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;

  List<StreamSubscription> subs = [];

  @override
  initState() {
    super.initState();
    theUser = widget.model;
    if (theUser.getObjectId().isEmpty ||
        theUser.getObjectId() != theUser.getUserId()) {
      theUser.put(OBJECT_ID, widget.model.getUserId());
    }

    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 0) return;
      print("scrolling up $p");
      scrollTotallyUp(scrollController);
    });
    subs.add(scrollSub);
    loadUser();
    loadItems(true);
  }

  BaseModel theUser;
  List<BaseModel> followers = [];
  List<BaseModel> followings = [];

  loadUser() async {
    print(widget.model.items);

    Firestore.instance
        .collection(USER_BASE)
        .document(theUser.getObjectId())
        .get()
        .then((value) {
      theUser = BaseModel(doc: value);
      loadFollowers();
      loadFollowing();
      /* thisPost
        ..put(FULL_NAME, theUser.getString(FULL_NAME))
        ..put(NICKNAME, theUser.getString(NICKNAME))
        ..put(USER_IMAGE, theUser.userImage)
      //..put(USER_ID, model.userImage.items)
        ..updateItems();*/
      if (mounted) setState(() {});
    });
  }

  loadFollowers() {
    final ref = Firestore.instance.collection(USER_BASE);
    for (String ids in theUser.myFollowers) {
      if (ids.isEmpty) {
        theUser
          ..putInList(MY_FOLLOWER_IDS, "", false)
          ..updateItems();
        continue;
      }
      ref.document(ids).get().then((value) {
        BaseModel model = BaseModel(doc: value);

        int p =
            followers.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          followers[p] = model;
        } else {
          followers.add(model);
        }

        setState(() {});
      });
    }
  }

  loadFollowing() {
    final ref = Firestore.instance.collection(USER_BASE);
    for (String ids in theUser.amFollowing) {
      if (ids.isEmpty) {
        theUser
          ..putInList(AM_FOLLOWING_IDS, "", false)
          ..updateItems();
        continue;
      }
      ref.document(ids).get().then((value) {
        BaseModel model = BaseModel(doc: value);
        int p = followings
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1)
          followings[p] = model;
        else
          followings.add(model);
        setState(() {});
      });
    }
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List items = [];
  bool setup = false;

  loadItems(bool isNew) async {
    print(theUser.getUserId());
    QuerySnapshot query = await Firestore.instance
        .collection(POST_BASE)
        .where(USER_ID, isEqualTo: theUser.getObjectId())
        .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ]).getDocuments();

    print(query.documents.length);

    for (var doc in query.documents) {
      BaseModel model = BaseModel(doc: doc);
      int p = items.indexWhere(
          (e) => BaseModel(items: e).getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model.items);
      else
        items[p] = model.items;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.documents.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    //items.sort((a, b) => b[TIME] ?? 0.compareTo(a[TIME] ?? 0));
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    bool followed = userModel.amFollowing.contains(theUser.getObjectId());
    String followers =
        formatToK(theUser.getList(MY_FOLLOWER_IDS).length).toString();

    String followings =
        formatToK(theUser.getList(AM_FOLLOWING_IDS).length).toString();

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context, "");
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "@${theUser.nickName}",
                style: textStyle(true, 20, black),
              )),
              InkWell(
                  onTap: () {
                    pushAndResult(context, Search());
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.search,
                      color: black,
                      size: 25,
                    )),
                  )),
            ],
          ),
        ),
        Expanded(
          child: SmartRefresher(
            controller: refreshController,
            scrollController: scrollController,
            enablePullDown: true,
            enablePullUp: true,
            header: WaterDropHeader(),
            footer: ClassicFooter(
              noDataText: "Nothing more for now, check later...",
              textStyle: textStyle(false, 12, black.withOpacity(.7)),
            ),
            onLoading: () {
              loadItems(false);
            },
            onRefresh: () {
              loadItems(true);
            },
            child: SingleChildScrollView(
              //controller: scrollController,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                pushAndResult(
                                    context,
                                    ShowFollowers(
                                      items: this.followers,
                                    ));
                              },
                              child: Column(
                                children: [
                                  Text(
                                    followers,
                                    style: textStyle(true, 18, black),
                                  ),
                                  Text(
                                    "Followers",
                                    style: textStyle(
                                        true, 14, black.withOpacity(.4)),
                                  ),
                                ],
                              ),
                            ),
                            addSpaceWidth(20),
                            Container(
                              height: 120,
                              width: 100,
                              child: Stack(
                                children: [
                                  Container(
                                    height: 100,
                                    width: 100,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        //borderRadius: BorderRadius.circular(20),
                                        gradient: LinearGradient(colors: [
                                          pink0,
                                          blue3,
                                          Colors.purple
                                        ])),
                                    padding: EdgeInsets.all(2),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(50),
                                      child: CachedNetworkImage(
                                        imageUrl: theUser.userImage,
                                        fit: BoxFit.cover,
                                        height: 105,
                                        width: 105,
                                        placeholder: (c, s) {
                                          return Container(
                                            height: 105,
                                            width: 105,
                                            color: black.withOpacity(.05),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                  if (theUser.myItem())
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        height: 40,
                                        width: 40,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: white, width: 2),
                                            shape: BoxShape.circle,

                                            // borderRadius: BorderRadius.circular(5),
                                            gradient: LinearGradient(colors: [
                                              pink0,
                                              blue3,
                                              Colors.purple
                                            ])),
                                        padding: EdgeInsets.all(3),
                                        child: Icon(
                                          Icons.add,
                                          color: white,
                                          size: 15,
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                            addSpaceWidth(20),
                            InkWell(
                              onTap: () {
                                pushAndResult(
                                    context,
                                    ShowFollowing(
                                      items: this.followings,
                                    ));
                              },
                              child: Column(
                                children: [
                                  Text(
                                    followings,
                                    style: textStyle(true, 18, black),
                                  ),
                                  Text(
                                    "Following",
                                    style: textStyle(
                                        true, 14, black.withOpacity(.4)),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        addSpace(10),
                        Text(
                          theUser.getString(NAME),
                          style: textStyle(true, 18, black),
                        ),
                        addSpace(5),
                        Text(
                          theUser.profession.isEmpty
                              ? "*******"
                              : theUser.profession,
                          style: textStyle(true, 16, black),
                        ),
                        addSpace(5),
                        if (!theUser.myItem())
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  child: FlatButton(
                                    onPressed: () {
                                      print("Clicked...");
                                      clickChat(context, theUser);
                                      // launchChat(context, chatId, defMessage)
                                    },
                                    color: Colors.transparent,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        side: BorderSide(color: appColor)),
                                    child: Center(
                                        child: Text(
                                      "Message",
                                      style: textStyle(true, 14, appColor),
                                    )),
                                  ),
                                ),
                                addSpaceWidth(10),
                                Flexible(
                                  child: FlatButton(
                                    onPressed: () {
                                      followThisUser(theUser, followed,
                                          onComplete: () {
                                        setState(() {});
                                      });
                                    },
                                    color: appColor,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        side: BorderSide(color: appColor)),
                                    child: Center(
                                        child: Text(
                                      followed ? "Unfollow" : "Follow",
                                      style: textStyle(true, 12, white),
                                    )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        addSpace(10),
                        if (theUser.aboutMe.isNotEmpty)
                          Container(
                              margin: EdgeInsets.all(8),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: black.withOpacity(.02),
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(
                                      color: black.withOpacity(.09))),
                              child: ReadMoreText(theUser.aboutMe))
                      ],
                    ),
                  ),
                  addSpace(10),
                  Builder(
                    builder: (c) {
                      if (!setup)
                        return Container(
                          height: 400,
                          child: loadingLayout(),
                        );

                      if (items.isEmpty)
                        return Container(
                          height: 400,
                          child: emptyLayout(Icons.add, "No Post",
                              "You have not share any post yet"),
                        );

                      return postInGrid();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  postInGrid() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3, crossAxisSpacing: 8, mainAxisSpacing: 8),
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: items.length,
      padding: EdgeInsets.zero,
      itemBuilder: (c, p) {
        Map item = items[p];
        BaseModel model = BaseModel(items: item);

        String image = model.userImage;
        String name = model.name;
        String username = model.nickName;
        String time = getTimeAgo(model.getTime());
        String message = model.getString(MESSAGE);
        final taggedPersons = model.getList(TAGGED_PERSONS);
        final photos = model.getListModel(IMAGES);
        BaseModel bm = photos[0];
        String imageUrl = bm.getString(IMAGE_URL);
        String imagePath = bm.getString(IMAGE_PATH);
        String thumbPath = bm.getString(THUMBNAIL_PATH);
        String thumbUrl = bm.getString(THUMBNAIL_URL);
        bool onlineImage = imageUrl.startsWith("http");
        bool isVideo = bm.isVideo;
        String heroTag = "profile_hero_image$p";

        return GestureDetector(
          onDoubleTap: () {
            showListDialog(context, [
              if (model.myItem() || isAdmin) "Edit",
              if (model.myItem() || isAdmin) "Delete",
              if (!model.myItem()) "Report"
            ], (_) {
              if (_ == "Delete") {
                model.deleteItem();
                items.remove(item);
                setState(() {});
              }
              if (_ == "Edit") {
                pushAndResult(
                    context,
                    AddPost(
                      model: model,
                    ));
              }
              if (_ == "Report") {
                pushAndResult(
                    context,
                    CreateReport(
                      item: model,
                    ), result: (_) {
                  if (null != _ || !!_) return;
                  items.remove(item);
                  setState(() {});
                });
              }
            });
          },
          onTap: () {
            /* pushAndResult(
                context,
                ShowComments(
                  thisPost: model,
                  heroTag: getRandomId(),
                ), result: (_) {
              if (null == _) return;
              items[p] = _.postItems;
              setState(() {});
            });*/
          },
          child: Stack(
            fit: StackFit.passthrough,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(1.2),
                child: CachedNetworkImage(
                  imageUrl: isVideo ? thumbUrl : imageUrl,
                  fit: BoxFit.cover,
                  placeholder: (_, s) {
                    return placeHolder(250, width: double.infinity);
                  },
                ),
              ),
              if (isVideo)
                Container(
                  height: 250,
                  child: Center(
                    child: Container(
                      height: 30,
                      width: 30,
                      padding: 3.padAll(),
                      margin: 6.padAll(),
                      decoration: BoxDecoration(
                        color: black.withOpacity(.5),
                        border: Border.all(color: white, width: 2),
                        shape: BoxShape.circle,
                      ),
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.play_arrow,
                        color: white,
                        size: 15,
                      ),
                    ),
                  ),
                ),
              /*Center(
                            child: Text(
                              p.toString(),
                              style: textStyle(true, 30, white),
                            ),
                          )*/
            ],
          ),
        );
      },
    );
  }

  postInList() {
    return ListView.builder(
      itemCount: items.length,
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (c, p) {
        Map item = items[p];
        BaseModel model = BaseModel(items: item);

        return PostItem(
            p: p,
            heroTag: getRandomId(),
            model: model,
            stateCallBack: (_) {
              if (_ == null) {
                items.remove(item);
                setState(() {});
                return;
              }
              items[p] = _;
              setState(() {});
            });
      },
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
