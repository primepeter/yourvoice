import 'dart:async';

import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/like_animation.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'ShowCandidateProfile.dart';

class OurChoice extends StatefulWidget {
  @override
  _OurChoiceState createState() => _OurChoiceState();
}

class _OurChoiceState extends State<OurChoice>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  ScrollController scrollController = ScrollController();
  double appBarOpacity = 0;
  bool isLiked = false;
  List<StreamSubscription> subs = [];
  ScrollController liveScroll = ScrollController();

  final pageController = PageController();
  int currentPage = 0;

  // AnimationController _animationController;
  // bool isPlaying = false;

  List electoralPosition = appSettingsModel.getList(POSITIONS);
  List sitType = appSettingsModel.getList(SIT_TYPE);

  @override
  initState() {
    super.initState();
    // _animationController =
    //     AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scrollController.addListener(() {
      if (scrollController.position.pixels == 0 &&
          !scrollController.position.outOfRange) {
        appBarOpacity = 0;
        setState(() {});
      } else {
        double value = (scrollController.position.pixels - 150) / 100;
        appBarOpacity = value.clamp(0.0, 1.0);
        setState(() {});
      }
    });

    var scrollSub = scrollTopController.stream.listen((p) {
      if (p != 3) return;
      scrollTotallyUp(scrollController);
    });
    subs.add(scrollSub);

    loadItems(false);
  }

  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  List<BaseModel> items = [];
  List topStories = [];
  List recentNews = [];

  bool setup = false;

  loadItems(bool isNew) async {
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection(CANDIDATE_BASE)
        //.where(PARTIES, arrayContains: userModel.getUserId())
        /* .limit(25)
        .orderBy(TIME, descending: !isNew)
        .startAt([
      !isNew
          ? (items.isEmpty
              ? DateTime.now().millisecondsSinceEpoch
              : items[items.length - 1][TIME] ?? 0.toInt())
          : (items.isEmpty ? 0 : items[0][TIME] ?? 0.toInt())
    ])*/
        .get();

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      String position = electoralPosition[model.getInt(ELECTORAL_POSITION)];
      bool president = position == "President";
      String state = model.getString(STATE);
      print(state);
      if (!president && state != userModel.getString(STATE) && !isAdmin)
        continue;
      int p = items.indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p == -1)
        items.add(model);
      else
        items[p] = model;
    }

    if (isNew) {
      refreshController.refreshCompleted();
    } else {
      int oldLength = otherPosts.length;
      int newLength = query.documents.length;
      if (newLength <= oldLength) {
        refreshController.loadNoData();
        canRefresh = false;
      } else {
        refreshController.loadComplete();
      }
    }
    setup = true;
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var s in subs) s?.cancel();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    Color iconColor = black.withOpacity(.7);

    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: black.withOpacity(.06)),
              //padding: EdgeInsets.all(2),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(sitType.length, (index) {
                    bool active = currentPage == index;
                    return GestureDetector(
                        onTap: () {
                          pageController.jumpToPage(index);
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                    color:
                                        black.withOpacity(active ? 0.02 : 0)),
                                color: black.withOpacity(active ? 0.06 : 0)),
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.all(2),
                            alignment: Alignment.center,
                            child: Text(
                              sitType[index],
                              style: textStyle(active, 14,
                                  black.withOpacity(active ? 1 : 0.6)),
                            )));
                  }),
                ),
              ),
            ),
            addSpace(5),
            Expanded(
              child: PageView.builder(
                controller: pageController,
                onPageChanged: (p) {
                  currentPage = p;
                  setState(() {});
                },
                itemCount: sitType.length,
                itemBuilder: (c, p) {
                  return listItems(p);
                },
              ),
            )
          ],
        ),
        LikeAnimation()
      ],
    );
  }

  listItems(int pMain) {
    var candidates = items.where((e) => e.getInt(SIT_TYPE) == pMain).toList();

    if (!setup) return loadingLayout();
    if (electoralPosition.isEmpty || candidates.isEmpty)
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            GetAsset.images('candidate.png'),
            height: 60,
            width: 60,
            fit: BoxFit.cover,
          ),
          addSpace(10),
          Text(
            'No Electoral Candidate.',
            style: textStyle(false, 16, black.withOpacity(.5)),
            textAlign: TextAlign.center,
          ),
          addSpace(10),
          FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              color: black.withOpacity(.2),
              onPressed: () {
                setup = false;
                setState(() {});
                loadItems(true);
              },
              child: Text(
                'Refresh',
                style: textStyle(true, 16, black),
              ))
        ],
      );

    return SmartRefresher(
      controller: refreshController,
      enablePullDown: true,
      enablePullUp: true,
      header: WaterDropHeader(),
      footer: ClassicFooter(
        noDataText:
            items.length < 9 ? '' : "Nothing more for now, check later...",
        textStyle: textStyle(false, 12, black.withOpacity(.7)),
      ),
      onLoading: () {
        //loadItems(false);
      },
      onRefresh: () {
        //loadItems(true);
      },
      child: ListView.builder(
        padding: EdgeInsets.only(bottom: 80),
        itemCount: electoralPosition.length,
        itemBuilder: (c, p) {
          return executiveItem(pMain, p);
        },
      ),
    );
  }

  executiveItem(int pMain, int p) {
    String position = electoralPosition[p];
    bool senators = position == "Senator";
    var candidates = items
        .where((e) =>
            e.getInt(ELECTORAL_POSITION) == p && e.getInt(SIT_TYPE) == pMain)
        .toList();

    if (candidates.isEmpty) return Container();

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              children: [
                /*Icon(Icons.bar_chart),
                addSpaceWidth(10),*/
                Text(
                  position,
                  style: textStyle(true, 18, black),
                ),
              ],
            ),
            padding: EdgeInsets.all(10),
            //margin: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: black.withOpacity(.00),
              border: Border.all(color: black.withOpacity(.02)),
              // borderRadius: BorderRadius.circular(8)
            ),
          ),
          //addSpace(10),
          if (candidates.isNotEmpty)
            SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List.generate(candidates.length, (p) {
                    BaseModel model = candidates[p];
                    String icon = model.getString(IMAGE_URL);
                    String name = model.getString(NAME);
                    String party = model.getString(POLITICAL_PARTY);
                    BaseModel district = model.getModel(DISTRICT);
                    List votes = model.getList(VOTES);
                    bool voted = votes.contains(userModel.getUserId());

                    return GestureDetector(
                      onTap: () {
                        pushAndResult(
                            context,
                            ShowCandidateProfile(
                              model: model,
                              heroTag: "e342\$...sdjsf",
                            ), result: (_) {
                          int p = items.indexWhere(
                              (e) => e.getObjectId() == model.getObjectId());
                          if (p == -1) return;
                          items[p] = _;
                          setState(() {});
                        });
                      },
                      child: Container(
                        width: 250,
                        height: 180,
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Center(
                                child: Container(
                                  width: 250,
                                  height: 140,
                                  margin: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      border: Border.all(
                                          width: .5,
                                          color: black.withOpacity(.05))),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20),
                                    child: Stack(
                                      children: [
                                        CachedNetworkImage(
                                          imageUrl: icon,
                                          width: 250,
                                          height: 140,
                                          fit: BoxFit.cover,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  colors: [
                                                /*appColor.withOpacity(.6),
                                                appColor_light.withOpacity(.6),*/
                                                black.withOpacity(.6),
                                                black.withOpacity(.2),
                                              ],
                                                  begin: Alignment.bottomCenter,
                                                  end: Alignment.topCenter)),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Container(
                                            padding: EdgeInsets.all(10),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  party,
                                                  style: textStyle(false, 12,
                                                      white.withOpacity(.8)),
                                                ),
                                                if (senators)
                                                  Text(
                                                    '${district.getString(NAME)}(${district.getString(STATE)})',
                                                    style: textStyle(false, 12,
                                                        white.withOpacity(.8)),
                                                  ),
                                                Text(
                                                  name,
                                                  maxLines: 2,
                                                  style: textStyle(
                                                      true, 16, white),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: RaisedButton(
                                shape: CircleBorder(),
                                onPressed: () {
                                  if (voted && !isAdmin) return;
                                  yesNoDialog(context, 'Important Message!',
                                      'You can only vote once. Ensure this is your Candidate.',
                                      () {
                                    model
                                      ..putInList(
                                          VOTES, userModel.getUserId(), !voted)
                                      ..updateItems();
                                    setState(() {});
                                  });
                                },
                                color: voted ? appColor : white,
                                elevation: 6,
                                child: (voted)
                                    ? Icon(
                                        Icons.check_circle_outline,
                                        color: white,
                                        size: 20,
                                      )
                                    : Image.asset(
                                        GetAsset.images('ic_archive.png'),
                                        height: 20,
                                        width: 20,
                                        fit: BoxFit.cover,
                                      ),
                                /* Icon(
                                  Icons.favorite,
                                  color: red0,
                                ),*/
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
                )),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
