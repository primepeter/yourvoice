import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/app.dart';

List<Map> rawTranslations = [];

Map<LangKey, Map<String, String>> localTranslation = {
  LangKey.english: {
    'english': 'English',
    'arabic': 'English',
    'french': 'English'
  },
  LangKey.arabic: {'english': 'Arabic', 'arabic': 'Arabic', 'french': 'Arabic'},
  LangKey.french: {'english': 'French', 'arabic': 'French', 'french': 'French'},
  LangKey.welcomePT1: {
    'english': 'Welcome to Hutaf',
    'arabic': '',
    'french': ''
  },
  LangKey.welcomePT2: {'english': 'Courses', 'arabic': '', 'french': ''},
  LangKey.welcomePT3: {
    'english': 'Have more friends',
    'arabic': '',
    'french': ''
  },
  LangKey.welcomePT4: {'english': 'Share Posts', 'arabic': '', 'french': ''},
  LangKey.welcomePD1: {
    'english':
        'Listen to plenty of Audio enriched books anytime and anywhere. You will find thousands of Audio books here',
    'arabic': '',
    'french': ''
  },
  LangKey.welcomePD2: {
    'english':
        'You’ll find in Hutaf many courses in different walks of life. We are sure you will find them of great benefit to enrich your knowledge and to sharpen your skills.',
    'arabic': '',
    'french': ''
  },
  LangKey.welcomePD3: {
    'english':
        'Here in this audio-world you can make new friends by communicating with Hutaf members in private messages, interacting, commenting and pressing like buttons of their posts in Hutaf.',
    'arabic': '',
    'french': ''
  },
  LangKey.welcomePD4: {
    'english':
        'Share your artwork by creating new posts of pictures or videos with your friends. Share your daily vibes and stories in Hutaf.',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpT1: {
    'english': 'Welcome to Hustaf',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpT2: {
    'english': 'Provide mobile & country',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpT3: {
    'english': 'Verify Your Number',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpT4: {
    'english': 'Create your Profile',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpD1: {
    'english': 'Provide your Email & Pass',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpD2: {
    'english': 'Link phone number',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpD3: {
    'english': 'Enter verification code sent',
    'arabic': '',
    'french': ''
  },
  LangKey.signUpD4: {
    'english': 'Tell us more about you',
    'arabic': '',
    'french': ''
  },
  LangKey.signUp: {'english': 'SignUp', 'arabic': '', 'french': ''},
  LangKey.login: {'english': 'Login', 'arabic': '', 'french': ''},
  LangKey.noAccount: {
    'english': "Don't have an account?",
    'arabic': '',
    'french': ''
  },
  LangKey.haveAccount: {
    'english': "Already have an account?",
    'arabic': '',
    'french': ''
  },
  LangKey.signIn: {'english': 'Sign In', 'arabic': '', 'french': ''},
  LangKey.logout: {'english': 'Logout', 'arabic': '', 'french': ''},
  LangKey.loginTitle: {
    'english': 'Login int your account',
    'arabic': '',
    'french': ''
  },
  LangKey.loginDes: {
    'english': 'Securely login into your account',
    'arabic': '',
    'french': ''
  },
  LangKey.enterEmail: {
    'english': 'Enter your email',
    'arabic': '',
    'french': ''
  },
  LangKey.enterPassword: {
    'english': 'Enter your password',
    'arabic': '',
    'french': ''
  },
  LangKey.reTypePassword: {
    'english': 'Retype Password',
    'arabic': '',
    'french': ''
  },
  LangKey.enterRetypePassword: {
    'english': 'Enter Retype Password',
    'arabic': '',
    'french': ''
  },
  LangKey.forgotPassword: {
    'english': "Forgot Password?",
    'arabic': '',
    'french': ''
  },
  LangKey.dontHaveAccount: {
    'english': "I don't have an Account",
    'arabic': '',
    'french': ''
  },
  LangKey.enterEmailRequired: {
    'english': "Enter your Email!",
    'arabic': '',
    'french': ''
  },
  LangKey.enterPasswordRequired: {
    'english': "Enter your Password!",
    'arabic': '',
    'french': ''
  },
  LangKey.enterFullNameRequired: {
    'english': "Enter your Name!",
    'arabic': '',
    'french': ''
  },
  LangKey.enterOTPRequired: {
    'english': "Enter OTP Code!",
    'arabic': '',
    'french': ''
  },
  LangKey.enterNickNameRequired: {
    'english': "Choose a NickName!",
    'arabic': '',
    'french': ''
  },
  LangKey.chooseDateOfBirthRequired: {
    'english': "Choose your Date of Birth!",
    'arabic': '',
    'french': ''
  },
  LangKey.enterProfessionRequired: {
    'english': "Enter your profession!",
    'arabic': '',
    'french': ''
  },
  LangKey.enterGenderRequired: {
    'english': "Choose your gender!",
    'arabic': '',
    'french': ''
  },
  LangKey.enterAboutRequired: {
    'english': "Tell us more about you",
    'arabic': '',
    'french': ''
  },
  LangKey.chooseCountryRequired: {
    'english': "Choose your country",
    'arabic': '',
    'french': ''
  },
  LangKey.passwordsDontMatch: {
    'english': "Passwords don't match!",
    'arabic': '',
    'french': ''
  },
  LangKey.recover: {'english': "Recover", 'arabic': '', 'french': ''},
  LangKey.reset: {'english': "Reset", 'arabic': '', 'french': ''},
  LangKey.recoverDesc: {
    'english': "Reset your account via email",
    'arabic': '',
    'french': ''
  },
  LangKey.resetPassword: {
    'english': "Reset Password",
    'arabic': '',
    'french': ''
  },
  LangKey.next: {'english': "Next", 'arabic': '', 'french': ''},
  LangKey.enterOTP: {'english': "Enter OTP", 'arabic': '', 'french': ''},
  LangKey.enterPhoneNumber: {
    'english': "Enter Phone Number",
    'arabic': '',
    'french': ''
  },
  LangKey.chooseCountry: {
    'english': "Choose Country",
    'arabic': 'Choose Country',
    'french': 'Choose Country'
  },
  LangKey.chooseDateOfBirth: {
    'english': "Date of Birth",
    'arabic': '',
    'french': ''
  },
  LangKey.chooseGender: {
    'english': "Choose Gender",
    'arabic': '',
    'french': ''
  },
  LangKey.enterProfession: {
    'english': "Enter Profession",
    'arabic': '',
    'french': ''
  },
  LangKey.chooseNickName: {
    'english': "Choose Nick Name",
    'arabic': '',
    'french': ''
  },
  LangKey.enterFullName: {
    'english': "Enter FullName",
    'arabic': '',
    'french': ''
  },
  LangKey.errorReceived: {
    'english': "Error Occurred!",
    'arabic': '',
    'french': ''
  },
  LangKey.successReceived: {
    'english': "Successful!",
    'arabic': '',
    'french': ''
  },
  LangKey.resetSuccess: {
    'english': "Reset Successful!",
    'arabic': '',
    'french': ''
  },
  LangKey.resetMessage: {
    'english':
        "A reset link has been sent to your email address.Please check your email",
    'arabic': '',
    'french': ''
  },
  LangKey.verifyCodeLoading: {
    'english': "Verifying OTP Code...",
    'arabic': '',
    'french': ''
  },
  LangKey.passResetLoading: {
    'english': "Sending password reset...",
    'arabic': '',
    'french': ''
  },
  LangKey.resendOTP: {'english': "Resend OTP", 'arabic': '', 'french': ''},
  LangKey.create: {'english': "Create Account", 'arabic': '', 'french': ''},
  LangKey.terms1: {
    'english': "By Proceeding you agree to our ",
    'arabic': '',
    'french': ''
  },
  LangKey.terms2: {'english': "Terms & Condition ", 'arabic': '', 'french': ''},
  LangKey.terms3: {'english': "and", 'arabic': '', 'french': ''},
  LangKey.terms4: {'english': "Privacy Policy", 'arabic': '', 'french': ''},
  LangKey.creatingAccount: {
    'english': "Creating your account...",
    'arabic': '',
    'french': ''
  },
  LangKey.male: {'english': "Male", 'arabic': '', 'french': ''},
  LangKey.female: {'english': "Female", 'arabic': '', 'french': ''},
  LangKey.interestTitle: {
    'english':
        "Your choice of right concerns It will help us know your desires and direct you to them",
    'arabic': '',
    'french': ''
  },
  LangKey.yourInterests: {
    'english': "Tell us your interests",
    'arabic': '',
    'french': ''
  },
  LangKey.letsStart: {
    'english': "Let's Get Started!",
    'arabic': '',
    'french': ''
  },
  LangKey.okString: {'english': "Ok", 'arabic': '', 'french': ''},
  LangKey.interestUpdated: {
    'english': "You have successfully updated your interests",
    'arabic': '',
    'french': ''
  },
  LangKey.fullName: {'english': "Full Name", 'arabic': '', 'french': ''},
  LangKey.username: {'english': "Username", 'arabic': '', 'french': ''},
  LangKey.profession: {'english': "Profession", 'arabic': '', 'french': ''},
  LangKey.gender: {'english': "Gender", 'arabic': '', 'french': ''},
  LangKey.country: {'english': "Gender", 'arabic': '', 'french': ''},
  LangKey.aboutYou: {'english': "About You", 'arabic': '', 'french': ''},
  LangKey.interests: {'english': "Interests", 'arabic': '', 'french': ''},
  LangKey.dateOfBirth: {'english': "Date of Birth", 'arabic': '', 'french': ''},
  LangKey.save: {'english': "Save", 'arabic': '', 'french': ''},
  LangKey.profileSettings: {
    'english': "Profile Settings",
    'arabic': '',
    'french': ''
  },
  LangKey.appSettings: {'english': "App Settings", 'arabic': '', 'french': ''},
  LangKey.addSuggestions: {
    'english': "Add Suggestions",
    'arabic': '',
    'french': ''
  },
  LangKey.successPartners: {
    'english': "Success Partners",
    'arabic': '',
    'french': ''
  },
  LangKey.privacyPolicy: {
    'english': "Privacy Policy",
    'arabic': '',
    'french': ''
  },
  LangKey.termsAndConditions: {
    'english': "Terms & Conditions",
    'arabic': '',
    'french': ''
  },
  LangKey.aboutUs: {'english': "About Us", 'arabic': '', 'french': ''},
  LangKey.writeAboutApp: {
    'english': "Write about this application",
    'arabic': '',
    'french': ''
  },
  LangKey.writeSuggestion: {
    'english': "Write your suggestion here...",
    'arabic': '',
    'french': ''
  },
  LangKey.addPhotos: {'english': "Add Photos", 'arabic': '', 'french': ''},
  LangKey.addVideos: {'english': "Add Videos", 'arabic': '', 'french': ''},
  LangKey.addPhotosAndVideos: {
    'english': "Add Photos & Videos",
    'arabic': '',
    'french': ''
  },
  LangKey.optional: {'english': "Optional", 'arabic': '', 'french': ''},
  LangKey.sendSuggestion: {
    'english': "Send Suggestions",
    'arabic': '',
    'french': ''
  },
  LangKey.suggestOrRequest: {
    'english': "Suggest or Request a new feature",
    'arabic': '',
    'french': ''
  },
  LangKey.requestSubSuccessful: {
    'english': "Your request/suggestion is being uploaded.Thank you",
    'arabic': '',
    'french': ''
  },
  LangKey.addaNote: {'english': "Add a Note", 'arabic': '', 'french': ''},
  LangKey.noteTitle: {'english': "Note Title", 'arabic': '', 'french': ''},
  LangKey.noteDescription: {
    'english': "Write your note description here...",
    'arabic': '',
    'french': ''
  },
  LangKey.noteSub: {
    'english': "Keep your notes with you",
    'arabic': '',
    'french': ''
  },
  LangKey.noteTitleRequired: {
    'english': "Add a title to this note",
    'arabic': '',
    'french': ''
  },
  LangKey.noteDescRequired: {
    'english': "Add a description to this note",
    'arabic': '',
    'french': ''
  },
  LangKey.suggestionRequired: {
    'english': "Add a description of the suggestions",
    'arabic': '',
    'french': ''
  },
  LangKey.noInternet: {
    'english': "No Internet Access!",
    'arabic': '',
    'french': ''
  },
  LangKey.location: {'english': "Location", 'arabic': '', 'french': ''},
  LangKey.friends: {'english': "Friends", 'arabic': '', 'french': ''},
  LangKey.shareSomething: {
    'english': "Want to share something?",
    'arabic': '',
    'french': ''
  },
  LangKey.addToYourPost: {
    'english': "Add to your post",
    'arabic': '',
    'french': ''
  },
  LangKey.createPost: {'english': "Create Post", 'arabic': '', 'french': ''},
  LangKey.people: {'english': "People", 'arabic': '', 'french': ''},
  LangKey.cameraRoll: {'english': "Camera Roll", 'arabic': '', 'french': ''},
  LangKey.addaVideo: {'english': "Add a video", 'arabic': '', 'french': ''},
  LangKey.tagSomeone: {'english': "Tag Someone", 'arabic': '', 'french': ''},
  LangKey.tagLocation: {'english': "Tag Location", 'arabic': '', 'french': ''},
  LangKey.chooseCategory: {
    'english': "Choose Category",
    'arabic': '',
    'french': ''
  },
  LangKey.search: {'english': "Search", 'arabic': '', 'french': ''},
  LangKey.searchPlace: {'english': "Search Place", 'arabic': '', 'french': ''},
  LangKey.signOut: {'english': "SignOut", 'arabic': '', 'french': ''},
  LangKey.goodNight: {'english': "Good Night", 'arabic': '', 'french': ''},
  LangKey.goodMorning: {'english': "Good Morning", 'arabic': '', 'french': ''},
  LangKey.goodAfternoon: {
    'english': "Good Afternoon",
    'arabic': '',
    'french': ''
  },
  LangKey.goodEvening: {'english': "Good Evening", 'arabic': '', 'french': ''},
  LangKey.bedTime: {'english': "BedTime", 'arabic': '', 'french': ''},
  LangKey.beenWithUs: {
    'english': "You have been with us since",
    'arabic': '',
    'french': ''
  },
  LangKey.followers: {'english': "Followers", 'arabic': '', 'french': ''},
  LangKey.following: {'english': "Following", 'arabic': '', 'french': ''},
  LangKey.post: {'english': "Post", 'arabic': '', 'french': ''},
  LangKey.sharePhotos: {
    'english': "Share Photos and Videos",
    'arabic': '',
    'french': ''
  },
  LangKey.whenYouShare: {
    'english':
        "When you share photos and videos, they'll appear on your profile",
    'arabic': '',
    'french': ''
  },
  LangKey.aboutMe: {'english': "About Me", 'arabic': '', 'french': ''},
  LangKey.choose: {'english': "Choose", 'arabic': '', 'french': ''},
  LangKey.enabled: {'english': "Enabled", 'arabic': '', 'french': ''},
  LangKey.disabled: {'english': "Disabled", 'arabic': '', 'french': ''},
  LangKey.pushNotifications: {
    'english': "Push Notifications",
    'arabic': '',
    'french': ''
  },
  LangKey.libraryNotifications: {
    'english': "Library Notifications",
    'arabic': '',
    'french': ''
  },
  LangKey.chatNotifications: {
    'english': "Chat Notifications",
    'arabic': '',
    'french': ''
  },
  LangKey.appAndSettings: {
    'english': "App & Settings",
    'arabic': '',
    'french': ''
  },
  LangKey.appLanguage: {'english': "App Language", 'arabic': '', 'french': ''},
  LangKey.shareApp: {'english': "Share App", 'arabic': '', 'french': ''},
  LangKey.rateApp: {'english': "Rate App", 'arabic': '', 'french': ''},
  LangKey.settings: {'english': "Settings", 'arabic': '', 'french': ''},
  LangKey.preview: {'english': "Preview", 'arabic': '', 'french': ''},
  LangKey.clickBack: {
    'english': "Click back again to exit",
    'arabic': '',
    'french': ''
  },
  LangKey.noFollowing: {'english': "No Following", 'arabic': '', 'french': ''},
  LangKey.noFollowingMsg: {
    'english': "You are not following anyone yet",
    'arabic': '',
    'french': ''
  },
  LangKey.noFollowers: {'english': "No Followers", 'arabic': '', 'french': ''},
  LangKey.noFollowersMsg: {
    'english': "You have no followers yet",
    'arabic': '',
    'french': ''
  },
  LangKey.nothingToPost: {
    'english': "Nothing to post!",
    'arabic': '',
    'french': ''
  },
  LangKey.send: {'english': "Send", 'arabic': '', 'french': ''},
  LangKey.submitReport: {
    'english': "Submit Report",
    'arabic': '',
    'french': ''
  },
};

enum LangKey {
  submitReport,
  send,
  nothingToPost,
  noFollowing,
  noFollowingMsg,
  noFollowers,
  noFollowersMsg,
  clickBack,
  preview,
  settings,
  shareApp,
  rateApp,
  appLanguage,
  appAndSettings,
  pushNotifications,
  libraryNotifications,
  chatNotifications,
  enabled,
  disabled,
  choose,
  aboutMe,
  whenYouShare,
  sharePhotos,
  post,
  following,
  followers,
  beenWithUs,
  bedTime,
  goodMorning,
  goodAfternoon,
  goodEvening,
  goodNight,
  signOut,
  search,
  searchPlace,
  chooseCategory,
  tagLocation,
  addaVideo,
  tagSomeone,
  cameraRoll,
  people,
  createPost,
  addToYourPost,
  shareSomething,
  location,
  friends,

  noInternet,
  noteTitleRequired,
  noteDescRequired,
  addaNote,
  noteSub,
  noteDescription,
  noteTitle,
  note,

  requestSubSuccessful,
  suggestOrRequest,
  writeSuggestion,
  suggestionRequired,
  addPhotos,
  addVideos,
  addPhotosAndVideos,
  optional,
  sendSuggestion,

  profileSettings,
  appSettings,
  addSuggestions,
  aboutUs,
  successPartners,
  privacyPolicy,
  termsAndConditions,
  writeAboutApp,

  fullName,
  save,
  username,
  profession,
  gender,
  dateOfBirth,
  country,
  aboutYou,
  interests,

  english,
  arabic,
  french,
  welcomePT1,
  welcomePT2,
  welcomePT3,
  welcomePT4,

  welcomePD1,
  welcomePD2,
  welcomePD3,
  welcomePD4,

  signUpT1,
  signUpT2,
  signUpT3,
  signUpT4,

  signUpD1,
  signUpD2,
  signUpD3,
  signUpD4,

  login,
  signUp,

  loginTitle,
  loginDes,
  noAccount,
  haveAccount,
  signIn,
  logout,

  enterEmail,
  enterPassword,
  enterRetypePassword,
  reTypePassword,
  passwordsDontMatch,
  forgotPassword,
  dontHaveAccount,

  enterEmailRequired,
  enterPasswordRequired,
  enterOTPRequired,
  enterFullNameRequired,
  enterNickNameRequired,
  chooseDateOfBirthRequired,
  enterProfessionRequired,
  enterGenderRequired,
  enterAboutRequired,
  chooseCountryRequired,

  reset,
  recover,
  recoverDesc,
  resetPassword,
  recoverAccount,
  next,
  enterOTP,
  enterPhoneNumber,

  chooseCountry,
  chooseDateOfBirth,
  chooseGender,
  male,
  female,

  enterProfession,
  chooseNickName,

  errorReceived,
  successReceived,
  resetSuccess,

  resetMessage,

  enterFullName,

  verifyCodeLoading,
  passResetLoading,

  resendOTP,
  create,

  terms1,
  terms2,
  terms3,
  terms4,

  creatingAccount,
  notFound,
  interestTitle,
  yourInterests,
  letsStart,
  okString,
  interestUpdated,
}

class Language {
  static init() {
    SharedPreferences.getInstance().then((value) {
      appLanguage = value.getString(APP_LANGUAGE) ?? 'english';
      defaultCountry = value.getString(COUNTRY) ?? 'Nigeria';
      languageVersion = value.getString(LANGUAGE_VERSION) ?? 'maugost';
      if (null == value.getString(TRANS_BASE)) listen();
      String encodedTrans = value.getString(TRANS_BASE) ?? '[]';
      rawTranslations = List.from(jsonDecode(encodedTrans));
      for (var d in rawTranslations) {
        LangKey key = stringToLangKey(d[OBJECT_ID]);
        Map data = d[TRANSLATIONS];
        localTranslation[key] = Map<String, String>.from(data);
        //print("key $key ${d[OBJECT_ID]} ${localTranslation[key]}");
      }
      Future.delayed(Duration(seconds: 5), () {
        String onlineVersion = appSettingsModel.getString(LANGUAGE_VERSION);
        if (null == onlineVersion) onlineVersion = "";
        print(
            "Updating language version $languageVersion  online $onlineVersion");
        if (languageVersion.isEmpty || (languageVersion != onlineVersion)) {
          value.clear();
          Language.listen();
        }
      });
    });
  }

  static manualOverride() {
    for (var item in rawTranslations) {
      BaseModel model = BaseModel(items: item);
      Future.delayed(Duration(seconds: 2), () {
        model.saveItem(TRANS_BASE, false, document: model.getObjectId());
      });
    }
  }

  static LangKey stringToLangKey(String key) {
    int p = LangKey.values.toList().indexWhere((e) {
      String k1 = e.toString().split('.')[1].toLowerCase();
      String k2 = key;
      if (k2.contains('.')) k2 = k2.toString().split('.')[1].toLowerCase();
      return k1 == k2;
    });
    if (p == -1) return LangKey.notFound;
    return LangKey.values.toList()[p];
  }

  static uploadLocalKey() async {
    for (LangKey keys in localTranslation.keys.toList()) {
      print("translation Uploading $keys");
      String docId = keys.toString().toLowerCase();
      Map<String, String> data = localTranslation[keys];
      for (String key in data.keys.toList()) {
        //if (data[key] == null) data[key] = "";
        if (data[key].isNotEmpty) continue;
        data.remove(key);
      }
      BaseModel model = BaseModel();
      model.put(TRANSLATIONS, data);
      model.put(OBJECT_ID, docId);
      Future.delayed(Duration(seconds: 2), () async {
        model.saveItem(TRANS_BASE, false, document: docId, merged: true);
      });
      // final doc = await Firestore.instance.collection(TRANS_BASE).document(docId).get();
      // if(doc==null||!doc.exists){
      //   Future.delayed(Duration(seconds: 2), () async{
      //     model.saveItem(TRANS_BASE, false, document: docId,merged: true);
      //   });
      //   continue;
      // }

      /*for (String keys in data.keys.toList()) {
        if (data[keys] != null) continue;
        data[keys] = "";
      }
      model.put(TRANSLATIONS, data);*/
      addToRawTranslations(model);
    }
    print("Uploading translation complete");
  }

  static updateFromOnline() async {
    final docs = await Firestore.instance
        .collection(TRANS_BASE)
        .getDocuments()
        .catchError((e) {
      print("Error $e");
    });

    // upload the local TRANSLATIONSs.....
    if (null == docs || docs.documents.isEmpty) {
      for (LangKey keys in localTranslation.keys.toList()) {
        print("translation Uploading $keys");
        String docId = keys.toString().toLowerCase();
        Map data = localTranslation[keys];
        BaseModel model = BaseModel();
        model.put(TRANSLATIONS, data);
        model.put(OBJECT_ID, docId);
        model.saveItem(TRANS_BASE, false, document: docId);
        addToRawTranslations(model);
      }
      print("Uploading translation complete");
      return;
    }

    for (var doc in docs.documents) {
      print("Updating translation ${doc.documentID}");
      LangKey key = stringToLangKey(doc.documentID);
      String englishTrans = localTranslation[key]['english'];
      BaseModel model = BaseModel(doc: doc);
      Map translation = model.getMap(TRANSLATIONS);
      translation['english'] = englishTrans;
      model.put(TRANSLATIONS, TRANSLATIONS);
      model.saveItem(TRANS_BASE, false, document: doc.documentID, merged: true);
      addToRawTranslations(model);
      await Future.delayed(Duration(seconds: 1));
    }
    print("Updating translation complete");
  }

  static listen() async {
    var sub =
        Firestore.instance.collection(TRANS_BASE).snapshots().listen((docs) {
      // download the local TRANSLATIONSs.....
      if (null == docs || docs.documents.isEmpty) {
        return;
      }

      for (final doc in docs.documents) {
        //print("Transactions Downloading ${doc.data}");
        BaseModel model = BaseModel(doc: doc);
        LangKey key = stringToLangKey(model.getObjectId());
        localTranslation[key] =
            Map<String, String>.from(model.getMap(TRANSLATIONS));
        addToRawTranslations(model);
      }
    });
    subs.add(sub);
    //     .catchError((e) {
    //   print("Error $e");
    // });

    await Future.delayed(Duration(seconds: 5));
    SharedPreferences.getInstance().then((pref) {
      pref.setString(TRANS_BASE, jsonEncode(rawTranslations));
      pref.setString(
          LANGUAGE_VERSION, appSettingsModel.getString(LANGUAGE_VERSION));
      print("Downloads $rawTranslations");
    });
  }

  static String value(LangKey key) {
    Map<String, String> language = localTranslation[key];

    //print(localTranslation);
    if (null == language || language.isEmpty) {
      return "No Trans";
    }
    String translation = localTranslation[key][appLanguage];
    if (null == translation || translation.isEmpty) {
      return "** ${localTranslation[key]["english"]}";
    }
    return localTranslation[key][appLanguage];
  }

  static addToRawTranslations(BaseModel model) {
    int p =
        rawTranslations.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
    if (p == -1)
      rawTranslations.add(model.stripTimestamp());
    else
      rawTranslations[p] = model.stripTimestamp();
    // print(model.stripTimestamp());
    // print("Downloads ${rawTranslations[0]}");
  }

  static saveAppLanguage(String language, update) {
    SharedPreferences.getInstance().then((value) {
      value.setString(APP_LANGUAGE, language);
      appLanguage = language;
      update();
    });
  }
}
