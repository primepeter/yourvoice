import 'dart:async';

import 'package:acclaim/app/app.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'add_partners.dart';
import 'app_config.dart';

class ShowPartners extends StatefulWidget {
  @override
  _ShowPartnersState createState() => _ShowPartnersState();
}

class _ShowPartnersState extends State<ShowPartners> {
  //List<BaseModel> appPartners =appPartners;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  postBody() {
    //if (!hasLoaded) return loadingLayout();

    if (usersOnline.isEmpty)
      return emptyLayoutX(
          Feather.activity, "No Partners", "No Partners has checked in yet.",
          isIcon: true);

    return ListView.builder(
        itemCount: usersOnline.length,
        itemBuilder: (ctx, p) {
          return postsViewBuilder(p);
        });
  }

  postsViewBuilder(int p) {
    BaseModel partner = usersOnline[p];
    String website = partner.getString(WEB_ADDRESS);

    return Container(
      margin: 5.padAll(),
      padding: 10.padAll(),
      height: 300,
      decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: black.withOpacity(.03))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          5.spaceHeight(),
          Text(
            website,
            style: textStyle(false, 16, black),
          ),
          5.spaceHeight(),
          PartnersImagesState(
            partner: partner,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context, LangsKey.drawer_4)),
        actions: <Widget>[
          //if (isAdmin)
          FlatButton(
            onPressed: () {
              pushAndResult(context, AddPartners(), result: (_) {
                setState(() {});
              });
            },
            child: Text(
              translate(context, LangsKey.add),
              style: textStyle(false, 16, white),
            ),
          )
        ],
      ),
      body: postBody(),
    );
  }
}

class PartnersImagesState extends StatefulWidget {
  final BaseModel partner;

  const PartnersImagesState({Key key, this.partner}) : super(key: key);
  @override
  _PartnersImagesStateState createState() => _PartnersImagesStateState();
}

class _PartnersImagesStateState extends State<PartnersImagesState> {
  List<BaseModel> photos;
  int currentPage = 0;
  final vp = PageController();
  Timer timer;
  bool reversing = false;
  final _codeWheeler = CodeWheeler(milliseconds: 8000);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (mounted)
      setState(() {
        photos = widget.partner.getListModel(PHOTOS);
      });
    if (photos.length > 1) _codeWheeler.run(pageWheeler);
  }

  pageWheeler() {
    if (null == vp || !mounted) return;
    if (currentPage < photos.length - 1 && !reversing) {
      reversing = false;
      if (mounted) setState(() {});
      vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      return;
    }
    if (currentPage == photos.length - 1 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = true;
        if (mounted) setState(() {});
        vp.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
    if (currentPage == 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (currentPage == 0 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (currentPage > 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        vp.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
  }

  @override
  dispose() {
    timer?.cancel();
    super.dispose();
  }

  imagesLayout(List<BaseModel> media) {
    return Expanded(
      child: GestureDetector(
        onTap: () async {
          await openLink(widget.partner.getString(WEB_ADDRESS));
        },
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            PageView.builder(
                controller: vp,
                onPageChanged: (p) {
                  setState(() {
                    currentPage = p;
                  });
                },
                itemCount: media.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (ctx, p) {
                  final photo = media[p];
                  return CachedNetworkImage(
                    imageUrl: photo.getString(URL_PATH),
                    width: double.infinity,
                    fit: BoxFit.cover,
                    alignment: Alignment.topCenter,
                    placeholder: (c, s) {
                      return placeHolder(200);
                    },
                  );
                }),
            Container(
              padding: EdgeInsets.all(1),
              margin: 5.padAll(),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: black.withOpacity(.7)),
              child: new DotsIndicator(
                dotsCount: media.length,
                position: currentPage,
                decorator: DotsDecorator(
                  size: const Size.square(5.0),
                  color: white,
                  activeColor: AppConfig.appColor,
                  activeSize: const Size(10.0, 7.0),
                  activeShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return imagesLayout(photos);
  }
}
