import 'package:acclaim/app/app.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'LangMain.dart';
import 'app_config.dart';

class CreateReport extends StatefulWidget {
  final BaseModel item;
  const CreateReport({Key key, this.item}) : super(key: key);
  @override
  _CreateReportState createState() => _CreateReportState();
}

class _CreateReportState extends State<CreateReport> {
  String selectedCategory = "";
  List allItems = appSettingsModel.getList(APP_REPORT_CATEGORIES);
  final reasonController = TextEditingController();
  BaseModel item;
  bool isUser = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    item = widget.item;
    isUser = item.getString(DATABASE_NAME) == USER_BASE;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        addSpace(30),
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                "Report item",
                style: textStyle(true, 20, black),
              )),
              //if (selections.isNotEmpty)
              FlatButton(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  color: dark_green5,
                  onPressed: () {
                    handleReport();
                  },
                  child: Text(
                    LangKey.submitReport.toAppLanguage,
                    style: textStyle(true, 14, white),
                  )),
              addSpaceWidth(10)
            ],
          ),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: showSuccess ? dark_green0 : red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        pageList()
      ],
    );
  }

  pageList() {
    return Expanded(
      child: SingleChildScrollView(
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Please select a problem to continue",
              style: textStyle(true, 20, black),
            ),
            Text(
              "You can only make a report after selecting the problem",
              style: textStyle(false, 14, black.withOpacity(.5)),
            ),
            addSpace(10),
            Container(
              width: double.infinity,
              child: Wrap(
                //alignment: WrapAlignment.start,
                children: allItems.map((e) {
                  BaseModel model = BaseModel(items: e);
                  Map translations = model.getMap(TRANSLATIONS);
                  bool selected = selectedCategory == model.getObjectId();
                  bool isDefault = model.getBoolean(IS_DEFAULT);

                  return GestureDetector(
                    onTap: () {
                      if (isDefault) return;
                      if (mounted)
                        setState(() {
                          selectedCategory = model.getObjectId();
                        });
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 10, right: 10, top: 4, bottom: 4),
                      margin: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color:
                              AppConfig.appColor.withOpacity(selected ? 1 : .1),
                          borderRadius: BorderRadius.circular(8)),
                      child: Text(
                        translations.toAppLanguage,
                        style:
                            textStyle(selected, 16, selected ? white : black),
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
            addSpace(10),
            inputTextView("Describe the issue", reasonController,
                maxLine: 5, isNum: false),
          ],
        ),
      ),
    );
  }

  String errorText = "";
  bool showSuccess = false;
  showError(String text, {bool wasLoading = false, bool success = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    showSuccess = success;
    if (mounted) setState(() {});

    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      showSuccess = false;
      if (mounted) setState(() {});
    });
  }

  handleReport() async {
    String reason = reasonController.text;

    if (selectedCategory.isEmpty) {
      showError("Choose what the problem is!");
      return;
    }
    if (reason.isEmpty) {
      showError("Describe the issue with the item");
      return;
    }

    if (!await isConnected()) {
      showErrorDialog(context, "No Internet Connectivity");
      return;
    }

    Map<String, dynamic> reportData = {
      REPORTS: FieldValue.arrayUnion([
        {
          NAME: userModel.name,
          USER_ID: userModel.getObjectId(),
          TITLE: reason,
          MESSAGE: reason,
          TIME: DateTime.now().millisecondsSinceEpoch
        }
      ]),
      ITEM_NAME: item.getString(isUser ? item.name : MESSAGE),
      ITEM_ID: item.getObjectId(),
      ITEM_DB: item.getString(DATABASE_NAME),
      STATUS: PENDING,
      DATABASE_NAME: REPORT_BASE,
      OBJECT_ID: item.getObjectId(),
      TIME_UPDATED: DateTime.now().millisecondsSinceEpoch
    };

    DocumentReference doc = FirebaseFirestore.instance
        .collection(REPORT_BASE)
        .doc(item.getObjectId());
    doc.update(reportData).catchError((e) {
      if (e.toString().toLowerCase().contains("found")) {
        reportData[TIME] = DateTime.now().millisecondsSinceEpoch;
        doc.set(reportData);
      }
    });

    NotificationService.sendPush(
      topic: "admin",
      title: "Report",
      body: "Some reports are pending approval",
    );
    item
      ..putInList(HIDDEN, userModel.getUserId())
      ..updateItems();
    await Future.delayed(Duration(seconds: 2));
    showProgress(false, context);
    await Future.delayed(Duration(milliseconds: 500));
    showMessage(context, Icons.check, pink3, "Report Submitted",
        "Thank you for keeping our platform safe", cancellable: false,
        onClicked: (_) {
      Navigator.pop(context, true);
    });
  }
}
