import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'app/app.dart';
import 'app_config.dart';

class AddPartners extends StatefulWidget {
  final BaseModel partner;

  const AddPartners({Key key, this.partner}) : super(key: key);
  @override
  _AddPartnersState createState() => _AddPartnersState();
}

class _AddPartnersState extends State<AddPartners> {
  final titleController = TextEditingController();
  final websiteController = TextEditingController();
  bool imagesEdited = false;
  List<BaseModel> photos = [];
  final scaffoldKey = GlobalKey<ScaffoldState>();
  BaseModel partner = BaseModel();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (null != widget.partner) {
      setState(() {
        partner = widget.partner;
        loadPartner();
      });
    }
  }

  loadPartner() {
    photos = partner.getPhotos;
    titleController.text = partner.getString(TITLE);
    websiteController.text = partner.getString(WEBSITE);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        showMessage(context, Icons.error, red0, "Quit saving?",
            "You would loose current record if you quit without saving. Are you sure you want to quit?",
            cancellable: true, clickYesText: 'Quit', onClicked: (_) {
          if (_) {
            Navigator.pop(context, "");
          }
        });
        return false;
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: white,
        body: pageBody(),
      ),
    );
  }

  pageBody() {
    return Container(
      margin: 20.padAt(t: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.centerRight,
            child: FlatButton(
              onPressed: () {
                showMessage(context, Icons.error, red0, "Quit saving?",
                    "You would loose current record if you quit without saving. Are you sure you want to quit?",
                    cancellable: true, clickYesText: 'Quit', onClicked: (_) {
                  if (_) {
                    Navigator.pop(context);
                  }
                });
              },
              shape: CircleBorder(),
              padding: EdgeInsets.all(20),
              child: Icon(Icons.close),
            ),
          ),
          Container(
            padding: EdgeInsets.all(14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  translate(context, LangsKey.drawer_4),
                  textAlign: TextAlign.start,
                  style: textStyle(true, 25, Colors.black),
                ),
                addSpace(4),
                Text(
                  translate(context, LangsKey.create_new_partners),
                  style: textStyle(false, 14, Colors.black.withOpacity(.7)),
                )
              ],
            ),
          ),
          addSpace(15),
          Flexible(
            child: ListView(
              padding: 0.padAll(),
              children: <Widget>[
                authInputField(
                  controller: titleController,
                  title: translate(context, LangsKey.title),
                  hint: translate(context, LangsKey.write_a_title),
                ),
                addSpace(10),
                authInputField(
                  controller: websiteController,
                  title: translate(context, LangsKey.website),
                  hint: translate(context, LangsKey.enter_partners_website),
                ),
                addSpace(10),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        translate(context, LangsKey.partners_photos),
                        style: textStyle(true, 18, black),
                      ),
                      Text(
                        translate(context, LangsKey.partners_photos_max),
                        style: textStyle(false, 13, black.withOpacity(.5)),
                      ),
                    ],
                  ),
                ),
                addSpace(10),
                if (photos.isNotEmpty)
                  Container(
                    height: 240,
                    child: LayoutBuilder(
                      builder: (ctx, b) {
                        bool isEmpty = photos.isEmpty;
                        int photoLength = photos.length;

                        return Column(
                          children: <Widget>[
                            Flexible(
                              child: ListView.builder(
                                  itemCount: photoLength,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (ctx, p) {
                                    BaseModel photo = photos[p];
                                    bool isLocal = photo.isLocal;

                                    return Stack(
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.all(8),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: isLocal
                                                ? Image.file(
                                                    File(photo.urlPath),
                                                    height: 200,
                                                    width: 150,
                                                    fit: BoxFit.cover,
                                                  )
                                                : CachedNetworkImage(
                                                    imageUrl: photo.urlPath,
                                                    height: 200,
                                                    width: 150,
                                                    fit: BoxFit.cover,
                                                    placeholder: (ctx, s) {
                                                      return placeHolder(200,
                                                          width: 120);
                                                    },
                                                  ),
                                          ),
                                        ),
                                        Container(
                                          height: 200,
                                          width: 150,
                                          alignment: Alignment.center,
                                          child: Container(
                                            height: 50,
                                            width: 50,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                color: black.withOpacity(.4),
                                                shape: BoxShape.circle),
                                            child: Text(
                                              '${p + 1}',
                                              style: textStyle(true, 16, white),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              EdgeInsets.fromLTRB(15, 15, 0, 0),
                                          width: 30,
                                          height: 30,
                                          child: new RaisedButton(
                                              padding: EdgeInsets.all(0),
                                              elevation: 2,
                                              shape: CircleBorder(),
                                              color: red0,
                                              child: Icon(
                                                Icons.close,
                                                color: white,
                                                size: 13,
                                              ),
                                              onPressed: () {
                                                toast(scaffoldKey, "Removed!");

                                                photos.removeAt(p);
                                                setState(
                                                  () {},
                                                );
                                              }),
                                        ),
                                      ],
                                    );
                                  }),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                if (photos.length < 3) ...[
                  10.spaceHeight(),
                  Container(
                    padding: 10.padAt(l: 10, r: 10),
                    child: FlatButton(
                      onPressed: () async {
                        getMultiCroppedImage(context,
                            topTitle: "Choose Media",
                            max: 3 - photos.length, onPicked: (path) {
                          if (null == path) return;
                          for (var pic in path) {
                            photos.addModel(pic);
                          }
                          if (mounted) setState(() {});
                        });
                      },
                      color: black.withOpacity(.5),
                      padding: EdgeInsets.all(8),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      child: Center(
                        child: Text(
                          translate(context, LangsKey.add_photos),
                          style: textStyle(true, 14, white),
                        ),
                      ),
                    ),
                  )
                ],
                addSpace(10),
                Container(
                  padding: EdgeInsets.all(14),
                  child: FlatButton(
                    onPressed: () {
                      onSavePressed();
                    },
                    color: AppConfig.appColor,
                    padding: EdgeInsets.all(16),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    child: Center(
                      child: Text(
                        translate(context, LangsKey.save),
                        style: textStyle(true, 16, white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  onSavePressed() async {
    String title = titleController.text;
    String website = websiteController.text;

    bool hasInternet = await isConnected();

    if (title.isEmpty) {
      toast(scaffoldKey, "Please enter a title");
      return;
    }

    if (website.isEmpty) {
      toast(scaffoldKey, "Please enter partners website address");
      return;
    }

    if (!website.startsWith("http")) {
      toast(scaffoldKey, "Please web address must start with http://");
      return;
    }

    if (photos.isEmpty) {
      toast(scaffoldKey, "Photos of the partners can't be empty");
      return;
    }

    if (photos.length < 3) {
      toast(scaffoldKey, "Photos of partners can't be less than 3");
      return;
    }

    if (!hasInternet) {
      toast(scaffoldKey, "No Internet Access!", color: Colors.red);
      return;
    }

    showProgress(true, context, msg: "Saving Note...");

    partner
      ..put(TITLE, title)
      ..put(DESCRIPTION, website)
      ..put(PHOTOS, photos.map((e) => e.items).toList())
      ..put(SEARCH_PARAM, getSearchString('$title $website'));

    if (photos.isEmpty) {
      partner.saveItem(NOTE_BASE, false, onComplete: () {
        showProgress(false, context);
        setState(() {
          usersOnline.addModel(partner);
        });
        showMessage(
            context,
            Icons.check,
            Colors.green,
            translate(context, LangsKey.successful),
            translate(context, LangsKey.profile_save),
            clickYesText: translate(context, LangsKey.ok_string),
            delayInMilli: 1000,
            cancellable: true, onClicked: (_) async {
          Navigator.pop(context, "");
        });
      });
      return;
    }

    /*   uploadMediaFiles(photos, onError: (err) {
      showProgress(false, context);
      showMessage(context, Icons.error, red0, "Upload Error", err.toString(),
          delayInMilli: 900, cancellable: true);
      return;
    }, onUploaded: (uploaded) {
      partner..put(PHOTOS, uploaded.map((e) => e.items).toList());
      partner.saveItem(PARTNER_BASE, false, onComplete: () {
        showProgress(false, context);
        setState(() {
          appPartners.addModel(partner);
        });
        showMessage(
            context,
            Icons.check,
            Colors.green,
            translate(context, LangsKey.successful),
            translate(context, LangsKey.profile_save),
            clickYesText: translate(context, LangsKey.ok_string),
            delayInMilli: 1000,
            cancellable: true, onClicked: (_) async {
          Navigator.pop(context, "");
        });
      });
    });*/
  }

  onError(e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0, "Error saving", e.bookmarkList,
        delayInMilli: 900, cancellable: true);
  }
}
