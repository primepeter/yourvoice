import 'dart:math' as math;

import 'package:flutter/material.dart';

class StackedList extends StatelessWidget {
  final List<Color> _colors = Colors.primaries;
  static const _minHeight = 16.0;
  static const _maxHeight = 150.0;

  final List<Widget> projects;

  const StackedList({Key key, this.projects}) : super(key: key);

  Color getBackgroundColor(int p) {
    if (p % 3 == 0) {
      return Color(0xffe5dbd3);
    }

    if (p % 3 == 1) {
      return Color(0xff31348c);
    }

    return Color(0xff7643b3);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: projects.map((e) {
        return StackedListChild(
            minHeight: _minHeight,
            maxHeight: projects.indexOf(e) == projects.length - 1
                ? MediaQuery.of(context).size.height
                : _maxHeight,
            pinned: true,
            child: Container(
              color: projects.indexOf(e) == 0
                  ? Colors.black.withOpacity(0)
                  : getBackgroundColor(projects.indexOf(e) - 1),
              margin: EdgeInsets.symmetric(
                horizontal: 15,
              ),
              child: ClipRRect(
                borderRadius:
                    BorderRadius.vertical(top: Radius.circular(_minHeight)),
                child: Container(
                  //child: e,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(_minHeight)),
                    color: getBackgroundColor(projects.indexOf(e)),
                  ),
                ),
              ),
            ));
      }).toList(),
    );
  }
}

class StackedListChild extends StatelessWidget {
  final double minHeight;
  final double maxHeight;
  final bool pinned;
  final bool floating;
  final Widget child;

  SliverPersistentHeaderDelegate get _delegate => _StackedListDelegate(
      minHeight: minHeight, maxHeight: maxHeight, child: child);

  const StackedListChild({
    Key key,
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
    this.pinned = false,
    this.floating = false,
  })  : assert(child != null),
        assert(minHeight != null),
        assert(maxHeight != null),
        assert(pinned != null),
        assert(floating != null),
        super(key: key);

  @override
  Widget build(BuildContext context) => SliverPersistentHeader(
      key: key, pinned: pinned, floating: floating, delegate: _delegate);
}

class _StackedListDelegate extends SliverPersistentHeaderDelegate {
  final double minHeight;
  final double maxHeight;
  final Widget child;

  _StackedListDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => math.max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_StackedListDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
