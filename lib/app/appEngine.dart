part of 'app.dart';

class CodeWheeler {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;
  int page = 0;

  CodeWheeler({this.milliseconds});

  run(Function action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer.periodic(Duration(milliseconds: milliseconds), (_) {
      action();
    });
  }

  close() {
    _timer?.cancel();
  }
}

List<Countries> getCountries() {
  return countryMap
      .map((c) => Countries(
          countryName: c["Name"],
          countryCode: '+${c["Code"]}',
          countryFlag: 'flags/${c["ISO"]}.png'))
      .toList();
}

int getSeconds(String time) {
  List parts = time.split(":");
  int mins = int.parse(parts[0]) * 60;
  int secs = int.parse(parts[1]);
  return mins + secs;
}

String getTimerText(int seconds, {bool three = false}) {
  int hour = seconds ~/ Duration.secondsPerHour;
  int min = (seconds ~/ 60) % 60;
  int sec = seconds % 60;

  String h = hour.toString();
  String m = min.toString();
  String s = sec.toString();

  String hs = h.length == 1 ? "0$h" : h;
  String ms = m.length == 1 ? "0$m" : m;
  String ss = s.length == 1 ? "0$s" : s;

  return three ? "$hs:$ms:$ss" : "$ms:$ss";
}

textStyle(bool bold, double size, color,
    {underlined = false,
    String font = "Nunito",
    bool padd = true,
    decorationColor = AppConfig.appColor}) {
  return TextStyle(
      color: color,
      fontWeight: FontWeight.normal,
      fontFamily: bold ? "${font}B" : font,
      fontSize: size,
      decorationColor: decorationColor,
      decorationThickness: 1,
      //decorationStyle: TextDecorationStyle.solid,
      decoration: underlined ? TextDecoration.underline : TextDecoration.none);
}

/*pushAndResult(context, item, {result}) {
  Navigator.push(
      context,
      PageRouteBuilder(
          transitionsBuilder: transition,
          opaque: false,
          pageBuilder: (context, _, __) {
            return item;
          })).then((_) {
    if (_ != null) {
      if (null != result) result(_);
    }
  });
}*/

pushAndResult(context, item,
    {result,
    opaque = false,
    bool replace = false,
    bool clear = false,
    transitionBuilder,
    transitionDuration}) {
  if (clear) {
    Navigator.of(context)
        .pushAndRemoveUntil(
            new PageRouteBuilder(
                opaque: false,
                transitionsBuilder: fadeTransition,
                pageBuilder: (BuildContext context, _, __) {
                  return item;
                }),
            (Route<dynamic> route) => false)
        .then((_) {
      if (_ != null) {
        if (result != null) result(_);
      }
    });
    return;
  }

  if (replace) {
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(
            transitionsBuilder: transitionBuilder ?? slideTransition,
            transitionDuration:
                transitionDuration ?? Duration(milliseconds: 300),
            opaque: opaque,
            pageBuilder: (context, _, __) {
              return item;
            })).then((_) {
      if (_ != null) {
        if (result != null) result(_);
      }
    });
    return;
  }
  Navigator.push(
      context,
      PageRouteBuilder(
          transitionsBuilder: transitionBuilder ?? slideTransition,
          transitionDuration: transitionDuration ?? Duration(milliseconds: 300),
          opaque: opaque,
          pageBuilder: (context, _, __) {
            return item;
          })).then((_) {
    if (_ != null) {
      if (result != null) result(_);
    }
  });

//  checkInternet();
//  checkPlanDaysLeft();
}

Widget slideTransition(BuildContext context, Animation<double> animation,
    Animation<double> secondaryAnimation, Widget child) {
  var begin = Offset(1.0, 0.0);
  var end = Offset.zero;
  var tween = Tween(begin: begin, end: end);
  var offsetAnimation = animation.drive(tween);

  return SlideTransition(
    position: offsetAnimation,
    child: child,
  );
  ;
}

Widget fadeTransition(BuildContext context, Animation<double> animation,
    Animation<double> secondaryAnimation, Widget child) {
  return FadeTransition(
    opacity: animation,
    child: child,
  );
}

pushReplaceAndResult(context, item,
    {result, opaque = true, bool depend = false}) {
  bool isIOS = Platform.isIOS;

  PageRoute route;

  if (isIOS && depend) {
    route = CupertinoPageRoute(builder: (ctx) {
      return item;
    });
  } else {
    route = PageRouteBuilder(
        transitionsBuilder: transition,
        opaque: false,
        pageBuilder: (context, _, __) {
          return item;
        });
  }

  Navigator.pushReplacement(context, route).then((_) {
    if (_ != null) {
      if (null != result) result(_);
    }
  });
}

Widget transition(BuildContext context, Animation<double> animation,
    Animation<double> secondaryAnimation, Widget child) {
  return FadeTransition(
    opacity: animation,
    child: child,
  );
}

SizedBox addSpace(double size) {
  return SizedBox(
    height: size,
  );
}

addSpaceWidth(double size) {
  return SizedBox(
    width: size,
  );
}

tipBox(color, text, textColor) {
  return Container(
    //width: double.infinity,
    color: color,
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        //mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Icon(
            Icons.info,
            size: 14,
            color: white,
          ),
          addSpaceWidth(10),
          Flexible(
            flex: 1,
            child: Text(
              text,
              style: textStyle(false, 15, textColor),
            ),
          )
        ],
      ),
    ),
  );
}

tipMessageItem(String title, String message, {Color color}) {
  return Container(
    child: new Card(
        color: color ?? red01,
        elevation: .5,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: new Padding(
          padding: const EdgeInsets.fromLTRB(8, 8, 8, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.info,
                    size: 14,
                    color: white,
                  ),
                  addSpaceWidth(5),
                  Text(
                    title,
                    style: textStyle(true, 12, white.withOpacity(.5)),
                  ),
                ],
              ),
              addSpace(5),
              Text(
                message,
                style: textStyle(false, 16, white),
                //overflow: TextOverflow.ellipsis,
              ),
              /*Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(3)),
                child: Text(
                  "APPLY",
                  style: textStyle(true, 9, black),
                ),
              ),*/
            ],
          ),
        )),
  );
}

dropDownSelector(String value,
    {String title,
    String hint = 'Select a dropdown value',
    bool showImage = false,
    bool isNet = false,
    bool showTitle = false,
    IconData icon,
    String image,
    Color titleColor = black,
    double titleOpacity = .8,
    onTap}) {
  return Container(
    padding: EdgeInsets.all(10),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (showTitle) ...[
          Text(
            title,
            style: textStyle(false, 12, titleColor.withOpacity(titleOpacity)),
          ),
          10.spaceHeight(),
        ],
        FlatButton(
          onPressed: onTap,
          padding: 15.padAll(),
          color: black.withOpacity(.03),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: white.withOpacity(.5))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: Row(
                  children: <Widget>[
                    if (showImage) ...[
                      if (isNet)
                        Image.network(
                          image,
                          height: 25,
                          width: 40,
                          fit: BoxFit.cover,
                        )
                      else
                        Image.asset(
                          image,
                          height: 25,
                          width: 40,
                          fit: BoxFit.cover,
                        ),
                      15.spaceWidth()
                    ],
                    Flexible(
                      child: Text(
                        value ?? hint,
                        style: textStyle(false, 16,
                            titleColor.withOpacity(null == value ? 0.3 : 1)),
                      ),
                    ),
                  ],
                ),
              ),
              Icon(
                icon ?? Icons.arrow_drop_down,
                color: titleColor.withOpacity(.5),
              )
            ],
          ),
        ),
      ],
    ),
  );
}

clickTextAuth(String value,
    {String image, String hint = 'Select a dropdown value', onTap}) {
  bool active = value != null;
  return Container(
    //padding: EdgeInsets.all(10),
    margin: EdgeInsets.only(left: 10, right: 10, top: 20),

    child: FlatButton(
      onPressed: () {
        onTap();
      },
      padding: EdgeInsets.all(15),
      color: active ? default_white : black.withOpacity(0.2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: white.withOpacity(.5))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            child: Row(
              children: <Widget>[
                if (null != image) ...[
                  Image.asset(
                    image,
                    height: 25,
                    width: 40,
                    fit: BoxFit.cover,
                  ),
                  15.spaceWidth()
                ],
                Flexible(
                  child: Text(
                    value ?? hint,
                    style: textStyle(
                        false, 18, active ? black : white.withOpacity(0.7)),
                  ),
                ),
              ],
            ),
          ),
          Icon(
            Icons.arrow_drop_down,
            color: (active ? black : white).withOpacity(.5),
          )
        ],
      ),
    ),
  );
}

inputFieldView({
  String hint,
  TextEditingController controller,
  bool showPhone = false,
  bool showTitle = false,
  String title,
  String countryCode,
  onCountryTapped,
  TextInputType keyboardType = TextInputType.text,
}) {
  final borderDeco = OutlineInputBorder(
      //color: black.withOpacity(.04),
      //border: Border.all(color: white.withOpacity(.5)),

      borderRadius: BorderRadius.circular(10),
      borderSide: BorderSide(color: white.withOpacity(.5)));

  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      if (showTitle) ...[
        Text(
          title,
          style: textStyle(true, 14, white.withOpacity(.8)),
        ),
        10.spaceHeight(),
      ],
      Row(
        children: <Widget>[
          if (showPhone) ...[
            FlatButton(
                onPressed: onCountryTapped,
                color: black.withOpacity(.04),
                padding: 18.padAll(),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: white.withOpacity(.5))),
                child: Text(
                  countryCode ?? "+234",
                  style: textStyle(false, 16, white.withOpacity(1)),
                )),
            10.spaceWidth()
          ],
          Flexible(
            child: TextField(
              controller: controller,
              keyboardType: keyboardType,
              style: textStyle(false, 15, white),
              decoration: InputDecoration(
                  contentPadding: 18.padAll(),
                  hintText: hint,
                  hintStyle: textStyle(false, 15, white.withOpacity(.5)),
                  fillColor: black.withOpacity(.04),
                  filled: true,
                  enabledBorder: borderDeco,
                  focusedBorder: borderDeco,
                  border: borderDeco),
            ),
          )
        ],
      ),
    ],
  );
}

authInputField(
    {@required TextEditingController controller,
    String title,
    String hint,
    TextStyle hintStyle,
    bool isPassword = false,
    bool showPassword = false,
    int maxLines,
    int maxLength,
    onPassChanged,
    Color titleColor = black,
    onEditingComplete,
    FocusNode focusNode,
    double titleOpacity = .6}) {
  hintStyle = hintStyle ?? textStyle(false, 14, titleColor.withOpacity(.5));

  final borderDeco = OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: BorderSide(color: white.withOpacity(.5)));

  return Container(
    padding: EdgeInsets.all(10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          title,
          style: textStyle(false, 12, titleColor.withOpacity(titleOpacity)),
        ),
        addSpace(5),
        TextField(
          focusNode: focusNode,
          controller: controller,
          textInputAction: TextInputAction.done,
          onEditingComplete: onEditingComplete,
          obscureText: showPassword,
          style: textStyle(false, 18, titleColor),
          //maxLines: (showPassword ? 1 : maxLines ?? 1),
          maxLength: maxLength,
          decoration: InputDecoration(
              enabledBorder: borderDeco,
              focusedBorder: borderDeco,
              border: borderDeco,
              hintText: hint,
              hintStyle: hintStyle,
              fillColor: black.withOpacity(.04),
              filled: true,
              suffixIcon: isPassword
                  ? Container(
                      // decoration: BoxDecoration(
                      //     color: AppConfig.appColor,
                      //     borderRadius: BorderRadius.circular(8)),
                      child: IconButton(
                          icon: Icon(
                            showPassword
                                ? Icons.visibility_off
                                : Icons.visibility,
                            color: white,
                          ),
                          onPressed: onPassChanged),
                    )
                  : null),
        ),
        /*CupertinoTextField(
          focusNode: focusNode,
          controller: controller,
          padding: 18.padAll(),
          autofocus: true,
          placeholder: hint,
          //placeholderStyle: textStyle(false, 14, white.withOpacity(.6)),
          placeholderStyle: hintStyle,
          textInputAction: TextInputAction.next,
          onEditingComplete: onEditingComplete,
          obscureText: showPassword,
          style: textStyle(true, 14, titleColor),
          maxLines: (showPassword ? 1 : maxLines ?? 1),
          maxLength: maxLength,
          suffix: isPassword
              ? Container(
                  decoration: BoxDecoration(
                      color: AppConfig.appColor,
                      borderRadius: BorderRadius.circular(8)),
                  child: IconButton(
                      icon: Icon(
                        showPassword ? Icons.visibility_off : Icons.visibility,
                        color: white,
                      ),
                      onPressed: onPassChanged),
                )
              : null,
          decoration: BoxDecoration(
              color: black.withOpacity(.04),
              border: Border.all(color: white.withOpacity(.5)),
              borderRadius: BorderRadius.circular(8)),
        ),*/
      ],
    ),
  );
}

imageHolder(
  double size,
  imageUrl, {
  double stroke = 0,
  strokeColor = AppConfig.appColor,
  localColor,
  margin,
  bool local = false,
  bool hideIcon = false,
  iconHolder = Icons.person,
  stackHolder = Icons.star,
  double iconHolderSize = 14,
  double localPadding = 0,
  bool round = true,
  bool borderCurve = false,
  bool showDot = false,
  bool showStack = false,
  double curve = 20,
  dotColor,
  onImageTap,
}) {
  return GestureDetector(
    onTap: onImageTap,
    child: ClipRRect(
      borderRadius: BorderRadius.circular(borderCurve ? curve : 0),
      child: AnimatedContainer(
        curve: Curves.ease,
        margin: margin,
        alignment: Alignment.center,
        duration: Duration(milliseconds: 300),
        padding: EdgeInsets.all(stroke),
        decoration: BoxDecoration(
          color: strokeColor,
          //borderRadius: BorderRadius.circular(borderCurve ? 15 : 0),
          //border: Border.all(width: 2, color: white),
          shape: round ? BoxShape.circle : BoxShape.rectangle,
        ),
        width: size,
        height: size,
        child: Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            new Card(
              margin: EdgeInsets.all(0),
              shape: round
                  ? CircleBorder()
                  : RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
              clipBehavior: Clip.antiAlias,
              color: Colors.transparent,
              elevation: .5,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    width: size,
                    height: size,
                    child: hideIcon
                        ? null
                        : Center(
                            child: Icon(
                            iconHolder,
                            color: white,
                            size: iconHolderSize,
                          )),
                  ),
                  imageUrl is File
                      ? (Image.file(
                          imageUrl,
                          width: size,
                          height: size,
                          alignment: Alignment.topCenter,
                          fit: BoxFit.cover,
                        ))
                      : local
                          ? Padding(
                              padding: EdgeInsets.all(localPadding),
                              child: Image.asset(
                                imageUrl,
                                width: size,
                                height: size,
                                color: localColor ?? white,
                                fit: BoxFit.cover,
                              ),
                            )
                          : CachedNetworkImage(
                              width: size,
                              height: size,
                              imageUrl: imageUrl,
                              fit: BoxFit.cover,
                              alignment: Alignment.topCenter,
                            ),
                ],
              ),
            ),
            if (showStack)
              Container(
                width: 22,
                height: 22,
                padding: EdgeInsets.all(3),
                child: Icon(
                  stackHolder,
                  color: white,
                  size: 12,
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: white, width: 2),
                  color: AppConfig.appColor,
                ),
              ),
            if (showDot)
              Container(
                width: 15,
                height: 15,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: white, width: 2),
                  color: dotColor ?? dark_green0,
                ),
              ),
          ],
        ),
      ),
    ),
  );
}

loadingLayout({
  Color color,
  Color load,
  bool showIndicator = false,
}) {
  return new Container(
    color: color ?? white,
    child: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        if (showIndicator)
          Center(
            child: SizedBox(
              height: 90,
              width: 90,
              child: LoadingIndicator(
                indicatorType: Indicator.orbit,
                color: appColor,
              ),
            ),
          ),
        Center(
          child: Opacity(
            opacity: .3,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(25),
              child: Image.asset(
                AppConfig.appIcon,
                width: 50,
                height: 50,
              ),
            ),
          ),
        ),
        Center(
          child: Container(
            width: 50,
            height: 50,
            child: CircularProgressIndicator(
              //value: 20,
              valueColor: AlwaysStoppedAnimation<Color>(load ?? white),
              strokeWidth: 2,
            ),
          ),
        ),
      ],
    ),
  );
}

emptyLayout(icon, String title, String text,
    {click,
    clickText,
    bool trans = false,
    bool noIconBack = false,
    iconColor = white}) {
  return Container(
    color: trans ? transparent : white,
    child: Center(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            !(icon is String)
                ? Icon(
                    icon,
                    size: 50,
                    color: appColor,
                  )
                : Image.asset(
                    icon,
                    height: 50,
                    width: 50,
                    color: appColor,
                  ),
            addSpace(10),
            Text(
              title,
              style: textStyle(true, 20, trans ? white : black),
              textAlign: TextAlign.center,
            ),
            if (text.isNotEmpty) addSpace(5),
            if (text.isNotEmpty)
              Text(
                text,
                style: textStyle(false, 16,
                    trans ? (white.withOpacity(.5)) : black.withOpacity(.5)),
                textAlign: TextAlign.center,
              ),
            addSpace(10),
            click == null
                ? new Container()
                : FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: appColor,
                    onPressed: click,
                    child: Text(
                      clickText,
                      style: textStyle(true, 16, white),
                    ))
          ],
        ),
      ),
    ),
  );
}

emptyLayoutX(icon, String title, String text,
    {click, clickText, bool isIcon = false}) {
  return Container(
    color: white,
    child: Center(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            if (isIcon)
              Icon(
                icon,
                size: 50,
                color: AppConfig.appColor,
              )
            else
              Image.asset(
                icon,
                height: 100,
                width: 100,
              ),
//            new Container(
//              width: 50,
//              height: 50,
//              child: Stack(
//                children: <Widget>[
//                  new Container(
//                    height: 50,
//                    width: 50,
//                    decoration:
//                        BoxDecoration(color: red0, shape: BoxShape.circle),
//                  ),
//                  new Center(
//                      child: isIcon
//                          ? Icon(
//                              icon,
//                              size: 30,
//                              color: white,
//                            )
//                          : Image.asset(
//                              icon,
//                              height: 30,
//                              width: 30,
//                              color: white,
//                            )),
//                  new Container(
//                    child: Column(
//                      mainAxisSize: MainAxisSize.max,
//                      crossAxisAlignment: CrossAxisAlignment.end,
//                      children: <Widget>[
//                        addExpanded(),
//                        Container(
//                          width: 20,
//                          height: 20,
//                          decoration: BoxDecoration(
//                              color: red3,
//                              shape: BoxShape.circle,
//                              border: Border.all(color: white, width: 1)),
//                          child: Center(
//                            child: Text(
//                              "!",
//                              style: textStyle(true, 14, white),
//                            ),
//                          ),
//                        )
//                      ],
//                    ),
//                  )
//                ],
//              ),
//            ),
            addSpace(10),
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: black.withOpacity(.03),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                      bottomLeft: Radius.circular(15))),
              child: Text(
                title,
                style: textStyle(true, 16, black.withOpacity(.7)),
                textAlign: TextAlign.center,
              ),
            ),
            addSpace(5),
            Text(
              text,
              style: textStyle(false, 14, black.withOpacity(.5)),
              textAlign: TextAlign.center,
            ),
            addSpace(10),
            click == null
                ? new Container()
                : FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: dark_green0,
                    onPressed: click,
                    child: Text(
                      clickText,
                      style: textStyle(true, 14, white),
                    ))
          ],
        ),
      ),
    ),
  );
}

toast(scaffoldKey, text, {Color color}) {
  return scaffoldKey.currentState.showSnackBar(new SnackBar(
    content: Padding(
      padding: const EdgeInsets.all(0.0),
      child: Text(
        text,
        style: textStyle(false, 15, white),
      ),
    ),
    backgroundColor: color,
    duration: Duration(seconds: 2),
  ));
}

Container addLine(
    double size, color, double left, double top, double right, double bottom) {
  return Container(
    height: size,
    width: double.infinity,
    color: color,
    margin: EdgeInsets.fromLTRB(left, top, right, bottom),
  );
}

void showProgress(bool show, BuildContext context,
    {String msg, bool cancellable = true, double countDown}) {
  String progressId = '1';
  if (!show) {
    currentProgress = progressId;
    return;
  }

  currentProgress = "";

  pushAndResult(
      context,
      progressDialog(
        progressId,
        message: msg,
        cancelable: cancellable,
      ),
      transitionBuilder: fadeTransition);
}

void showMessage(context, icon, iconColor, title, message,
    {int delayInMilli = 0,
    clickYesText = "OK",
    onClicked,
    clickNoText,
    bool cancellable = true,
    double iconPadding,
    bool = true}) {
  Future.delayed(Duration(milliseconds: delayInMilli), () {
    pushAndResult(
        context,
        messageDialog(
          icon,
          iconColor,
          title,
          message,
          clickYesText,
          noText: clickNoText,
          cancellable: cancellable,
          iconPadding: iconPadding,
        ),
        result: onClicked,
        opaque: false,
        transitionBuilder: scaleTransition,
        transitionDuration: Duration(milliseconds: 500));
  });
}

/*void showListDialog(
  context,
  items, {
  images,
  title,
  onClicked,
  useIcon = true,
  usePosition = true,
  useTint = false,
  int delayInMilli = 0,
}) {
  Future.delayed(Duration(milliseconds: delayInMilli), () {
    pushAndResult(
        context,
        listDialog(
          items,
          title: title,
          images: images,
          isIcon: useIcon,
          usePosition: usePosition,
          useTint: useTint,
        ),
        result: onClicked);
  });
}*/

checkBox(bool selected,
    {double size: 13, checkColor = dark_green0, iconColor = white}) {
  return new Container(
    //padding: EdgeInsets.all(2),
    child: Container(
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: blue09,
          border: Border.all(color: black.withOpacity(.1), width: 1)),
      child: Container(
        width: size,
        height: size,
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: selected ? checkColor : transparent,
        ),
        child: Icon(
          Icons.check,
          size: size <= 16 ? 8 : null,
          color: !selected ? transparent : iconColor,
        ),
      ),
    ),
  );
}

showListDialog(context, List items, onSelected,
    {title,
    images,
    bool useTint = true,
    selections,
    bool returnIndex = false,
    bool singleSelection = false}) {
  pushAndResult(
      context,
      listDialog(
        items,
        title: title,
        images: images,
        useTint: useTint,
        selections: selections,
        singleSelection: singleSelection,
      ), result: (_) {
    if (_ is List) {
      onSelected(_);
    } else {
      onSelected(returnIndex ? items.indexOf(_) : _);
    }
  },
      opaque: false,
      transitionBuilder: scaleTransition,
      transitionDuration: Duration(milliseconds: 500));
}

Widget scaleTransition(
  BuildContext context,
  Animation<double> animation,
  Animation<double> secondaryAnimation,
  Widget child,
) {
  return ScaleTransition(
    scale: Tween<double>(
      begin: 2.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: animation,
        curve: Curves.fastOutSlowIn,
      ),
    ),
    child: child,
  );
}

String getRandomId() {
  var uuid = new Uuid();
  return uuid.v1();
}

Future<File> cropThisImage(String path, {bool circle = false}) async {
  return await ImageCropper.cropImage(
      sourcePath: path,
      cropStyle: circle ? CropStyle.circle : CropStyle.rectangle,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: AppConfig.appColor,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
      ));
}

getSingleCroppedImage(BuildContext context,
    {@required onPicked(BaseModel photo),
    bool crop = false,
    bool circle = false}) async {
  PhotoPicker.openPicker(
    mediaType: 'image', // image | video | any
    multiple: false,
    limit: 1,
  ).then((value) async {
    String urlPath = value[0].url.replaceAll("file://", "");
    String thumbnail = value[0].thumbnailUrl.replaceAll("file://", "");

    if (crop) {
      File file = await cropThisImage(urlPath, circle: circle);
      if (null == file) return;
      final photo = createPhotoModel(urlPath: file.path, thumbnail: thumbnail);
      onPicked(photo);
      return;
    }

    final photo = createPhotoModel(urlPath: urlPath, thumbnail: thumbnail);
    onPicked(photo);
  }).catchError((e) {
    logError(e.code, e.bookmarkList);
  });
  ;
}

getSingleVideo(BuildContext context,
    {@required onPicked(BaseModel photo)}) async {
  PhotoPicker.openPicker(
    mediaType: 'video', // image | video | any
    multiple: false,
    limit: 1,
  ).then((value) {
    List<BaseModel> path = value.map((e) {
      return createPhotoModel(
          urlPath: e.url.replaceAll("file://", ""),
          thumbnail: e.thumbnailUrl.replaceAll("file://", ""),
          isVideo: true);
    }).toList();
    onPicked(path[0]);
  }).catchError((e) {
    logError(e.code, e.bookmarkList);
  });
  ;
}

bool fromOnline(String text) {
  text = text.trim();
  return text.startsWith("http://") || text.startsWith("https://");
}

int UPLOAD_TYPE_PHOTO = 0;
int UPLOAD_TYPE_VIDEO = 1;
int UPLOAD_TYPE_THUMBNAIL = 2;

uploadFile(File file, onComplete(res, error)) {
  final String ref = getRandomId();
   Reference storageReference = FirebaseStorage.instance.ref().child(ref);
   UploadTask uploadTask = storageReference.putFile(file);
  uploadTask
      /*.timeout(Duration(seconds: 3600), onTimeout: () {
    onComplete(null, "Error, Timeout");
  })*/
      .then((task) {
    if (task != null) {
      task.ref.getDownloadURL().then((_) {
        BaseModel model = new BaseModel();
        model.put(FILE_URL, _.toString());
        model.put(REFERENCE, ref);
        model.saveItem(REFERENCE_BASE, false);

        onComplete(_.toString(), null);
      }, onError: (error) {
        onComplete(null, error);
      });
    }
  }, onError: (error) {
    onComplete(null, error);
  });
}

clearRecords() {
  isAdmin = false;
  isLoggedIn = false;
  userModel = BaseModel();
  appSettingsModel = BaseModel();
  FirebaseAuth.instance.signOut();
  usersStream?.cancel();
  appSettingsStream?.cancel();
  chatMessageController?.close();

  followers.clear();
  following.clear();
  myPosts.clear();
  otherPosts.clear();
  allMessages.clear();
  myNotes.clear();
  myNotifications.clear();
  allPosts.clear();

  otherPeronInfo.clear();
  unreadCounter.clear();
  lastMessages.clear();
  showNewMessageDot.clear();
  stopListening.clear();

  chatSetup = false;
  hasChatLoaded = false;
  readyNotes = false;
  setup = false;
  storiesReady = false;
  hasSetup = false;
}

placeHolder(double height,
    {double width = 200, Color color = blue0, double opacity = .1}) {
  return new Container(
    height: height,
    width: width,
    color: color.withOpacity(opacity),
    child: Center(
        child: Opacity(
            opacity: .3,
            child: Image.asset(
              AppConfig.appIcon,
              width: 20,
              height: 20,
            ))),
  );
}

Future<bool> isConnected() async {
  var result = await (Connectivity().checkConnectivity());
  if (result == ConnectivityResult.none) {
    return Future<bool>.value(false);
  }
  return Future<bool>.value(true);
}

getMultiCroppedImage(BuildContext context,
    {@required onPicked(List<BaseModel> path),
    int max = 2,
    bool withVideo = false,
    String topTitle}) async {
  PhotoPicker.openPicker(
    mediaType: withVideo ? "any" : 'image', // image | video | any
    multiple: true,
    limit: max,
  ).then((value) {
    if (null == value) return;
    List<BaseModel> path = value.map((e) {
      //print(e.toJson());
      return createPhotoModel(
        urlPath: e.url.replaceAll("file://", ""),
        isVideo: e.type == "video",
        thumbnail: e.thumbnailUrl.replaceAll("file://", ""),
      );
    }).toList();
    onPicked(path);
  }).catchError((e) {
    logError(e.code, e.bookmarkList);
  });
}

String formatDOB(int v) {
  if (v < 10) return "0$v";
  return "$v";
}

List<String> getSearchString(String text) {
  text = text.toLowerCase().trim();
  if (text.isEmpty) return List();

  List<String> list = List();
  list.add(text);
  var parts = text.split(" ");
  for (String s in parts) {
    if (s.isNotEmpty) list.add(s);
    for (int i = 0; i < s.length; i++) {
      String sub = s.substring(0, i);
      if (sub.isNotEmpty) list.add(sub);
    }
  }
  for (int i = 0; i < text.length; i++) {
    String sub = text.substring(0, i);
    if (sub.isNotEmpty) list.add(sub.trim());
  }
  return list;
}

String translate(BuildContext context, String key) {
  return AppTranslations.of(context).text(key);
}

adjustNumberSize(String price) {
  if (price.contains("000000")) {
    price = price.replaceAll("000000", "");
    price = "${price}M";
  } else if (price.length > 6) {
    double pr = (int.parse(price)) / 1000000;
    return "${pr.toStringAsFixed(1)}M";
  } else if (price.contains("000")) {
    price = price.replaceAll("000", "");
    price = "${price}K";
  } else if (price.length > 3) {
    double pr = (int.parse(price)) / 1000;
    return "${pr.toStringAsFixed(1)}K";
  }

  return price;
}

String getTimeAgo(int milli) {
  return timeAgo.format(DateTime.fromMillisecondsSinceEpoch(milli),
      locale: appLocale.languageCode);
}

openMap(lat, lon) {
  openLink("https://www.google.com/maps/search/?api=1&query=$lat,$lon");
}

openLink(String url) async {
  if (await canLaunch(url)) {
    await launch(
      url,
      //forceSafariVC: false,
      //forceWebView: false,enableJavaScript: true
    );
  } else {
    print('Could not launch $url');
  }
}

String get timeOfDayText {
  var now = DateTime.now();
  TimeOfDay time = TimeOfDay.fromDateTime(now /*.toDateTimeLocal()*/);

//  if (time.period == DayPeriod.am && (time.hour >= 0 && time.hour <= 4)) {
//    return TimeOfTheDay.inBed;
//  }

  if (time.period == DayPeriod.am && (time.hour > 5 && time.hour <= 11)) {
    return 'Good Morning';
  }

//  if (time.period == DayPeriod.pm && (time.hour >= 12 && time.hour <= 15)) {
//    return TimeOfTheDay.afternoon;
//  }

  if (time.period == DayPeriod.pm && (time.hour >= 12 && time.hour <= 19)) {
    'Good Evening';
  }
  return 'Good Night';
}

void yesNoDialog(context, title, message, clickedYes) {
  Navigator.push(
      context,
      PageRouteBuilder(
          transitionsBuilder: transition,
          opaque: false,
          pageBuilder: (context, _, __) {
            return messageDialog(
              Icons.warning,
              red0,
              title,
              message,
              "Yes",
              noText: "No, Cancel",
            );
          })).then((_) {
    if (_ != null) {
      if (_ == true) {
        clickedYes();
      }
    }
  });
}

getOtherPersonId(List parties) {
  if (parties == null) return "";
//  List parties = items[AppAssets.parties];
  parties.remove(userModel.userId);
  if (parties.isEmpty) return "";
  return parties[0];
}

clickChat(context, BaseModel theUser) {
  /* String chatId = createChatId(theUser.userId);
  Map<String, dynamic> chatMap = {};
  chatMap[PARTIES] = [userModel.userId, theUser.userId];
  Firestore.instance
      .collection(AppDataBase.chat_ids_base)
      .document(chatId)
      .setData(chatMap);

  userModel
    ..putInList(DELETED_CHATS, chatId, false)
    ..updateItems();
  pushAndResult(
      context,
      ChatMain(
        chatId,
        otherPerson: theUser,
      ));*/

  String chatId = createChatId(theUser.userId);
  BaseModel chat = BaseModel();
  //chat.put(PARTIES, [userModel.getObjectId(),farmAdmin]);
  chat.put(PARTIES, [userModel.userId, theUser.userId]);
  chat.put(PARTIES_MODEL, [chatPartyModel(userModel), chatPartyModel(theUser)]);

  // chat.put(FARM_NAME, farmName);
  // chat.put(FARM_ID, farmId);
  // chat.put(FARM_ADMIN, farmAdmin);
  // chat.put(PRODUCE_ID, produceId);
  // chat.put(FARM_LOGO, farmLogo);
  chat.saveItem(CHAT_IDS_BASE, false, document: chatId, merged: true);

  userModel.putInList(MUTED, chatId, false);
  pushAndResult(
    context,
    ChatMain(
      chatId,
      //defMessage:defMessage
    ),
  );
}

Map chatPartyModel(BaseModel theUser) {
  Map chatParty = {
    USER_ID: theUser.getUserId(),
    OBJECT_ID: theUser.getUserId(),
    NAME: theUser.getString(NAME),
    IMAGE_URL: theUser.userImage,
  };

  return chatParty;
}

String getLastSeen(BaseModel user) {
  int time = user.getInt(TIME_ONLINE); //?? 0;
  int now = DateTime.now().millisecondsSinceEpoch;
  int diff = now - time;
//  if (diff > (Duration.millisecondsPerDay * 77)) return null;
  return diff > (Duration.millisecondsPerDay * 30)
      ? "Last seen: some weeks ago"
      : "Last seen: ${timeAgo.format(DateTime.fromMillisecondsSinceEpoch(time), locale: "en")}";
}

createChatNotice(String chatId, String message, {String key = ""}) {
  BaseModel model = BaseModel();
  model.put(MESSAGE, message);
  model.put(CHAT_ID, chatId);
  model.saveItem(CHAT_BASE, false, document: "${userModel.getObjectId()}$key");
}

launchChat(context, String chatId, String defMessage) {
//  subscribeToTopic(chatId, true);
  userModel.putInList(MUTED, chatId, false);
  createChatNotice(chatId, "${userModel.getString(NAME)} has joined the group",
      key: "joined");
  pushAndResult(
    context,
    ChatMain(chatId, defMessage: defMessage),
  );
}

String createChatId(String hisId) {
  String myId = userModel.getObjectId();
  List ids = [];
  for (int i = 0; i < myId.length; i++) {
    ids.add(myId[i]);
  }
  for (int i = 0; i < hisId.length; i++) {
    ids.add(hisId[i]);
  }
  ids.sort((a, b) => a.compareTo(b));
  StringBuffer sb = StringBuffer();
  for (String s in ids) {
    sb.write(s);
  }
  return sb.toString().trim();
}

bool isSameDay(int time1, int time2) {
  DateTime date1 = DateTime.fromMillisecondsSinceEpoch(time1);

  DateTime date2 = DateTime.fromMillisecondsSinceEpoch(time2);

  return (date1.day == date2.day) &&
      (date1.month == date2.month) &&
      (date1.year == date2.year);
}

bool chatRemoved(BaseModel chat) {
  if (chat.deleted) {
    return true;
  }
  if (chat.hidden.contains(userModel.userId)) {
    return true;
  }
  return false;
}

String getChatDate(int milli) {
  final formatter = DateFormat("MMM d 'AT' h:mm a");
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

String getChatTime(int milli) {
  final formatter = DateFormat("h:mm a");
  DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
  return formatter.format(date);
}

incomingChatText(context, BaseModel chat) {
  if (chat.deleted) {
    return incomingChatDeleted(context, chat);
  }
  if (chat.hidden.contains(userModel.userId)) {
    return Container();
  }

  String message = chat.message;

  return new GestureDetector(
    onLongPress: () {
      showChatOptions(context, chat);
    },
    child: new Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 15),
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      decoration: BoxDecoration(
          color: AppConfig.appColor, borderRadius: BorderRadius.circular(25)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            message,
            style: textStyle(false, 17, white),
          ),
          /* addSpace(3),
          Text(
            getChatTime(chat.createdAt),
            style: textStyle(false, 12, black.withOpacity(.3)),
          ),*/
        ],
      ),
    ),
  );
}

incomingChatDeleted(context, BaseModel chat) {
  if (chat.hidden.contains(userModel.userId)) {
    return Container();
  }
  return GestureDetector(
    onLongPress: () {
      showChatOptions(context, chat, deletedChat: true);
    },
    child: new Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 15),
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      decoration: BoxDecoration(
          color: AppConfig.appColor, borderRadius: BorderRadius.circular(25)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "Deleted",
                style: textStyle(false, 17, white),
              ),
              addSpaceWidth(5),
              Icon(
                Icons.info,
                color: white,
                size: 17,
              )
            ],
          ),
          /*addSpace(3),
          Text(
            getChatTime(chat.createdAt),
            style: textStyle(false, 12, black.withOpacity(.3)),
          ),*/
        ],
      ),
    ),
  );
}

userImageItem(context, String imageUrl, {double size = 40, onTapped}) {
  return new GestureDetector(
    onTap: () {
      if (onTapped != null) onTapped();
    },
    child: new Container(
//      decoration: BoxDecoration(
//        color: AppConfig.appColor,
//        border: Border.all(width: 2, color: white),
//        shape: BoxShape.circle,
//      ),
//      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      width: size,
      height: size,
      child: Stack(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(0),
            shape: CircleBorder(),
            clipBehavior: Clip.antiAlias,
            color: Colors.transparent,
            elevation: .5,
            child: Stack(
              children: <Widget>[
                Container(
                  width: size,
                  height: size,
                  color: AppConfig.appColor_dark,
                  child: Center(
                      child: Icon(
                    Icons.person,
                    color: white,
                    size: 15,
                  )),
                ),
                CachedNetworkImage(
                  width: size,
                  height: size,
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

showChatOptions(context, BaseModel chat, {bool deletedChat = false}) {
  int type = chat.type;
  pushAndResult(
    context,
    listDialog(
      type == CHAT_TYPE_TEXT && !deletedChat ? ["Copy", "Delete"] : ["Delete"],
      //usePosition: false,
    ),
    result: (_) {
      if (_ == "Copy") {
        Clipboard.setData(ClipboardData(text: chat.getString(MESSAGE)));
      }
      if (_ == "Delete") {
        if (chat.myItem()) {
          chat
            ..put(DELETED, true)
            ..updateItems();
        } else {
          chat
            ..putInList(HIDDEN, userModel.userId, true)
            ..updateItems();
        }
      }
    },
  );
}

class ViewImage extends StatefulWidget {
  List images;
  int position;
  ViewImage(
    this.images,
    this.position,
  );
  @override
  _ViewImageState createState() => _ViewImageState();
}

class _ViewImageState extends State<ViewImage> {
  List images;
  int position;

  @override
  void initState() {
    // TODO: implement initState
    images = widget.images;
    position = widget.position;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    PageController controller = PageController(initialPage: position);
    List<PhotoViewGalleryPageOptions> list = List();
    for (String image in images) {
      list.add(PhotoViewGalleryPageOptions(
        imageProvider:
            (image.startsWith("https://") || image.startsWith("http://"))
                ? NetworkImage(image)
                : FileImage(File(image)),
        initialScale: PhotoViewComputedScale.contained,
        /* maxScale: PhotoViewComputedScale.contained * 0.3*/
      ));
    }
    // TODO: implement build
    return Container(
      color: black,
      child: Stack(children: <Widget>[
        PhotoViewGallery(
          pageController: controller,
          pageOptions: list,
          onPageChanged: (p) {
            position = p;
            setState(() {});
          },
        ),
        new Container(
          margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
          width: 50,
          height: 50,
          child: FlatButton(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Center(
                child: Icon(
              Icons.keyboard_backspace,
              color: white,
              size: 25,
            )),
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(flex: 1, child: Container()),
            new Padding(
              padding: const EdgeInsets.all(20),
              child: tabIndicator(images.length, position),
            ),
          ],
        )
      ]),
    );
  }
}

gradientLine(
    {double height = 4, bool reverse = false, alpha = .3, color: black}) {
  return Container(
    width: double.infinity,
    height: height,
    decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: reverse
                ? [
                    color.withOpacity(alpha),
                    transparent,
                  ]
                : [transparent, color.withOpacity(alpha)])),
  );
}

tabIndicator(int tabCount, int currentPosition, {margin}) {
  return Container(
    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
    margin: margin,
    decoration: BoxDecoration(
        color: black.withOpacity(.7), borderRadius: BorderRadius.circular(25)),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: getTabs(tabCount, currentPosition),
    ),
  );
  /*return Marker(
      markerId: MarkerId(""),
      infoWindow: InfoWindow(),
      icon: await BitmapDescriptor.fromAssetImage(ImageConfiguration(), ""));*/
}

getTabs(int count, int cp) {
  List<Widget> items = List();
  for (int i = 0; i < count; i++) {
    bool selected = i == cp;
    items.add(Container(
      width: selected ? 10 : 8,
      height: selected ? 10 : 8,
      //margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
      decoration: BoxDecoration(
          color: white.withOpacity(selected ? 1 : (.5)),
          shape: BoxShape.circle),
    ));
    if (i != count - 1) items.add(addSpaceWidth(5));
  }

  return items;
}

class PlayVideo extends StatefulWidget {
  String id;
  String link;
  File videoFile;

  PlayVideo(
    this.id,
    this.link, {
    this.videoFile,
  });
  @override
  _PlayVideoState createState() => _PlayVideoState();
}

class _PlayVideoState extends State<PlayVideo> {
  File videoFile;
  String id;
  String videoLink;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    id = widget.id;
    videoLink = widget.link;
    videoFile = widget.videoFile;
    if (videoFile == null) checkVideo();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        color: black,
        child: Stack(children: [
          videoLink == null && videoFile == null
              ? Container()
              : videoFile != null
                  ? SimpleVideoPlayer(
                      file: videoFile,
                    )
                  : SimpleVideoPlayer(
                      source: videoLink,
                    ),
          new Container(
            margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
            width: 50,
            height: 50,
            child: FlatButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Center(
                  child: Icon(
                Icons.keyboard_backspace,
                color: white,
                size: 25,
              )),
            ),
          )
        ]));
  }

  void checkVideo() async {
    String videoFileName = "${widget.id}${widget.link.hashCode}.mp4";

    File file = await getLocalFile(videoFileName);
    bool exist = await file.exists();

    if (!exist) {
      downloadFile(file);
      videoLink = widget.link;
      setState(() {});
      //createVideo(true, link: widget.link);
    } else {
      //createVideo(false, file: file);
      videoFile = file;
      setState(() {});
    }
  }

  void downloadFile(File file) async {
    //toastInAndroid("Downloading...");

    QuerySnapshot shots = await Firestore.instance
        .collection(AppDataBase.referenceBase)
        .where(FILE_URL, isEqualTo: widget.link)
        .limit(1)
        .getDocuments();
    if (shots.documents.isEmpty) {
      //toastInAndroid("Link not found");
    } else {
      for (DocumentSnapshot doc in shots.documents) {
        if (!doc.exists || doc.data().isEmpty) continue;
        String ref = doc.data()[REFERENCE];
         Reference storageReference =
            FirebaseStorage.instance.ref().child(ref);
        storageReference.writeToFile(file).then((_) {
          //toastInAndroid("Download Complete");
        }, onError: (error) {
          //toastInAndroid(error);
        }).catchError((error) {
          //toastInAndroid(error);
        });

        break;
      }
    }
  }
}

int getDurationInSeconds(String length) {
  var parts = length.split(":");
  int ms = int.parse(parts[0]);
  int ss = int.parse(parts[1]);
  return (ms * 60) + ss;
}

Future<String> get localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> getLocalFile(String name) async {
  final path = await localPath;
  return File('$path/$name');
}

Future<File> getDirFile(String name) async {
  final dir = await (Platform.isIOS
      ? getApplicationDocumentsDirectory()
      : getExternalStorageDirectory());
  var testDir = await Directory("${dir.path}/HUTAF").create(recursive: true);
  return File("${testDir.path}/$name");
}

Future<bool> checkLocalFile(String name) async {
  final path = await localPath;
  File file = File('$path/$name');
  return await file.exists();
}

scrollTotallyUp(ScrollController scrollController) {
  if (scrollController != null && !scrollController.position.outOfRange) {
    scrollController.animateTo(scrollController.position.minScrollExtent,
        duration: new Duration(milliseconds: 500), curve: Curves.linear);
  }
}

scrollTotallyDown(ScrollController scrollController) {
  if (scrollController != null && !scrollController.position.outOfRange) {
    scrollController.animateTo(scrollController.position.maxScrollExtent,
        duration: new Duration(milliseconds: 500), curve: Curves.linear);
  }
}

addItemToList(
    {@required T,
    @required List thisList,
    bool sort = true,
    bool isNew = false,
    onAdded(list)}) {
  int p = thisList.indexWhere((e) => e.documentId == T.documentId);
  bool exists = p != -1;
  if (exists) {
    thisList[p] = T;
  } else {
    if (isNew) {
      thisList.insert(0, T);
    } else {
      thisList.add(T);
    }
  }
  if (sort) thisList.sort((p1, p2) => p2.createdAt.compareTo(p1.createdAt));
  onAdded(thisList);
}

formatFeedCount(int size) {
  return size == 0 ? "" : adjustNumberSize(size.toString());
}

getVideoThumbnail(String path) async {
  return (await VideoCompress.getFileThumbnail(path,
          quality: 100, position: -1))
      .path;
}

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

followThisUser(BaseModel theUser, bool following, {onComplete}) async {
  String notifyId = "${userModel.getObjectId()}_${theUser.getObjectId()}";

  print(notifyId);

  if (following) {
    FirebaseFirestore.instance.collection(NOTIFY_BASE).doc(notifyId).delete();
  } else {
    NotificationService.pushToUsers(
        title: "New Follower",
        type: NOTIFY_FOLLOW,
        message: "${userModel.fullName} followed you",
        parties: [theUser.items],
        theModel: userModel);
  }
  theUser
    ..putInList(MY_FOLLOWER_IDS, userModel.userId, !following)
    ..updateItems();
  userModel
    ..putInList(AM_FOLLOWING_IDS, theUser.userId, !following)
    ..updateItems();
  onComplete();
  updateRecordsOfParties(theUser.userId, !following);
}

updateRecordsOfParties(String id, bool add) async {
  List bases = [POST_BASE, STORY_BASE];
  for (String db in bases) {
    final docs = await FirebaseFirestore.instance.collection(db).get();
    if (docs == null) return;
    for (var doc in docs.docs) {
      BaseModel model = BaseModel(doc: doc);
      model
        ..putInList(PARTIES, id, add)
        ..updateItems(delaySeconds: 2);
    }
  }
}

likeThisPost(BaseModel thisPost, bool haveLiked,
    {onComplete(BaseModel post), int type = NOTIFY_TYPE_POST}) {
  String notifyId = "${userModel.getUserId()}${thisPost.getObjectId()}";
  thisPost.putInList(LIKED_BY, userModel.getObjectId(), !haveLiked);
  if (haveLiked) {
    Firestore.instance.collection(NOTIFY_BASE).document(notifyId).delete();
  } else {
    sendPushNotification(
        title: "${type == NOTIFY_TYPE_POST ? "Post" : "Comment"} Liked",
        type: type,
        message:
            "${userModel.fullName} like your ${type == NOTIFY_TYPE_POST ? "post" : "comment"}",
        notifiedId: notifyId,
        parties: [thisPost.userId],
        notifiedOnThis: thisPost);
  }
  thisPost.updateItems();
  onComplete(thisPost);
}

reportThis(BuildContext context, BaseModel thisPost,
    {onDeleted, int type = REPORT_TYPE_POST}) {
  String typeString;
  if (type == REPORT_TYPE_POST) typeString = "Post";
  if (type == REPORT_TYPE_COMMENT) typeString = "Comment";
  if (type == REPORT_TYPE_USER) typeString = "User";
  if (type == REPORT_TYPE_STORY) typeString = "Story";

  pushAndResult(
      context,
      inputDialog(
        "Report $typeString",
        hint: "Tell us why your reporting...?",
      ), result: (_) {
    if (_ == null) return;

    BaseModel report = BaseModel();
    report
      ..put(MESSAGE, _)
      ..put(TYPE, type)
      ..put(REPORTED_ID, thisPost.getObjectId());
    report.saveItem(REPORT_BASE, true);
    userModel
      ..putInList(MUTED, thisPost.getObjectId(), true)
      ..updateItems();
  });
}

String formatDuration(Duration position) {
  final ms = position.inMilliseconds;

  int seconds = ms ~/ 1000;
  final int hours = seconds ~/ 3600;
  seconds = seconds % 3600;
  var minutes = seconds ~/ 60;
  seconds = seconds % 60;

  final hoursString = hours >= 10
      ? '$hours'
      : hours == 0
          ? '00'
          : '0$hours';

  final minutesString = minutes >= 10
      ? '$minutes'
      : minutes == 0
          ? '00'
          : '0$minutes';

  final secondsString = seconds >= 10
      ? '$seconds'
      : seconds == 0
          ? '00'
          : '0$seconds';

  final formattedTime =
      '${hoursString == '00' ? '' : hoursString + ':'}$minutesString:$secondsString';

  return formattedTime;
}

void downloadFile(File file, String urlLink, onComplete(e),
    {bool forced = false}) async {
  if (upOrDown.contains(urlLink)) return;
  upOrDown.add(urlLink);
  print("Downloading $urlLink");
  QuerySnapshot shots = await Firestore.instance
      .collection(AppDataBase.referenceBase)
      .where(FILE_URL, isEqualTo: urlLink)
      .limit(1)
      .getDocuments();
  if (shots.documents.isEmpty) {
    file.create();
    upOrDown.removeWhere((l) => l == urlLink);
    onComplete("not found");
    print("$urlLink not found");
    return;
  }
  for (DocumentSnapshot doc in shots.documents) {
    if (!doc.exists || doc.data().isEmpty) continue;
    print("OKORE >>>> DOWNLOADING....<<<<");
    String ref = doc.data()[REFERENCE];
     Reference storageReference =
        FirebaseStorage.instance.ref().child(ref);
    storageReference.writeToFile(file).then((_) {
      upOrDown.removeWhere((l) => l == urlLink);
      onComplete(null);
    }, onError: (error) {}).catchError((error) {
      upOrDown.removeWhere((l) => l == urlLink);
      file.delete();
      if (forced) {
        downloadFile(file, urlLink, onComplete);
      } else {
        onComplete(error);
      }
    });
    break;
  }
}

VideoPlayerController recAudioController;
String currentPlayingAudio;

bool recPlayEnded = false;
getChatAudioWidget(context, BaseModel chat, onEdited(bool removed)) {
  if (chat.deleted) {
    return Container();
  }
//    return Container();
  String audioUrl = chat.getString(AUDIO_URL);
  String audioPath = chat.getString(AUDIO_PATH);
  String audioLength = chat.getString(AUDIO_LENGTH);
  bool uploading = upOrDown.contains(chat.getObjectId());
  bool noFile = noFileFound.contains(chat.getObjectId());
  bool currentPlay = currentPlayingAudio == chat.getObjectId();
  bool isPlaying = recAudioController != null &&
      recAudioController.value.initialized &&
      recAudioController.value.isPlaying;
  var parts = audioPath.split("/");
  String baseFileName = parts[parts.length - 1];
  return Opacity(
    opacity: audioUrl.isEmpty && !chat.myItem() ? (.4) : 1,
    child: new GestureDetector(
      onTap: () async {
        if (audioUrl.isEmpty && !chat.myItem()) return;
        if (uploading) return;
        if (noFile) {
          showMessage(context, Icons.error, red0, "File not found",
              "This file no longer exist on your device");
          return;
        }

        if (!chat.myItem()) {
          String path = await localPath;
          File file = File("$path/${chat.getObjectId()}$baseFileName");
          print("File Path: ${file.path}");
          bool exists = await file.exists();
          if (!exists) {
            upOrDown.add(chat.getObjectId());
            onEdited(false);
            downloadFile(file, audioUrl, (e) {
              upOrDown.removeWhere((element) => element == chat.getObjectId());
              onEdited(false);
            });
            return;
          } else {
            audioPath = file.path;
          }
        }

        if (currentPlayingAudio == chat.getObjectId()) {
          if (recAudioController != null &&
              recAudioController.value.initialized) {
            if (recAudioController.value.isPlaying) {
              recAudioController.pause();
            } else {
              currentPlayingAudio = chat.getObjectId();
              recAudioController.play();
              recPlayEnded = false;
              onEdited(false);
            }
          }
        } else {
          if (recAudioController != null) {
            await recAudioController.pause();
            await recAudioController.dispose();
            recAudioController = null;
          }

          recAudioController = VideoPlayerController.file(File(audioPath));
          recAudioController.addListener(() async {
            if (recAudioController != null) {
              int currentTime = recAudioController.value.position.inSeconds;
              int fullTime = recAudioController.value.duration.inSeconds;
              if (currentTime == fullTime && currentTime != 0) {
                if (recPlayEnded) return;
                recPlayEnded = true;
                recAudioController.pause();
                recAudioController.seekTo(Duration(seconds: 0));
                /* Future.delayed(Duration(milliseconds: 200),()async{
                    currentPlayingAudio="";
                    recAudioController=null;
                  });*/
              }
            }
            onEdited(false);
          });
          recAudioController.initialize().then((value) {
            currentPlayingAudio = chat.getObjectId();
            recAudioController.play();
            recPlayEnded = false;
            onEdited(false);
          });
        }
      },
      onLongPress: () {
        showChatOptions(context, chat);
      },
      child: new Container(
        height: 30,
        width: 150,
        color: transparent,
        margin: EdgeInsets.fromLTRB(20, 0, 20, 15),
        child: Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0,
          margin: EdgeInsets.all(0),
          color: AppConfig.appColor
              .withOpacity(isPlaying && currentPlay ? 1 : (.7)),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25))),
          child: Stack(
            fit: StackFit.expand,
            children: [
              /*LinearProgressIndicator(
                  value:currentPlay?(playPosition / 100):0,
                  backgroundColor: transparent,
                  valueColor:
                  AlwaysStoppedAnimation<Color>(black.withOpacity(.7)),
                ),*/
              Row(
                children: [
                  addSpaceWidth(10),
                  if (uploading)
                    Container(
                      width: 14,
                      height: 14,
                      child: CircularProgressIndicator(
                        //value: 20,
                        valueColor: AlwaysStoppedAnimation<Color>(white),
                        strokeWidth: 2,
                      ),
                    ),
                  if (!uploading)
                    chat.myItem()
                        ? (Icon(
                            currentPlay && isPlaying
                                ? (Icons.pause)
                                : Icons.play_circle_filled,
                            color: white,
                          ))
                        : FutureBuilder(
                            builder: (c, d) {
                              if (!d.hasData) return Container();
                              bool exists = d.data;
                              return Icon(
                                !exists
                                    ? Icons.file_download
                                    : currentPlay && isPlaying
                                        ? (Icons.pause)
                                        : Icons.play_circle_filled,
                                color: white,
                              );
                            },
                            future: checkLocalFile(
                                "${chat.getObjectId()}$baseFileName"),
                          ),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Container(
                      height: 2,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
                    decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.all(Radius.circular(25))),
                    child: Text(
                      audioLength,
                      style: textStyle(false, 12, AppConfig.appColor),
                    ),
                  ),
                  addSpaceWidth(5),
                  if (noFile)
                    Container(
                        padding: EdgeInsets.all(1),
                        decoration:
                            BoxDecoration(color: white, shape: BoxShape.circle),
                        child: Icon(
                          Icons.error,
                          color: red0,
                          size: 18,
                        )),
                  addSpaceWidth(10),
                ],
              )
            ],
          ),
        ),
      ),
    ),
  );
}

Future<File> loadFile(String path, String name) async {
  final ByteData data = await rootBundle.load(path);
  Directory tempDir = await getTemporaryDirectory();
  File tempFile = File('${tempDir.path}/$name');
  await tempFile.writeAsBytes(data.buffer.asUint8List(), flush: true);
  return tempFile;
}

showErrorDialog(context, String message,
    {onOkClicked, bool cancellable = true}) {
  showMessage(context, Icons.error, red0, "Oops!", message,
      delayInMilli: 500, cancellable: cancellable, onClicked: (_) {
    if (_ == true) {
      if (onOkClicked != null) onOkClicked();
    }
  });
}

showAdmin(BuildContext context) {
  showListDialog(context, [
    "General Message",
    "Add Contact",
    "Candidates",
    "Districts",
    "Parties Settings",
    "Contact Category Settings",
    "Report Category Settings",
    "Sit Type Category Settings",
    "Reports",
    "About Us Settings",
    "Privacy Policy Settings",
    "Terms & Conditions Settings",
    "Logout"
  ], (_) {
    if (_ == 'General Message') {
      pushAndResult(context, GeneralMessage());
    }
    if (_ == 'Add Contact') pushAndResult(context, AddContact());
    if (_ == 'Candidates') pushAndResult(context, ShowCandidates());
    if (_ == 'Districts') pushAndResult(context, ShowDistricts());

    if (_ == 'Reports') {
      pushAndResult(context, Reports());
    }
    if (_ == 'Suggestions') {
      pushAndResult(context, Suggestions());
    }

    if (_ == 'Contact Category Settings') {
      pushAndResult(
          context,
          fieldDialog(
            "Contact Category",
            allowEmpty: false,
            items: appSettingsModel.getList(CONTACT_CATEGORIES),
            okText: "Save",
          ), result: (_) {
        appSettingsModel
          ..put(CONTACT_CATEGORIES, _)
          ..updateItems();
      });
    }
    if (_ == 'Sit Type Category Settings') {
      pushAndResult(
          context,
          fieldDialog(
            "Sit Type",
            allowEmpty: false,
            items: appSettingsModel.getList(SIT_TYPE),
            okText: "Save",
          ), result: (_) {
        appSettingsModel
          ..put(SIT_TYPE, _)
          ..updateItems();
      });
    }
    if (_ == 'Parties Settings') {
      pushAndResult(
          context,
          fieldDialog(
            "Parties",
            allowEmpty: false,
            items: appSettingsModel.getList(PARTIES),
            okText: "Save",
          ), result: (_) {
        appSettingsModel
          ..put(PARTIES, _)
          ..updateItems();
      });
    }

    if (_ == 'About Us Settings') {
      pushAndResult(
          context,
          inputDialog(
            "About us",
            allowEmpty: false,
            message: appSettingsModel.getString(ABOUT_US),
            okText: "Save",
          ), result: (_) {
        appSettingsModel
          ..put(ABOUT_US, _)
          ..updateItems();
      });
    }
    if (_ == 'Privacy Policy Settings') {
      pushAndResult(
          context,
          inputDialog(
            "Privacy Policy",
            allowEmpty: false,
            message: appSettingsModel.getString(PRIVACY_POLICY),
            okText: "Save",
          ), result: (_) {
        appSettingsModel
          ..put(PRIVACY_POLICY, _)
          ..updateItems();
      });
    }
    if (_ == 'Terms & Conditions Settings') {
      pushAndResult(
          context,
          inputDialog(
            "Terms & Conditions",
            allowEmpty: false,
            message: appSettingsModel.getString(TERMS_AND_CONDITIONS),
            okText: "Save",
          ), result: (_) {
        appSettingsModel
          ..put(TERMS_AND_CONDITIONS, _)
          ..updateItems();
      });
    }

    if (_ == "Logout") clickLogout(context);
  });
}

nameItem(String title, String text,
    {color: black, bool center = false, bool paddBottom = true}) {
  return Container(
    margin: EdgeInsets.only(bottom: paddBottom ? 10 : 0),
    child: RichText(
      text: TextSpan(children: [
        TextSpan(text: title, style: textStyle(true, 13, color)),
        TextSpan(
            text: "  ", style: textStyle(false, 14, color.withOpacity(.5))),
        TextSpan(
            text: "$text", style: textStyle(false, 14, color.withOpacity(.5))),
      ]),
      textAlign: center ? TextAlign.center : TextAlign.left,
    ),
  );
}

pickCountry(context, onPicked(Country country)) {
  pushAndResult(
    context,
    SelectCountry(),
    result: (_) {
      onPicked(_);
    },
  );
}

String oldNumber = "";
bool checking = false;

inputTextView(String title, controller,
    {@required isNum,
    onEditted,
    int maxLine = 1,
    onTipClicked,
    String errorText,
    double corner = 5,
    bool isAmount = false,
    var icon,
    focusNode,
    bool useCurrentCountry = false}) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      if (errorText != null)
        Text(
          errorText,
          style: textStyle(true, 12, red0),
        ),
      if (errorText != null) addSpace(5),
//      addSpace(10),
      Container(
        //height: 45,
        constraints: BoxConstraints(maxHeight: 150),
        margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
        decoration: BoxDecoration(
            color: blue09,
            borderRadius: BorderRadius.circular(corner),
            border: Border.all(
                color: errorText != null ? red0 : black.withOpacity(.1),
                width: errorText != null ? 1 : .5)),
        child: Row(
          children: <Widget>[
            if (isAmount || icon != null) addSpaceWidth(10),
            if (isAmount)
              Container(
                margin: EdgeInsets.only(top: 2),
                child: CachedNetworkImage(
                  imageUrl: getCurrencyLogo(
                      useCurrentCountry ? currentCountry : defaultCountry),
                  width: 14,
                  height: 14,
                  fit: BoxFit.cover,
                  //color: black.withOpacity(.3),
                ),
              ),
            if (icon != null)
              icon is String
                  ? (Image.asset(
                      icon,
                      height: 14,
                      width: 14,
                      color: black.withOpacity(.3),
                    ))
                  : (Icon(
                      icon,
                      size: 14,
                      color: black.withOpacity(.3),
                    )),
            Flexible(
              child: new TextField(
                onSubmitted: (_) {
                  //postHeadline();
                },
//                textInputAction: maxLine==1?TextInputAction.done:TextInputAction.newline,
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.fromLTRB(
                      (isAmount || icon != null) ? 10 : 15, 10, 15, 10),
                  counter: null,
                  labelText: title,
                  labelStyle: textStyle(false, 18, black.withOpacity(.3)),
                  /*counterStyle: textStyle(true, 0, white)*/
                ),
                style: textStyle(
                  false,
                  18,
                  black,
                ),
                controller: controller, focusNode: focusNode,
                cursorColor: black,
                cursorWidth: 1,
//                          maxLength: 50,
                maxLines: maxLine > 1 ? null : maxLine,
                keyboardType: isNum
                    ? (TextInputType.number)
                    : maxLine == 1
                        ? TextInputType.text
                        : TextInputType.multiline,
                inputFormatters: [],
                scrollPadding: EdgeInsets.all(0),
                onChanged: (s) {
                  if (null != onEditted) onEditted();
                },
              ),
            ),
          ],
        ),
      ),
    ],
  );
}

String getCurrencyLogo(String country) {
  String currencyLogo = "";
  List countryList = appSettingsModel.getList(COUNTRY_LIST);
  for (Map m in countryList) {
    if (m[NAME] == country) {
      currencyLogo = m[CURRENCY_LOGO];
    }
  }
  return currencyLogo;
}

String getCurrencyText(String country) {
  String currency = "";
  List countryList = appSettingsModel.getList(COUNTRY_LIST);
  for (Map m in countryList) {
    if (m[NAME] == country) {
      currency = m[CURRENCY];
    }
  }
  return currency;
}

clickText(
  String title,
  String text,
  onClicked, {
  icon,
  double height: 60,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
//      if(text.isNotEmpty)Text(
//        title,
//        style: textStyle(true, 14, dark_green03),
//      ),
//      if(text.isNotEmpty)addSpace(10),
      GestureDetector(
        onTap: () {
          onClicked();
        },
        child: Container(
//          height: height,
          width: double.infinity, constraints: BoxConstraints(minHeight: 60),
          margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
          padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
          decoration: BoxDecoration(
              color: blue09,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: black.withOpacity(.1), width: .5)),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (text.isNotEmpty)
                  Text(
                    title,
                    style: textStyle(false, 12, black.withOpacity(.3)),
                  ),
                Row(
                  children: <Widget>[
                    if (icon != null)
                      Icon(
                        icon,
                        size: 18,
                        color: black.withOpacity(.5),
                      ),
                    if (icon != null) addSpaceWidth(5),
                    Expanded(
                      child: new Text(
                        text.isNotEmpty ? text : title,
                        style: textStyle(false, 18,
                            text.isEmpty ? black.withOpacity(.3) : black),
//                        maxLines: 1,overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Icon(
                      Icons.arrow_drop_down_circle,
                      size: 18,
                      color: black.withOpacity(.4),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    ],
  );
}

clickLogout(context) async {
  yesNoDialog(context, "Logout?", "Are you sure you want to logout?", () async {
    resetApp();
    userModel.put(IS_ONLINE, false);
    userModel.updateItems();
    for (String s in userModel.getList(TOPICS)) {
      fbMessaging.unsubscribeFromTopic(s);
    }
    await FirebaseAuth.instance.signOut();
    userModel = BaseModel();
    pushAndResult(context, Login(),
        clear: true, transitionBuilder: fadeTransition);
  });
}

getDefaultLine() {
  return addLine(.5, black.withOpacity(.1), 0, 0, 0, 0);
}

class ReadMoreText extends StatefulWidget {
  String text;
  bool full;
  var toggle;
  int minLength;
  double fontSize;
  var textColor;
  var moreColor;
  bool center;
  bool canExpand;

  ReadMoreText(
    this.text, {
    this.full = false,
    this.minLength = 150,
    this.fontSize = 14,
    this.toggle,
    this.textColor = black,
    this.moreColor = dark_green03,
    this.center = false,
    this.canExpand = true,
  });

  @override
  _ReadMoreTextState createState() => _ReadMoreTextState();
}

class _ReadMoreTextState extends State<ReadMoreText> {
  bool expanded;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    expanded = widget.full;
  }

  @override
  Widget build(BuildContext context) {
    return text();
  }

  text() {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
              text: widget.text.length <= widget.minLength
                  ? widget.text
                  : expanded
                      ? widget.text
                      : (widget.text.substring(0, widget.minLength)),
              style: textStyle(false, widget.fontSize, widget.textColor)),
          TextSpan(
              text: widget.text.length < widget.minLength || expanded
                  ? ""
                  : "...",
              style: textStyle(false, widget.fontSize, black)),
          TextSpan(
            text: widget.text.length < widget.minLength
                ? ""
                : expanded
                    ? " Read Less"
                    : "Read More",
            style: textStyle(true, widget.fontSize - 2, widget.moreColor,
                underlined: false),
            recognizer: new TapGestureRecognizer()
              ..onTap = () {
                setState(() {
                  if (widget.canExpand) expanded = !expanded;
                  if (widget.toggle != null) widget.toggle(expanded);
                });
              },
          )
        ],
      ),
      textAlign: widget.center ? TextAlign.center : TextAlign.left,
    );
  }
}

createReport(context, BaseModel theModel, int type, String message) {
  BaseModel report = BaseModel();
  report.put(MESSAGE, message);
  report.put(THE_MODEL, theModel.items);
  report.put(REPORT_TYPE, type);
  report.put(STATUS, STATUS_UNDONE);
  report.saveItem(REPORT_BASE, true);
  NotificationService.sendPush(
      topic: 'admin',
      title: 'Pending',
      tag: 'reports',
      body: 'Some reports are pending');
  showMessage(context, Icons.report, dark_green03, "Report Sent",
      "Thank you for submitting a report, we will review this and take neccessary actions",
      cancellable: true, delayInMilli: 500);
}

checkError(context, e, {bool indexErrorOnly = true}) {
  String error = e.toString();
  if (error.contains("PRECONDITION")) {
    String link =
        error.substring(error.indexOf("https://"), error.indexOf(", null"));
    showMessage(context, Icons.error, blue0, "Index Needed", link,
        clickYesText: "Create Index", onClicked: (_) {
      if (_ == true) {
        openLink(link);
      }
    });
  } else {
    if (!indexErrorOnly)
      showErrorDialog(
        context,
        e.toString(),
      );
  }
}

Widget get videoLoading => Container(
      color: black.withOpacity(0.1),
      alignment: Alignment.center,
      child: SpinKitFadingCircle(
        color: Colors.white,
      ),
    );

clickTagged(context, model) {
  List taggedPersons = model.getList(TAGGED_PERSONS);
  if (taggedPersons.isEmpty) {
    showErrorDialog(context, "No persons found");
    return;
  }

  showModalBottomSheet(
      context: context,
      builder: (
        c,
      ) {
        return Container(
          color: white,
          padding: EdgeInsets.only(top: 20, bottom: 20),
          child: SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: List.generate(taggedPersons.length, (p) {
                  Map item = taggedPersons[p];
                  BaseModel model = BaseModel(items: item);
                  bool followed =
                      userModel.amFollowing.contains(model.getObjectId());

                  return Container(
                      margin: EdgeInsets.only(left: 10, right: 10, bottom: 2),
                      //padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (p == 0)
                            Text("Persons", style: textStyle(true, 18, black)),
                          if (p == 0) addSpace(5),
                          GestureDetector(
                            onTap: () {
                              pushReplaceAndResult(
                                  context,
                                  ShowPerson(
                                    model: model,
                                  ));
                            },
                            child: Container(
                              color: transparent,
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: Stack(
                                      children: [
                                        Container(
                                          height: 40,
                                          width: 40,
                                          color: appColor.withOpacity(.6),
                                        ),
                                        if (model.userImage.isNotEmpty)
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            child: CachedNetworkImage(
                                              imageUrl: model.userImage,
                                              height: 40,
                                              width: 40,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                  addSpaceWidth(10),
                                  Column(
                                    children: [Text(model.getString(NAME))],
                                  ),
                                  Spacer(),
                                  FlatButton(
                                    onPressed: () {
                                      pushReplaceAndResult(
                                          context,
                                          ShowPerson(
                                            model: model,
                                          ));
                                      /*followThisUser(model, followed, onComplete: () {
                                          setState(() {});
                                        });*/
                                    },
                                    color: appColor,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                        side: BorderSide(color: appColor)),
                                    child: Center(
                                        child: Text(
                                      followed ? "Unfollow" : "Follow",
                                      style: textStyle(true, 12, white),
                                    )),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ));
                })),
          ),
        );
      });
}

String formatToK(int num) {
  return NumberFormat.compactCurrency(decimalDigits: 0, symbol: "").format(num);
}

bool isBlocked(model, {String userId}) {
  if (userId != null) {
    if (userId.isNotEmpty && blockedIds.contains(userId)) return true;
    return false;
  }

  String oId = model.getObjectId();
  String uId = model.getString(USER_ID);
  String dId = model.getString(DEVICE_ID);
  if (oId.isNotEmpty && blockedIds.contains(oId)) return true;
  if (uId.isNotEmpty && blockedIds.contains(uId)) return true;
//  if(dId.isNotEmpty && blockedIds.contains(dId))return true;

  return false;
}

getTheOtherId(
  BaseModel chatSetupModel,
) {
  //String farmAdmin = chatSetupModel.getString(FARM_ADMIN);
  //String farmId = chatSetupModel.getString(FARM_ID);
  List parties = chatSetupModel.getList(PARTIES);
  /*if(farmAdmin!=userModel.getObjectId()){
    return farmAdmin;
  }*/
  parties.remove(userModel.getUserId());
  return parties[0];
}

getScreenHeight(context) {
  return MediaQuery.of(context).size.height;
}

getScreenWidth(context) {
  return MediaQuery.of(context).size.width;
}

//String getDeviceId() {}
String getExtImage(String fileExtension) {
  if (fileExtension == null) return "";
  fileExtension = fileExtension.toLowerCase().trim();
  if (fileExtension.contains("doc")) {
    return icon_file_doc;
  } else if (fileExtension.contains("pdf")) {
    return icon_file_pdf;
  } else if (fileExtension.contains("xls")) {
    return icon_file_xls;
  } else if (fileExtension.contains("ppt")) {
    return icon_file_ppt;
  } else if (fileExtension.contains("txt")) {
    return icon_file_text;
  } else if (fileExtension.contains("zip")) {
    return icon_file_zip;
  } else if (fileExtension.contains("xml")) {
    return icon_file_xml;
  } else if (fileExtension.contains("png") ||
      fileExtension.contains("jpg") ||
      fileExtension.contains("jpeg")) {
    return icon_file_photo;
  } else if (fileExtension.contains("mp4") ||
      fileExtension.contains("3gp") ||
      fileExtension.contains("mpeg") ||
      fileExtension.contains("avi")) {
    return icon_file_video;
  } else if (fileExtension.contains("mp3") ||
      fileExtension.contains("m4a") ||
      fileExtension.contains("m4p")) {
    return icon_file_audio;
  }

  return icon_file_unknown;
}

personMap(items, {bool isAdmin = false, bool isCreator = false}) {
  final base = BaseModel(items: items);
  return {
    OBJECT_ID: base.getObjectId(),
    USER_ID: base.getObjectId(),
    USER_IMAGE: base.userImage,
    PUSH_TOKEN: base.pushToken,
    NICKNAME: base.nickName,
    NAME: base.name,
    IS_ADMIN: isAdmin,
    IS_CREATOR: isCreator,
    TIME: DateTime.now().millisecondsSinceEpoch,
  };
}

String formatToActualDate(int time, {bool isTime = false}) {
  Timestamp t = userModel.get(CREATED_AT);
  DateTime d = DateTime.fromMillisecondsSinceEpoch(time);
  final date = DateTime.parse(d.toString());
  return new DateFormat(
    isTime ? "h:mm a" : "MMMM d,yyyy",
  ).format(date);
}

placeCall(String phone) {
  openLink("tel://$phone");
}

sendEmail(String email) {
  openLink("mailto:$email");
}

openWhatsapp(context, String phone, {String text = ""}) async {
  var whatsappUrl = "whatsapp://send?phone=$phone&text=$text";
  await canLaunch(whatsappUrl)
      ? launch(whatsappUrl)
      : showErrorDialog(context, "Whatsapp not found");
}

String createPhoneNumer(String phonePref, String phone) {
  phone = phone.startsWith("0") ? phone.substring(1) : phone;
  phone = "+$phonePref$phone";
  return phone;
}

double calculateRating(double value) {
  if (value.isBetween(0.8, 1.0)) return 5.0;
  if (value.isBetween(0.6, 0.8)) return 4.0;
  if (value.isBetween(0.4, 0.6)) return 3.0;
  if (value.isBetween(0.2, 0.4)) return 2.0;
  if (value.isBetween(0.1, 0.2)) return 1.0;
  return 0.0;
}

sharePost(BaseModel model) {
  String appLink = appSettingsModel.getString(APP_LINK_IOS);
  if (Platform.isAndroid)
    appLink = appSettingsModel.getString(APP_LINK_ANDROID);
      Share.share('check out OurVoice on $appLink', subject: model.getString(TITLE));
}

// postItem(BuildContext context, BaseModel);
