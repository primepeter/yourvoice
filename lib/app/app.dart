library app;

import 'package:share/share.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:acclaim/AddContact.dart';
import 'package:acclaim/ChatMain.dart';
import 'package:acclaim/GeneralMessage.dart';
import 'package:acclaim/LangMain.dart';
import 'package:acclaim/ManageReportCategory.dart';
import 'package:acclaim/Reports.dart';
import 'package:acclaim/SelectCountry.dart';
import 'package:acclaim/ShowCandidates.dart';
import 'package:acclaim/ShowDistricts.dart';
import 'package:acclaim/SimpleVideoPlayer.dart';
import 'package:acclaim/Suggestions.dart';
import 'package:acclaim/app_config.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/auth/login.dart';
// import 'package:acclaim/chat/chat_main.dart';
import 'package:acclaim/edit_about_us.dart';
import 'package:acclaim/edit_privacy_policy.dart';
import 'package:acclaim/edit_terms_conditions.dart';
import 'package:acclaim/localization/app_translations.dart';
import 'package:acclaim/main_screens/show_person.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:country_pickers/country.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_better_camera/camera.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:flutter_multimedia_picker/data/MediaFile.dart';
import 'package:flutter_photo_picker/flutter_photo_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hashtagable/hashtagable.dart';
import 'package:http/http.dart';
import 'package:i18n_extension/i18n_extension.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
// import 'package:time_machine/time_machine.dart';
import 'package:timeago/timeago.dart' as timeAgo;
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';
import 'package:video_compress/video_compress.dart';
import 'package:video_player/video_player.dart';

import 'countries.dart';

part 'appAssets.dart';
part 'appDatabases.dart';
part 'appEngine.dart';
part 'appExtensions.dart';
part 'baseModel.dart';
part 'colors.dart';
part 'countryChooser.dart';
part 'dotsIndicator.dart';
part 'gridCollage.dart';
part 'infoDialog.dart';
part 'fieldDialog.dart';
part 'inputDialog.dart';
part 'listDialog.dart';
part 'messageDialog.dart';
part 'navigation.dart';
part 'notificationService.dart';
// part 'placeChooser.dart';
part 'preview_image.dart';
part 'progressDialog.dart';
part 'rating.dart';
part 'unicons.dart';

class Countries {
  final String countryName;
  final String countryFlag;
  final String countryCode;

  Countries({this.countryName, this.countryFlag, this.countryCode});
}

Locale appLocale = Locale('en');
bool isLoggedIn = false;
bool isAdmin = false;
String currentProgress = "";
bool showProgressLayout = false;
final progressId = getRandomId();
StreamSubscription usersStream;
StreamSubscription appSettingsStream;
bool hasChatLoaded = false;
List<BaseModel> allMessages = List();

bool readyNotes = false;
List<BaseModel> myNotes = List();

bool partnersLoaded = false;

List<BaseModel> usersOnline = List();
List<BaseModel> usersTotal = List();

// String? ok;
bool setup = false;
bool postReady = false;
List<BaseModel> myNotifications = List();

List<BaseModel> myPosts = List();
List<BaseModel> otherPosts = List();
List<BaseModel> allPosts = List();
List postList = List();

bool storiesReady = false;
List<BaseModel> allStories = List();

final List<String> imageFolders = [];

bool hasSetup = false;
bool isPostingStory = false;

List<CameraDescription> cameras = [];
var notificationsPlugin = FlutterLocalNotificationsPlugin();

BaseModel userModel = BaseModel();
BaseModel appSettingsModel;
List<BaseModel> followers = [];
List<BaseModel> following = [];
List<File> trimmedVideoFrames = [];
List<File> trimmedThumbFrames = [];
List<String> uploadingPost = [];

Map otherPeronInfo = Map();
Map unreadCounter = Map();
List<BaseModel> lastMessages = List();
bool chatSetup = false;
List showNewMessageDot = [];
List showNotifyDot = [];
final storyController = StreamController<bool>.broadcast();
final postingController = StreamController<bool>.broadcast();
final bookUploadController = StreamController<BaseModel>.broadcast();
final postUploadController = StreamController<BaseModel>.broadcast();
List<String> stopListening = List();
// String visibleChatId = "";
int timeOnline = 0;

final scrollControllers = List.generate(3, (p) => ScrollController());
VideoPlayerController videoController;
String currentPlayingId = "";
bool canPlayVideo = true;
bool pageIsFollowers = false;

bool librarySetup = false;
List<BaseModel> libraryList = [];

final pageVisibilityController = StreamController<int>.broadcast();

final translationController = StreamController<bool>.broadcast();
String appLanguage;

String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
final fbMessaging = FirebaseMessaging();
bool settingsLoaded = false;
List blockedIds = [];
List<StreamSubscription> subs = List();

void resetApp() {
  for (var sub in subs) sub.cancel();

  currentProgress = "";
  settingsLoaded = false;
  isLoggedIn = false;
  isAdmin = false;
  showProgressLayout = false;
  hasChatLoaded = false;
  readyNotes = false;
  partnersLoaded = false;
  hasSetup = false;
  isPostingStory = false;
  storiesReady = false;
  setup = false;

  allMessages.clear();
  lastMessages.clear();
  myNotes.clear();
  usersOnline.clear();
  myNotifications.clear();
  allPosts.clear();
  myPosts.clear();
  otherPosts.clear();
  allStories.clear();
  imageFolders.clear();
  following.clear();
  followers.clear();

  uploadingPost = [];

  otherPeronInfo = Map();
  unreadCounter = Map();
  lastMessages = List();
  chatSetup = false;
  showNewMessageDot = [];
  stopListening = List();
  visibleChatId = "";
  timeOnline = 0;
  currentPlayingId = "";
  canPlayVideo = true;
  pageIsFollowers = false;

  librarySetup = false;
  libraryList = [];
  // currentVisibleTab = -1;

  userModel = BaseModel();
}
