part of 'app.dart';

class PreviewImage extends StatelessWidget {
  final BaseModel photo;
  final List<BaseModel> photos;
  final int initialPicture;
  PreviewImage({this.photo, this.photos, this.initialPicture = 0});

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      onDismissed: (d) => Navigator.pop(context),
      direction: DismissDirection.vertical,
      key: Key('key'),
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            if (photos != null)
              Container(
                  child: PhotoViewGallery.builder(
                pageController: PageController(initialPage: initialPicture),
                scrollPhysics: const BouncingScrollPhysics(),
                builder: (BuildContext context, int p) {
                  BaseModel photo = photos[p];
                  return PhotoViewGalleryPageOptions(
                    imageProvider: CachedNetworkImageProvider(photo.urlPath),
                    //initialScale: PhotoViewComputedScale.contained * 0.8,
                    //heroTag: index,
                  );
                },
                itemCount: photos.length,
                loadingChild:
                    loadingLayout(color: Colors.transparent, load: white),
                backgroundDecoration: BoxDecoration(color: Colors.black),
              ))
            else
              Container(
                  child: PhotoView(
                //imageProvider: NetworkImage(imageURL),
                imageProvider: CachedNetworkImageProvider(photo.urlPath),
                loadingChild:
                    loadingLayout(color: Colors.transparent, load: white),
                backgroundDecoration: BoxDecoration(color: Colors.black),
              )),
            new SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: new IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
