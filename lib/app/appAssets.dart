part of 'app.dart';

class GetAsset {
  static String fonts(String asset) {
    return 'assets/fonts/$asset';
  }

  static String images(String asset) {
    return 'assets/images/$asset';
  }

  static String sounds(String asset) {
    return 'assets/sounds/$asset';
  }
}

class LangsKey {
  static const String welcome_title_1 = "welcome_title_1";
  static const String welcome_title_2 = "welcome_title_2";
  static const String welcome_title_3 = "welcome_title_3";
  static const String welcome_title_4 = "welcome_title_4";

  static const String welcome_desc_1 = "welcome_desc_1";
  static const String welcome_desc_2 = "welcome_desc_2";
  static const String welcome_desc_3 = "welcome_desc_3";
  static const String welcome_desc_4 = "welcome_desc_4";

  static const String loginString = "login_string";
  static const String signUpString = "signup_string";
  static const String enter_email = "enter_email";
  static const String enter_password = "enter_password";
  static const String remember_password = "remember_password";
  static const String log_me_in = "log_me_in";
  static const String no_account = "no_account";
  static const String create = "create";
  static const String have_account = "have_account";
  static const String login_into_account = "login_into_account";
  static const String securely_login_into = "securely_login_into";
  static const String recover_account = "recover_account";
  static const String enter_account_email = "recover_account";
  static const String reset_account = "reset_account";
  static const String create_your_account = "reset_account";
  static const String setup_account = "reset_account";
  static const String enter_email_pass = "enter_email_pass";
  static const String create_an_account_for = "create_an_account_for";
  static const String resend_verification_code = "resend_verification_code";
  static const String enter_verification_code = "enter_verification_code";
  static const String next = "next";
  static const String setup_account_info = "setup_account_info";

  static const String verify_phone_number = "verify_phone_number";
  static const String enter_your_phone_number = "enter_your_phone_number";
  static const String enter_phone_number = "enter_phone_number";
  static const String select_your_country = "select_your_country";

  static const String create_account = "create_account";
  static const String forgot_password = "forgot_password";
  static const String reset_password = "reset_password";

  static const String full_name = "full_name";
  static const String enter_full_name = "enter_full_name";
  static const String nickname = "nickname";
  static const String enter_nickname = "enter_nickname";
  static const String date_of_birth = "date_of_birth";
  static const String choose_date_of_birth = "choose_date_of_birth";

  static const String email = "email";
  static const String password = "password";
  static const String retype_password = "retype_password";

  static const String mobile_number = "mobile_number";
  static const String what_is_your_profession = "what_is_your_profession";
  static const String profession = "profession";

  static const String gender = "gender";
  static const String choose_your_gender = "choose_your_gender";

  static const String male = "male";
  static const String female = "female";
  static const String resend_code = "resend_code";
  static const String verifying_code = "verifying_code";
  static const String error = "error";
  static const String logging_please_wait = "logging_please_wait";
  static const String logging_error = "login_error";
  static const String please_enter_email = "please_enter_email";
  static const String please_enter_password = "please_enter_password";
  static const String please_enter_re_password = "please_enter_re_password";
  static const String password_not_a_match = "password_not_a_match";
  static const String please_enter_mobile = "please_enter_mobile";
  static const String please_choose_country = "please_choose_country";
  static const String please_enter_code = "please_enter_code";
  static const String please_enter_full_name = "please_enter_full_name";
  static const String please_enter_nick_name = "please_enter_nick_name";
  static const String please_choose_dob = "please_choose_dob";
  static const String please_enter_profession = "please_enter_profession";
  static const String please_choose_gender = "please_choose_gender";
  static const String please_enter_about = "please_enter_about";

  static const String interest_title = "interest_title";
  static const String interest_desc = "interest_desc";

  static const String interest_1 = "interest_1";
  static const String interest_2 = "interest_2";
  static const String interest_3 = "interest_3";
  static const String interest_4 = "interest_4";
  static const String interest_5 = "interest_5";
  static const String interest_6 = "interest_6";
  static const String interest_7 = "interest_7";
  static const String interest_8 = "interest_8";
  static const String interest_9 = "interest_9";
  static const String interest_10 = "interest_10";
  static const String interest_11 = "interest_11";
  static const String interest_12 = "interest_12";
  static const String interest_13 = "interest_13";
  static const String interest_14 = "interest_14";
  static const String interest_15 = "interest_15";
  static const String interest_16 = "interest_16";
  static const String interest_17 = "interest_17";

  static const String submit_interest = "submit_interest";

  static const String dummy_full_name = "dummy_full_name";
  static const String dummy_nick_name = "dummy_nick_name";
  static const String drawer_0 = "drawer_0";
  static const String drawer_1 = "drawer_1";
  static const String drawer_2 = "drawer_2";
  static const String drawer_3 = "drawer_3";
  static const String drawer_4 = "drawer_4";
  static const String drawer_5 = "drawer_5";
  static const String drawer_6 = "drawer_6";
  static const String sign_out = "sign_out";
  static const String greeting = "greeting";
  static const String been_here_since = "been_here_since";
  static const String logout = "logout";
  static const String logout_string = "logout_string";
  static const String logout_string_yes = "logout_string_yes";
  static const String logging_out = "logging_out";

  static const String successful = "successful";
  static const String interests_updated = "interests_updated";
  static const String ok_string = "ok_string";
  static const String archive = "archive";
  static const String courses = "courses";
  static const String library = "library";

  static const String cheers = "cheers";
  static const String followers = "followers";
  static const String following = "following";

  static const String profile_name = "profile_name";
  static const String profile_occupation = "profile_occupation";
  static const String occupation = "occupation";
  static const String username = "username";
  static const String about_me = "about_me";
  static const String about_me_string = "about_me_string";
  static const String save = "save";
  static const String cancel = "cancel";
  static const String profile_save = "profile_save";

  static const String enabled = "enabled";
  static const String disabled = "disabled";
  static const String push_notifications = "push_notifications";
  static const String email_notifications = "email_notifications";
  static const String library_notifications = "library_notifications";
  static const String chat_notifications = "chat_notifications";

  //
  static const String create_a_post = "create_a_post";

  static const String post_type_0 = "post_type_0";
  static const String write_a_description = "write_a_description";
  static const String mention_someone = "mention_someone";
  static const String mention_place = "mention_place";
  static const String post = "post";
  static const String post_category = "post_category";
  static const String add_photos = "add_photos";
  static const String camera = "camera";
  static const String gallery = "gallery";

  static const String comments = "comments";
  static const String report_abuse = "report_abuse";
  static const String share_post = "share_post";
  static const String follow_post = "follow_post";
  static const String unFollow_post = "unFollow_post";
  static const String posts = "posts";
  static const String notifications = "notifications";
  static const String add = "add";
  static const String create_new_partners = "create_new_partners";
  static const String website = "website";
  static const String enter_partners_website = "enter_partners_website";
  static const String partners_photos = "partners_photos";
  static const String partners_photos_max = "partners_photos_max";

  static const String morning = "morning";
  static const String afternoon = "afternoon";
  static const String evening = "evening";

  static const String tc_1 = "tc_1";
  static const String tc_2 = "tc_2";
  static const String tc_3 = "tc_3";
  static const String tc_4 = "tc_4";

  static const String suggest_feature = "suggest_feature";
  static const String photos = "photos";
  static const String add_photos_max_3 = "add_photos_max_3";
  static const String add_photos_optional = "add_photos_optional";
  static const String your_suggest = "your_suggest";
  static const String what_you_suggest = "what_you_suggest";
  static const String send = "send";
  static const String request = "request";
  static const String request_sub_successful = "request_successful";

  static const String add_note = "add_note";
  static const String title = "title";
  static const String write_a_title = "write_a_title";
  static const String note = "note";
  static const String note_save_successful = "note_save_successful";
  static const String add_photos_3_optional = "add_photos_3_optional";
  static const String parties = "parties";
  static const String chat_id = "chatId";

  static const String chat_user = "chat_user";
  static const String edit_post = "edit_post";
  static const String delete_post = "delete_post";

  static const String notifiedId = "notifiedId";

  //
}

const String icon_file_unknown = 'assets/images/icon_file_unknown.png';
const String icon_file_photo = 'assets/images/icon_file_photo.png';
const String icon_file_zip = 'assets/images/icon_file_zip.png';
const String icon_file_xml = 'assets/images/icon_file_xml.png';
const String icon_file_audio = 'assets/images/icon_file_audio.png';
const String icon_file_doc = 'assets/images/icon_file_doc.png';
const String icon_file_pdf = 'assets/images/icon_file_pdf.png';
const String icon_file_text = 'assets/images/icon_file_text.png';
const String icon_file_video = 'assets/images/icon_file_video.png';
const String icon_file_xls = 'assets/images/icon_file_xls.png';
const String icon_file_ppt = 'assets/images/icon_file_ppt.png';

const String chat_bg = 'assets/images/chat_bg.jpg';
const String ic_google = 'assets/images/ic_google.png';
const String ic_facebook2 = 'assets/images/ic_fb.png';
const String ic_facebook = 'assets/images/ic_facebook.png';
const String ic_apple = 'assets/images/ic_apple.png';
const String ic_study = 'assets/images/ic_study1.png';
const String ic_book = 'assets/images/ic_book.png';
const String ic_message = 'assets/images/ic_message.png';
const String ic_chat = 'assets/images/ic_chat.png';
const String ic_mute = 'assets/images/ic_mute.png';
const String ic_chat_box = 'assets/images/ic_chat_box.png';
const String ic_note = 'assets/images/ic_note.png';
const String ic_search = 'assets/images/ic_search.png';
const String ic_comment = 'assets/images/ic_comment.png';
const String ic_like = 'assets/images/like1.png';
const String ic_share = 'assets/images/ic_share.png';
const String ic_exit_group = 'assets/images/ic_exit_group.png';
const String google_map = 'assets/images/google_map.jpg';
const String ic_plain_small = 'assets/images/launch_image.png';
const String ic_church = 'assets/images/church.png';

const int PUSH_TYPE_CHAT = 0;
const int CHAT_TYPE_TEXT = 0;
const int CHAT_TYPE_IMAGE = 1;
const int CHAT_TYPE_DOC = 2;
const int CHAT_TYPE_VIDEO = 3;
const int CHAT_TYPE_REC = 4;
const int CHAT_TYPE_LOCATION = 6;

const int NOTIFY_TYPE_CHAT = 0;
const int NOTIFY_TYPE_POST = 1;
const int NOTIFY_TYPE_FOLLOW = 2;
const int NOTIFY_TYPE_LIKE = 3;
const int NOTIFY_TYPE_COMMENT = 4;
const int NOTIFY_TYPE_COMMENT_LIKE = 5;
const int NOTIFY_TYPE_REPLY = 6;
const int NOTIFY_TYPE_REPLY_LIKE = 7;
const int NOTIFY_TYPE_STORY = 8;

const int REPORT_TYPE_POST = 0;
const int REPORT_TYPE_STORY = 1;
const int REPORT_TYPE_COMMENT = 2;
const int REPORT_TYPE_USER = 3;

Map<String, String> postTags = {
  "post_type_00": "Following",
  "post_type_0": "General",
  "post_type_1": "Films",
  "post_type_2": "Culture",
  "post_type_3": "Fan",
  "post_type_4": "Advertisement",
  "post_type_5": "News",
  "post_type_6": "Islamic",
  "post_type_7": "Technologies",
  "post_type_8": "Music",
  "post_type_9": "Sports",
  "post_type_10": "Toys",
  "post_type_11": "Trade",
  "post_type_12": "Entertainment",
  "post_type_13": "Health",
  "post_type_14": "Cruises",
  "post_type_15": "Science",
  "post_type_16": "Innovations",
  "post_type_17": "Feed him",
};

const String OBJECT_ID = "objectId";
const String TYPE = "type";
const String TITLE = "title";
const String ICON = "icon";
const String WEBSITE = "website";
const String DESCRIPTION = "description";
const String MESSAGE = "message";
const String FILE_URL = "fileUrl";
const String REFERENCE = "reference";
const String ADD = "add";
const String VALUE = "value";
const String KEY = "key";
const String EMAIL = "email";
const String USERNAME = "username";
const String PASSWORD = "password";
const String MUTED = "muted";
const String BLOCKED_IDS = "blockedIds";
const String GENDER = "gender";
const String HIDDEN = "hidden";
const String USER_ID = "userId";
const String USER_IMAGE = "userImage";
const String SELFIE_IMAGE_URL = "selfieImageUrl";
const String SELFIE_IMAGE_PATH = "selfieImagePath";
const String FULL_NAME = "name";
const String IS_ADMIN = "isAdmin";
const String IS_MAUGOST = "isMaugost";
const String MY_SENT_CHAT = "mySentChat";
const String NICKNAME = "nickName";
const String DATE_OF_BIRTH = "dateOfBirth";
const String PROFESSION = "profession";
const String ABOUT_ME = "aboutMe";
const String WHAT_I_DO = "whatIdo";
const String ADDRESS_ME = "addressMe";
const String MOBILE_NUMBER = "mobileNumber";
const String ABOUT_US = "aboutUs";
const String PRIVACY_POLICY = "privacyPolicy";
const String TERMS_AND_CONDITIONS = "termsAndConditions";
const String PARTNERS = "partners";
const String WEB_ADDRESS = "webAddress";
const String URL_PATH = "urlPath";
const String COMPRESSED_PHOTO = "compressedPhoto";
const String COMPRESSED_THUMB = "compressedThumb";
const String THUMBNAIL_PATH = "thumbnailPath";
const String THUMBNAIL_URL = "thumbnailUrl";
const String CAPTION = "caption";
const String IS_VIDEO = "isVideo";
const String IS_LOCAL = "isLocal";
const String PHOTOS = "photos";
const String IMAGES = "images";
const String SEARCH_PARAM = "searchParam";
const String PARTIES = "parties";
const String PARTIES_MODEL = "partiesModel";
const String TRANSLATION = "translation";
const String MY_FOLLOWER_IDS = "followerIds";
const String AM_FOLLOWING_IDS = "followingIds";
const String STORY_IDS = "storyIds";
const String POSTS_IDS = "postIds";
const String INTERESTS = "interests";
const String INTEREST_SHOWN = "interestShown";
const String NOTIFIED_ID = "notifiedId";
const String NOTIFIED_ON = "notifiedOn";
const String PUSH_TOKEN = "pushToken";
const String DELETED_CHATS = "deletedChats";
const String LAST_COMMENTS = "lastComments";
const String SEEN_BY = "seenBy";
const String LIKED_BY = "likedBy";
const String REPORTED_ID = "reportedId";
const String SHOW_TRANSLATED = "showTranslated";
const String POST_ID = "postId";
const String COMMENTED_BY = "commentedBy";

const String IS_ONLINE = "isOnline";
const String TYPING_ID = "typingId";
const String VIDEO_LENGTH = "videoLength";
const String CHAT_ID = "chatId";
const String READ_BY = "readBy";
const String DELETED = "deleted";
const String AM_INTERESTED = "amInterested";
const String PLATFORM = "platform";
const String ANDROID = "android";
const String IOS = "ios";
const String WEB = "web";
const String VIDEO_AUTO_PLAY = "videoAutoPlay";
const String VIDEO_AUTO_MUTE = "videoAutoMute";

const String PUSH_NOTIFICATIONS = "pushNotifications";
const String EMAIL_NOTIFICATIONS = "email_notifications";
const String LIBRARY_NOTIFICATIONS = "library_notifications";
const String CHAT_NOTIFICATIONS = "chat_notifications";

const String DATABASE_NAME = "dataBaseName";
const String CREATED_AT = "createdAt";
const String UPDATED_AT = "updatedAt";
const String TIME_UPDATED = "timeUpdated";
const String TIME = "time";
const String TIME_ONLINE = "timeOnline";

const String COVER_PHOTO = "coverPhoto";
const String DURATION = "duration";
const String WRITER = "writer";
const String PUBLISHER = "publisher";
const String NARRATORS = "narrators";
const String STUDIO = "studio";
const String ASSISTED = "assisted";
const String OTHER_DETAILS = "otherDetails";
const String AUDIO_BOOKS = "audioBooks";
const String DURATION_INFO = "durationInfo";
const String ASPECT_RATIO = "aspectRatio";
const String VIDEO_HEIGHT = "videoHeight";
const String VIDEO_WIDTH = "videoWidth";
const String VIDEO_DIMENSIONS = "videoDimensions";

const String REFERENCE_BASE = "referenceBase";
const String USER_BASE = "userBase";
const String APP_SETTINGS = "appSettings";
const String APP_SETTINGS_BASE = "appSettingsBase";
const String NOTIFY_BASE = "notifyBase";
const String POST_BASE = "postBase";
const String COMMENT_BASE = "commentBase";
const String REPLY_BASE = "replyBase";
const String PARTNER_BASE = "partnerBase";
const String NOTE_BASE = "noteBase";
const String STORY_BASE = "storyBase";
const String REPORT_BASE = "reportBase";
const String CHAT_BASE = "chatBase";
const String SUGGESTION_BASE = "suggestionBase";
const String DOWNLOAD_BASE = "downloadBase";
const String LIBRARY_BASE = "libraryBase";
const String TRANS_BASE = "transBase";
const String CHAT_IDS_BASE = "chatIdsBase";

const String APP_LANGUAGE = "appLanguage";
const String TRANSLATIONS = "translations";

const String COUNTRY = "country";
const String COUNTRY_LIST = "countryList";
const String COUNTRY_CODE = "countryCode";
const String NAME = "name";
const String CURRENCY = "currency";
const String CURRENCY_LOGO = "currencyLogo";
const String LANGUAGE_VERSION = "languageVersion";

String currentCountry = "Nigeria";
String defaultCountry = "Nigeria";
String defaultState = "";
String defaultCity = "";
String languageVersion = "";

const String VERSION_CODE = "versionCode";
const String GEN_MESSAGE = "genMessage";
const String GEN_MESSAGE_TIME = "genMessageTime";
const String BLOCKED = "blocked";
const String DEVICE_ID = "deviceId";
const String TOPICS = "topics";
const String APP_INTERESTS = "appInterests";
const String APP_REPORT_CATEGORIES = "appReportCategories";
const String APP_CATEGORIES = "appCategories";
const String IMAGE_PATH = "imagePath";
const String VIDEO_PATH = "videoPath";
const String VIDEO_CONTROLLER = "videoController";
const String IMAGE_URL = "imageUrl";
const String VIDEO_URL = "videoUrl";
const String STATUS = "status";

const String ITEM_DB = "itemDb";
const String REPORTS = "reports";
const String ITEM_NAME = "ItemName";
const String REPORT_TYPE = "reportType";
const String COMMENT_ID = "commentId";
const String POST_USER_ID = "postUserId";
const String COMMENT_USER_ID = "commentUserId";
const String ITEM_ID = "itemId";
const String REPLY_ID = "replyId";

const String PLACE_NAME = "placeName";
const String LATITUDE = "latitude";
const String LONGITUDE = "longitude";
const String IS_DEFAULT = "isDefault";
const String CATEGORY_IDS = "categoryIds";

const String TAGGED_PERSONS_IDS = "taggedPersonsIds";
const String TAGGED_PERSONS = "taggedPersons";
const String TAGGED_CATEGORY = "taggedCategory";
const String TAGGED_PLACE = "taggedPlace";
const String TAGGED_PLACE_LAT = "taggedPlaceLat";
const String TAGGED_PLACE_LONG = "taggedPlaceLong";

const String THE_MODEL = "theModel";
const String EDITED = "edited";
const String NOTIFY_ME_IDS = "notifyMeIds";
const String BOOKMARKS = "bookmarks";
const String FAVORITES = "favorites";
const String COMMENTS = "comments";
const String THE_COMMENT = "theComments";
const String REPLIES = "replies";
const String SEEN = "seen";
const String RECENT_SEARCH = "resentSearch";
const String REC_ID = "recId";

const String AUDIO_DURATION = "audioDuration";
const String AUDIO_URL = "audioUrl";
const String AUDIO_SIZE = "audioSize";
const String AUDIO_PATH = "audioPath";
const String AUDIO_LENGTH = "audioLength";
const String REPLY_DATA = "replyData";
const String VOICE_TYPE = "voiceType";

const String FILE_PATH = "filePath";
const String FILE_EXTENSION = "fileExtension";
const String FILE_ORIGINAL_PATH = "fileOriginalPath";
const String FILE_NAME = "fileName";
const String FILE_SIZE = "fileSize";
const String FILE_DATA = "fileData";
const String PATH = "path";
const String DONT_PUSH = "dontPush";
const String SUPPORT_ID = "supportId";
const String PERSONS_SHOWN = "personsShown";
const String STORY_PROGRESS = "storyProgress";
const String ADDRESS_ME_BY = "addressMeBy";
const String FULLY_REGISTERED = "fullyRegistered";

const String IS_CREATOR = "isCreator";

const String GROUP_NAME = "groupName";
const String GROUP_IMAGE = "groupImage";
const String GROUP_CHANNEL_ID = "groupChannelId";
const String GROUP_INFO = "groupInfo";
const String GROUP_CATEGORY = "groupCategory";
const String GROUP_MEMBERS = "groupMembers";
const String GROUP_BASE = "groupBase";

const String COUNTRY_BASE = "countryBase";
const String STATE = "state";
const String STATE_CONSTITUENCY = "stateConstituency";
const String DISTRICT = "district";
const String CANDIDATE = "candidate";
const String POST_TYPE = "postType";
const String STATE_LOCALS = "stateLocals";
const String POSITIONS = "positions";
const String ELECTORAL_POSITION = "electoralPositions";
const String SIT_TYPE = "sitType";
const String VOTES = "votes";
const String LIKES = "likes";
const String DISLIKES = "disLikes";
const String POLITICAL_PARTY = "politicalParty";
const String CANDIDATE_BASE = "candidateBase";
const String DISTRICT_BASE = "districtBase";

const String LATITUDE_PICKUP = "latitudePickup";
const String LATITUDE_DELIVERY = "latitudeDelivery";
const String LONGITUDE_DELIVERY = "longitudeDelivery";
const String LONGITUDE_PICKUP = "longitudePickup";
const String PICKUP_ADDRESS = "pickupAddress";
const String DELIVERY_ADDRESS = "deliveryAddress";
const String PICKUP_PHONE = "pickupPhone";
const String PICKUP_EMAIL = "pickupEmail";
const String PICKUP_NAME = "pickupName";
const String DELIVERY_NAME = "deliveryName";
const String DELIVERY_PHONE = "deliveryPhone";
const String DELIVERY_EMAIL = "deliveryEmail";
const String DELIVERY_DESCRIPTION = "deliveryDescription";

const String CONTACT_NO = "contactNo";
const String LANDMARK = "landMark";
const String STREET = "street";
const String ADDRESS = "address";
const String ORDER_ID = "orderID";
const String DISTANCE = "distance";
const String FARE = "fare";

const String DATE = "date";
const String BASE_FARE = "baseFare";
const String CHARGE_PER_KM = "chargePerKM";
const String CHARGE_PER_TIME = "chargePerTime";

const int PENDING = 0;
const int APPROVED = 1;
const int PAUSED = 2;
const int STOPPED = 3;
const int DECLINED = 4;

const int STATUS_UNDONE = 0;
const int STATUS_COMPLETED = 1;
const int STATUS_FAILED = 2;

const String WHATSAPP_NUMBER = "whatsappNumber";
const String WHATSAPP_PREF = "whatsappPref";
const String PHONE_NUMBER = "phoneNumber";
const String PHONE_PREF = "phonePref";
const String IMPRESSIONS = "impressions";
const String CONTACT_BASE = "contactBase";
const String CONTACT_CATEGORY = "contactCategory";
const String CONTACT_CATEGORIES = "contactCategories";

const String ONE_SIGNAL_KEY = "oneSignalKey";
const String ONE_SIGNAL_API_KEY = "oneSignalApiKey";
const String ONE_SIGNAL_ID = "oneSignalId";

const int NOTIFY_COMMENT = 0;
const int NOTIFY_MENTIONS = 1;
const int NOTIFY_LIKES = 2;
const int NOTIFY_REPLY = 3;
const int NOTIFY_COMMENT_LIKE = 4;
const int NOTIFY_REPLY_LIKE = 5;
const int NOTIFY_FOLLOW = 6;
const int NOTIFY_ADMIN = 7;

const String APP_LINK_IOS = "appLinkAndroid";
const String APP_LINK_ANDROID = "appLinkIOS";
const String IS_IMPORTANT = "isImportant";

const String SMS_API_KEY = "smsApiKey";
const String SMS_API_SECRET = "smsApiSecret";

const String TWILIO_ACCOUNT_SID = "twilioAcctSID";
const String TWILIO_AUTH_TOKEN = "twilioAuthToken";
const String TWILIO_NUMBER = "twilioNumber";
const String TWILIO_SERVICE_SID = "twilioServiceSID";

const String CANDIDATE_ID = "candidateId";

// TWILIO_ACCOUNT_SID
// TWILIO_AUTH_TOKEN
// TWILIO_SERVICE_SID
