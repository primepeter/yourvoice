part of 'app.dart';

class NotificationService {
  static final Client client = Client();

  static const String serverKey =
      "AAAATsdvSnQ:APA91bH12oIUg5UzR337a23rJZgRgXeX5UIiRHT95eC3OMTgcuMOzsEFH_pQA4OmaxabeLZg7R38ITyIG-OlxEhBzng3ALwNIZsUnavyL5p3r4O9D_q1dMFSEK259IHbc0etToZh2rUW";

  static sendPush({
    String topic,
    String token,
    int liveTimeInSeconds = (Duration.secondsPerDay * 7),
    String title,
    String body,
    String image,
    Map data,
    String tag,
  }) async {
    String fcmToken = topic != null ? '/topics/$topic' : token;
    data = data ?? Map();
    data['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
    data['id'] = '1';
    data['status'] = 'done';
    client.post(
      'https://fcm.googleapis.com/fcm/send',
      body: json.encode({
        'notification': {
          'body': body,
          'title': title,
          'image': image,
          'icon': "ic_notify",
          'color': "#00000000",
          // 'color': "#ffffff",
          'tag': tag
        },
        'data': data,
        'to': fcmToken,
        'time_to_live': liveTimeInSeconds
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverKey',
      },
    );
  }

  static pushToUsers({
    @required String title,
    @required int type,
    @required String message,
    @required List parties,
    @required BaseModel theModel,
  }) {
    if (parties.isEmpty) return;

    //if (type == NOTIFY_MENTIONS)
    for (Map items in parties) {
      BaseModel user = BaseModel(items: items);
      if (user.myItem()) continue;
      sendPush(
        title: title,
        image: userModel.userImage,
        body: message,
        token: user.pushToken,
        tag: '${userModel.getUserId()}${user.getUserId()}',
      );
    }


    String id = "${userModel.getUserId()}${theModel.getObjectId()}";

    print("pushId $id");

    final base = BaseModel()
      ..put(OBJECT_ID, id)
      ..put(TITLE, title)
      ..put(TYPE, type)
      ..put(MESSAGE, message)
      ..put(THE_MODEL, theModel.items)
      ..put(
          PARTIES,
          parties[0] is Map
              ? parties.map((e) => BaseModel(items: e).getObjectId()).toList()
              : parties);
    base.saveItem(NOTIFY_BASE, true, document: id);
  }
}
