part of 'app.dart';

/*class messageDialog extends StatelessWidget {
  var icon;
  Color iconColor;
  String title;
  String message;
  String yesText;
  String noText;
  BuildContext context;
  bool cancellable;
  bool isIcon;
  double iconPadding;

  messageDialog(
    icon,
    iconColor,
    title,
    message,
    yesText, {
    noText,
    bool cancellable = false,
    bool isIcon = false,
    double iconPadding = 12.0,
  }) {
    this.iconColor = iconColor;
    this.icon = icon;
    this.title = title;
    this.message = message;
    this.yesText = yesText;
    this.noText = noText;
    this.cancellable = cancellable;
    this.isIcon = isIcon;
    this.iconPadding = iconPadding;
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return WillPopScope(
      onWillPop: () {
        if (cancellable) Navigator.pop(context);
      },
      child: Stack(fit: StackFit.expand, children: <Widget>[
        GestureDetector(
          onTap: () {
            if (cancellable) Navigator.pop(context);
          },
          child: Container(
            color: black.withOpacity(.8),
          ),
        ),
        page()
      ]),
    );
  }

  page() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(25, 45, 25, 25),
        child: new Container(
          decoration: BoxDecoration(
              color: white, borderRadius: BorderRadius.circular(10)),
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    new Container(
                      width: double.infinity,
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          *//* if (isIcon)
                            Icon(
                              icon,
                              color: iconColor,
                            )
                          else*//*
                          Image.asset(
                            AppConfig.appIcon,
                            height: 15,
                            width: 15,
                          ),
                          addSpaceWidth(10),
                          Flexible(
                            flex: 1,
                            child: new Text(
                              AppConfig.appName,
                              style:
                                  textStyle(false, 11, black.withOpacity(.5)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    if (isIcon)
                      Center(
                        child: Container(
                            width: 35,
                            height: 35,
                            decoration: BoxDecoration(
                              color: iconColor,
                              shape: BoxShape.circle,
                            ),
                            child: Icon(
                              icon,
                              color: white,
                              size: 15,
                            )),
                      )
                    else
                      Center(
                        child: Container(
                            width: 35,
                            height: 35,
                            padding: EdgeInsets.all(iconPadding),
                            decoration: BoxDecoration(
                              color: iconColor,
                              shape: BoxShape.circle,
                            ),
                            child: Image.asset(
                              icon,
                              color: white,
                              height: 15,
                              width: 15,
                            )),
                      ),
                  ],
                ),
                //addSpace(5),

                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height / 2),
                  child: new Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        addSpace(10),
                        Text(
                          title,
                          style: textStyle(true, 15, iconColor),
                          textAlign: TextAlign.center,
                        ),
                        addSpace(5),
                        Text(
                          message,
                          style: textStyle(false, 12, black.withOpacity(.5)),
                          textAlign: TextAlign.center,
                        ),
                        addSpace(15),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Flexible(
                              flex: 1,
                              fit: FlexFit.tight,
                              child: FlatButton(
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25)),
                                  color: AppConfig.appColor,
                                  onPressed: () {
                                    Navigator.pop(context, true);
                                  },
                                  child: Text(
                                    yesText,
                                    style: textStyle(true, 14, white),
                                  )),
                            )
                          ],
                        ),
                        noText == null
                            ? new Container()
                            : Container(
                                width: double.infinity,
                                child: FlatButton(
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    *//*shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              color: blue3,*//*
                                    onPressed: () {
                                      Navigator.pop(context, false);
                                    },
                                    child: Text(
                                      noText,
                                      style: textStyle(false, 14, red0),
                                    )),
                              ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}*/


class messageDialog extends StatefulWidget {
  var icon;
  Color iconColor;
  String title;
  String message;
  String yesText;
  String noText;
  bool cancellable;
  double iconPadding;

  messageDialog(
      this.icon, this.iconColor, this.title, this.message, this.yesText,
      {this.noText,
        bool this.cancellable = false,
        double this.iconPadding = 0});
  @override
  _messageDialogState createState() => _messageDialogState();
}

class _messageDialogState extends State<messageDialog> {
  var icon;
  Color iconColor;
  String title;
  String message;
  String yesText;
  String noText;
  bool cancellable;
  double iconPadding;
  bool showBack=false;
  bool hideUI=true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    icon = widget.icon;
    iconColor = widget.iconColor;
    title = widget.title;
    message = widget.message;
    yesText = widget.yesText;
    noText = widget.noText;
    cancellable = widget.cancellable;
    iconPadding = widget.iconPadding;

    Future.delayed(Duration(milliseconds: 200),(){
      hideUI=false;
      setState(() {});
    });
    Future.delayed(Duration(milliseconds: 500),(){
      showBack=true;
      setState(() {

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (cancellable)closePage((){ Navigator.pop(context);});
      },
      child: Stack(fit: StackFit.expand, children: <Widget>[
        GestureDetector(
          onTap: () {
            if (cancellable) closePage((){ Navigator.pop(context);});
          },
          child: AnimatedOpacity(
            opacity: showBack?1:0,duration: Duration(milliseconds: 300),
            child: ClipRect(
                child:BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                    child: Container(
                      color: black.withOpacity(.7),
                    ))
            ),
          ),
        ),
        page()
      ]),
    );
  }

  page() {
    return OrientationBuilder(
      builder: (c,o){
        return AnimatedOpacity(
          opacity: hideUI?0:1,duration: Duration(milliseconds: 400),
          child: Center(
            child: Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              constraints: BoxConstraints(
                  maxWidth: getScreenWidth(context)>500?500
                      :double.infinity
              ),
              child: new Card(
                clipBehavior: Clip.antiAlias,
                color: white,elevation: 5,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
//              crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Padding(
                      padding: const EdgeInsets.fromLTRB(20,20,20,10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                              width: 60,
                              height: 60,
                              padding: EdgeInsets.all(
                                  iconPadding == null ? 0 : iconPadding),
                              decoration: BoxDecoration(
                                border: Border.all(color: iconColor,width: 2),
                                shape: BoxShape.circle,
                              ),
                              child: (icon is String)
                                  ? (Image.asset(
                                icon,
                                color: iconColor,
                                width: 35,
                                height: 35,
                              ))
                                  : Icon(
                                icon,
                                color: iconColor,
                                size: 35,
                              )),
                          addSpace(10),
                          Text(
                            title,
                            style: textStyle(true, 20, black),
                            textAlign: TextAlign.center,
                          ),
                          if(message.isNotEmpty)addSpace(5),
                          if(message.isNotEmpty)Flexible(
                            child: SingleChildScrollView(
                              child: Text(
                                "$message",
                                style: textStyle(false, 16, black.withOpacity(.5)),
                                textAlign: TextAlign.center,
//                                    maxLines: 1,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),


                    Padding(
                      padding: const EdgeInsets.fromLTRB(10,0,10,10),
                      child: Row(mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Flexible(
                            child: Container(
//                              height: 50,
                              child: FlatButton(
                                  materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25)),
                                  color: appColor,
                                  onPressed: () {
                                    closePage(() async {
//                                    await Future.delayed(Duration(milliseconds: 1000));
                                      Navigator.pop(context,true);});
                                  },
                                  child: Text(
                                    yesText,maxLines: 1,
                                    style: textStyle(true, 18, white),
                                  )),
                            ),
                          ),
                          if(noText!=null)addSpaceWidth(10),
                          if(noText!=null)Flexible(child: Container(
//                          width: double.infinity,
//                              height: 50,
//                              margin: EdgeInsets.only(top: 5),
                            child: FlatButton(
                                materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25),
                                    side: BorderSide(color: appColor,width: 2)),
//                                    color: blue3,
                                onPressed: () {
                                  closePage((){ Navigator.pop(context,false);});
                                },
                                child: Text(
                                  noText,maxLines: 1,
                                  style: textStyle(true, 18, appColor),
                                )),
                          )),
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  closePage(onClosed){
    showBack=false;
    setState(() {

    });
    Future.delayed(Duration(milliseconds: 100),(){
      Future.delayed(Duration(milliseconds: 100),(){
        hideUI=true;
        setState(() {});
      });
      onClosed();
    });
  }
}