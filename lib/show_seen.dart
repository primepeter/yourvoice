import 'package:acclaim/app/app.dart';
import 'package:acclaim/show_profile.dart';
import 'package:flutter/material.dart';

import 'app_config.dart';

class ShowStatusSeen extends StatefulWidget {
  final List<BaseModel> seenBy;

  const ShowStatusSeen({Key key, this.seenBy}) : super(key: key);
  @override
  _ShowStatusSeenState createState() => _ShowStatusSeenState();
}

class _ShowStatusSeenState extends State<ShowStatusSeen> {
  List<BaseModel> seenBy = following;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      seenBy = widget.seenBy;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key('seenBy'),
      onDismissed: (d) => Navigator.pop(context, ""),
      direction: DismissDirection.vertical,
      child: Scaffold(
        backgroundColor: white,
        appBar: AppBar(
          title: Text("Viewed by ${seenBy.length}"),
        ),
        body: searchBody(),
      ),
    );
  }

  searchBody() {
    if (!hasSetup) return loadingLayout();
    return ListView.builder(
        itemCount: seenBy.length,
        padding: 0.padAll(),
        itemBuilder: (ctx, p) {
          BaseModel thisUser = seenBy[p];
          bool following = userModel.amFollowing.contains(thisUser.getUserId());

          return InkWell(
            onTap: () {
              //print("hello");
              pushAndResult(
                  context,
                  ShowProfile(
                    thisUser: thisUser,
                  ));
            },
            child: Container(
              padding: 8.padAll(),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: black.withOpacity(.02)))),
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: Row(
                      children: <Widget>[
                        imageHolder(50, thisUser.userImage),
                        10.spaceWidth(),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              thisUser.fullName,
                              style: textStyle(true, 14, black),
                            ),
                            5.spaceHeight(),
                            Text(
                              '@${thisUser.nickName}',
                              style:
                                  textStyle(false, 12, black.withOpacity(.7)),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  5.spaceWidth(),
                  FlatButton(
                    onPressed: () {
                      followThisUser(thisUser, following, onComplete: () {
                        setState(() {});
                      });
                    },
                    color: AppConfig.appColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                        side: BorderSide(color: AppConfig.appColor)),
                    child: Center(
                        child: Text(
                      following ? "Unfollow" : "Follow",
                      style: textStyle(true, 12, white),
                    )),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
