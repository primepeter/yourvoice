import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:acclaim/app_config.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashed_circle/dashed_circle.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:synchronized/synchronized.dart';
import 'package:translator/translator.dart';

import 'AddPost.dart';
import 'CameraMain.dart';
import 'CreateProfile.dart';
import 'VideoController.dart';
import 'app/app.dart';
import 'countryStates/nigeria.dart';
import 'main_screens/Bookmark.dart';
import 'main_screens/Contacts.dart';
import 'main_screens/OurChoice.dart';
import 'main_screens/home.dart';
import 'main_screens/notifications.dart';
import 'preview_post_images.dart';
import 'show_profile.dart';

final noteController = StreamController<BaseModel>.broadcast();
final suggestionsController = StreamController<BaseModel>.broadcast();
final uploadingController = StreamController<String>.broadcast();
final progressController = StreamController<bool>.broadcast();
final flareController = StreamController<BaseModel>.broadcast();
final stateController = StreamController<bool>.broadcast();
final scrollTopController = StreamController<int>.broadcast();
final functionController = StreamController<Function>.broadcast();
final chatMessageController = StreamController<bool>.broadcast();
final mainController = StreamController<dynamic>.broadcast();
final selfieUploadController = StreamController<String>.broadcast();

Map chatSetupInfo = Map();

String uploadingText;
bool flareShowing = false;
final FlareControls flareControls = FlareControls();

List<BaseModel> random100Users = [];
List recentSearches = [];
bool recentLoaded = false;
bool random100Loaded = false;

List newUnreadMessageIds = [];
List lastSeenMessages = [];
bool showNewNotifyDot = false;
String visibleChatId = "";
List<String> upOrDown = List();
List<String> noFileFound = List();
List<String> fileThatExists = List();
List<BaseModel> allVideos = List();
List<BaseModel> bookmarkList = [];

class AppIndex extends StatefulWidget {
  @override
  _AppIndexState createState() => _AppIndexState();
}

createConstituencies() {
  for (int p = 0; p < NIGERIA_STATES.length; p++) {
    final states = NIGERIA_STATES[p];
    BaseModel model = BaseModel();
    String country = 'nigeria';
    String state = states['state']['name'];
    List locals = states['state']['locals'];
    String id = country + '.' + state.replaceAll(" ", "");
    model.put(COUNTRY, country);
    model.put(STATE, state);
    model.put(STATE_LOCALS, locals);
    model.put(OBJECT_ID, id);
    model.saveItem(COUNTRY_BASE, false, document: id, merged: true);
    print(locals);
  }
}

class _AppIndexState extends State<AppIndex>
    with
        SingleTickerProviderStateMixin,
        WidgetsBindingObserver,
        AutomaticKeepAliveClientMixin,
        TickerProviderStateMixin {
  final drawerKey = GlobalKey<InnerDrawerState>();
  bool drawerOpened = false;
  AnimationController animationController;

  int currentVisibleTab = 0;
  PageController pageController = PageController();

  //bool isPlaying=false;
  //List subs = [];
  void _toggle() {
    drawerKey.currentState.toggle(direction: InnerDrawerDirection.start);
    drawerOpened = !drawerOpened;
    drawerOpened
        ? animationController.forward()
        : animationController.reverse();
    setState(() {});
  }

  var scaffoldKey = GlobalKey<ScaffoldState>();
  final valueListenable = ValueNotifier<int>(0);
  bool isPosting = false;

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    for (var sub in subs) sub.cancel();
    super.dispose();
  }

  double _progress = 0.0;
  int _videoDuration = 0;
  bool _canceled = false;

  int peopleCurrentPage = 0;
  bool tipHandled = false;
  bool tipShown = true;

  bool posting = false;
  bool hasPosted = false;

  @override
  initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    WidgetsBinding.instance.addObserver(this);
    Future.delayed(Duration(seconds: 1), () {
      createUserListener();
    });

    var suggestionSub = suggestionsController.stream.listen((model) async {
      String id = model.getObjectId();
      model.saveItem(SUGGESTION_BASE, true, document: id);
      List<BaseModel> images = model.getListModel(IMAGES);
      List<BaseModel> completeUploads = [];
      if (images.isNotEmpty)
        saveImages(images, completeUploads, (_) {
          model.put(IMAGES, _.map((e) => e.items).toList());
          model.saveItem(SUGGESTION_BASE, true, document: id, merged: true);
        });
    });

    var noteSub = noteController.stream.listen((model) async {
      String id = model.getObjectId();
      model.saveItem(NOTE_BASE, true, document: id);
      List<BaseModel> images = model.getListModel(IMAGES);
      List<BaseModel> completeUploads = [];
      if (images.isNotEmpty)
        saveImages(images, completeUploads, (_) {
          model.put(IMAGES, _.map((e) => e.items).toList());
          model.saveItem(NOTE_BASE, true, document: id, merged: true);
        });
    });

    var selfieSub = selfieUploadController.stream.listen((path) {});

    var storySub = storyController.stream.listen((model) async {
      pushAndResult(context, CameraMain(), result: (List<BaseModel> _) {
        if (_ == null || _.isEmpty) return;
        pushAndResult(
            context,
            PreviewPostImages(
              _,
              [],
              edittable: true,
              storyMode: true,
              indexOf: 0,
              heroTag: getRandomId(),
            ), result: (List<BaseModel> _) {
          if (_ == null || _.isEmpty) return;
          saveStories(_);
        });
      });
    });

    var postSub = postUploadController.stream.listen((BaseModel model) {
      String id = model.getObjectId();
      model.saveItem(POST_BASE, true, document: id);
      List<BaseModel> images = model.getListModel(IMAGES);
      List<BaseModel> completeUploads = [];
      if (images.isNotEmpty)
        saveImages(images, completeUploads, (_) {
          model.put(IMAGES, _.map((e) => e.items).toList());
          model.saveItem(POST_BASE, true, document: id, merged: true);
          // NotificationService.pushToUsers(
          //     title: userModel.nickName,
          //     type: NOTIFY_MENTIONS,
          //     message: "Mentioned you on a post",
          //     parties: model.getList(TAGGED_PERSONS),
          //     theModel: model);

          String pushKey = appSettingsModel.getString(ONE_SIGNAL_KEY);
          var notification = OSCreateNotification(
              playerIds: [],
              content: model.getString(TITLE),
              heading: "New Post Shared",
              additionalData: model.items);
          var notifyJson = notification.mapRepresentation();
          notifyJson['app_id'] = pushKey;
          notifyJson['included_segments'] = ["Subscribed Users"];
          sendOneSignal(notifyJson);

          int p =
              postList.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
          if (p == -1)
            postList.add(model.items);
          else
            postList[p] = model.items;
          postList.sort((p1, p2) => p2[TIME].compareTo(p1[TIME]));
          stateController.add(true);
          setState(() {});
        });
    });

    var postingSub = postingController.stream.listen((bool p) {
      isPosting = p;
      if (!p) _progress = 0.0;
      if (mounted) setState(() {});
      print("HEY POSTING");
    });

    var progressSub = progressController.stream.listen((show) {
      setState(() {
        posting = show;
      });
    });

    var uploadingSub = uploadingController.stream.listen((text) {
      setState(() {
        uploadingText = text;
      });
    });

    var functionSub = functionController.stream.listen((fn) {
      fn();
    });

    subs.add(selfieSub);
    subs.add(storySub);
    subs.add(suggestionSub);
    subs.add(noteSub);
    subs.add(progressSub);
    subs.add(uploadingSub);
    subs.add(postSub);
    subs.add(postingSub);
    subs.add(functionSub);
  }

  sendOneSignal(Map<String, dynamic> data) async {
    http
        .post('https://onesignal.com/api/v1/notifications',
            headers: {
              "Content-Type": "application/json; charset=utf-8",
              "Authorization":
                  "Basic ${appSettingsModel.getString(ONE_SIGNAL_API_KEY)}"
            },
            body: jsonEncode(data))
        .then((value) {
      print(value.body);
    });
  }

  saveSelfie(String path) {
    uploadingController.add("Uploading Selfie");
    uploadFile(File(path), (res, error) {
      if (error != null) {
        saveSelfie(path);
        return;
      }
      userModel.put(SELFIE_IMAGE_URL, res);
      userModel.put(SELFIE_IMAGE_PATH, '');
      if (mounted) setState(() {});
      uploadingController.add("Uploading Successful");
      Future.delayed(Duration(seconds: 1), () {
        uploadingController.add(null);
      });
    });
  }

  saveStories(List models, {bool thumb = false}) async {
    if (models.isEmpty) {
      uploadingController.add("Uploading Successful");
      Future.delayed(Duration(seconds: 1), () {
        uploadingController.add(null);
      });
      return;
    }
    uploadingController.add("Uploading Story");

    BaseModel model = models[0];
    String id = model.getObjectId();
    bool isVideo = model.isVideo;
    String thumbPath = model.getString(THUMBNAIL_PATH);
    String imagePath = model.getString(IMAGE_PATH);
    List parties = [userModel.getUserId()];
    parties.addAll(userModel.myFollowers);
    model.put(PARTIES, parties);

    if (isVideo && thumbPath.isEmpty && imagePath.isEmpty) {
      models.removeAt(0);
      saveStories(models);
      return;
    }

    if (imagePath.isEmpty) {
      models.removeAt(0);
      saveStories(models);
      return;
    }

    String image = model.getString(IMAGE_PATH);
    uploadFile(File(image), (res, error) {
      if (error != null) {
        saveStories(models, thumb: thumb);
        return;
      }

      model.put(IMAGE_PATH, "");
      model.put(IMAGE_URL, res);

      if (isVideo) {
        File thumbFile = File(thumbPath);
        uploadFile(thumbFile, (thumbRes, error) {
          if (error != null) {
            saveStories(models, thumb: thumb);
            return;
          }
          model.put(THUMBNAIL_PATH, "");
          model.put(THUMBNAIL_URL, thumbRes);
          model.saveItem(STORY_BASE, true, document: id, merged: true);
          models.removeAt(0);
          saveStories(models);
        });
        return;
      }

      model.saveItem(STORY_BASE, true, document: id, merged: true);
      models.removeAt(0);
      saveStories(models);
    });
  }

  saveImages(List<BaseModel> models, List<BaseModel> modelsUploaded,
      onCompleted(List<BaseModel> _)) async {
    if (models.isEmpty) {
      uploadingController.add("Uploading Successful");
      Future.delayed(Duration(seconds: 1), () {
        uploadingController.add(null);
        progressController.add(false);
        onCompleted(modelsUploaded);
      });
      return;
    }
    /*Future.delayed(Duration(seconds: 1), () {
      uploadingController.add(null);
    });*/

    BaseModel model = models[0];
    bool isVideo = model.isVideo;
    String thumbPath = model.getString(THUMBNAIL_PATH);
    String imagePath = model.getString(IMAGE_PATH);

    if (isVideo && thumbPath.isEmpty && imagePath.isEmpty) {
      modelsUploaded.add(model);
      models.removeAt(0);
      saveImages(models, modelsUploaded, onCompleted);
      return;
    }

    if (imagePath.isEmpty) {
      modelsUploaded.add(model);
      models.removeAt(0);
      saveImages(models, modelsUploaded, onCompleted);
      return;
    }

    uploadingController.add("Uploading Post");

    File file = File(imagePath);
    uploadFile(file, (res, error) {
      if (error != null) {
        saveImages(models, modelsUploaded, onCompleted);
        return;
      }

      model.put(IMAGE_PATH, "");
      model.put(IMAGE_URL, res);

      if (isVideo) {
        File thumbFile = File(thumbPath);
        uploadFile(thumbFile, (thumbRes, error) {
          if (error != null) {
            saveImages(models, modelsUploaded, onCompleted);
            return;
          }
          model.put(THUMBNAIL_PATH, "");
          model.put(THUMBNAIL_URL, thumbRes);
          modelsUploaded.add(model);
          models.removeAt(0);
          saveImages(models, modelsUploaded, onCompleted);
        });
        return;
      }

      modelsUploaded.add(model);
      models.removeAt(0);
      saveImages(models, modelsUploaded, onCompleted);
    });
  }

  bool selfieEngaged = false;

  void createUserListener() async {
    User user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      var userSub = FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(user.uid)
          .snapshots()
          .listen((shot) async {
        if (shot != null) {
          isLoggedIn = true;
          userModel = BaseModel(doc: shot);
          isAdmin = userModel.getBoolean(IS_ADMIN) ||
              userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
              userModel.getString(EMAIL) == "ammaugost@gmail.com";
          if (mounted) setState(() {});

          loadBlocked();
          String selfiePath = userModel.getString(SELFIE_IMAGE_PATH);
          if (!selfieEngaged && selfiePath.isNotEmpty) {
            selfieEngaged = true;
            if (mounted) setState(() {});
            selfieUploadController.add(selfiePath);
          }

          if (!settingsLoaded) {
            settingsLoaded = true;
            loadSettings();
          }
        }
      });
      subs.add(userSub);
    }
  }

  loadBlocked() async {
    var lock = Lock();
    lock.synchronized(() async {
      QuerySnapshot shots = await Firestore.instance
          .collection(USER_BASE)
          .where(BLOCKED, arrayContains: userModel.getObjectId())
          .getDocuments();

      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        String uId = model.getObjectId();
        String deviceId = model.getString(DEVICE_ID);
        if (!blockedIds.contains(uId)) blockedIds.add(uId);
        if (deviceId.isNotEmpty) if (!blockedIds.contains(deviceId))
          blockedIds.add(deviceId);
      }
    }, timeout: Duration(seconds: 10));
  }

  chkUpdate() async {
    // int version = appSettingsModel.getInt(VERSION_CODE);
    // PackageInfo pack = await PackageInfo.fromPlatform();
    // String v = pack.buildNumber;
    // int myVersion = int.parse(v);
    // if (myVersion < version) {
    //   pushAndResult(context, UpdateLayout(), opaque: false);
    // }
  }

  loadSettings() async {
    var settingsSub = FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        // List banned = appSettingsModel.getList(BANNED);
        // if (banned.contains(userModel.getObjectId()) ||
        //     banned.contains(userModel.getString(DEVICE_ID)) ||
        //     banned.contains(userModel.getEmail())) {
        //   io.exit(0);
        // }

        String genMessage = appSettingsModel.getString(GEN_MESSAGE);
        int genMessageTime = appSettingsModel.getInt(GEN_MESSAGE_TIME);

        if (userModel.getInt(GEN_MESSAGE_TIME) != genMessageTime &&
            genMessageTime > userModel.getTime()) {
          userModel.put(GEN_MESSAGE_TIME, genMessageTime);
          userModel.updateItems();

          String title = !genMessage.contains("+")
              ? "Announcement!"
              : genMessage.split("+")[0].trim();
          String message = !genMessage.contains("+")
              ? genMessage
              : genMessage.split("+")[1].trim();
          showMessage(context, Icons.info, blue0, title, message);
        }

        if (mounted) {
          loadFollowers();
          loadFollowing();
        }

        if (hasSetup) return;
        hasSetup = true;
        listenToUsers();
        setUpPushOneSignal();
        loadPosts();
        loadLibrary();
        loadRecentSearch();
        onResume();
        if (mounted) setState(() {});
      }
    });
    subs.add(settingsSub);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    if (state == AppLifecycleState.paused) {
      onPause();
    }
    if (state == AppLifecycleState.resumed) {
      onResume();
    }
    super.didChangeAppLifecycleState(state);
  }

  void onPause() {
    if (userModel == null) return;
    int prevTimeOnline = userModel.getInt(TIME_ONLINE);
    int timeActive = (DateTime.now().millisecondsSinceEpoch) - timeOnline;
    int newTimeOnline = timeActive + prevTimeOnline;
    userModel.put(IS_ONLINE, false);
    userModel.put(TIME_ONLINE, newTimeOnline);
    userModel.updateItems();
    timeOnline = 0;
  }

  void onResume() async {
    timeOnline = DateTime.now().millisecondsSinceEpoch;
    if (userModel == null) return;
    userModel.put(IS_ONLINE, true);
    userModel.put(
        PLATFORM,
        Platform.isAndroid
            ? ANDROID
            : Platform.isIOS
                ? IOS
                : WEB);
    //String deviceId = await PlatformDeviceId.getDeviceId;
    //userModel.put(DEVICE_ID,deviceId);
    userModel.updateItems();

//    Future.delayed(Duration(seconds: 2), () {
//      checkLaunch();
//    });
  }

  loadNotificationsSettings() async {
    notificationsPlugin = FlutterLocalNotificationsPlugin();
    var androidSettings = AndroidInitializationSettings('ic_launcher');
    var iosSettings = IOSInitializationSettings(
        requestSoundPermission: false,
        requestBadgePermission: false,
        requestAlertPermission: false,
        onDidReceiveLocalNotification:
            (int id, String title, String body, String payload) async {
          print(payload);
        });

    var initializationSettings =
        InitializationSettings(androidSettings, iosSettings);
    await notificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {});

    if (Platform.isIOS) {
      var result = await notificationsPlugin
          .resolvePlatformSpecificImplementation<
              IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );
      print("Permission result $result");
    }
  }

  String _debugLabelString = "";
  String _emailAddress;
  String _externalUserId;
  bool _enableConsentButton = false;

  // CHANGE THIS parameter to true if you want to test GDPR privacy consent
  bool _requireConsent = true;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> setUpPushOneSignal() async {
    if (!mounted) return;

    // if (Platform.isAndroid) return;

    OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) {
      print("Accepted permission: $accepted");
    });

    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);

    var settings = {
      OSiOSSettings.autoPrompt: false,
      OSiOSSettings.promptBeforeOpeningPushUrl: true
    };

    OneSignal.shared
        .setNotificationReceivedHandler((OSNotification notification) {
      this.setState(() {
        _debugLabelString =
            "Received notification: \n${notification.jsonRepresentation().replaceAll("\\n", "\n")}";
      });
    });

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      BaseModel model =
          BaseModel(items: result.notification.payload.additionalData);
      String db = model.getString(DATABASE_NAME);
      bool toProfile = db == USER_BASE;
      _debugLabelString =
          "Opened notification: \n${result.notification.jsonRepresentation().replaceAll("\\n", "\n")}";

      this.setState(() {});

      // if (!toProfile) return;
      // pushAndResult(
      //     context,
      //     ShowProfile(
      //       model: model,
      //     ));
    });

    // NOTE: Replace with your own app ID from https://www.onesignal.com
    await OneSignal.shared.init(appSettingsModel.getString(ONE_SIGNAL_KEY),
        iOSSettings: settings);

    OneSignal.shared
        .setInAppMessageClickedHandler((OSInAppMessageAction action) {
      this.setState(() {
        _debugLabelString =
            "In App Message Clicked: \n${action.jsonRepresentation().replaceAll("\\n", "\n")}";
      });
    });

    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges changes) {
      print("EMAIL SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.getPermissionSubscriptionState().then((status) {
      String playerId = status.subscriptionStatus.userId;
      print('playerId $playerId');
      userModel
        ..put(ONE_SIGNAL_ID, playerId)
        ..updateItems();
    });

    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    bool requiresConsent = await OneSignal.shared.requiresUserPrivacyConsent();

    String playerId = (await OneSignal.shared.getPermissionSubscriptionState())
        .subscriptionStatus
        .userId;
    print('playerId $playerId');
    userModel
      ..put(ONE_SIGNAL_ID, playerId)
      ..updateItems();

    String adminId = appSettingsModel.getString(ONE_SIGNAL_ID);

    OneSignal.shared.consentGranted(true);
    OneSignal.shared.setSubscription(true);
    OneSignal.shared.setLocationShared(true);
    OneSignal.shared.setExternalUserId(adminId).then((value) {
      print('setExternalUserId $value adminId $adminId');
    });
  }

  loadFCMSettings() async {
    fbMessaging.configure(
      onMessage: notificationOnMessage,
      onLaunch: notificationOnLaunch,
      onResume: notificationOnResume,
    );
    fbMessaging.setAutoInitEnabled(true);
    fbMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    fbMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print(settings);
    });

    if (userModel.isAdminItem()) {
      fbMessaging.subscribeToTopic('admin');
    }

    fbMessaging.subscribeToTopic('all');
    fbMessaging.subscribeToTopic(userModel.getObjectId());
    List myTopics = List.from(userModel.getList(TOPICS));

    if (userModel.isAdminItem() && !myTopics.contains('admin')) {
      myTopics.add('admin');
    }
    if (!myTopics.contains('all')) myTopics.add('all');
    if (!myTopics.contains(userModel.getObjectId()))
      myTopics.add(userModel.getObjectId());

    userModel.put(TOPICS, myTopics);
    userModel.updateItems();

    fbMessaging.getToken().then((String token) async {
      if (null == token) return;
      userModel
        ..put(PUSH_TOKEN, token)
        ..updateItems();
    });

    fbMessaging.onTokenRefresh.listen((token) async {
      if (null == token) return;
      userModel
        ..put(PUSH_TOKEN, token)
        ..updateItems();
    });
  }

  Future notificationOnMessage(Map<String, dynamic> message) async {
    BaseModel model = BaseModel(items: message);
    await showNotification(Platform.isAndroid ? model.getModel("data") : model);
    //loadNotifyType(Platform.isAndroid ? model.getModel("data") : model);
  }

  Future notificationOnResume(Map<String, dynamic> message) async {
    BaseModel model = BaseModel(items: message);
    await showNotification(Platform.isAndroid ? model.getModel("data") : model);
    //loadNotifyType(Platform.isAndroid ? model.getModel("data") : model);
  }

  Future notificationOnLaunch(Map<String, dynamic> message) async {
    BaseModel model = BaseModel(items: message);
    await showNotification(Platform.isAndroid ? model.getModel("data") : model);
    //loadNotifyType(Platform.isAndroid ? model.getModel("data") : model);
  }

  showNotification(BaseModel model) async {
    String title = model.getString("title");
    String message = model.getString("message");
    String notifiedId = model.getString("notifiedId");

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'arab.maugost.nt', 'Maugost', 'your channel description',
        importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await notificationsPlugin.show(0, title, message, platformChannelSpecifics,
        payload: 'item x');

    print("Shown...");
  }

  loadNotifyType(BaseModel model) {
    int type = model.getType();
    String database;
    if (type == NOTIFY_TYPE_POST) database = POST_BASE;
    if (type == NOTIFY_TYPE_COMMENT) database = COMMENT_BASE;
    if (type == NOTIFY_TYPE_STORY) database = STORY_BASE;
    if (type == NOTIFY_TYPE_FOLLOW) database = USER_BASE;

    String notifiedId = model.notifiedId;

    if (notifiedId.isEmpty) return;

    Firestore.instance
        .collection(database)
        .document(notifiedId)
        .get()
        .then((doc) {
      BaseModel notifyThis = BaseModel(doc: doc);

      if (type == NOTIFY_TYPE_FOLLOW) {
        pushAndResult(
            context,
            ShowProfile(
              thisUser: notifyThis,
            ));
        return;
      }

      /*if (type == NOTIFY_TYPE_POST ||
          type == NOTIFY_TYPE_COMMENT ||
          type == NOTIFY_TYPE_REPLY) {
        pushAndResult(
            context,
            ShowComments(
              thisPost: notifyThis,
            ));
        return;
      }*/
    });
  }

  checkInterests() {
    Future.delayed(Duration(seconds: 2), () {
      bool fullyRegistered = userModel.getBoolean(FULLY_REGISTERED);
      if (!fullyRegistered) {
        pushAndResult(context, CreateProfile());
        return;
      }
      // if (userModel.getBoolean(INTEREST_SHOWN)) return;
      // pushAndResult(context, ShowInterests());
    });
  }

  listenToUsers() async {
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(IS_ONLINE, isEqualTo: true)
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = usersOnline
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          usersOnline[p] = model;
        } else {
          usersOnline.add(model);
        }
      }
      if (mounted) setState(() {});
    });

    FirebaseFirestore.instance.collection(USER_BASE).get().then((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = usersTotal
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          usersTotal[p] = model;
        } else {
          usersTotal.add(model);
        }
      }
      if (mounted) setState(() {});
    });
  }

  loadNotes() async {
    final lockSync = Lock();
    await lockSync.synchronized(() async {
      Firestore.instance
          .collection(NOTE_BASE)
          .where(USER_ID, isEqualTo: userModel.getUserId())
          .snapshots()
          .listen((value) {
        for (var doc in value.documentChanges) {
          BaseModel model = BaseModel(doc: doc.document);

          if (doc.type == DocumentChangeType.removed) {
            myNotes.removeWhere((e) => e.getObjectId() == model.getObjectId());
            continue;
          }
          int p =
              myNotes.indexWhere((e) => e.getObjectId() == model.getObjectId());
          if (p != -1) {
            myNotes[p] = model;
          } else {
            myNotes.add(model);
          }
        }

        myNotes.sort((p1, p2) => p2.time.compareTo(p1.time));
        readyNotes = true;
        if (mounted) setState(() {});
      });
    });
  }

  loadNotifications(bool isNew) async {
    final lockSync = Lock();
    await lockSync.synchronized(() async {
      FirebaseFirestore.instance
          .collection(NOTIFY_BASE)
          .where(PARTIES, arrayContainsAny: [userModel.getObjectId(), "all"])
          .limit(25)
          .orderBy(TIME, descending: !isNew)
          .snapshots()
          .listen((event) {
            for (var doc in event.docChanges) {
              BaseModel model = BaseModel(doc: doc.doc);

              if (doc.type == DocumentChangeType.removed) {
                myNotifications
                    .removeWhere((e) => e.getObjectId() == model.getObjectId());
                continue;
              }

              int p = myNotifications
                  .indexWhere((e) => e.getObjectId() == model.getObjectId());
              if (p != -1) {
                myNotifications[p] = model;
              } else {
                myNotifications.add(model);
              }

              if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                  model.parties.contains(userModel.getUserId())) {
                showNewNotifyDot = true;
                setState(() {});
              }
            }
            myNotifications.sort((p1, p2) => p2.time.compareTo(p1.time));
            setup = true;
            if (mounted) setState(() {});
          });
    });
  }

  loadPosts() async {
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection(POST_BASE)
        .limit(25)
        //.orderBy(TIME, descending: true)
        .get();

    for (var doc in query.docs) {
      BaseModel model = BaseModel(doc: doc);
      if (userModel.mutedItems.contains(model.getObjectId())) continue;

      if (model.myItem() && model.itChanged()) {
        model
          ..addMyDetails()
          ..updateItems(delaySeconds: 2);
      }
      int p = postList.indexWhere((e) => e[OBJECT_ID] == model.getObjectId());
      if (p != -1)
        postList[p] = model.items;
      else
        postList.add(model.items);
    }
    postList.sort((p1, p2) => p2[TIME].compareTo(p1[TIME]));

    postReady = true;
    stateController.add(true);
    if (mounted) setState(() {});
  }

  translateToLocale(BaseModel model) async {
    final translator = new GoogleTranslator();
    final input = model.getString(MESSAGE);
    if (input.isEmpty) return;
    if (model.myItem()) return;
    translator.translate(input, to: appLocale.languageCode).then((s) {
      print("input $input translated $s");
      model.put(TRANSLATION, s);
      bool theSame = input == s;
      if (!theSame) otherPosts.addModel(model);
      if (mounted) setState(() {});
    });
  }

  loadFollowers() {
    setState(() {
      followers.clear();
    });
    final ref = Firestore.instance.collection(USER_BASE);
    for (String ids in userModel.myFollowers) {
      if (ids.isEmpty) {
        userModel
          ..putInList(MY_FOLLOWER_IDS, "", false)
          ..updateItems();
        continue;
      }
      ref.document(ids).get().then((value) {
        BaseModel model = BaseModel(doc: value);

        int p =
            followers.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          followers[p] = model;
        } else {
          followers.add(model);
        }

        setState(() {});
      });
    }
  }

  loadFollowing() {
    setState(() {
      following.clear();
    });
    final ref = Firestore.instance.collection(USER_BASE);
    for (String ids in userModel.amFollowing) {
      if (ids.isEmpty) {
        userModel
          ..putInList(AM_FOLLOWING_IDS, "", false)
          ..updateItems();
        continue;
      }
      ref.document(ids).get().then((value) {
        BaseModel model = BaseModel(doc: value);
        int p =
            following.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1)
          following[p] = model;
        else
          following.add(model);
        setState(() {});
      });
    }
  }

  loadStories() async {
    final arrayContains = userModel.myFollowers;
    arrayContains.add(userModel.getUserId());

    var storySub = FirebaseFirestore.instance
        .collection(STORY_BASE)
        .where(PARTIES, arrayContainsAny: arrayContains)
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .snapshots()
        .listen((value) {
      for (var doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        int p = allStories
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          allStories[p] = model;
        } else {
          allStories.add(model);
        }
        if (model.isVideo)
          VideoController.loadVideo(model, () {
            if (mounted) setState(() {});
          });
      }
      allStories.sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
      storiesReady = true;
    });
    subs.add(storySub);
  }

  loadRecentSearch() async {
    List recent = userModel.getList(RECENT_SEARCH);
    for (var item in recent) {
      String id = item[OBJECT_ID];
      String database = item[DATABASE_NAME];
      final doc =
          await Firestore.instance.collection(database).document(id).get();
      if (!doc.exists) continue;
      int p = recentSearches
          .indexWhere((e) => e[OBJECT_ID] == doc.data()[OBJECT_ID]);
      if (p == -1)
        recentSearches.add(doc.data());
      else
        recentSearches[p] = doc.data();
    }
    setState(() {});
  }

  btmTabs() {
    int count = myNotifications
        .where((e) => !e.getList(READ_BY).contains(userModel.getUserId()))
        .toList()
        .length;
    count = count.clamp(0, 9);
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        height: 60,
        padding: EdgeInsets.only(top: 8, bottom: 8),
        margin: EdgeInsets.all(15),
        decoration: BoxDecoration(
            color: Color(0XFFF4F4F4), borderRadius: BorderRadius.circular(50)),
        child: Stack(
          children: [
            /*BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: Container(
                  color: black.withOpacity(.6),
                  width: double.infinity,
                  height: 80,
                )),*/
            Row(
              children: [
                btmTabItem("assets/images/ic_home", 0),
                btmTabItem("assets/images/ic_archive", 1),
                if (isAdmin)
                  Flexible(
                    child: GestureDetector(
                      onTap: () {
                        // createConstituencies();
                        // return;
                        pushAndResult(context, AddPost(),
                            transitionBuilder: fadeTransition);
                      },
                      onLongPress: () {
                        showAdmin(context);
                      },
                      onDoubleTap: () {},
                      child: Container(
                        height: 55,
                        width: 55,
                        decoration: BoxDecoration(
                          color: white,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 30.0,
                              color: black.withOpacity(0.2),
                            )
                          ],
                          shape: BoxShape.circle,
                          //border: Border.all(color: black.withOpacity(.5), width: 5)
                        ),
                        child: Center(
                          child: Icon(
                            Icons.add,
                            color: black,
                          ),
                        ),
                      ),
                    ),
                    fit: FlexFit.tight,
                  ),
                btmTabItem(
                  "assets/images/ic_help",
                  2,
                ),
                btmTabItem("assets/images/ic_notification", 3,
                    showDot: count > 0),
              ],
            ),
          ],
        ),
      ),
    );
  }

  btmTabItem(icon, int p, {bool showDot = false, bool profile = false}) {
    bool active = p == currentVisibleTab;

    return Flexible(
        fit: FlexFit.tight,
        child: GestureDetector(
          onDoubleTap: () {
            print("hhhmmm $p");
            scrollTopController.add(p);
          },
          onTap: () {
            pageController.jumpToPage(p);
          },
          child: Container(
            width: screenW(context) / 5,
            height: 40,
            color: transparent,
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    if (profile)
                      Container(
                        height: 35,
                        width: 35,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: black.withOpacity(active ? 1 : 0.1),
                                width: 2)),
                        padding: EdgeInsets.all(1),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: CachedNetworkImage(
                            imageUrl: icon,
                            height: 35,
                            width: 35,
                            fit: BoxFit.cover,
                            placeholder: (c, x) {
                              return Container(
                                height: 35,
                                width: 35,
                                child: Icon(
                                  Icons.person,
                                  size: 18,
                                  color: black.withOpacity(active ? 1 : (.1)),
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    else if ((icon is String) && (!icon.contains("http")))
                      Image.asset(
                        '$icon.png',
                        //"$icon${active ? "_active" : ""}.png",
                        height: 24,
                        color: active ? null : black.withOpacity(0.5),
                        fit: BoxFit.cover,
                      ),
                    if (showDot)
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          height: 10,
                          width: 10,
                          margin: EdgeInsets.only(right: 20),
                          alignment: Alignment.center,
                          /* child: Text(
                            "$showDot+",
                            style: textStyle(true, 8, white),
                          ),*/
                          decoration: BoxDecoration(
                              border: Border.all(color: white),
                              color: Colors.red,
                              shape: BoxShape.circle),
                        ),
                      ),
                  ],
                ),
                addSpace(5),
                if (active)
                  Container(
                    height: 5,
                    width: 30,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border:
                          Border.all(width: 1.5, color: black.withOpacity(1)),
                      color: white,
                    ),
                  )
              ],
            ),
          ),
        ));
  }

  btmTabItemX(icon, int p, {int count = 0, bool profile = false}) {
    bool active = p == currentVisibleTab;

    return Flexible(
        fit: FlexFit.tight,
        child: Center(
            child: GestureDetector(
          onDoubleTap: () {
            print("hhhmmm $p");
            scrollTopController.add(p);
          },
          onTap: () {
            pageController.jumpToPage(p);
          },
          child: Container(
            width: 80,
            height: 40,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                    color: active ? black : white.withOpacity(.4),
                    width: active ? 2 : 1),
                color: transparent),
            alignment: Alignment.center,
            child: Container(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  if (profile)
                    ClipRRect(
                      borderRadius: BorderRadius.circular(25),
                      child: Container(
                        child: CachedNetworkImage(
                          imageUrl: icon,
                          height: 35,
                          width: 35,
                          fit: BoxFit.cover,
                          placeholder: (c, x) {
                            return Container(
                              height: 35,
                              width: 35,
                              child: Icon(
                                Icons.person,
                                size: 18,
                                color: white.withOpacity(active ? 1 : (.5)),
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  else if ((icon is String) && (!icon.contains("http")))
                    Image.asset(
                      icon,
                      width: 18,
                      height: 18,
                      color: white.withOpacity(active ? 1 : (.5)),
                    )
                  else
                    Icon(icon,
                        size: 18,
                        color: white.withOpacity(
                          active ? 1 : (.5),
                        )),
                  if (count > 0)
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        height: 20,
                        width: 20,
                        margin: EdgeInsets.only(right: 10),
                        alignment: Alignment.center,
                        child: Text(
                          "${count > 9 ? "$count+" : count}",
                          style: textStyle(false, 11, white),
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(color: white),
                            color: Colors.red,
                            shape: BoxShape.circle),
                      ),
                    )
                ],
              ),
            ),
          ),
        )));
  }

  photoView() {
    final myStories = allStories.where((e) => e.myItem()).toList();
    bool noStories = myStories.isEmpty;

    return Container(
      height: 130,
      margin: 35.padAt(t: 0, b: 5),
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: () {},
        child: Container(
          height: 120,
          width: 120,
          child: Stack(
            alignment: Alignment.bottomRight,
            children: <Widget>[
              DashedCircle(
                strokeWidth: 5,
                color: black,
                dashes: myStories.length,
                gapSize: 5,
                child: Container(
                  padding: 5.padAll(),
                  child: imageHolder(
                      120,
                      userModel.userImage
                      /* noStories
                          ? userModel.userImage.urlPath
                          : (myStories[0].photo.isVideo
                              ? myStories[0].photo.thumbnailPath
                              : myStories[0].photo.urlPath)*/
                      ,
                      strokeColor: black.withOpacity(noStories ? 0.2 : 1),
                      iconHolderSize: 40,
                      stroke: 1, onImageTap: () async {
                    storyController.add(true);
                  }),
                ),
              ),
              /*if (isPostingStory)
                IgnorePointer(ignoring: true,
                  child: Align(
                    alignment: Alignment.center,
                    child: SizedBox(
                      height: 125,
                      width: 125,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(white),
                        strokeWidth: 2,
                      ),
                    ),
                  ),
                ),*/
              Container(
                height: 35,
                width: 35,
                padding: 6.padAll(),
                margin: 6.padAll(),
                child: Image.asset(
                  "assets/images/camera.png",
                  color: white,
                ),
                decoration: BoxDecoration(
                    color: black.withOpacity(.8),
                    border: Border.all(color: white, width: 1),
                    shape: BoxShape.circle),
              ),
            ],
          ),
        ),
      ),
    );
  }

  menuItemView(
      {String title,
      String subTitle,
      onTap,
      bool colorIt = false,
      bool active = false}) {
    return FlatButton(
      onPressed: onTap,
      padding: EdgeInsets.all(10),
      child: Container(
          alignment: Alignment.centerLeft,
          padding: (null == subTitle ? 5 : 3).padAll(),
          //height: 80,
          decoration: BoxDecoration(color: black.withOpacity(active ? 0.2 : 0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(title, style: textStyle(false, 18, white)),
              if (null != subTitle) ...[
                2.spaceHeight(),
                Text(
                  subTitle,
                  style: textStyle(false, 14, (white).withOpacity(.7)),
                ),
              ]
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    ScreenUtil.init(context,
        height: context.screenHeight(), width: context.screenWidth());

    /*  return Scaffold(
      key: scaffoldKey,
      backgroundColor: white,
      body: homeBody(),
    );*/

    return InnerDrawer(
        key: drawerKey,
        onTapClose: true,
        swipe: true,
        //colorTransition: Color.red, // default Color.black54
        innerDrawerCallback: (a) {
          print(a);
          drawerOpened = a;
          drawerOpened
              ? animationController.forward()
              : animationController.reverse();
          setState(() {});
        },
        proportionalChildArea: true,
        borderRadius: 25,
        scale: IDOffset.horizontal(.7),
        leftAnimationType: InnerDrawerAnimation.static,
        rightAnimationType: InnerDrawerAnimation.quadratic,
        onDragUpdate: (double val, InnerDrawerDirection direction) {
          //print(direction);
        },
        leftChild: menuDrawer(),
        backgroundDecoration: BoxDecoration(
          color: appColor,
        ),
        scaffold: Scaffold(
          key: scaffoldKey,
          backgroundColor: white,
          body: homeBody(),
        ));
  }

  menuDrawer() {
    // Timestamp t = userModel.get(CREATED_AT);
    // String joinedAt = '';
    // if (t != null) {
    //   DateTime d = t.toDate();
    //   final date = DateTime.parse(d.toString());
    //
    //   joinedAt = new DateFormat(
    //     "MMMM d,yyyy",
    //   ).format(date);
    // }

    return Material(
      color: Colors.transparent,
      child: Container(
        color: appColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              child: Column(
                children: <Widget>[
                  30.spaceHeight(),
                  Container(
                      alignment: Alignment.centerLeft,
                      padding: 10.padAll(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(timeOfDayText,
                              style: textStyle(true, 16, white)),
                          // 5.spaceHeight(),
                          // Text('Registered Since',
                          //     style: textStyle(false, 12, white)),
                          // 5.spaceHeight(),
                          // if (joinedAt.isNotEmpty)
                          //   Text(joinedAt, style: textStyle(false, 12, white)),
                        ],
                      )),
                  Container(
                    alignment: Alignment.center,
                    height: 100,
                    child: Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                          color: black.withOpacity(.3),
                          shape: BoxShape.circle,
                          border: Border.all(color: white, width: 1)),
                      child: Image.asset(
                        'assets/images/ic_launcher.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  menuItemView(
                      title: '@${userModel.getString(USERNAME)}',
                      subTitle: "${userModel.getEmail()}",
                      onTap: () {
                        print("ok");
                      }),
                  Flexible(
                    child: ListView(
                      shrinkWrap: true,
                      padding: 0.padAll(),
                      children: [
                        menuItemView(
                            title: "Home",
                            active: currentVisibleTab == 0,
                            onTap: () {
                              _toggle();
                              pageController.jumpToPage(0);
                            }),
                        menuItemView(
                            title: "OurChoice",
                            active: currentVisibleTab == 1,
                            onTap: () {
                              _toggle();
                              pageController.jumpToPage(1);
                            }),
                        menuItemView(
                            title: "Help Lines",
                            active: currentVisibleTab == 2,
                            onTap: () {
                              _toggle();
                              pageController.jumpToPage(2);
                            }),
                        menuItemView(
                            title: "Notifications",
                            active: currentVisibleTab == 3,
                            onTap: () {
                              _toggle();
                              pageController.jumpToPage(3);
                            }),
                        menuItemView(
                            title: "Bookmarks",
                            active: currentVisibleTab == 4,
                            onTap: () {
                              _toggle();
                              pageController.jumpToPage(4);
                              //openLink(appSettingsModel.getString(PRIVACY_POLICY));
                            }),
                        menuItemView(
                            title: "About Us",
                            //active: currentVisibleTab == 3,
                            onTap: () {
                              _toggle();
                              openLink(appSettingsModel.getString(ABOUT_US));
                            }),
                        menuItemView(
                            title: "Privacy Policy",
                            //active: currentVisibleTab == 3,
                            onTap: () {
                              _toggle();
                              openLink(
                                  appSettingsModel.getString(PRIVACY_POLICY));
                            }),
                        menuItemView(
                            title: "Terms & Conditions",
                            //active: currentVisibleTab == 3,
                            onTap: () {
                              _toggle();
                              openLink(appSettingsModel
                                  .getString(TERMS_AND_CONDITIONS));
                            }),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            menuItemView(
                colorIt: true,
                title: 'Logout',
                subTitle: "Version 1.0",
                onTap: () {
                  clickLogout(context);
                }),
            20.spaceHeight(),
          ],
        ),
      ),
    );
  }

  postingIndicator() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (posting)
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            height: 2,
            color: AppConfig.appColor_dark,
            child: LinearProgressIndicator(
              value: _progress == 0 ? null : _progress,
              backgroundColor: AppConfig.appColor,
              valueColor: AlwaysStoppedAnimation(blue0),
            ),
          ),
        AnimatedContainer(
          height: uploadingText == null ? 0 : 60,
          duration: Duration(milliseconds: 500),
          color: dark_green0,
          padding: EdgeInsets.only(top: 30),
          child: uploadingText == null
              ? Container()
              : Row(
                  //mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.check, size: 18, color: white),
                    addSpaceWidth(10),
                    Text(
                      uploadingText,
                      style: textStyle(true, 12, white),
                    )
                  ],
                ),
        )
      ],
    );
  }

  homeBody() {
    return Container(
      child: Stack(
        children: [
          Column(
            children: <Widget>[
              postingIndicator(),
              Container(
                padding: EdgeInsets.only(
                    top: uploadingText == null ? 40 : 10,
                    right: 10,
                    left: 10,
                    bottom: 10),
                child: Row(
                  children: [
                    FlatButton(
                      onPressed: () {
                        _toggle();
                      },
                      height: 18,
                      minWidth: 18,
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(5),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      child: AnimatedIcon(
                        icon: AnimatedIcons.menu_arrow,
                        progress: animationController,
                        size: 30,
                      ),
                    ),
                    addSpaceWidth(5),
                    Text(
                      currentVisibleTab == 0
                          ? "OurVoice"
                          : currentVisibleTab == 1
                              ? "OurChoice"
                              : currentVisibleTab == 2
                                  ? "Help"
                                  : currentVisibleTab == 3
                                      ? "Notification"
                                      : "Bookmarks",
                      style: textStyle(true, 25, black),
                    ),
                    Spacer(),
                    addSpaceWidth(5),
                    if (currentVisibleTab != 3)
                      FlatButton(
                        onPressed: () {
                          //pushAndResult(context, Search());
                          pageController.jumpToPage(3);
                        },
                        height: 18,
                        minWidth: 18,
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(5),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Icon(
                          Icons.notifications_none,
                          size: 25,
                          color: black.withOpacity(.7),
                        ),
                      ),
                    // addSpaceWidth(5),
                    // FlatButton(
                    //   onPressed: () {
                    //     pushAndResult(context, Search());
                    //   },
                    //   height: 18,
                    //   minWidth: 18,
                    //   shape: CircleBorder(),
                    //   padding: EdgeInsets.all(5),
                    //   materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    //   child: Image.asset(
                    //     ic_search,
                    //     height: 18,
                    //     width: 18,
                    //     fit: BoxFit.cover,
                    //     color: black.withOpacity(.7),
                    //   ),
                    // ),
                  ],
                ),
              ),
              Expanded(
                  child: PageView(
                controller: pageController,
                physics: NeverScrollableScrollPhysics(),
                onPageChanged: (p) {
                  setState(() {
                    currentVisibleTab = p;
                  });
                },
                children: [
                  Home(),
                  OurChoice(),
                  Contacts(),
                  Notifications(),
                  Bookmarks()
                ],
              )),
            ],
          ),
          btmTabs()
        ],
      ),
    );
  }

  List loadedIds = [];
  loadMessages() async {
    var lock = Lock();
    await lock.synchronized(() async {
//      List<Map> myChats = List.from(userModel.getList(MY_CHATS));
      var sub = Firestore.instance
          .collection(AppDataBase.chat_ids_base)
          .where(PARTIES, arrayContains: userModel.getObjectId())
          .snapshots()
          .listen((shots) {
        for (DocumentSnapshot doc in shots.documents) {
          BaseModel chatIdModel = BaseModel(doc: doc);
          String chatId = chatIdModel.getObjectId();
          if (userModel.getList(DELETED_CHATS).contains(chatId)) continue;
          if (loadedIds.contains(chatId)) {
            continue;
          }
          loadedIds.add(chatId);

          var sub = Firestore.instance
              .collection(CHAT_BASE)
              .where(PARTIES, arrayContains: userModel.getUserId())
              .where(CHAT_ID, isEqualTo: chatId)
              .orderBy(TIME, descending: true)
              .limit(1)
              .snapshots()
              .listen((shots) async {
            if (shots.documents.isNotEmpty) {
              BaseModel cModel = BaseModel(doc: (shots.documents[0]));
              /*if(isBlocked(null,userId: getOtherPersonId(cModel))){
                lastMessages.removeWhere((bm)=>bm.getString(CHAT_ID)==cModel.getString(CHAT_ID));
                chatMessageController.add(true);
                return;
              }*/
            }
            if (stopListening.contains(chatId)) return;
            for (DocumentSnapshot doc in shots.documents) {
              BaseModel model = BaseModel(doc: doc);
              String chatId = model.getString(CHAT_ID);
              int index = lastMessages.indexWhere(
                  (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
              if (index == -1) {
                lastMessages.add(model);
              } else {
                lastMessages[index] = model;
              }

              if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                  !model.myItem() &&
                  visibleChatId != model.getString(CHAT_ID)) {
                try {
                  if (!showNewMessageDot.contains(chatId))
                    showNewMessageDot.add(chatId);
                  setState(() {});
                } catch (E) {
                  if (!showNewMessageDot.contains(chatId))
                    showNewMessageDot.add(chatId);
                  setState(() {});
                }
                countUnread(chatId);
              }
            }

            String otherPersonId =
                getOtherPersonId(chatIdModel.getList(PARTIES));
            loadOtherPerson(otherPersonId);

            try {
              lastMessages
                  .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
            } catch (E) {}
          });

          subs.add(sub);
        }
        chatSetup = true;
        if (mounted) setState(() {});
      });
      subs.add(sub);
    });
  }

  setupMessages() async {
    var sub = Firestore.instance
        .collection(CHAT_IDS_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chatIdModel = BaseModel(doc: doc);
        String chatId = chatIdModel.getObjectId();
        chatSetupInfo[chatId] = chatIdModel;
        if (chatIdModel.getList(BLOCKED).contains(userModel.getObjectId()))
          continue;
        if (loadedIds.contains(chatId)) {
          continue;
        }
        loadedIds.add(chatId);

        var sub = Firestore.instance
            .collection(CHAT_BASE)
            .where(CHAT_ID, isEqualTo: chatId)
            .orderBy(TIME, descending: true)
            .limit(1)
            .snapshots()
            .listen((shots) async {
          if (shots.documents.isNotEmpty) {
            BaseModel cModel = BaseModel(doc: (shots.documents[0]));

            /*if(isBlocked(null,userId: getOtherPersonId(cModel))){
              lastMessages.removeWhere((bm)=>bm.getString(CHAT_ID)==cModel.getString(CHAT_ID));
              chatMessageController.add(true);
              return;
            }*/
          }
          if (stopListening.contains(chatId)) return;
          for (DocumentSnapshot doc in shots.documents) {
            BaseModel model = BaseModel(doc: doc);
            String chatId = model.getString(CHAT_ID);
            List deletedList = userModel.getList(DELETED_CHATS);
            int deletedIndex =
                deletedList.indexWhere((element) => element[CHAT_ID] == chatId);
            if (deletedIndex != -1) {
              Map deletedItem = deletedList[deletedIndex];
              int time = deletedItem[TIME];
              if (time > model.getTime()) continue;
            }
            if (model.getUserId().isEmpty) continue;

            int index = lastMessages.indexWhere(
                (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
            if (index == -1) {
              lastMessages.add(model);
            } else {
              lastMessages[index] = model;
            }

            if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                !model.myItem() &&
                visibleChatId != model.getString(CHAT_ID)) {
              if (!newUnreadMessageIds.contains(chatId))
                newUnreadMessageIds.add(chatId);
              setState(() {});
              unreadCounter.remove(chatId);
              countUnread(chatId);
//              chatMessageController.add(true);
              print("New Unread $visibleChatId");
            }
          }

          String otherPersonId = getTheOtherId(chatIdModel);
          if (otherPeronInfo[otherPersonId] == null)
            loadOtherPerson(otherPersonId);

          lastMessages
              .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
          chatMessageController.add(true);
        });
        subs.add(sub);
      }
//      print("Chat Loaded");
      chatSetup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  }

  loadOtherPerson(String uId, {bool fromCache = true}) async {
    print("Loading User $uId fromCache: $fromCache");
    DocumentSnapshot doc = await Firestore.instance
        .collection(USER_BASE)
        .document(uId)
        .get(GetOptions(
            source: fromCache ? Source.cache : Source.serverAndCache))
        .catchError((e) {
      if (e.toString().contains("CACHE")) {
        loadOtherPerson(uId, fromCache: false);
      }
    });
    if ((doc == null || !doc.exists)) {
      print("User $uId not found");
      return;
    }

    BaseModel user = BaseModel(doc: doc);
    otherPeronInfo[uId] = user;
    if (fromCache) loadOtherPerson(uId, fromCache: false);
    if (mounted) setState(() {});
  }

  countUnread(String chatId) async {
    var lock = Lock();
    lock.synchronized(() async {
      int count = 0;
      QuerySnapshot shots = await Firestore.instance
          .collection(CHAT_BASE)
          .where(CHAT_ID, isEqualTo: chatId)
          .limitToLast(20)
          .getDocuments();

      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (!model.getList(READ_BY).contains(userModel.getObjectId())) count++;
      }
      unreadCounter[chatId] = count;
      chatMessageController.add(true);
    });
  }

  loadLibrary() async {
    var lock = Lock();
    await lock.synchronized(() async {
      Firestore.instance.collection(LIBRARY_BASE).getDocuments().then((value) {
        for (var doc in value.documents) {
          BaseModel model = BaseModel(doc: doc);
          int p = libraryList.indexWhere(
              (element) => element.getObjectId() == model.getObjectId());
          if (p != -1)
            libraryList[p] = model;
          else
            libraryList.add(model);
        }
        librarySetup = true;
        if (mounted) setState(() {});
      });
    });
  }

  @override
  bool get wantKeepAlive => true;
}
