import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:screen/screen.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:video_player/video_player.dart';

import 'app/app.dart';

import 'app_config.dart';
import 'show_profile.dart';
import 'stories/story_video.dart';

class ShowStories extends StatefulWidget {
  final List<BaseModel> stories;
  final bool myStory;
  final bool isLastStory;
  final int defPosition;
  //final List<String> storyKeys;
  //final String defKey;
  //final Map<String, List<BaseModel>> foldedStories;
  const ShowStories(
      {Key key,
      this.stories,
      this.myStory = false,
      this.defPosition = 0,
      //this.storyKeys,
      //this.defKey,
      //this.foldedStories,
      this.isLastStory = false})
      : super(key: key);
  @override
  _ShowStoriesState createState() => _ShowStoriesState();
}

class _ShowStoriesState extends State<ShowStories>
    with TickerProviderStateMixin {
  List<BaseModel> stories = [];
  final currentStory = ValueNotifier<int>(0);
  List<BaseModel> seenBy = [];
  bool showViews = true;
  List<double> progressValue = List();
  final pc = PageController();
  double currentPage = 0;
  int currentPageInt = 0;

  // Timer timer;
  bool tappingDown = false;
  int defPosition;

  BaseModel personModel;
  final panelController = PanelController();
  VideoPlayerController controller;
  bool storyIsVideo = false;
  bool isPlaying = false;
  bool isLastStory;
  AnimationController animController;
  Animation<double> animation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //loadAnimation();
    Screen.keepOn(true);
    stories = widget.stories;
    isLastStory = widget.isLastStory;
    defPosition = widget.defPosition;
    stories = widget.stories;
    if (widget.myStory || isAdmin) loadSeenBy();
    for (int i = 0; i < stories.length; i++) progressValue.add(0);
    pc.addListener(() {
      if (mounted)
        setState(() {
          currentPage = pc.page;
          storyIsVideo = stories[currentPageInt].photo.isVideo;
        });
    });

    Future.delayed(Duration(milliseconds: 500), () {
      pc.jumpToPage(defPosition);
      createTimer(defPosition);
      handleSeen(0);
    });
    loadPerson();
    loadSeenBy();
  }

  @override
  void dispose() {
    Screen.keepOn(false);
    //animController?.stop();
    // panelController?.close();
    controller?.dispose();
    animController?.dispose();
    super.dispose();
  }

  loadPerson() async {
    if (mounted) {
      setState(() {
        personModel = BaseModel()
          ..put(FULL_NAME, stories[0].fullName)
          ..put(NICKNAME, stories[0].nickName)
          ..put(USER_IMAGE, stories[0].userImage)
          ..put(USER_ID, stories[0].userId);
      });
    }
    String userId = stories[0].userId;
    DocumentSnapshot doc =
        await Firestore.instance.collection(USER_BASE).document(userId).get();
    personModel = BaseModel(doc: doc);
    if (mounted) setState(() {});
  }

  loadSeenBy() async {
    final seenIds = [];

    for (int i = 0; i < stories.length; i++) {
      BaseModel bm = stories[i];
      final seenBy = bm.seenBy;
      if (seenBy.isEmpty) continue;
      for (var theId in seenBy) {
        if (seenIds.contains(theId)) continue;
        if (theId == userModel.userId && bm.myItem()) {
          if (mounted)
            setState(() {
              stories[i]
                ..putInList(SEEN_BY, theId, false)
                ..updateItems();
            });
          continue;
        }
        seenIds.add(theId);
      }
    }

    for (var theId in seenIds) {
      Firestore.instance
          .collection(USER_BASE)
          .document(theId)
          .get()
          .then((doc) {
        BaseModel model = BaseModel(doc: doc);
        print(model.fullName);
        this.seenBy.addModel(model);
        if (mounted) setState(() {});
      });
    }
  }

  handleChange(int index) {
    isPlaying = false;
    controller = null;
    setState(() {});
    if (index < currentPageInt) {
      progressValue[index] = 0;
      for (int i = currentPageInt; i > index; i--) {
        progressValue[i] = 0;
      }
    } else if (index > currentPageInt) {
      progressValue[index] = 0;
      for (int i = currentPageInt; i < index; i++) {
        progressValue[i] = 100;
      }
    }
  }

  handleSeen(int p) {
    setState(() {});
    BaseModel model = stories[p];
    if (stories[p].myItem()) return;
    List shown = model.seenBy;
    if (shown.contains(userModel.getObjectId())) return;
    model.putInList(SEEN_BY, userModel.getObjectId(), true);
    model.updateItems(updateTime: false);
    setState(() {});
  }

  createTimer(int p, {bool allow = false}) {
    BaseModel model = stories[p];
    BaseModel media = model.photo;
    bool isVideo = media.isVideo;
    if (isVideo && !isPlaying) return;

    animController =
        AnimationController(duration: const Duration(seconds: 20), vsync: this);
    animation = Tween(begin: 0.0, end: 1.0).animate(animController)
      ..addListener(() async {
        bool completed = animation.status == AnimationStatus.completed;
        progressValue[p] = animation.value;
        if (completed) {
          animController?.stop();
          p++;
          if (p < stories.length) {
            await pc.animateToPage(p,
                duration: Duration(milliseconds: 500), curve: Curves.ease);
            //progressValue[p] = 1;
            animController?.forward();
          } else {
            animController?.stop();
            Navigator.pop(context, "${isLastStory ? "" : "next"}");
          }
        }
        if (mounted) setState(() {});
      });
    animController.forward();
  }

  changePage(bool next) {
    int position = currentPageInt;
    if (next) position++;
    if (!next) position--;

    if (position < stories.length && next) {
      pc.animateToPage(position,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    } else if (!next && position >= 0) {
      pc.animateToPage(position,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    } else {
      // Navigator.pop(context, "${isLastStory ? "" : "next"}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      onDismissed: (d) => Navigator.pop(context, ""),
      direction: DismissDirection.vertical,
      key: Key("xx"),
      child: Scaffold(
        backgroundColor: black,
        body: SlidingUpPanel(
            controller: panelController,
            body: storyBody(),
            panelBuilder: (ScrollController sc) => storyPanel(sc),
            minHeight: 0,
            maxHeight: context.screenHeight()),
      ),
    );
  }

  storyPanel(ScrollController sc) {
    return Container(
      padding: 30.padAtTop(),
      color: black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CloseButton(
            color: white,
            onPressed: () {
              panelController.close();
              createTimer(currentPageInt);
            },
          ),
          Container(
            height: 200,
            color: black,
            child: PageView.builder(
                controller: PageController(viewportFraction: .3),
                itemCount: stories.length,
                onPageChanged: (p) {
                  pc.jumpToPage(p);
                },
                itemBuilder: (ctx, p) {
                  BaseModel model = stories[p];
                  BaseModel media = model.photo;
                  bool isVideo = media.isVideo;
                  bool active = currentPage == p;
                  String urlPath =
                      isVideo ? media.thumbnailPath : media.urlPath;
                  return GestureDetector(
                    onTap: () {
                      pc.jumpToPage(p);
                    },
                    child: Container(
                      margin: 5.padAt(t: 15, r: 5, l: 5, b: 5),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: active ? 2 : 1,
                              color: white.withOpacity(active ? 1 : .5))),
                      child: CachedNetworkImage(
                        imageUrl: urlPath,
                        height: 120,
                        width: 150,
                        fit: BoxFit.fitWidth,
                        placeholder: (c, s) {
                          return Container(
                            height: 100,
                            width: 80,
                            child: Container(
                                width: 20,
                                height: 20,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    //value: 20,
                                    valueColor:
                                        AlwaysStoppedAnimation<Color>(white),
                                    strokeWidth: 2,
                                  ),
                                )),
                          );
                        },
                      ),
                    ),
                  );
                }),
          ),
          10.spaceHeight(),
          Container(
            height: 50,
            padding: 10.padAll(),
            decoration: BoxDecoration(
                border: Border.all(width: 1, color: white.withOpacity(.2))),
            child: Row(
              children: [
                Text("Seen by ${stories[currentPageInt].seenBy.length}",
                    style: textStyle(true, 14, white))
              ],
            ),
          ),
          10.spaceHeight(),
          Flexible(
            child: ListView.builder(
                itemCount: stories[currentPageInt].seenBy.length,
                padding: 0.padAll(),
                itemBuilder: (ctx, p) {
                  String theId = stories[currentPageInt].seenBy[p];

                  int ps = seenBy.indexWhere((e) => e.userId == theId);
                  bool yels = ps != -1;

                  if (!yels) return Container();

                  BaseModel thisUser = seenBy[ps];
                  bool following =
                      userModel.amFollowing.contains(thisUser.getUserId());

                  return InkWell(
                    onTap: () {
                      //print("hello");
                      pushAndResult(
                          context,
                          ShowProfile(
                            thisUser: thisUser,
                          ));
                    },
                    child: Container(
                      padding: 8.padAll(),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom:
                                  BorderSide(color: white.withOpacity(.02)))),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: Row(
                              children: <Widget>[
                                imageHolder(50, thisUser.userImage),
                                10.spaceWidth(),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      thisUser.fullName,
                                      style: textStyle(true, 14, white),
                                    ),
                                    5.spaceHeight(),
                                    Text(
                                      '@${thisUser.nickName}',
                                      style: textStyle(
                                          false, 12, white.withOpacity(.7)),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          5.spaceWidth(),
                          FlatButton(
                            onPressed: () {
                              followThisUser(thisUser, following,
                                  onComplete: () {
                                setState(() {});
                              });
                            },
                            color: AppConfig.appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                                side: BorderSide(color: AppConfig.appColor)),
                            child: Center(
                                child: Text(
                              following ? "Unfollow" : "Follow",
                              style: textStyle(true, 12, white),
                            )),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }

  int indexedPage = 0;
  int indexPosition = 0;

  storyBody() {
    return Stack(
      children: [
        //CubePageView(children: ),

        PageView.builder(
            onPageChanged: (p) {
              handleChange(p);
              currentPageInt = p;
              animController?.stop();
              createTimer(p);
              handleSeen(p);
            },
            controller: pc,
            itemCount: widget.stories.length,
            itemBuilder: (ctx, p) {
              return page(p);
              if (p == currentPage.floor()) {
                return Transform(
                  transform: Matrix4.identity()..rotateX(currentPage - p),
                  child: page(p),
                );
              } else if (p == currentPage.floor() + 1) {
                return Transform(
                  transform: Matrix4.identity()..rotateX(currentPage - p),
                  child: page(p),
                );
              } else {
                return page(p);
              }
            }),
        // IgnorePointer(
        //   ignoring: true,
        //   child: Container(
        //     width: double.infinity,
        //     height: context.screenHeight(),
        //     child: LinearProgressIndicator(
        //       //value: (progressValue[currentPageInt] / 100),
        //       value: null == animation ? 0 : animation.value,
        //       backgroundColor: transparent,
        //       valueColor: AlwaysStoppedAnimation<Color>(
        //           AppConfig.appColor.withOpacity(.1)),
        //     ),
        //   ),
        // ),
        storyControls(),
        storyProgressIndicator(),
        storySeenBy(),
      ],
    );
  }

  storyControls() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        GestureDetector(
          onTap: () {
            changePage(false);
          },
          onHorizontalDragStart: (_) {
            changePage(false);
          },
          child: Container(
            height: double.infinity,
            width: 50,
            color: transparent,
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(),
          fit: FlexFit.tight,
        ),
        GestureDetector(
          onTap: () {
            changePage(true);
          },
          onHorizontalDragStart: (_) {
            changePage(true);
          },
          child: Container(
            height: double.infinity,
            width: 50,
            color: transparent,
          ),
        ),
      ],
    );
  }

  storySeenBy() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
            padding: 10.padAll(),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (personModel.myItem())
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (stories[currentPageInt].seenBy.isNotEmpty)
                        GestureDetector(
                          onTap: () {
                            panelController.open();
                            animController.stop(canceled: false);
                            controller?.pause();
                          },
                          child: Container(
                            width: 150,
                            alignment: Alignment.centerRight,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Stack(
                                  children: List.generate(
                                      stories[currentPageInt].seenBy.length,
                                      (p) {
                                    //BaseModel seen = seenBy[p];
                                    //String imageUrl = seen.userImage.urlPath;
                                    String theId =
                                        stories[currentPageInt].seenBy[p];

                                    int ps = seenBy
                                        .indexWhere((e) => e.userId == theId);
                                    bool yels = ps != -1;
                                    //print("$theId $yels");
                                    if (yels) {
                                      String imageUrl = seenBy[ps].userImage;
                                      double padding = p == 0 ? 0 : (30.0 * p);
                                      return Container(
                                        padding: padding.padAtRight(),
                                        child: imageHolder(45, imageUrl,
                                            stroke: 2, strokeColor: white),
                                      );
                                    }

                                    return Container(
                                      height: 0,
                                      width: 0,
                                    );
                                  }),
                                ),
                                // 10.spaceHeight(),
                                // Text("Seen by 5",
                                //     style: textStyle(true, 12, white)),
                              ],
                            ),
                          ),
                        ),
                      5.spaceHeight(),
                      Container(
                          padding: 3.padAt(l: 7, r: 7, t: 2, b: 2),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: white.withOpacity(.5)),
                          child: Row(
                            children: <Widget>[
                              Text(
                                "${stories[currentPageInt].seenBy.length}",
                                style: textStyle(true, 14, white),
                              ),
                              5.spaceWidth(),
                              Icon(
                                Icons.remove_red_eye,
                                color: white,
                              )
                            ],
                          ))
                    ],
                  ),
                Spacer(),
                IconButton(
                  onPressed: () {
                    animController.stop(canceled: false);
                    pushAndResult(
                        context,
                        listDialog(
                          [
                            if (personModel.myItem()) "Delete",
                            if (!personModel.myItem()) "Report"
                          ],
                          //usePosition: false,
                        ), result: (_) {
                      if (_ == "Delete") {
                        setState(() {
                          stories[currentPageInt].deleted;
                          stories.remove(currentPageInt);
                        });
                      } else if (_ == "Report") {}
                      createTimer(currentPageInt);
                    });
                  },
                  icon: Icon(
                    Icons.more_horiz,
                    color: white,
                  ),
                )
              ],
            )));
  }

  storyProgressIndicator() {
    String fullName = personModel.fullName;
    String nickName = personModel.nickName;
    String imageUrl = personModel.userImage;
    bool myPost = personModel.myItem();
    //String time = getTimeAgo(stories[currentPage.toInt()].time);
    String time = getTimeAgo(stories[currentPage.toInt()].getInt(TIME_UPDATED));

    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        padding: 30.padAt(t: 35, r: 10, l: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: List.generate(progressValue.length, (p) {
                return Flexible(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      height: 8,
                      padding: 2.padAll(),
                      child: LinearProgressIndicator(
                        value: progressValue[p],
                        //value: progressValue[p] /
                        //100, // (progressValue[currentPageInt] / 100),
                        backgroundColor: white.withOpacity(0.4),
                        valueColor: AlwaysStoppedAnimation<Color>(white),
                      ),
                    ),
                  ),
                );
              }),
            ),
            10.spaceHeight(),
            Row(
              children: [
                Flexible(
                  child: Row(
                    children: [
                      imageHolder(50, imageUrl, stroke: 2, strokeColor: white),
                      10.spaceWidth(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(myPost ? "Your Story" : fullName,
                              style: textStyle(true, 14, white)),
                          5.spaceHeight(),
                          Text(time, style: textStyle(false, 12, white)),
                        ],
                      )
                    ],
                  ),
                ),
                CloseButton(
                  color: white,
                  onPressed: () {
                    Navigator.pop(context, "");
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  page(int p) {
    BaseModel model = stories[p];
    BaseModel media = model.photo;
    bool isVideo = media.isVideo;
    String urlPath = media.urlPath;

    return GestureDetector(
      onTapDown: (_) {
        tappingDown = true;
        controller?.pause();
        animController?.stop(canceled: false);
      },
      onTapUp: (_) {
        tappingDown = false;
        controller?.play();
        animController?.forward();
      },
      child: Builder(
        builder: (ctx) {
          if (isVideo) {
            return StoryVideo(
              play: true,
              thisVideo: media,
              controller: (c) {
                isPlaying = c.value.isPlaying;
                if (null == controller) controller = c;
                if (mounted) setState(() {});
              },
            );
          }
          return CachedNetworkImage(
            imageUrl: urlPath,
            //fit: BoxFit.cover,
            placeholder: (c, s) {
              return Container(
                  width: 20,
                  height: 20,
                  child: Center(
                    child: CircularProgressIndicator(
                      //value: 20,
                      valueColor: AlwaysStoppedAnimation<Color>(white),
                      strokeWidth: 2,
                    ),
                  ));
            },
          );
        },
      ),
    );
  }
}
