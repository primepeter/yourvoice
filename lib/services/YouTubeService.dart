import 'package:googleapis/youtube/v3.dart' as yTube;
import 'package:googleapis_auth/auth_io.dart';
import 'package:http/http.dart' as http;

final clientCredentials = new ServiceAccountCredentials.fromJson(r'''
{
  "type": "service_account",
  "project_id": "hispresence-f40f8",
  "private_key_id": "aa799a3a52ee1d6f6491a118b81338c0603eb15b",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC7Sx6TUIMB3/zD\nltb40PdnNePJILN+ByDjUFOWnEEypZ7NmLXPt3cDWWmMTMrejj+CgQZmXo5lbqJY\nSDAc3+HovyY4bxp9wPQ2Ah1mBdh590d9/svh4wH2AYc7AfE3kErgINeNj94OMRBO\nPkiYM24t7Tac5U9P9WTmmulDspX4XWMN4v+euolAquYDGtlym6+it26aPNUpVgzR\naAh1Vyb/CI+oPsK5PV5PAAXWH7adazq9chnosnnlg0Ttt7lP56wzW3t3+G9uAyvX\nF4NAv26xgJZQU/PnJ9RpgauU6M0cZ0lfENCCY/RJ8o6J0l+Tp20yLrpqYI8YCZia\n4TbOijJlAgMBAAECggEAE8xeTEzBuIBEsClBCrbOPE0MSe3Iw4z920GZwG0+kBvF\nVG7s4yr96+728xOIC2wmhwVcF5StwUenZiUlXtwEva19HRCTkJEFz7K8CFWcW1I6\nLrWxOCzev3JMaP6s0MaSP6yBFLANVVs24cqAMoSXREgT0pSD5IwocR8Vgug9Ja6N\nNI6F+rEQbkUBffsE8DDMQcV7RRIEzW2esV2FQosS5roepoNj8SBZq2RFXXOXbW/l\ne0VuKy5EbPpdz00WMq/np+OIbJvyWMqdZaeadSQXv7eLQrF7hKWQccAvMm8xvke5\nWoM0rERxbW5NB1EbyD3VElHEotL7OaKXsxHxOFfQCQKBgQDf7Kk0Ar642iXBlau5\n8YixwJeRKSE2ORvhgaOd0EpBX2CupSUk1fUBdT0jUn6qdouUaPdMP4y0j9vTc7BA\nHC6cJ1leePmjPKL8AhZj4P7Sl1m03LH4eVW3yx7mG9L+J1pMCJ91BhMVWmgSYs8Q\nrdNSsSDa+lK2yDS78VrUqz7NfQKBgQDWHzGzi4m8c7uqb/vABQFsL1jan0wpwPCQ\nXBLrEmsemAPXMjtdltp49nnPcpFWdoN0ldWD+162Er/ZZnwm7PkPwkfGuXRwSEm0\nDG8BxApfJ83q5MOcU3rFOQwSnewEd0O2OPFpGo2KSmilm++UucbJ+BNx8WvvLmxy\n130wme4tCQKBgFu0uLcsSJ+796qIeYP1f14M2gYBKjDiwqBgT3naTA7p2Natm8Cy\nP10RfrBwoJ9/XTqyolT/0chBAm5e+tfGsvAQzXD7aoGH29gJotHU24ezpHGF0P1J\n8+kJruXej1K3Gyv4fBWZH0endufQv6y9A8bh8+q6ihWBeNdfySs/1sChAoGBAJCu\nVUwHflMrNGRO9KHj//kU6QoYV0NjTmmM/W9/a4gpmXbAR2LOuuK/UMF+nXv56otN\nbw+WO4H1m1KPsFYxC329ffhmeaog/kQ9nohn6cYwRDBy0UYBFlTLXj/6G29NEyC+\ny0iEm50U+C7m9fTdrtfr21/DjHX6PWc/+iRr7obZAoGAar6QUM3SBbx7fazxNTXj\nbsI/Lj3Duxgv8PyTNZuon4pnYa8pvTiGBwHtfnGToSCz5Pfwt0otCNaJ6krltwjH\nu8nKjQrcXuYC7H1DsLc9sJqeqBkRcdSsz0eAnoQcduhn3IxeQ6awG+xFebTgl95l\nxCx+Sg8OMzBD+X7GSfZaWms=\n-----END PRIVATE KEY-----\n",
  "client_email": "authservice@hispresence-f40f8.iam.gserviceaccount.com",
  "client_id": "103524485715015452298",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/authservice%40hispresence-f40f8.iam.gserviceaccount.com"
}
''');

class YouTubeService {
  static const String mainUrl = "https://www.googleapis.com/youtube/v3/";
  static const String apiKey = "?key=AIzaSyCz1iCO4uS65wxaEAR8JC9BfDRAerWCDg4";
  static const String liveStreamUrl = mainUrl + "liveBroadcasts" + apiKey;

  static fetchLiveStream() async {
    /* var clientId = new ClientId(
        "915342267280-3jt1a8r3ndce8c70mkufib112j4fdeio.apps.googleusercontent.com",
        "...");
    var httpClient = new http.Client();

    var credentials = await obtainAccessCredentialsViaUserConsent(
        clientId, [yTube.YoutubeApi.YoutubeReadonlyScope], httpClient, (s) {
      print("Prompt $s");
    });*/

    var clien = clientViaApiKey('<api-key-from-devconsole>');

    print("fetching result...");
    var client = await clientViaServiceAccount(clientCredentials,
        ['https://www.googleapis.com/auth/youtube.readonly']);
    print("token ${client.credentials.accessToken.data}");
    var uTube = yTube.YoutubeApi(client);

    var result = await uTube.channels.list(
      ['snippet', 'contentDetails', 'status'],
      id: ['UCEXGDNclvmg6RW0vipJYsTQ'],
    ).then((value) {
      print(value.kind);
      // print(value.items[0].toJson());
    }).catchError((e) {
      print(e);
    });

    //snippet,contentDetails,status

    return;
    http.get(liveStreamUrl);
  }
}
