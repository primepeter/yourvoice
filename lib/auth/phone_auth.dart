import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class PhoneAuth {
  static verifyNumber(BuildContext context,
      {@required String phoneNumber,
      @required Function(AuthCredential phoneAuthCredential) onVerified,
      @required Function(String id, [int force]) codeSent,
      @required Function(String verificationId) codeAutoRetrievalTimeout,
      @required Function(FirebaseAuthException error) onFailed,
      @required onError,
      Duration timeout = const Duration(seconds: 60)}) async {
    FirebaseAuth.instance
        .verifyPhoneNumber(
            phoneNumber: phoneNumber,
            timeout: timeout,
            verificationCompleted: onVerified,
            verificationFailed: onFailed,
            codeSent: codeSent,
            codeAutoRetrievalTimeout: null)
        .catchError(onError);
  }
}
