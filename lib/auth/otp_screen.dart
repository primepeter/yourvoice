import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:acclaim/LangMain.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:acclaim/auth/signup.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:nexmo_verify/nexmo_sms_verify.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class OtpScreen extends StatefulWidget {
  final String phoneNumber;
  final bool login;

  const OtpScreen({Key key, @required this.phoneNumber, this.login = false})
      : super(key: key);
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> with WidgetsBindingObserver {
  String phoneNumber = '';
  int forceResendingToken = 0;
  bool verifying = false;
  String verificationId = "";
  String timeText = "";
  final otpController = TextEditingController();
  final errorController = StreamController<ErrorAnimationType>();

  String currentOTP = '';
  NexmoSmsVerificationUtil _nexmoSmsVerificationUtil =
      NexmoSmsVerificationUtil();

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    phoneNumber = widget.phoneNumber;
    // twilioPhoneVerify.sendSmsCode(phoneNumber);
    // _nexmoSmsVerificationUtil.initNexmo(appSettingsModel.get(SMS_API_KEY),
    //     appSettingsModel.get(SMS_API_SECRET));
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {});
      clickVerify();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    errorController?.close();
    super.dispose();
  }

  int time = 0;

  /*clickVerify() {
    print(widget.phoneNumber);
    showProgress(true, context, msg: "Sending OTP");

    twilioPhoneVerify.sendSmsCode(phoneNumber).then((result) {
      if (result['message'] != 'success') {
        showProgress(false, context);
        showMessage(context, Icons.error_outline, red0,
            "Error ${result['statusCode']}", result['message'],
            delayInMilli: 1000);
        return;
      }

      createTimer(true);
    });

    return;
    _nexmoSmsVerificationUtil
        .sendOtp(widget.phoneNumber, "Your OurVoice Verification Code")
        .then((res) {
      showProgress(false, context);
      createTimer(true);
    }).catchError((e) {
      showError(e.toString(), wasLoading: true);
    });
  }*/

  clickVerify({bool hide = false}) async {
    final _auth = FirebaseAuth.instance;
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: const Duration(seconds: 60),
          verificationCompleted:
              (PhoneAuthCredential phoneAuthCredential) async {
            if (hide) showProgress(false, context);
            Future.delayed(Duration(seconds: 1), () {
              linkCredentials(phoneAuthCredential);
            });
          },
          verificationFailed: (authException) {
            if (hide) showProgress(false, context);
            Future.delayed(Duration(seconds: 1), () {
              showErrorDialog(context, authException.message);
            });
          },
          codeSent: (String id, [int forceResendingToken]) async {
            //showError("Check your phone for OTP", wasLoading: true);
            if (hide) showProgress(false, context);
            verificationId = id;
            print('verificationId $verificationId');
            createTimer(true);
            forceResendingToken = forceResendingToken;
            if (mounted) setState(() {});
          },
          codeAutoRetrievalTimeout: (String id) {
            verificationId = id;
          });
    } catch (e) {
      //showProgress(false, context);
      if (hide) showProgress(false, context);
      Future.delayed(Duration(seconds: 1), () {
        showErrorDialog(context, e);
      });
    }
  }

  checkCode() async {
    String code = otpController.text.replaceAll(" ", "");
    if (code.isEmpty) {
      showError("Enter OTP");
      errorController.add(ErrorAnimationType.shake);
      return;
    }
    showProgress(true, context, msg: "Verifying OTP");
    //_nexmoSmsVerificationUtil.verifyOtp(code)
    // twilioPhoneVerify.verifySmsCode(phoneNumber, code).then((value) {
    //   if (value['message'] != 'approved') {
    //     showProgress(false, context);
    //     showMessage(context, Icons.error_outline, red0,
    //         "Error ${value['statusCode']}", value['message'],
    //         delayInMilli: 1000);
    //     return;
    //   }
    //
    //   String authKey = widget.phoneNumber + "@ourvoice.com";
    //   FirebaseAuth.instance
    //       .signInWithEmailAndPassword(email: authKey, password: authKey)
    //       .then((result) {
    //     print(result.user.uid);
    //     FirebaseFirestore.instance
    //         .collection(USER_BASE)
    //         .doc(result.user.uid)
    //         .get()
    //         .then((doc) {
    //       if (!doc.exists && widget.login) {
    //         showProgress(false, context);
    //         Future.delayed(Duration(seconds: 1), () {
    //           showErrorDialog(
    //               context, "Sorry no record of account with this Mobile Number",
    //               onOkClicked: () {
    //             pushAndResult(context, SignUp());
    //           }, cancellable: false);
    //         });
    //         return;
    //       }
    //       if (!widget.login) {
    //         userModel
    //           ..put(USER_ID, result.user.uid)
    //           ..put(OBJECT_ID, result.user.uid)
    //           ..put(MOBILE_NUMBER, phoneNumber)
    //           ..put(IS_MAUGOST, userModel.isMaugost)
    //           ..put(IS_ADMIN, userModel.isMaugost);
    //         userModel.saveItem(USER_BASE, false,
    //             document: result.user.uid, merged: true, onComplete: () {
    //           pushAndResult(context, AppIndex(),
    //               clear: true, transitionBuilder: fadeTransition);
    //         });
    //         return;
    //       }
    //
    //       pushAndResult(context, AppIndex(),
    //           clear: true, transitionBuilder: fadeTransition);
    //     }).catchError((e) {
    //       showProgress(false, context);
    //       Future.delayed(Duration(seconds: 1), () {
    //         showErrorDialog(context, e.toString());
    //       });
    //     });
    //   }).catchError((e) {
    //     showError(e.toString(), wasLoading: true);
    //   });
    // }).catchError((e) {
    //   showError(e.toString(), wasLoading: true);
    // });
    //
    // return;

    //showProgress(true, context, msg: 'Verifying OTP-Code');
    AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: code);
    await linkCredentials(credential);
  }

  linkCredentials(AuthCredential phoneAuthCredential) async {
    showProgress(true, context, msg: "Checking Please wait...");
    FirebaseAuth.instance
        .signInWithCredential(phoneAuthCredential)
        .then((result) {
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(result.user.uid)
          .get()
          .then((doc) {
        if (!doc.exists && widget.login) {
          showProgress(false, context);
          Future.delayed(Duration(seconds: 1), () {
            showErrorDialog(
                context, "Sorry no record of account with this Mobile Number",
                onOkClicked: () {
              pushAndResult(context, SignUp());
            }, cancellable: false);
          });
          return;
        }

        if (!widget.login) {
          userModel
            ..put(USER_ID, result.user.uid)
            ..put(OBJECT_ID, result.user.uid)
            ..put(MOBILE_NUMBER, phoneNumber)
            ..put(IS_MAUGOST, userModel.isMaugost)
            ..put(IS_ADMIN, userModel.isMaugost);
          userModel.saveItem(USER_BASE, false,
              document: result.user.uid, merged: true, onComplete: () {
            pushAndResult(context, AppIndex(),
                clear: true, transitionBuilder: fadeTransition);
          });
          return;
        }

        pushAndResult(context, AppIndex(),
            clear: true, transitionBuilder: fadeTransition);
      }).catchError((e) {
        showProgress(false, context);
        Future.delayed(Duration(seconds: 1), () {
          showErrorDialog(context, e.toString());
        });
      });
    }).catchError((e) {
      showProgress(false, context);
      Future.delayed(Duration(seconds: 1), () {
        showErrorDialog(context, e.toString());
      });
    });
  }

  createTimer(bool create) {
    if (create) {
      time = 60;
    }
    if (time < 0) {
      if (mounted)
        setState(() {
          timeText = "";
        });
      return;
    }
    timeText = getTimeText();
    Future.delayed(Duration(seconds: 1), () {
      time--;
      if (mounted) setState(() {});
      createTimer(false);
    });
  }

  getTimeText() {
    if (time <= 0) return "";
    return "${time >= 60 ? "01" : "00"}:${time % 60}";
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.detached:
        FirebaseAuth.instance.signOut();
        break;
      default:
    }
    super.didChangeAppLifecycleState(state);
  }

  onError(e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0,
        Language.value(LangKey.errorReceived), e.bookmarkList,
        delayInMilli: 900, cancellable: true);
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 30, right: 10, left: 10),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BackButton(),
                    Text(
                      "",
                      style: textStyle(true, 20, black),
                    ),
                    Spacer(),
                    Container(
                      decoration: BoxDecoration(
                          color: black.withOpacity(.02),
                          borderRadius: BorderRadius.circular(4),
                          border: Border.all(color: black.withOpacity(.09))),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            addSpaceWidth(5),
                            Text(
                              countrySelected,
                              style: textStyle(true, 14, black.withOpacity(.5)),
                            ),
                            addSpaceWidth(5),
                            Image.asset(
                              countryFlag,
                              height: 25,
                              width: 30,
                              fit: BoxFit.cover,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 500),
                width: double.infinity,
                height: errorText.isEmpty ? 0 : 40,
                color: red0,
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Center(
                    child: Text(
                  errorText,
                  style: textStyle(true, 16, white),
                )),
              ),
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Phone Verification',
                        style: textStyle(true, 30, appColor),
                      ),
                      addSpace(10),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            //color: black.withOpacity(.05),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Text(
                              "Please enter the Verification Code sent to you as an SMS to the number $phoneNumber")),
                      addSpace(10),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          //color: black.withOpacity(.05),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: PinCodeTextField(
                          length: 6,
                          obscureText: false,
                          animationType: AnimationType.fade,
                          keyboardType: TextInputType.number,
                          autoDisposeControllers: true,
                          errorAnimationController: errorController,
                          cursorColor: transparent,
                          pinTheme: PinTheme(
                              shape: PinCodeFieldShape.circle,
                              borderRadius: BorderRadius.circular(25),
                              fieldHeight: 50,
                              fieldWidth: 50,
                              inactiveFillColor: black.withOpacity(.3),
                              disabledColor: black.withOpacity(.02),
                              inactiveColor: black.withOpacity(.02),
                              selectedColor: black.withOpacity(.02),
                              activeFillColor: black.withOpacity(.02),
                              activeColor: black.withOpacity(.02),
                              selectedFillColor: black.withOpacity(.5)),
                          animationDuration: Duration(milliseconds: 300),
                          backgroundColor: transparent,
                          enableActiveFill: true,
                          controller: otpController,
                          onCompleted: (v) {},
                          onChanged: (value) {},
                          beforeTextPaste: (text) {
                            return true;
                          },
                          appContext: context,
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Opacity(
                          opacity: timeText.isEmpty ? 1 : (.5),
                          child: Container(
                            height: 40,
                            //width: 105,
                            child: FlatButton(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25),
                                    side: BorderSide(
                                        color: white.withOpacity(.5),
                                        width: .5)),
                                color: black.withOpacity(.02),
                                onPressed: () {
                                  if (timeText.isNotEmpty) return;
                                  showProgress(true, context,
                                      msg: "Verifying Number");
                                  clickVerify(hide: true);
                                },
                                child: Column(
                                  // crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text.rich(TextSpan(children: [
                                      TextSpan(text: "Didn't get OTP Code? "),
                                      TextSpan(
                                        text: "Resend",
                                        style: textStyle(true, 14, appColor),
                                      ),
                                    ])),
                                    if (timeText.isNotEmpty)
                                      Text(
                                        timeText,
                                        style: textStyle(false, 10, white),
                                      ),
                                  ],
                                )),
                          ),
                        ),
                      ),
                      addSpace(40),
                      RaisedButton(
                        onPressed: () {
                          checkCode();
                        },
                        color: appColor,
                        padding: EdgeInsets.all(14),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        child: Center(
                            child: Text(
                          'Proceed',
                          style: textStyle(true, 16, white),
                        )),
                      ),
                      addSpace(50),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

String profilePhoto = '';

selfieField() {
  return Container(
    padding: EdgeInsets.only(top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              color: black.withOpacity(.02),
              border: Border.all(color: black.withOpacity(.02)),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.only(left: 0, right: 10),
          alignment: Alignment.centerLeft,
          child: Row(
            children: [
              Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(color: appColor.withOpacity(.06)),
                child: profilePhoto.isEmpty
                    ? Icon(
                        Icons.person,
                        color: black.withOpacity(.4),
                      )
                    : Image.file(File(profilePhoto)),
              ),
              addSpaceWidth(10),
              Expanded(
                child: Container(
                  //color: red0,
                  child: Text(
                    "It's important that you take a selfie photo of yourself."
                    " This is to prevent fraud your identity remains anonymous.",
                    style: textStyle(false, 14, red0),
                  ),
                ),
              )
            ],
          ),
        )
      ],
    ),
  );
}
