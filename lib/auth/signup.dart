import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:acclaim/SelectRegion.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_index.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_pickers/country.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:masked_controller/mask.dart';
import 'package:masked_controller/masked_controller.dart';
import 'package:nexmo_verify/nexmo_sms_verify.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../CameraMain.dart';
import '../SelectCountry.dart';
import 'login.dart';
import 'otp_screen.dart';

enum UsernameState { none, checking, available, notAvailable, error }

String countrySelected = getCountries()
    .singleWhere((element) => element.countryName == 'Nigeria')
    .countryName;
String countryFlag = getCountries()
    .singleWhere((element) => element.countryName == 'Nigeria')
    .countryFlag;
String countryCode = getCountries()
    .singleWhere((element) => element.countryName == 'Nigeria')
    .countryCode;

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> with WidgetsBindingObserver {
  UsernameState usernameState = UsernameState.none;
  String currentDisplay = GetAsset.images("dp0.jpg");
  final vp = PageController();
  final pageController = PageController();
  int imagePage = 0;
  int currentPage = 0;

  final otpController = TextEditingController();
  final errorController = StreamController<ErrorAnimationType>.broadcast();
  String phoneNumber = '';
  int forceResendingToken = 0;
  bool verifying = false;
  String verificationId = "";
  String timeText = "";

  List get images {
    return [
      GetAsset.images("p0.jpg"),
      GetAsset.images("p1.jpg"),
      GetAsset.images("p2.jpg"),
      GetAsset.images("p3.jpg"),
    ];
  }

  final mobileController = MaskedController(mask: Mask(mask: '(NNN) NNN NNNN'));

  final usernameController = TextEditingController();
  var emailController = TextEditingController();

  String stateConstituency = '';
  String state = '';
  String constituency = '';

  bool keyboardVisible = false;

  Timer timer;
  bool reversing = false;
  final _codeWheeler = CodeWheeler(milliseconds: 8000);

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    //_nexmoSmsVerificationUtil.initNexmo(appSettingsModel.get(SMS_API_KEY), appSettingsModel.get(SMS_API_SECRET));
    _codeWheeler.run(pageWheeler);
    usernameController.addListener(() async {
      String check = usernameController.text;

      if (check.isEmpty) {
        usernameState = UsernameState.none;
        if (mounted) setState(() {});
        return;
      }

      if (check.contains(' ')) {
        showError('Username cannot contain space!');
        return;
      }

      if (check.isNotEmpty && check.length > 4) {
        usernameState = UsernameState.checking;
        checkUsernameAvailability(check.toLowerCase());
      }
      if (mounted) setState(() {});
    });

    KeyboardVisibility.onChange.listen((bool visible) {
      keyboardVisible = visible;
      print("Keyboard Visible $visible");
      if (mounted) setState(() {});
    });
  }

  pageWheeler() {
    if (null == vp || !mounted) return;
    if (imagePage < images.length - 1 && !reversing) {
      reversing = false;
      if (mounted) setState(() {});
      vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      return;
    }
    if (imagePage == images.length - 1 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = true;
        if (mounted) setState(() {});
        vp.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
    if (imagePage == 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (imagePage == 0 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (imagePage > 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        vp.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
  }

  checkUsernameAvailability(String name) async {
    print(name);
    var doc = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(USERNAME, isEqualTo: name)
        .limit(1)
        .get();

    print(doc.size);

    if (doc.size == 0) {
      usernameState = UsernameState.available;
    } else {
      usernameState = UsernameState.notAvailable;
    }

    if (mounted) setState(() {});
  }

  get getAvailabilityTxt {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return '';
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return 'Checking username availability';
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return 'Username is available';
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return 'Username is not available';
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return 'Username contains space!';
        break;
    }
  }

  get getAvailabilityColor {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return transparent;
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return transparent;
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return dark_green0;
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return red0;
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return red0;
        break;
    }
  }

  get getAvailabilityIcon {
    switch (usernameState) {
      case UsernameState.none:
        // TODO: Handle this case.
        return null;
        break;
      case UsernameState.checking:
        // TODO: Handle this case.
        return CircularProgressIndicator(
          //valueColor: AlwaysStoppedAnimation(white),
          strokeWidth: 2,
        );
        break;
      case UsernameState.available:
        // TODO: Handle this case.
        return Icon(Icons.check, color: white, size: 20);
        break;
      case UsernameState.notAvailable:
        // TODO: Handle this case.
        return Icon(Icons.close, color: white, size: 20);
        break;
      case UsernameState.error:
        // TODO: Handle this case.
        return Icon(Icons.close, color: white, size: 20);
        break;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    errorController?.close();
    usernameController?.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.detached:
        FirebaseAuth.instance.signOut();
        break;
      default:
    }
    super.didChangeAppLifecycleState(state);
  }

  int time = 0;

  String currentOTP = '';
  NexmoSmsVerificationUtil _nexmoSmsVerificationUtil =
      NexmoSmsVerificationUtil();

  /*clickVerify() {
    print(phoneNumber);
    showProgress(true, context, msg: "Sending OTP");
    _nexmoSmsVerificationUtil
        .sendOtp(phoneNumber, "Your OurVoice Verification Code")
        .then((res) {
      showProgress(false, context);
      createTimer(true);
    }).catchError((e) {
      showError(e.toString(), wasLoading: true);
    });
  }*/

  clickVerify({bool hide = false}) async {
    final _auth = FirebaseAuth.instance;
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: const Duration(seconds: 60),
          verificationCompleted:
              (PhoneAuthCredential phoneAuthCredential) async {
            if (hide) showProgress(false, context);
            Future.delayed(Duration(seconds: 1), () {
              linkCredentials(phoneAuthCredential);
            });
          },
          verificationFailed: (authException) {
            if (hide) showProgress(false, context);
            Future.delayed(Duration(seconds: 1), () {
              showErrorDialog(context, authException.message);
            });
          },
          codeSent: (String id, [int forceResendingToken]) async {
            //showError("Check your phone for OTP", wasLoading: true);
            if (hide) showProgress(false, context);
            verificationId = id;
            print('verificationId $verificationId');
            createTimer(true);
            forceResendingToken = forceResendingToken;
            if (mounted) setState(() {});
          },
          codeAutoRetrievalTimeout: (String id) {
            verificationId = id;
          });
    } catch (e) {
      //showProgress(false, context);
      if (hide) showProgress(false, context);
      Future.delayed(Duration(seconds: 1), () {
        showErrorDialog(context, e.toString());
      });
    }
  }

  checkCode() async {
    String code = otpController.text.replaceAll(" ", "");
    if (code.isEmpty) {
      showError("Enter OTP");
      errorController.add(ErrorAnimationType.shake);
      return;
    }

    print('currentPage $currentPage');

    showProgress(true, context, msg: "Verifying OTP");
    // _nexmoSmsVerificationUtil.verifyOtp(code).then((value) {
    //   linkCredentials(null);
    // }).catchError((e) {
    //   showError(e.toString(), wasLoading: true);
    // });
    //
    // return;

    //showProgress(true, context, msg: 'Verifying OTP-Code');
    AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: code);
    await linkCredentials(credential);
  }

  linkCredentials(AuthCredential phoneAuthCredential) async {
    showProgress(true, context, msg: "Checking Please wait...");
    String authKey = phoneNumber + "@ourvoice.com";

    FirebaseAuth.instance.signInWithCredential(phoneAuthCredential)
        // .createUserWithEmailAndPassword(email: authKey, password: authKey)
        .then((result) {
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(result.user.uid)
          .get()
          .then((doc) {
        showProgress(false, context);

        if (doc.exists) {
          showMessage(context, Icons.error_outline, red0, 'Account Exists',
              'Sorry an account already exist with the same credentials.Login or Cross-Check Again.',
              delayInMilli: 1200,
              clickYesText: 'Login',
              clickNoText: 'Cancel',
              cancellable: false, onClicked: (_) {
            if (!_) return;
            pushAndResult(context, Login(),
                replace: true, transitionBuilder: fadeTransition);
          });
          return;
        }

        userModel
          ..put(USER_ID, result.user.uid)
          ..put(OBJECT_ID, result.user.uid)
          ..put(MOBILE_NUMBER, phoneNumber)
          ..put(IS_MAUGOST, userModel.isMaugost)
          ..put(IS_ADMIN, userModel.isMaugost);
        userModel.saveItem(USER_BASE, false,
            document: result.user.uid, merged: true, onComplete: () {
          Future.delayed(Duration(seconds: 1), () {
            //handlePageChange(next: true);
            currentPage = 2;
            currentDisplay = images[currentPage];
            pageController.jumpToPage(currentPage);
            if (mounted) setState(() {});
          });
        });
      }).catchError((e) {
        showProgress(false, context);
        Future.delayed(Duration(seconds: 1), () {
          showErrorDialog(context, e.toString());
        });
      });
    }).catchError((e) {
      showProgress(false, context);
      Future.delayed(Duration(seconds: 1), () {
        showErrorDialog(context, e.toString());
      });
    });
  }

  createTimer(bool create) {
    if (create) {
      time = 60;
    }
    if (time < 0) {
      if (mounted)
        setState(() {
          timeText = "";
        });
      return;
    }
    timeText = getTimeText();
    Future.delayed(Duration(seconds: 1), () {
      time--;
      if (mounted) setState(() {});
      createTimer(false);
    });
  }

  getTimeText() {
    if (time <= 0) return "";
    return "${time >= 60 ? "01" : "00"}:${time % 60}";
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    FocusScope.of(context).requestFocus(FocusNode());
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        children: <Widget>[
          // Image.asset(
          //   currentDisplay,
          //   fit: BoxFit.cover,
          //   alignment: Alignment.center,
          //   height: getScreenHeight(context),
          // ),
          PageView(
            controller: vp,
            onPageChanged: (p) {
              imagePage = p;
              setState(() {});
            },
            children: List.generate(
                images.length,
                (index) => Image.asset(
                      images[index],
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                      height: getScreenHeight(context),
                    )),
          ),
          BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 10.0,
              sigmaY: 10.0,
            ),
            child: Container(
              color: transparent,
              // decoration: BoxDecoration(
              //     gradient: LinearGradient(colors: [
              //   appColor.withOpacity(.9),
              //   appColor.withOpacity(.2),
              // ], begin: Alignment.bottomCenter, end: Alignment.topCenter)),
            ),
          ),

          PageView(
            controller: pageController,
            onPageChanged: (p) {
              currentPage = p;
              setState(() {});
            },
            physics: NeverScrollableScrollPhysics(),
            children: List.generate(4, (index) {
              return Center(
                  child: Container(
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(25)),
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Builder(
                        builder: (c) {
                          if (index == 0) return phoneEmailPage();
                          if (index == 1) return otpPage();
                          if (index == 2) return selfieUsernamePage();

                          return constituencyPage();
                        },
                      ),
                      addSpace(5),
                      if (currentPage != 3)
                        Row(
                          children: [
                            if (currentPage > 0)
                              Flexible(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RaisedButton(
                                    onPressed: () async {
                                      handlePageChange();
                                    },
                                    color: white,
                                    padding: EdgeInsets.all(14),
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(color: appColor),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Center(
                                        child: Text(
                                      'BACK',
                                      style: textStyle(true, 16, appColor),
                                    )),
                                  ),
                                ),
                              ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  onPressed: () async {
                                    handlePageChange(next: true);
                                  },
                                  color: appColor,
                                  padding: EdgeInsets.all(14),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Center(
                                      child: Text(
                                    currentPage == 3 ? "FINISH" : 'NEXT',
                                    style: textStyle(true, 16, white),
                                  )),
                                ),
                              ),
                            ),
                          ],
                        ),
                      addSpace(5),
                    ],
                  ),
                ),
              ));
            }),
          ),

          /*Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                  color: white, borderRadius: BorderRadius.circular(25)),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      width: double.infinity,
                      height: errorText.isEmpty ? 0 : 40,
                      color: red0,
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Center(
                          child: Text(
                        errorText,
                        style: textStyle(true, 16, white),
                      )),
                    ),
                    addSpace(15),
                    IndexedStack(
                      index: currentPage,
                      alignment: Alignment.center,
                      children: [
                        phoneEmailPage(),
                        otpPage(),
                        selfieUsernamePage(),
                        constituencyPage()
                      ],
                    ),
                    //addSpace(5),
                    if (currentPage != 3)
                      Row(
                        children: [
                          if (currentPage > 0)
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  onPressed: () async {
                                    handlePageChange();
                                  },
                                  color: white,
                                  padding: EdgeInsets.all(14),
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(color: appColor),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Center(
                                      child: Text(
                                    'BACK',
                                    style: textStyle(true, 16, appColor),
                                  )),
                                ),
                              ),
                            ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                onPressed: () async {
                                  handlePageChange(next: true);
                                },
                                color: appColor,
                                padding: EdgeInsets.all(14),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                child: Center(
                                    child: Text(
                                  currentPage == 3 ? "FINISH" : 'NEXT',
                                  style: textStyle(true, 16, white),
                                )),
                              ),
                            ),
                          ),
                        ],
                      ),
                    addSpace(5),
                  ],
                ),
              ),
            ),
          ),*/
          if (!keyboardVisible)
            Container(
              padding: EdgeInsets.all(16),
              child: Column(
                children: [
                  addSpace(30),
                  Spacer(),
                  FlatButton(
                    onPressed: () {
                      pushAndResult(context, Login(),
                          replace: true, transitionBuilder: fadeTransition);
                    },
                    padding: EdgeInsets.all(12),
                    color: black.withOpacity(.5),
                    child: Center(
                        child: Text.rich(
                      TextSpan(children: [
                        TextSpan(
                          text: "Already have an account? ",
                          style: textStyle(false, 16, white.withOpacity(.7)),
                        ),
                        TextSpan(
                          text: "Login",
                          style: textStyle(true, 16, white),
                        ),
                      ]),
                    )),
                  ),
                ],
              ),
            )
        ],
      ),
    );
  }

  handlePageChange({bool next = false}) {
    String username = usernameController.text;
    String mobile = mobileController.unmaskedText;
    String email = emailController.text;
    FocusScope.of(context).requestFocus(FocusNode());

    // currentPage = (next ? currentPage + 1 : currentPage - 1).clamp(0, 3);
    // pageController.jumpToPage(currentPage);
    // currentDisplay = images[currentPage];
    // if (mounted) setState(() {});
    // return;

    if (next && currentPage == 0 && mobile.isEmpty) {
      showError("Enter your Mobile number!");
      return;
    }
    if (next && currentPage == 0 && mobile.length < 10) {
      showError("Mobile number is incomplete! ${10 - mobile.length} left!");
      return;
    }

    if (next && currentPage == 0 && email.isEmpty) {
      showError("Enter your email address");
      return;
    }

    if (next && currentPage == 0) {
      phoneNumber = countryCode + mobile;
      currentPage = 1;
      pageController.jumpToPage(currentPage);
      currentDisplay = images[1];
      if (mounted) setState(() {});
      clickVerify();
      return;
    }

    if (next && currentPage == 1) {
      checkCode();
      return;
    }

    if (next && currentPage == 2 && selfieImage.isEmpty) {
      selfieError = true;
      showError("You have not taken any selfie yet!");
      return;
    }

    if (next && currentPage == 2 && username.isEmpty) {
      showError("Enter a username!");
      return;
    }

    if (next && currentPage == 2 && usernameState != UsernameState.available) {
      showError(getAvailabilityTxt);
      return;
    }

    if (next && currentPage == 3 && stateConstituency.isEmpty) {
      showError("Choose a constituency!");
      return;
    }

    if (next && currentPage == 3) {
      userModel.put(SELFIE_IMAGE_PATH, selfieImage);
      userModel.put(EMAIL, email);
      userModel.put(USERNAME, username);
      userModel.put(MOBILE_NUMBER, mobile);
      userModel.put(STATE, state);
      userModel.put(STATE_CONSTITUENCY, stateConstituency);
      userModel.updateItems();
      pushAndResult(context, AppIndex(),
          clear: true, transitionBuilder: fadeTransition);
      selfieUploadController.add(selfieImage);
      return;
    }

    currentPage = (next ? currentPage + 1 : currentPage - 1).clamp(0, 3);
    print('currentPage $currentPage');
    pageController.jumpToPage(currentPage);
    currentDisplay = images[currentPage];
    if (mounted) setState(() {});
  }

  phoneEmailPage() {
    return AnimatedOpacity(
      opacity: 1,
      curve: Curves.bounceInOut,
      duration: Duration(milliseconds: 500),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Account Registration',
            style: textStyle(true, 30, appColor),
          ),
          addSpace(10),
          textField("Mobile number", '(811) 234 1234', mobileController,
              numb: true, showFlag: true, onFlagTap: () {
            //FocusScope.of(context).requestFocus(FocusNode());
            pushAndResult(context, SelectCountry(),
                transitionBuilder: fadeTransition, result: (Country _) {
              countrySelected = _.name;
              countryCode = '+${_.phoneCode}';
              print(countrySelected);
              print(countryCode);

              countryFlag = 'flags/${_.isoCode.toLowerCase()}.png';
              if (mounted) setState(() {});
            });
          }),
          textField("Email Address", 'Enter email Address', emailController),
        ],
      ),
    );
  }

  otpPage() {
    return AnimatedOpacity(
      opacity: 1,
      curve: Curves.bounceInOut,
      duration: Duration(milliseconds: 500),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Phone Verification',
            style: textStyle(true, 30, appColor),
          ),
          addSpace(10),
          Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                //color: black.withOpacity(.05),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                  "Please enter the Verification Code sent to you as an SMS to the number $phoneNumber")),
          addSpace(10),
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              //color: black.withOpacity(.05),
              borderRadius: BorderRadius.circular(10),
            ),
            child: PinCodeTextField(
              length: 6,
              obscureText: false,
              animationType: AnimationType.fade,
              keyboardType: TextInputType.number,
              autoDisposeControllers: false,
              errorAnimationController: errorController,
              cursorColor: transparent,
              pinTheme: PinTheme(
                  shape: PinCodeFieldShape.circle,
                  borderRadius: BorderRadius.circular(25),
                  fieldHeight: 50,
                  fieldWidth: 50,
                  inactiveFillColor: black.withOpacity(.3),
                  disabledColor: black.withOpacity(.02),
                  inactiveColor: black.withOpacity(.02),
                  selectedColor: black.withOpacity(.02),
                  activeFillColor: black.withOpacity(.02),
                  activeColor: black.withOpacity(.02),
                  selectedFillColor: black.withOpacity(.5)),
              animationDuration: Duration(milliseconds: 300),
              backgroundColor: transparent,
              enableActiveFill: true,
              controller: otpController,
              onCompleted: (v) {},
              onChanged: (value) {},
              beforeTextPaste: (text) {
                return true;
              },
              appContext: context,
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Opacity(
              opacity: timeText.isEmpty ? 1 : (.5),
              child: Container(
                height: 40,
                //width: 105,
                child: FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                        side: BorderSide(
                            color: white.withOpacity(.5), width: .5)),
                    color: black.withOpacity(.02),
                    onPressed: () {
                      if (timeText.isEmpty) {
                        otpController.clear();
                        showProgress(true, context, msg: "Verifying Number");
                        clickVerify(hide: true);
                      }
                    },
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text.rich(TextSpan(children: [
                          TextSpan(text: "Didn't get OTP Code? "),
                          TextSpan(
                            text: "Resend",
                            style: textStyle(true, 14, appColor),
                          ),
                        ])),
                        if (timeText.isNotEmpty)
                          Text(
                            timeText,
                            style: textStyle(false, 10, white),
                          ),
                      ],
                    )),
              ),
            ),
          ),
          addSpace(10),
        ],
      ),
    );
  }

  selfieUsernamePage() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
            pushAndResult(
                context,
                CameraMain(
                  cameraAlone: true,
                  selfie: true,
                ), result: (_) {
              if (null == _ || _.toString().isEmpty) return;
              selfieImage = _;
              selfieError = false;
              setState(() {});
            });
          },
          child: Container(
            padding: EdgeInsets.only(top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 0, right: 10),
                  alignment: Alignment.center,
                  child: Container(
                    width: 125,
                    height: 125,
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(60),
                          child: Container(
                            width: 120,
                            height: 120,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: appColor.withOpacity(.06),
                                border: Border.all(
                                    width: selfieError ? 3 : 1,
                                    color: selfieError
                                        ? red0
                                        : black.withOpacity(.02))),
                            child: selfieImage.isEmpty
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.person_add,
                                        size: 30,
                                        color: black.withOpacity(.5),
                                      ),
                                      Text("Add")
                                    ],
                                  )
                                : Image.file(
                                    File(selfieImage),
                                    width: 120,
                                    height: 120,
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: GestureDetector(
                            onTap: () {
                              showMessage(
                                  context,
                                  Icons.person,
                                  appColor,
                                  'Selfie Image',
                                  "It's important that you take a selfie photo of yourself."
                                      " This is to prevent fraud your identity remains anonymous.");
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: appColor,
                                  shape: BoxShape.circle,
                                  border: Border.all(color: white, width: 3)),
                              height: 35,
                              width: 35,
                              padding: EdgeInsets.all(2),
                              child: Center(
                                  child: Icon(
                                Icons.info_outline,
                                color: white,
                                size: 20,
                              )),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        textField("Username", '', usernameController),
        if (usernameState != UsernameState.none)
          Container(
            padding: EdgeInsets.only(top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: black.withOpacity(.02),
                      border: Border.all(width: 2, color: getAvailabilityColor),
                      borderRadius: BorderRadius.circular(5)),
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Container(
                        width: 25,
                        height: 25,
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: getAvailabilityColor,
                        ),
                        alignment: Alignment.center,
                        child: getAvailabilityIcon,
                      ),
                      addSpaceWidth(10),
                      Expanded(
                        child: Container(
                          //color: red0,
                          child: Text(
                            getAvailabilityTxt,
                            style: textStyle(false, 14, black),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
      ],
    );
  }

  constituencyPage() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Constituency',
          style: textStyle(true, 30, appColor),
        ),
        addSpace(10),
        clickField('State/Constituency', 'Choose', stateConstituency, () {
          FocusScope.of(context).requestFocus(FocusNode());
          pushAndResult(context, SelectRegion(),
              transitionBuilder: fadeTransition, result: (_) {
            if (null == _ || !(_ is List)) return;
            state = _[0];
            constituency = _[1];
            stateConstituency = '$constituency in $state';
            setState(() {});
          });
        }),
        addSpace(50),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
            onPressed: () async {
              handlePageChange();
            },
            color: white,
            padding: EdgeInsets.all(14),
            shape: RoundedRectangleBorder(
                side: BorderSide(color: appColor),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
                child: Text(
              'BACK',
              style: textStyle(true, 16, appColor),
            )),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
            onPressed: () async {
              handlePageChange(next: true);
            },
            color: appColor,
            padding: EdgeInsets.all(14),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Center(
                child: Text(
              currentPage == 3 ? "FINISH" : 'NEXT',
              style: textStyle(true, 16, white),
            )),
          ),
        ),
      ],
    );
  }

  proceed() async {
    String username = usernameController.text;
    String mobile = mobileController.unmaskedText;
    String email = emailController.text;
    FocusScope.of(context).requestFocus(FocusNode());

    if (selfieImage.isEmpty) {
      selfieError = true;
      showError("You have not taken a selfie yet!");
      return;
    }

    if (username.isEmpty) {
      showError("Enter a username!");
      return;
    }

    if (stateConstituency.isEmpty) {
      showError("Choose a constituency!");
      return;
    }

    if (email.isEmpty) {
      showError("Enter your email address!");
      return;
    }

    if (mobile.isEmpty) {
      showError("Enter your Mobile number!");
      return;
    }

    if (mobile.length < 10) {
      showError("Cellphone number is incomplete! ${10 - mobile.length} left!");
      return;
    }

    if (usernameState != UsernameState.available) {
      showError(getAvailabilityTxt);
      return;
    }

    userModel.put(SELFIE_IMAGE_PATH, selfieImage);
    userModel.put(EMAIL, email);
    userModel.put(USERNAME, username);
    userModel.put(MOBILE_NUMBER, mobile);

    pushAndResult(
        context,
        OtpScreen(
          phoneNumber: countryCode + mobile,
        ));
  }

  String selfieImage = '';
  bool selfieError = false;

  selfieField() {
    return InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        pushAndResult(
            context,
            CameraMain(
              cameraAlone: true,
              selfie: true,
            ), result: (_) {
          if (null == _ || _.toString().isEmpty) return;
          selfieImage = _;
          selfieError = false;
          setState(() {});
        });
      },
      child: Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                  color: black.withOpacity(.02),
                  border: Border.all(
                      width: selfieError ? 3 : 1,
                      color: selfieError ? red0 : black.withOpacity(.02)),
                  borderRadius: BorderRadius.circular(5)),
              padding: EdgeInsets.only(left: 0, right: 10),
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(color: appColor.withOpacity(.06)),
                    child: selfieImage.isEmpty
                        ? Icon(
                            Icons.person,
                            color: black.withOpacity(.4),
                          )
                        : Image.file(
                            File(selfieImage),
                            width: 100,
                            height: 100,
                            fit: BoxFit.cover,
                          ),
                  ),
                  addSpaceWidth(10),
                  Expanded(
                    child: Container(
                      //color: red0,
                      child: Text(
                        "It's important that you take a selfie photo of yourself."
                        " This is to prevent fraud your identity remains anonymous.",
                        style: textStyle(false, 14, red0),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  clickField(String title, String hint, String value, onTap) {
    bool isEmpty = value.isEmpty;

    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: textStyle(false, 15, appColor),
          ),
          addSpace(10),
          InkWell(
            onTap: onTap,
            child: Container(
              decoration: BoxDecoration(
                  color: black.withOpacity(.02),
                  border: Border.all(color: black.withOpacity(.02)),
                  borderRadius: BorderRadius.circular(5)),
              padding: EdgeInsets.only(left: 10, right: 10),
              alignment: Alignment.centerLeft,
              height: 50,
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      isEmpty ? hint : value,
                      style: textStyle(
                          false, 15, black.withOpacity(isEmpty ? 0.5 : 1)),
                    ),
                  ),
                  Icon(
                    Icons.arrow_drop_down_circle_outlined,
                    size: 20,
                    color: black.withOpacity(.5),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
