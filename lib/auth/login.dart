import 'dart:async';
import 'dart:ui';

import 'package:acclaim/SelectCountry.dart';
import 'package:acclaim/app/app.dart';
import 'package:acclaim/auth/signup.dart';
import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:masked_controller/mask.dart';
import 'package:masked_controller/masked_controller.dart';

import 'otp_screen.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with WidgetsBindingObserver {
  List get images {
    return [
      GetAsset.images("p0.jpg"),
      // GetAsset.images("p1.jpg"),
      GetAsset.images("p2.jpg"),
      GetAsset.images("p3.jpg"),
    ];
  }

  int currentPage = 0;
  final vp = PageController();
  bool checkingAvailability = false;
  bool usernameAvailable = false;

  String phoneNumber = '';

  int forceResendingToken = 0;
  bool verifying = false;
  String verificationId = "";

//813-4373-3836
  final mobileController = MaskedController(mask: Mask(mask: '(NNN) NNN NNNN'));
  bool keyboardVisible = false;

  Timer timer;
  bool reversing = false;
  final _codeWheeler = CodeWheeler(milliseconds: 8000);

  @override
  initState() {
    super.initState();
    _codeWheeler.run(pageWheeler);
  }

  pageWheeler() {
    if (null == vp || !mounted) return;
    if (currentPage < images.length - 1 && !reversing) {
      reversing = false;
      if (mounted) setState(() {});
      vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      return;
    }
    if (currentPage == images.length - 1 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = true;
        if (mounted) setState(() {});
        vp.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
    if (currentPage == 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (currentPage == 0 && !reversing) {
      Future.delayed(Duration(seconds: 2), () {
        reversing = false;
        if (mounted) setState(() {});
        vp.nextPage(duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }

    if (currentPage > 0 && reversing) {
      Future.delayed(Duration(seconds: 2), () {
        vp.previousPage(
            duration: Duration(milliseconds: 12), curve: Curves.ease);
      });
      return;
    }
  }

  @override
  dispose() {
    timer?.cancel();
    super.dispose();
  }

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   switch (state) {
  //     case AppLifecycleState.paused:
  //       break;
  //     case AppLifecycleState.inactive:
  //       break;
  //     case AppLifecycleState.resumed:
  //       break;
  //     case AppLifecycleState.detached:
  //       FirebaseAuth.instance.signOut();
  //       break;
  //     default:
  //   }
  //   super.didChangeAppLifecycleState(state);
  // }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    FocusScope.of(context).requestFocus(FocusNode());

    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        children: <Widget>[
          PageView.builder(
            controller: vp,
            itemCount: images.length,
            onPageChanged: (p) {
              currentPage = p;
              setState(() {});
            },
            itemBuilder: (ctx, p) {
              return AnimatedOpacity(
                opacity: 1,
                curve: Curves.bounceInOut,
                duration: Duration(milliseconds: 500),
                child: Stack(
                  children: <Widget>[
                    Image.asset(
                      images[p],
                      fit: BoxFit.cover,
                      width: getScreenWidth(context),
                      // height: getScreenHeight(context),
                    ),
                    Container(
                      width: getScreenWidth(context),
                      height: getScreenHeight(context),
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                        black.withOpacity(.15),
                        black.withOpacity(.09),
                      ], begin: Alignment.center, end: Alignment.bottomCenter)),
                    ),
                  ],
                ),
              );
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                /*Opacity(
                  opacity: keyboardVisible ? 0 : 1,
                  child: Container(
                    padding: EdgeInsets.all(1),
                    margin: EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                        border: Border.all(color: white.withOpacity(.2)),
                        borderRadius: BorderRadius.circular(12),
                        color: black.withOpacity(.3)),
                    child: new DotsIndicator(
                      dotsCount: images.length,
                      position: currentPage,
                      decorator: DotsDecorator(
                        size: const Size.square(5.0),
                        color: white,
                        activeColor: appColor,
                        activeSize: const Size(10.0, 7.0),
                        activeShape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                      ),
                    ),
                  ),
                ),*/
                Container(
                  decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25),
                      )),
                  padding: EdgeInsets.all(10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        AnimatedContainer(
                          duration: Duration(milliseconds: 500),
                          width: double.infinity,
                          height: errorText.isEmpty ? 0 : 40,
                          color: red0,
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Center(
                              child: Text(
                            errorText,
                            style: textStyle(true, 16, white),
                          )),
                        ),
                        Text(
                          'Welcome back',
                          style: textStyle(
                            true,
                            25,
                            appColor,
                          ),
                        ),
                        addSpace(10),
                        Text(
                          'Login to your account',
                          style: textStyle(
                            false,
                            14,
                            black.withOpacity(.5),
                          ),
                        ),
                        textField(
                            "Mobile number", '(811) 234 1234', mobileController,
                            numb: true, showFlag: true, onFlagTap: () {
                          //FocusScope.of(context).requestFocus(FocusNode());
                          pushAndResult(context, SelectCountry(),
                              transitionBuilder: fadeTransition,
                              result: (Country _) {
                            countrySelected = _.name;
                            countryCode = '+${_.phoneCode}';
                            print(countrySelected);
                            print(countryCode);

                            countryFlag =
                                'flags/${_.isoCode.toLowerCase()}.png';
                            if (mounted) setState(() {});
                          });
                        }),
                        addSpace(30),
                        RaisedButton(
                          onPressed: () {
                            String mobile = mobileController.unmaskedText;

                            if (mobile.isEmpty) {
                              showError("Enter your Mobile number!");
                              return;
                            }

                            if (mobile.length < 10) {
                              showError(
                                  "Cellphone number is incomplete! ${10 - mobile.length} left!");
                              return;
                            }

                            pushAndResult(
                                context,
                                OtpScreen(
                                  phoneNumber: countryCode + mobile,
                                  login: true,
                                ));
                          },
                          color: appColor,
                          padding: EdgeInsets.all(14),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                              child: Text(
                            'LOGIN',
                            style: textStyle(
                              true,
                              18,
                              white,
                            ),
                          )),
                        ),
                        addSpace(10),
                        FlatButton(
                          onPressed: () {
                            pushAndResult(context, SignUp(),
                                replace: true,
                                transitionBuilder: fadeTransition);
                          },
                          padding: EdgeInsets.all(12),
                          color: white,
                          child: Center(
                              child: Text.rich(
                            TextSpan(children: [
                              TextSpan(
                                text: "Don't have an account? ",
                                style:
                                    textStyle(false, 16, black.withOpacity(.7)),
                              ),
                              TextSpan(
                                text: "Register",
                                style: textStyle(true, 16, appColor),
                              ),
                            ]),
                          )),
                        ),
                        addSpace(5),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

textField(String title, String hint, TextEditingController controller,
    {bool numb = false, bool showFlag = false, onFlagTap}) {
  return Container(
    padding: EdgeInsets.only(top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyle(false, 15, appColor),
        ),
        addSpace(10),
        Container(
          decoration: BoxDecoration(
              color: black.withOpacity(.02),
              border: Border.all(color: black.withOpacity(.02)),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.only(left: showFlag ? 2 : 10, right: 10),
          child: Row(
            children: [
              if (showFlag)
                InkWell(
                  onTap: () {
                    if (onFlagTap == null) return;
                    onFlagTap();
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 5),
                    padding: EdgeInsets.only(),
                    decoration: BoxDecoration(
                        color: black.withOpacity(.0),
                        border: Border.all(color: black.withOpacity(.0)),
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        /*addSpaceWidth(5),
                        Text(
                          countrySelected,
                          style: textStyle(true, 14, black.withOpacity(.5)),
                        ),
                        addSpaceWidth(5),*/
                        Image.asset(
                          countryFlag,
                          height: 20,
                          width: 30,
                          fit: BoxFit.cover,
                        ),
                        addSpaceWidth(10),
                        Text(
                          countryCode,
                          style: textStyle(true, 16, black.withOpacity(.5)),
                        ),
                      ],
                    ),
                  ),
                ),
              Expanded(
                child: TextField(
                  controller: controller,
                  keyboardType: numb ? TextInputType.number : null,
                  decoration: InputDecoration(
                      //contentPadding: EdgeInsets.zero,
                      border: InputBorder.none,
                      hintText: hint,
                      hintStyle: textStyle(false, 16, black.withOpacity(.5))),
                ),
              ),
            ],
          ),
        )
      ],
    ),
  );
}
