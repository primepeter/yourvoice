// Copyright 2014 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:acclaim/AppLogoStyle.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

/// The Flutter logo, in widget form. This widget respects the [IconTheme].
/// For guidelines on using the Flutter logo, visit https://flutter.dev/brand.
///
/// See also:
///
///  * [IconTheme], which provides ambient configuration for icons.
///  * [Icon], for showing icons the Material design icon library.
///  * [ImageIcon], for showing icons from [AssetImage]s or other [ImageProvider]s.
class AppLogo extends StatefulWidget {
  /// Creates a widget that paints the Flutter logo.
  ///
  /// The [size] defaults to the value given by the current [IconTheme].
  ///
  /// The [textColor], [style], [duration], and [curve] arguments must not be
  /// null.
  const AppLogo({
    Key key,
    this.size,
    this.textColor = const Color(0xFF757575),
    this.style = AppLogoStyle.markOnly,
    this.duration = const Duration(milliseconds: 750),
    this.curve = Curves.fastOutSlowIn,
    @required this.text,
    @required this.assetPath,
  })  : assert(textColor != null),
        assert(style != null),
        assert(duration != null),
        assert(curve != null),
        super(key: key);

  /// The size of the logo in logical pixels.
  ///
  /// The logo will be fit into a square this size.
  ///
  /// Defaults to the current [IconTheme] size, if any. If there is no
  /// [IconTheme], or it does not specify an explicit size, then it defaults to
  /// 24.0.
  final double size;

  final String text;

  final String assetPath;

  /// The color used to paint the "Flutter" text on the logo, if [style] is
  /// [FlutterLogoStyle.horizontal] or [FlutterLogoStyle.stacked].
  ///
  /// If possible, the default (a medium grey) should be used against a white
  /// background.
  final Color textColor;

  /// Whether and where to draw the "Flutter" text. By default, only the logo
  /// itself is drawn.
  final AppLogoStyle style;

  /// The length of time for the animation if the [style] or [textColor]
  /// properties are changed.
  final Duration duration;

  /// The curve for the logo animation if the [style] or [textColor] change.
  final Curve curve;

  @override
  _AppLogoState createState() => _AppLogoState();
}

class _AppLogoState extends State<AppLogo> {
  ui.Image assetImage;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadImage(widget.assetPath);
  }

  loadImage(String asset) async {
    var fileData = Uint8List.sublistView(await rootBundle.load(asset));
    assetImage = await decodeImageFromList(fileData);
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final IconThemeData iconTheme = IconTheme.of(context);
    final double iconSize = widget.size ?? iconTheme.size;

    if (assetImage == null) return Container();
    return AnimatedContainer(
      width: iconSize,
      height: iconSize,
      duration: widget.duration,
      curve: widget.curve,
      decoration: AppLogoDecoration(
        text: widget.text,
        assetImage: assetImage,
        style: widget.style,
        textColor: widget.textColor,
      ),
    );
  }
}
