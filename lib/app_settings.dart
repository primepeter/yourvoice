import 'package:acclaim/app/app.dart';
import 'package:acclaim/app_config.dart';
import 'package:acclaim/localization/app_translations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'LangMain.dart';
import 'localization/application.dart';

class AppSettings extends StatefulWidget {
  @override
  _AppSettingsState createState() => _AppSettingsState();
}

class _AppSettingsState extends State<AppSettings> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  String translate(String key) {
    return AppTranslations.of(context).text(key);
  }

  static final List<String> langList = application.supportedLanguages;
  static final List<String> langCodesList = application.supportedLanguagesCodes;

  final Map<dynamic, dynamic> langMap = {
    langList[0]: langCodesList[0],
    langList[1]: langCodesList[1],
  };

  String label = langList[0];

  @override
  void initState() {
    super.initState();
    application.onLocaleChanged = onLocaleChange;
    onLocaleChange(appLocale);
  }

  void onLocaleChange(Locale locale) async {
    print(locale.toString());
    setState(() {
      appLocale = locale;
      AppTranslations.load(locale);
      int p = langCodesList.indexWhere((e) => e == locale.languageCode);
      bool exists = p != -1;
      String language = langList[p];
      if (exists) label = language == 'Arabic' ? "العربية" : language;
      //_select(langList[p]);
    });

    setState(() {});
  }

  itemCheckView({title, subTitle, active, onClicked}) {
    return InkWell(
      onTap: () {
        onClicked(!active);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        title,
                        style: textStyle(false, 16, Colors.black),
                      ),
                      addSpace(5),
                      Text(
                        subTitle,
                        style: textStyle(false, 12, Colors.grey),
                      ),
                      addSpace(10),
                    ],
                  ),
                ),
                CupertinoSwitch(
                    value: active,
                    activeColor: AppConfig.appColor,
                    onChanged: (_) {
                      onClicked();
                    }),
                /*if (active)
                  Container(
                    height: 20,
                    width: 20,
                    padding: EdgeInsets.all(2),
                    child: Container(
                      height: 20,
                      width: 20,
                      child: Icon(
                        Icons.check,
                        size: 10,
                        color: Colors.white,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.green[700],
                          shape: BoxShape.circle,
                          border:
                              Border.all(color: Colors.black.withOpacity(.4))),
                    ),
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        shape: BoxShape.circle,
                        border:
                            Border.all(color: Colors.black.withOpacity(.4))),
                  )
                else
                  Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        shape: BoxShape.circle,
                        border:
                            Border.all(color: Colors.black.withOpacity(.4))),
                  )*/
              ],
            ),
          ),
          addLine(1, Colors.black.withOpacity(.04), 0, 0, 0, 0),
        ],
      ),
    );
  }

  itemView(title, items, onClicked, {bool showAdmin = false, onAdminClicked}) {
    return Container(
      //color: Colors.grey[50],
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              height: 60,
              //color: Colors.white,
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 15),
              child: Text(
                title,
                style: textStyle(true, 16, black.withOpacity(.7)),
              )),
          if (showAdmin)
            InkWell(
              onTap: onAdminClicked,
              splashColor: Colors.green[800],
              highlightColor: Colors.green[800],
              child: Ink(
                color: Colors.red,
                child: Container(
                    height: 60,
                    //color: Colors.red,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Support/Admin Panel",
                          style: TextStyle(fontSize: 18, color: black),
                        ),
                        Icon(
                          Icons.navigate_next,
                          color: black,
                        )
                      ],
                    )),
              ),
            ),
          Divider(
            height: 1,
            color: white.withOpacity(0.1),
          ),
          ...List.generate(items.length, (index) {
            return InkWell(
              onTap: () {
                onClicked(index);
              },
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      //height: 50,
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(items[index],
                              style: textStyle(false, 16, black)),
                          Icon(
                            Icons.navigate_next,
                            color: black.withOpacity(.4),
                          )
                        ],
                      ),
                    ),
                    addLine(1, black.withOpacity(.04), 0, 0, 0, 0)
                  ],
                ),
              ),
            );
          }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, "");
        return false;
      },
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(LangKey.settings.toAppLanguage),
        ),
        //backgroundColor: AppConfig.appColor,
        backgroundColor: white,
        body: ListView(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.only(bottom: 60),
          children: <Widget>[
            itemCheckView(
                title: LangKey.pushNotifications.toAppLanguage,
                subTitle: (userModel.pushNotifications
                    ? LangKey.enabled.toAppLanguage
                    : LangKey.disabled.toAppLanguage),
                active: userModel.pushNotifications,
                onClicked: (v) {
                  userModel
                    ..put(PUSH_NOTIFICATIONS, v)
                    ..updateItems();
                  setState(() {});
                }),
            itemCheckView(
                title: LangKey.libraryNotifications.toAppLanguage,
                subTitle: (userModel.libraryNotifications
                    ? LangKey.enabled.toAppLanguage
                    : LangKey.disabled.toAppLanguage),
                active: userModel.libraryNotifications,
                onClicked: (v) {
                  userModel
                    ..put(LIBRARY_NOTIFICATIONS, v)
                    ..updateItems();
                  setState(() {});
                }),
            itemCheckView(
                title: LangKey.chatNotifications.toAppLanguage,
                subTitle: (userModel.chatNotifications
                    ? LangKey.enabled.toAppLanguage
                    : LangKey.disabled.toAppLanguage),
                active: userModel.chatNotifications,
                onClicked: (v) {
                  userModel
                    ..put(CHAT_NOTIFICATIONS, v)
                    ..updateItems();
                  setState(() {});
                }),
            itemView(LangKey.appAndSettings.toAppLanguage, [
              LangKey.appLanguage.toAppLanguage,
              LangKey.shareApp.toAppLanguage,
              LangKey.rateApp.toAppLanguage,
              // "About App",
            ], (p) {
              if (p == 0) {
                List items = appSettingsModel.getList(APP_LANGUAGE);
                print(items);
                showListDialog(
                    context,
                    items
                        .map((e) => capitalize(
                            Language.value(Language.stringToLangKey(e))))
                        .toList(),  (_) {
                  Language.saveAppLanguage(items[_], () {
                    setState(() {});
                  });
                });
                return;
                pushAndResult(
                    context,
                    listDialog(langList
                        .map((e) => e == 'Arabic' ? "Arabic (العربية)" : e)
                        .toList()), result: (_) {
                  if (null == _) return;
                  setAppLanguage(langList[_]);
                });
                return;
              }
            }, onAdminClicked: () {}),
          ],
        ),
      ),
    );
  }

  void setAppLanguage(String language) {
    print("dd " + language);
    onLocaleChange(Locale(langMap[language]));
    setState(() {
      if (language == "Arabic") {
        label = "العربية";
      } else {
        label = language;
      }
    });
    setState(() {});
  }
}
