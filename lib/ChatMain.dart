import 'dart:async';
//import 'dart:html' as html;
import 'dart:io';
import 'dart:ui';

//import 'package:ABM/dart';

import 'package:acclaim/VideoController.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:emoji_picker/emoji_picker.dart';
// import 'package:file_picker/file_picker.dart';
import 'package:filesize/filesize.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_photo_picker/photo_picker.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:open_file/open_file.dart';
//import 'package:firebase/firebase.dart' as fb;
import 'package:path/path.dart' as Path;
//import 'package:flutter_sound/android_encoder.dart';
//import 'package:flutter_sound/flutter_sound_recorder.dart';
import 'package:path/path.dart' as pathLib;
import 'package:path_provider/path_provider.dart' show getTemporaryDirectory;
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:synchronized/synchronized.dart';
import 'package:vibration/vibration.dart';
import 'package:video_player/video_player.dart';

import 'CameraMain.dart';
import 'SearchPlace.dart';
import 'app/app.dart';
import 'app_index.dart';
import 'preview_post_images.dart';

const int SAMPLE_RATE = 8000;
const int BLOCK_SIZE = 4096;

class ChatMain extends StatefulWidget {
  String chatId;
  String defMessage;
  ChatMain(
    this.chatId, {
    this.defMessage,
  });

  @override
  _ChatMainState createState() => _ChatMainState();
}

class _ChatMainState extends State<ChatMain>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  TextEditingController messageController = new TextEditingController();
  final ItemScrollController messageListController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener =
      ItemPositionsListener.create();
//  ScrollController messageListController = new ScrollController();
  List<BaseModel> chatList = List();

  FocusNode messageNode = FocusNode();
  bool checking = false;
  bool hasMessage = false;
  String chatId;
  BaseModel otherUser = BaseModel();
  BaseModel farmModel = BaseModel();
  BaseModel produceModel = BaseModel();
  BaseModel chatSetup = BaseModel();
//  bool setup = true;

  bool showEmoji = false;
  bool keyboardVisible = false;
  bool amTyping = false;
  Timer timerType;
  String lastTypedText = "";
  int lastTyped = 0;

  final scaffoldKey = GlobalKey<ScaffoldState>();

  double xPosition = -1;
  double yPosition = -1;
  bool tappingDown = false;
//  double recButtonSize = 40;

  double sendSize = 40;
  var sendIcon = Icons.mic;
  VideoPlayerController typingSoundController;
  VideoPlayerController messageSoundController;
  BaseModel replyModel;
  int blinkPosition = -1;
//  ChatUtils chatUtils;

  bool canSound = true;
  bool atBottom = true;
  bool atTop = false;
  String CHAT_DRAFT = "ChatDraft";
  bool splitMode = false;
  bool supportMode = false;

//  int startTime = DateTime.now().millisecondsSinceEpoch;

  int unreadPosition = -1;
  int unreadCount = 0;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    checkDraft();
    /* bool down = false;
      for(var p in itemPositionsListener.itemPositions.value.toList()){
        if(p.index==0){
          down=true;
          break;
        }
      }
      if(down && !atBottom) {
        atBottom=true;
        atTop=false;
        setState(() {

        });
      }
      if(!down && !atTop){
        atTop=true;
        atBottom=false;
        setState(() {

        });
      }*/
//    });

    chatId = widget.chatId;
    visibleChatId = chatId;
    stopListening.removeWhere((element) => element == chatId);
    splitMode = chatId.endsWith("split");
    supportMode = chatId.endsWith("support");

    loadChat();
    loadChatSetup();

    if (isBlocked(otherUser)) {
      showMessage(context, Icons.error, red0, "Blocked",
          "You have been blocked from this user",
          delayInMilli: 500, cancellable: false, onClicked: (_) {
        clickedBack();
      });
    }
    Future.delayed(Duration(seconds: 1), () {
      loadSound();
      loadRecorder();
    });
    WidgetsBinding.instance.addObserver(this);

    timerType = new Timer.periodic(Duration(seconds: 1), (_) {
      if (!keyboardVisible) {
        if (amTyping) {
          setState(() {
            amTyping = false;
            updateTyping(false);
          });
        }
        return;
      }
      String newText = messageController.text;
      if (newText.trim().isEmpty) return;

      int now = DateTime.now().millisecondsSinceEpoch;

      if (newText != (lastTypedText)) {
        if (!amTyping) {
          amTyping = true;
          updateTyping(true);
          setState(() {});
        }
      } else {
        if ((now - lastTyped) > 2000) {
          if (amTyping) {
            amTyping = false;
            updateTyping(false);
            setState(() {});
          }
        }
      }

      lastTypedText = newText;
    });
    KeyboardVisibility.onChange.listen((bool visible) {
      keyboardVisible = visible;
      print("Keyboard Visible $visible");
      if (!visible) {
        amTyping = false;
        updateTyping(false);
        if (mounted) setState(() {});
      }
      if (showEmoji && visible) {
        showEmoji = false;
        if (mounted) setState(() {});
      }
    });
  }

  loadSound() async {
    File bubble = await loadFile('assets/sounds/typing.m4a', "typing.m4a");
    File bubble1 = await loadFile('assets/sounds/sent.m4a', "sent.m4a");

    typingSoundController = VideoPlayerController.file(bubble);
    messageSoundController = VideoPlayerController.file(bubble1);

    typingSoundController.initialize();
    messageSoundController.initialize();
  }

  loadChatSetup({bool fromLocal = true}) async {
    DocumentSnapshot doc = await Firestore.instance
        .collection(CHAT_IDS_BASE)
        .document(chatId)
        .get(GetOptions(
            source: fromLocal ? Source.cache : Source.serverAndCache))
        .catchError((e) {
      if (e.toString().contains("CACHE")) {
        loadChatSetup(fromLocal: false);
      }
    });
    if ((doc == null || !doc.exists)) {
      return;
    }
    chatSetup = BaseModel(doc: doc);
    if (!userModel.getList(MUTED).contains(chatId) &&
        !userModel.getList(TOPICS).contains(chatId)) {
//      subscribeToTopic(chatId, true);
    }
    if (userModel.getList(MUTED).contains(chatId) &&
        userModel.getList(TOPICS).contains(chatId)) {
//      subscribeToTopic(chatId, false);
    }

    //String farmAdmin = chatSetup.getString(FARM_ADMIN);
    String otherUserId = getTheOtherId(chatSetup);
    loadOtherUser(otherUserId);

    //loadProduce(chatSetup.getString(PRODUCE_ID));
    //loadFarm(chatSetup.getString(FARM_ID));

    var setupSub = Firestore.instance
        .collection(CHAT_IDS_BASE)
        .document(chatId)
        .snapshots()
        .listen((event) {
      chatSetup = BaseModel(doc: doc);
      if (chatSetup.getList(BLOCKED).contains(userModel.getObjectId())) {
//        subscribeToTopic(chatId,false);
        lastMessages
            .removeWhere((element) => element.getString(CHAT_ID) == chatId);
        stopListening.add(chatId);
        chatMessageController.add(true);
        showErrorDialog(
            context, "You have been blocked from this group by the owner",
            onOkClicked: () {
          clickedBack();
        }, cancellable: false);
        return;
      }
      setState(() {});
    });
    subs.add(setupSub);
  }

  /*loadProduce(String produceId,{bool fromLocal=true})async {
    DocumentSnapshot doc = await  Firestore.instance
        .collection(PRODUCT_BASE)
        .document(chatSetup.getString(PRODUCE_ID))
        .get(source:fromLocal?Source.cache:Source.serverAndCache)
        .catchError((e){if(e.toString().contains("CACHE")){
      loadProduce(produceId,fromLocal: false);
    }});

    produceModel = BaseModel(doc:doc);
    if(mounted)setState(() {});
    if(fromLocal)loadProduce(produceId,fromLocal: false);
  }*/

  /*loadFarm(String farmId,{bool fromLocal=true})async {
    DocumentSnapshot doc = await  Firestore.instance
        .collection(FARM_BASE)
        .doc(farmId)
        .get(GetOptions(source: fromLocal ? Source.cache : Source.serverAndCache))
        .catchError((e){if(e.toString().contains("CACHE")){
      loadFarm(farmId,fromLocal: false);
    }});

    farmModel = BaseModel(doc:doc);
    if(mounted)setState(() {});
    if(fromLocal)loadProduce(farmId,fromLocal: false);

    if(!fromLocal){
      chatSetup.put(FARM_NAME, farmModel.getString(FARM_NAME));
      chatSetup.put(FARM_LOGO, farmModel.getString(FARM_LOGO));
      chatSetup.updateItems();
    }
  }*/

  loadOtherUser(String userId, {bool fromLocal = true}) async {
    print("Loading user $userId");
    //if(splitMode && chatSetup.getString(FARM_ADMIN)==userModel.getObjectId())return;
    DocumentSnapshot doc = await Firestore.instance
        .collection(USER_BASE)
        .document(userId)
        .get(GetOptions(
            source: fromLocal ? Source.cache : Source.serverAndCache))
        .catchError((e) {
      if (e.toString().contains("CACHE")) {
        loadOtherUser(userId, fromLocal: false);
      }
    });

    otherUser = BaseModel(doc: doc);
    if (mounted) setState(() {});
    if (fromLocal) loadOtherUser(userId, fromLocal: false);
    refreshOtherUser(userId);
  }

  bool refreshingOtherUser = false;
  refreshOtherUser(String userId) async {
    if (refreshingOtherUser) return;
    refreshingOtherUser = true;
    var sub = Firestore.instance
        .collection(USER_BASE)
        .document(userId)
        .snapshots()
        .listen((shot) {
      otherUser = BaseModel(doc: shot);
      if (isBlocked(otherUser)) {
        if (mounted)
          showMessage(context, Icons.error, red0, "Blocked",
              "You have been blocked from this user",
              delayInMilli: 500, cancellable: false, onClicked: (_) {
            clickedBack();
          });
      }
      if (userModel.getList(BLOCKED).contains(userId)) {
        Future.delayed(Duration(seconds: 1), () {
          clickedBack();
        });
      }
      int now = DateTime.now().millisecondsSinceEpoch;
      int lastUpdated = otherUser.getInt(TIME_UPDATED);
      int tsDiff = (now - lastUpdated);
      bool notOnline = (tsDiff > (Duration.millisecondsPerMinute * 10));
      bool notTyping = (tsDiff > (Duration.millisecondsPerMinute * 1));
      isOnline = otherUser.getBoolean(IS_ONLINE) && (!notOnline);
      isTyping = otherUser.getString(TYPING_ID) == chatId && (!notTyping);
      bool notRecording = (tsDiff > (Duration.millisecondsPerMinute * 5));
      isRecording = otherUser.getString(REC_ID) == chatId && (!notRecording);
      if (mounted) setState(() {});

      /*if(typingController!=null && typingController.value.initialized) {
        if (isTyping) {
          typingController.play();
        }else{
          typingController.pause();
        }
      }*/
      if (isTyping) {
        playTypingSound();
      } else if (typingSoundController != null &&
          typingSoundController.value.initialized) {
        typingSoundController.pause();
      }
    });

    subs.add(sub);
  }

  List audioSetupList = [];
  List<StreamSubscription> subs = List();
  bool unreadHandled = false;
  loadChat() async {
    StreamSubscription<QuerySnapshot> sub = Firestore.instance
        .collection(CHAT_BASE)
        .where(CHAT_ID, isEqualTo: chatId)
        .orderBy(CREATED_AT, descending: false)
        .snapshots()
        .listen((shots) async {
      if (!unreadHandled) {
        unreadHandled = true;
        List rList = List.from(shots.documents.reversed);
        int count = 0;
        print("Counting Unread");
        for (int i = 0; i < rList.length; i++) {
          DocumentSnapshot doc = rList[i];
          BaseModel model = BaseModel(doc: doc);
          if (chatRemoved(model)) continue;
          if (model.getUserId().isEmpty) continue;
          if (i == 0) print("XXX 0: ${model.getString(MESSAGE)}");
          if (model.myItem() ||
              model.getList(READ_BY).contains(userModel.getObjectId())) {
            print("XXX: ${model.getString(MESSAGE)}");
            unreadPosition = i;
            break;
          }
//          if (!) &&
//              !model.myItem()) {
//          }
          count++;
        }
        if (count == 0) unreadPosition = -1;
        if (count != 0) {
          unreadCount = count;
//          unreadPosition = count;
          print("Found some unread");
//          unreadPosition++;
          if (shownChatsCount < unreadCount) shownChatsCount = unreadCount + 1;
/*          Future.delayed(Duration(milliseconds: 1000), () {
            print("Jumped To Unread at $unreadPosition");

            messageListController.jumpTo(
              index: unreadPosition,);

          });*/
          if (mounted) setState(() {});
        }
      }

      for (DocumentChange docChange in shots.documentChanges) {
        if (docChange.type == DocumentChangeType.added &&
            loadedChatIds.contains(docChange.document.documentID)) continue;
        DocumentSnapshot doc = docChange.document;
        BaseModel chat = BaseModel(doc: doc);
        print("New Message in Chat... $chatId");
        addChatToList(chat);
        List readBy = chat.getList(READ_BY);
        bool myRead = readBy.contains(userModel.getObjectId());
        if (!chat.myItem() && !myRead) {
          chat.putInList(READ_BY, userModel.getObjectId());
        }
      }

      refreshChatDates();
      loadVideo();
      if (mounted) setState(() {});
    });

    subs.add(sub);
  }

  updateChatToRead() async {
    unreadCounter[chatId] = null;

    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(CHAT_BASE)
        .where(CHAT_ID, isEqualTo: chatId)
        .get();

    for (DocumentSnapshot doc in shots.documents) {
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (model.getList(READ_BY).contains(userModel.getObjectId())) continue;
      model
        ..putInList(READ_BY, userModel.getUserId(), true)
        ..updateItems(delaySeconds: 1);
    }
  }

  List loadedChatIds = [];
  void addChatToList(BaseModel chat) async {
    //chatList.insert(0, chat);
    var lock = Lock();
    await lock.synchronized(
      () {
        print("Adding Chat to List");
        int p = chatList
            .indexWhere((BaseModel c) => chat.getObjectId() == c.getObjectId());
        if (p != -1) {
          chatList[p] = chat;
          print("updating chat at $p");
//        if(mounted)setState(() {});
        } else {
          print("inserting chat at $p");
          if (!chat.myItem()) playNewMessageSound();
          chatList.insert(0, chat);
          if (!loadedChatIds.contains(chat.getObjectId()))
            loadedChatIds.add(chat.getObjectId());
          if (chat.getInt(TYPE) == CHAT_TYPE_DOC ||
              chat.getInt(TYPE) == CHAT_TYPE_REC) {
            checkIfFileExists(chat, false);
          }
        }
      },
    );
  }

  checkDraft() async {
    if (widget.defMessage != null) {
      messageController.text = widget.defMessage;
      if (widget.defMessage.isNotEmpty) hasMessage = true;
      setState(() {});
      return;
    }
    SharedPreferences shed = await SharedPreferences.getInstance();
    String text = shed.getString(CHAT_DRAFT) ?? "";
    messageController.text = text;
    if (text.isNotEmpty) hasMessage = true;
    setState(() {});
  }

  int lastNewMessagePlayed = 0;
  playNewMessageSound() async {
    if (visibleChatId != chatId) return;
    int now = DateTime.now().millisecondsSinceEpoch;
    int diff = now - lastNewMessagePlayed;
    if (diff < 1000) return;
    lastNewMessagePlayed = now;

    if (messageSoundController == null) return;
    if (!messageSoundController.value.initialized) return;

    messageSoundController.setLooping(true);
    messageSoundController.setVolume(.3);
    messageSoundController.play();
    Future.delayed(Duration(seconds: 1), () {
      messageSoundController.pause();
    });
  }

  int lastTypingPlayed = 0;
  playTypingSound() async {
    if (visibleChatId != chatId) return;
    int now = DateTime.now().millisecondsSinceEpoch;
    int diff = now - lastTypingPlayed;
    if (diff < 2000) return;
    lastTypingPlayed = now;
    typingSoundController.setLooping(true);
    typingSoundController.setVolume(.1);
    typingSoundController.play();
  }

//  List checkedList = [];
  checkIfFileExists(BaseModel model, bool notify) async {
    if (fileThatExists.contains(model.getObjectId())) return;

    if (model.getInt(TYPE) == CHAT_TYPE_REC) {
      String audioPath = model.getString(AUDIO_PATH);
      var parts = audioPath.split("/");
      String baseFileName = parts[parts.length - 1];
      bool exists = await checkLocalFile("${model.getObjectId()}$baseFileName");
      if (exists) fileThatExists.add(model.getObjectId());
      if (mounted) setState(() {});
      return;
    }

    String fileName =
        "${model.getObjectId()}.${model.getString(FILE_EXTENSION)}";
    File file = await getDirFile(fileName);
    bool fileExists = await file.exists();
    if (fileExists) {
      fileThatExists.add(model.getObjectId());
    }

    if (/*notify &&*/ mounted) setState(() {});
  }

  handleAudio(BaseModel model) async {
    var lock = Lock();
    await lock.synchronized(() {
      if (!audioSetupList.contains(model.getObjectId())) {
        audioSetupList.add(model.getObjectId());
        if (model.getString(AUDIO_URL).isEmpty && model.myItem()) {
          saveChatAudio(model);
        }
      }
    });
  }

  saveChatAudio(BaseModel chat) async {
    String audioPath = chat.getString(AUDIO_PATH);
    bool exists = await File(audioPath).exists();
    if (!exists) {
      noFileFound.add(chat.getObjectId());
      setState(() {});
      return;
    }
    upOrDown.add(chat.getObjectId());
    uploadFile(File(audioPath), (url, error) {
      upOrDown.removeWhere((s) => s == chat.getObjectId());
      if (error != null) {
        setState(() {});
        return;
      }
      chat.put(AUDIO_URL, url);
      chat.updateItems();
      setState(() {});
    });
  }

  List timePositions = List();
  int lastRefreshed = 0;
  int refreshedCounter = 0;
  refreshChatDates({bool force = false}) async {
    int now = DateTime.now().millisecondsSinceEpoch;
    int diff = now - lastRefreshed;
    if (diff < 10000 && !force) return;
    lastRefreshed = now;
    if (refreshedCounter > 2 && !force) return;
    refreshedCounter++;
    print("Finding Time...");
    timePositions.clear();
    timePositions.add("");

    List<BaseModel> newList = [];
    for (BaseModel chat in chatList) {
      if (chatRemoved(chat)) continue;
      newList.add(chat);
    }

    if (newList.isEmpty) {
      timePositions.clear();
      print("Returning Time positions empty ${timePositions.length}");
      setState(() {});
      return;
    }
    print("New List ${newList.length}");
    for (int i = newList.length - 1; i >= 0; i--) {
      BaseModel chat = newList[i];
      int prevIndex = i + 1;
      BaseModel prevChat =
          prevIndex > newList.length - 1 ? null : newList[prevIndex];
      if (prevChat != null) {
        bool sameDay = isSameDay(chat.getTime(), prevChat.getTime());
        if (!sameDay) {
          timePositions.add(prevChat.getObjectId());
        }
//      timePositions.add(prevChat.id);
      }
    }
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    try {
//      recorderModule.release();
    } catch (err) {
      print('stopRecorder error: $err');
    }
    if (timerType != null) timerType.cancel();
//    updateTyping(false);

    for (StreamSubscription sub in subs) {
      sub.cancel();
    }
    WidgetsBinding.instance.removeObserver(this);
//    flutterSound.release();
    typingSoundController?.dispose();
    messageSoundController?.dispose();
    super.dispose();
  }

  ScreenHeight screenHeight;
  @override
  Widget build(BuildContext c) {
    return WillPopScope(
      onWillPop: () async {
        clickedBack();
        return;
      },
      child: SafeArea(
        bottom: false,
        top: false,
        child: KeyboardDismisser(
          gestures: [
            GestureType.onTap,
            GestureType.onPanUpdateAnyDirection,
          ],
          child: KeyboardSizeProvider(
            smallSize: 500.0,
            child: Consumer<ScreenHeight>(
                builder: (context, ScreenHeight sh, child) {
              this.screenHeight = sh;
//              print("New Height: ${screenHeight.screenHeight}");
              return Scaffold(
                  resizeToAvoidBottomInset: true,
                  key: scaffoldKey,
                  backgroundColor: gray_light,
                  body: Stack(
                    fit: StackFit.expand,
                    children: [
                      Opacity(
                          opacity: .7,
                          child: Image.asset(
                            chat_bg,
                            fit: BoxFit.cover,
                          )),
                      Container(
                        color: appColor.withOpacity(.03),
                      ),
                      page(),
                      if (tooShort)
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: IgnorePointer(
                            ignoring: true,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                              margin: EdgeInsets.fromLTRB(10, 0, 10, 70),
                              decoration: BoxDecoration(
                                  color: black.withOpacity(.8),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                "Hold to start recording",
                                style: textStyle(true, 12, white),
                              ),
                            ),
                          ),
                        ),
                      if (recording && !canCancelRecording())
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                            child: Text(
                              recordTimerText,
                              style: textStyle(true, 20, light_green3),
                            ),
                          ),
                        ),
                      if (!hasMessage)
                        Positioned(
                          top: screenHeight.screenHeight -
                              (screenHeight.keyboardHeight) -
                              (77),
                          left: xPosition == -1
                              ? screenW(context) - (200)
                              : xPosition - 170,
//                  width: 80,
//                  height: 80,
                          child: Opacity(
                            opacity: !recording ? 0 : 1,
                            child: Row(
                              children: <Widget>[
                                GestureDetector(
                                  onPanDown: (u) async {
                                    holdToRecord(u);
                                  },
                                  onPanStart: (u) {
                                    holdToRecord(u);
                                  },
                                  onPanEnd: (u) {
                                    tappingDown = false;
                                    stopRecording();
                                  },
                                  onPanCancel: () {
                                    tappingDown = false;
                                    stopRecording();
                                  },
                                  onPanUpdate: (u) {
                                    if (stopUpdatingPosition) return;
                                    xPosition = u.globalPosition.dx;
                                    yPosition = u.globalPosition.dy;
                                    setState(() {});
                                    if (canCancelRecording() && recording)
                                      Future.delayed(
                                          Duration(milliseconds: 100), () {
                                        stopUpdatingPosition = true;
                                        cancelled = true;
                                        stopRecording();
                                      });
                                  },
                                  child: Container(
                                    height: 80,
                                    width: 80,
                                    decoration: BoxDecoration(
                                        color: appColor,
                                        shape: BoxShape.circle),
                                    child: Center(
                                      child: Icon(
                                        Icons.mic,
                                        color: white,
                                        size: 50,
                                      ),
                                    ),
                                  ),
                                ),
                                IgnorePointer(
                                  ignoring: true,
                                  child: Container(
                                    margin: EdgeInsets.only(right: 10, top: 10),
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.red,
                                      highlightColor: Colors.yellow,
                                      direction: ShimmerDirection.rtl,
                                      child: Row(
                                        children: [
                                          Text(
                                            "Slide to Cancel",
                                            style: textStyle(false, 14,
                                                black.withOpacity(.5)),
                                          ),
                                          addSpaceWidth(5),
                                          Icon(
                                            Icons.arrow_forward_ios,
                                            color: black.withOpacity(.5),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      /*Positioned(
                          top: screenHeight.screenHeight -
                              (screenHeight.keyboardHeight) -
                              (77),
                          right: xPosition == -1
                              ? getScreenWidth(context) - (200)
                              : xPosition - 170,
                          */ /*bottom: 0,
                          left: 0,*/ /*
                          child: Opacity(
                            opacity: !recording ? 0 : 1,
                            // opacity: 1,
                            child: Row(
                              children: <Widget>[
                                IgnorePointer(
                                  ignoring: true,
                                  child: Container(
                                    margin: EdgeInsets.only(right: 10, top: 10),
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.red,
                                      highlightColor: Colors.yellow,
                                      direction: ShimmerDirection.rtl,
                                      child: Row(
                                        children: [
                                          Text(
                                            "Slide to Cancel",
                                            style: textStyle(false, 14,
                                                black.withOpacity(.5)),
                                          ),
                                          addSpaceWidth(5),
                                          Icon(
                                            Icons.arrow_forward_outlined,
                                            color: black.withOpacity(.5),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                addSpaceWidth(10),
                                GestureDetector(
                                  onPanDown: (u) async {
                                    holdToRecord(u);
                                  },
                                  onPanStart: (u) {
                                    holdToRecord(u);
                                  },
                                  onPanEnd: (u) {
                                    tappingDown = false;
                                    stopRecording();
                                  },
                                  onPanCancel: () {
                                    tappingDown = false;
                                    stopRecording();
                                  },
                                  onPanUpdate: (u) {
                                    if (stopUpdatingPosition) return;
                                    xPosition = u.globalPosition.dx;
                                    yPosition = u.globalPosition.dy;
                                    setState(() {});
                                    if (canCancelRecording() && recording)
                                      Future.delayed(
                                          Duration(milliseconds: 100), () {
                                        stopUpdatingPosition = true;
                                        cancelled = true;
                                        stopRecording();
                                      });
                                  },
                                  child: Container(
                                    height: 80,
                                    width: 80,
                                    decoration: BoxDecoration(
                                        color: appColor,
                                        shape: BoxShape.circle),
                                    child: Center(
                                      child: Icon(
                                        Icons.mic,
                                        color: white,
                                        size: 50,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),*/
                      Align(
                        alignment: Alignment.bottomRight,
                        child: IgnorePointer(
                          ignoring: atBottom,
                          child: AnimatedOpacity(
                            duration: Duration(milliseconds: 500),
                            opacity: atBottom ? 0 : 1,
                            child: Container(
                              width: 50,
                              height: 50,
                              margin: EdgeInsets.fromLTRB(20, 0, 20, 100),
                              child: FloatingActionButton(
                                onPressed: () {
                                  messageListController.jumpTo(index: 0);
                                },
                                heroTag: "p3cx3",
                                clipBehavior: Clip.antiAlias,
                                shape: CircleBorder(),
                                backgroundColor: light_green3,
                                child: Icon(
                                  Icons.arrow_downward,
                                  color: white,
                                  size: 20,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ));
            }),
          ),
        ),
      ),
    );
  }

  int shownChatsCount = 10;
  int countIncrement = 10;

  bool isTyping = false;
  bool isRecording = false;
  bool isOnline = false;

  page() {
    // bool myFarm = chatSetup.getString(FARM_ADMIN) == userModel.getObjectId();
    List chatSamples = []; //appSettingsModel.getList(CHAT_SAMPLES);
    String warning = ""; // appSettingsModel.getString(WARNING_MESSAGE);

    String pageTitle = otherUser.getString(NAME);

    List members = chatSetup.getList(PARTIES);
    // members.remove(chatSetup.getString(FARM_ADMIN));
    int memberCount = chatSetup.getList(PARTIES).length - 1;
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 35, 0, 10),
          // padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
          color: appColor,
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    clickedBack();
                  },
                  child: Container(
                    width: 60,
                    height: 30,
                    child: Center(
                        child: Icon(
                      Icons.arrow_back_ios,
                      color: white,
                      size: 20,
                    )),
                  )),
              Flexible(
                  fit: FlexFit.tight,
                  child: GestureDetector(
                    onTap: () {
                      /*bool myFarm = chatSetup.getString(FARM_ADMIN) ==
                          userModel.getObjectId();
                      if (!myFarm) {
                        if (supportMode) {
                          pushAndResult(context, ContactUs());
                        } else if (farmModel.items.isNotEmpty) {
                          pushAndResult(
                              context,
                              FarmMain(
                                farmModel: farmModel, */ /*fromChat: true,*/ /*
                              ));
                        }
                      }*/
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          pageTitle,
                          style: textStyle(true, 18, white),
                          maxLines: 1,
                        ),
                        if (!splitMode || splitMode)
                          if (getLastSeen(otherUser) != null)
                            Text(
                              getLastSeen(otherUser),
                              style:
                                  textStyle(false, 12, white.withOpacity(.8)),
                              maxLines: 1,
                            ),
                      ],
                    ),
                  )),
              addSpaceWidth(10),
              if (farmModel.items.isNotEmpty)
                Container(
//                    margin: EdgeInsets.only(right: 10),
                  width: 40, height: 40,
                  child: RaisedButton(
                    onPressed: () {
                      //clickContact(context, farmModel);
                    },
                    color: dark_green0,
                    shape: CircleBorder(),
                    padding: EdgeInsets.all(0),
                    child: Icon(
                      Icons.call, color: white, size: 18,
//                              size: 14,
                    ),
                  ),
                ),
              if (splitMode)
                Container(
                  height: 40,
                  width: 40,
                  margin: EdgeInsets.only(left: 10),
                  child: new RaisedButton(
                      padding: EdgeInsets.all(0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {
                        yesNoDialog(context, "Exit Group",
                            "Are you sure you want to exit?", () {
//                        subscribeToTopic(chatId,false);
                          chatSetup.putInList(
                              PARTIES, userModel.getObjectId(), false);
                          lastMessages.removeWhere((element) =>
                              element.getString(CHAT_ID) == chatId);
                          stopListening.add(chatId);
                          chatMessageController.add(true);
                          createChatNotice(chatId,
                              "${userModel.getString(NAME)} has left the group",
                              key: "left");
//                        handleSplitting(chatSetup.getString(PRODUCE_ID), enable: true);
                          clickedBack();
                        });
                      },
                      shape: CircleBorder(
//                      side: BorderSide(
//                        color: black,width: 1
//                      )
                          ),
                      color: red1,
                      child: Center(
                          child:
//                        Icon(Icons.settings,color: white,)
                              Image.asset(
                        ic_exit_group,
                        width: 20,
                        height: 20,
                        color: white,
                      ))),
                ),
              new Container(
                height: 30,
                width: 40,
                child: new FlatButton(
                    padding: EdgeInsets.all(0),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: () {
                      bool muted = userModel.getList(MUTED).contains(chatId);
                      List options = [
                        muted ? "Unmute Chat" : "Mute Chat",
                      ];
                      if (splitMode) {
                        options.add("View Members");
                      }

                       showListDialog(
                        context,
                        options,  (_) {
                          if (_ == 0) {
                            /* if(muted) {
                           userModel.putInList(MUTED, chatId, add: false);
                           subscribeToTopic(chatId, false);
                         }else{
                           userModel.putInList(MUTED, chatId, add: true);
                           subscribeToTopic(chatId, true);
                         }*/
                            userModel.putInList(MUTED, chatId, !muted);
//                         subscribeToTopic(chatId, muted);
                            setState(() {});
                          }
                          if (_ == 1) {
                            /*  pushAndResult(context, ViewSplitMembers(chatSetup),
                              result: (_) {
                            setState(() {
                              chatSetup = _;
                            });
                          });*/
                          }
                        }, /*returnIndex: true*/
                      );
                    },
                    child: Center(
                        child:
//                        Icon(Icons.settings,color: white,)
                            Icon(
                      Icons.more_vert,
                      size: 30,
                      color: white,
                    ))),
              ),

//                addSpaceWidth(50),
            ],
          ),
        ),
        if (splitMode)
          GestureDetector(
            onTap: () {
              //pushAndResult(context, ViewSplitMembers(chatSetup));
            },
            child: Container(
              color: red0,
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "SPLIT GROUP",
                    style: textStyle(true, 12, white),
                  ),
                  addSpaceWidth(5),
                  Flexible(
                      child: Text(
                    "$memberCount ${memberCount > 1 ? "Members" : "Member"}",
                    style: textStyle(false, 12, white.withOpacity(.8)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ))
                ],
              ),
            ),
          ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          height: errorText.isEmpty ? 0 : 40,
          color: red0,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Center(
              child: Text(
            errorText,
            style: textStyle(true, 16, white),
          )),
        ),
        new Expanded(
            flex: 1,
            child: Container(
//                color: light_green08,
              child: ScrollablePositionedList.builder(
                itemScrollController: messageListController,
                itemPositionsListener: itemPositionsListener,
                padding: EdgeInsets.all(0),
                reverse: true,
                itemBuilder: (c, p) {
                  BaseModel chat = chatList[p];
                  bool myItem = chat.getUserId() == userModel.getObjectId();
                  int type = chat.getType();
                  bool showDate = timePositions.contains(chat.getObjectId()) ||
                      (p == getItemCount() - 1 && timePositions.isNotEmpty);

                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      new Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Center(
                            child: chatList.length < shownChatsCount ||
                                    p != shownChatsCount - 1 ||
                                    chatList.length == shownChatsCount
                                ? Container()
                                : new GestureDetector(
                                    onTap: () {
                                      loadMorePrev();
                                    },
                                    child: new Container(
                                      margin:
                                          EdgeInsets.fromLTRB(10, 10, 10, 0),
                                      padding:
                                          EdgeInsets.fromLTRB(10, 5, 10, 5),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Icon(
                                            Icons.rotate_left,
                                            size: 15,
                                            color: appColor,
                                          ),
                                          addSpaceWidth(5),
                                          Text(
                                            "Previous messages",
                                            style: textStyle(true, 12,
                                                black.withOpacity(.6)),
                                          )
                                        ],
                                      ),
                                      decoration: BoxDecoration(
//                                                  color: brown04,
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          border: Border.all(
                                              color: appColor, width: 1)),
                                    ),
                                  ),
                          ),
                          if (showDate)
                            new Center(
                                child: Container(
                              margin: EdgeInsets.fromLTRB(
                                  0, (p == getItemCount() - 1) ? 15 : 0, 0, 15),
                              child: Text(
                                getChatDate(chat.getTime()),
                                style:
                                    textStyle(false, 12, black.withOpacity(.6)),
                              ),
                              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                              decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(25),
//                                    border: Border.all(
//                                        width: .5, color: light_green3)
                              ),
                            )),

                          if (p == unreadPosition - 1)
                            GestureDetector(
                              onTap: () {
                                scrollToBottom();
                              },
                              child: Container(
                                margin: EdgeInsets.only(bottom: 10),
                                color: blue0,
                                height: 30,
                                width: double.infinity,
                                child: Center(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(
                                        "${unreadCount} New Message${(unreadCount + 1) > 1 ? "s" : ""}",
                                        style: textStyle(true, 14, white),
                                      ),
//                                              addSpaceWidth(5),
//                                              Icon(Icons.arrow_downward,color: white,size: 20,)
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          AnimatedOpacity(
                            opacity: blinkPosition == p ? (.3) : 1,
                            duration: Duration(milliseconds: 500),
                            child: Container(
                              width: double.infinity,
                              child: Dismissible(
                                key: Key(getRandomId()),
                                direction: DismissDirection.startToEnd,
                                confirmDismiss: (_) async {
                                  if (chatRemoved(chat))
                                    return Future.value(false);
                                  replyModel = chat;
                                  FocusScope.of(context)
                                      .requestFocus(messageNode);
                                  setState(() {});
                                  return Future.value(false);
                                },
                                dismissThresholds: {
                                  DismissDirection.startToEnd: 0.1,
                                  DismissDirection.endToStart: 0.7
                                },
                                child: Column(
                                  crossAxisAlignment: myItem
                                      ? CrossAxisAlignment.start
                                      : CrossAxisAlignment.end,
                                  children: [
                                    chat.getUserId().isEmpty
                                        ? Center(
                                            child: Container(
                                            margin: EdgeInsets.fromLTRB(
                                                0,
                                                (p == getItemCount() - 1)
                                                    ? 10
                                                    : 0,
                                                0,
                                                10),
                                            child: Text(
                                              chat.getString(MESSAGE),
                                              style:
                                                  textStyle(false, 12, black),
                                            ),
                                            padding: EdgeInsets.fromLTRB(
                                                10, 5, 10, 5),
                                            decoration: BoxDecoration(
                                                color: default_white,
                                                borderRadius:
                                                    BorderRadius.circular(25),
                                                border: Border.all(
                                                    width: .5,
                                                    color:
                                                        black.withOpacity(.1))),
                                          ))
                                        : myItem
                                            ? Builder(
                                                builder: (ctx) {
                                                  if (type == CHAT_TYPE_TEXT)
                                                    return outgoingChatText(
                                                        context, chat, p == 0);

                                                  if (type == CHAT_TYPE_IMAGE)
                                                    return outgoingChatImage(
                                                        context, chat, () {
                                                      setState(() {});
                                                    }, p == 0);

                                                  if (type ==
                                                      CHAT_TYPE_LOCATION)
                                                    return outgoingChatLocation(
                                                        context, chat, () {
                                                      setState(() {});
                                                    }, p == 0);

                                                  if (type == CHAT_TYPE_REC) {
                                                    return Container(
                                                      child: getChatAudioWidget(
                                                          context, chat,
                                                          (removed) {
                                                        if (removed) {
                                                          chatList.removeAt(p);
                                                        }
                                                        if (mounted)
                                                          setState(() {});
                                                      }, p == 0),
                                                    );
                                                  }
                                                  if (type == CHAT_TYPE_DOC)
                                                    return outgoingChatDoc(
                                                        context, chat, () {
                                                      pushChat(
                                                          "Sent a document");
                                                      setState(() {});
                                                    }, p == 0);
                                                  return outgoingChatVideo(
                                                      context, chat, () {
                                                    pushChat("Sent a video");
                                                    setState(() {});
                                                  }, p == 0);
                                                },
                                              )
                                            : Builder(builder: (ctx) {
                                                if (type == CHAT_TYPE_TEXT) {
                                                  return incomingChatText(
                                                      context, chat);
                                                }
                                                if (type == CHAT_TYPE_REC) {
                                                  return Container(
                                                    child: getChatAudioWidget(
                                                        context, chat,
                                                        (removed) {
                                                      if (removed) {
                                                        chatList.removeAt(p);
                                                      }
                                                      if (mounted)
                                                        setState(() {});
                                                    }, p == 0),
                                                  );
                                                }

                                                if (type == CHAT_TYPE_IMAGE)
                                                  return incomingChatImage(
                                                      context, chat);

                                                if (type == CHAT_TYPE_LOCATION)
                                                  return incomingChatLocation(
                                                      context, chat);

                                                if (type == CHAT_TYPE_DOC)
                                                  return incomingChatDoc(
                                                      context,
                                                      chat,
                                                      fileThatExists.contains(
                                                          chat.getObjectId()),
                                                      () {
                                                    checkIfFileExists(
                                                        chat, true);
                                                  });
                                                return incomingChatVideo(
                                                    context, chat);
                                              }),
                                  ],
                                ),
                              ),
                            ),
                          ),

                          // if(blinkPosition==p)Container(width: double.infinity,
                          // height: 20,color:light_green3,margin: EdgeInsets.only(bottom:20),)
                        ],
                      ),
                      if (p == 0)
                        (isTyping
                            ? incomingChatTyping(context, typing: true)
                            : isRecording
                                ? incomingChatTyping(context, typing: false)
                                : Container())
                    ],
                  );
                },
                itemCount: getItemCount(),
              ),
            )),
        if (chatSamples.isNotEmpty &&
            chatList.isEmpty &&
            !splitMode &&
            !supportMode)
          SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(chatSamples.length, (index) {
                  Map map = chatSamples[index];
                  String title = map[TITLE];
                  String message = map[MESSAGE];
                  return Container(
                    margin: EdgeInsets.fromLTRB(15, 5, 0, 10),
                    child: RaisedButton(
                      onPressed: () {
                        messageController.text = message;
                        hasMessage = true;
                        setState(() {});
                      },
                      color: white,
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          side: BorderSide(
                              color: black.withOpacity(.1), width: 1)),
                      padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                      child: Text(
                        title,
                        style: textStyle(false, 14, black),
                      ),
                    ),
                  );
                }),
              )),
        addLine(.5, black.withOpacity(.2), 0, 0, 0, 0),
        if (!recording)
          Container(
            width: double.infinity,
            color: white,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    height: 45,
                    width: 45,
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: FloatingActionButton(
                      onPressed: () {
                        if (!hasMessage) {
                          return;
                        }
                        postChatText();
                      },
                      heroTag: "sendBut",
                      child: Icon(
                        !hasMessage ? Icons.mic : Icons.send,
                        color: black.withOpacity(.5),
                        size: hasMessage ? 16 : null,
                      ),
                      backgroundColor: white,
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      decoration: BoxDecoration(
//                          color: white,
                          borderRadius: replyModel != null
                              ? (BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(25),
                                  bottomLeft: Radius.circular(25)))
                              : BorderRadius.all(Radius.circular(25))),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (replyModel != null)
                            chatReplyWidget(
                              replyModel,
                              fitScreen: true,
                              onRemoved: () {
                                replyModel = null;
                                setState(() {});
                              },
                            ),
                          new ConstrainedBox(
                            constraints:
                                BoxConstraints(maxHeight: 120, minHeight: 45),
                            child: Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  addSpaceWidth(15),
                                  Flexible(
                                    fit: FlexFit.tight,
                                    child: new TextField(
                                      onChanged: (String text) {
                                        lastTyped = DateTime.now()
                                            .millisecondsSinceEpoch;
                                        bool empty = text.trim().isEmpty;
                                        if (empty && hasMessage) {
                                          hasMessage = false;
                                          setState(() {});
                                        }
                                        if (!empty && !hasMessage) {
                                          hasMessage = true;
                                          setState(() {});
                                        }
//                                        checking=false;
                                      },
                                      //textInputAction: TextInputAction.newline,
                                      textCapitalization:
                                          TextCapitalization.sentences,
                                      decoration: InputDecoration(
                                          hintText: "Type a message",
                                          isDense: true,
                                          hintStyle: textStyle(
                                              false, 17, black.withOpacity(.3)),
                                          border: InputBorder.none),
                                      style: textStyle(false, 17, black),
                                      controller: messageController,
                                      cursorColor: black,
                                      cursorWidth: 1,
                                      maxLines: null,
                                      keyboardType: TextInputType.multiline,
                                      focusNode: messageNode,
//                                      autofocus: true,
                                      scrollPadding:
                                          EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    ),
                                  ),
                                  Container(
                                    height: 40,
                                    width: 30,
                                    child: FlatButton(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        padding: EdgeInsets.all(0),
                                        onPressed: () {
                                          if (keyboardVisible && !showEmoji)
                                            messageNode.unfocus();
                                          showEmoji = !showEmoji;
                                          setState(() {});
                                        },
                                        child: Icon(
                                          showEmoji
                                              ? Icons.close
                                              : Icons.add_circle_outline,
                                          color: black.withOpacity(.5),
                                          size: 20,
                                        )),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

        /* if (setup && !recording)
          Container(
            width: double.infinity,
            color: appColor,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 40,
                    margin: EdgeInsets.only(left: 10, right: 10),
                    width: 40,
                    decoration:
                    BoxDecoration(color: white, shape: BoxShape.circle),
                    child: Center(
                      child: Icon(
                        Icons.mic,
                        color: appColor,
                        size: 23,
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: new ConstrainedBox(
                      constraints: BoxConstraints(maxHeight: 120),
                      child: Container(
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        decoration: BoxDecoration(
                            color: white,
                            borderRadius:
                            BorderRadius.all(Radius.circular(25))),
                        child: new TextField(
                          onSubmitted: (_) {
                            postChatText();
                          },
                          onChanged: (_) {
                            lastTyped = DateTime.now().millisecondsSinceEpoch;
                            messageController.text = _.trim();
                            setState(() {});
                          },
                          //textInputAction: TextInputAction.newline,
                          textCapitalization: TextCapitalization.sentences,
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                              hintText: "Type a message",
                              isDense: true,
                              hintStyle:
                              textStyle(false, 17, black.withOpacity(.3)),
                              border: InputBorder.none),
                          style: textStyle(false, 17, black),
                          controller: messageController,
                          cursorColor: black,
                          cursorWidth: 1,
                          maxLines: null,
                          keyboardType: TextInputType.multiline,
                          focusNode: messageNode,
                          scrollPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        ),
                      ),
                    ),
                  ),
                  if (messageController.text.isEmpty)
                    Container(
                      height: 50,
                      width: 50,
                      child: FlatButton(
                          materialTapTargetSize:
                          MaterialTapTargetSize.shrinkWrap,
                          onPressed: () {

                          },
                          child: Icon(
                            Icons.camera_alt,
                            color: white,
                            size: 20,
                          )),
                    ),
                  if (messageController.text.isNotEmpty)
                    Container(
                      height: 50,
                      width: 50,
                      child: FlatButton(
                          materialTapTargetSize:
                          MaterialTapTargetSize.shrinkWrap,
                          onPressed: () {
                            postChatText();
                          },
                          child: Icon(
                            Icons.send,
                            color: white,
                            size: 20,
                          )),
                    ),
                ],
              ),
            ),
          ),*/

        if (recording)
          Container(
            height: 60,
            color: (canCancelRecording()) ? red0 : white,
          ),
        if (showEmoji && !recording)
          Container(
            padding: EdgeInsets.all(18),
            decoration: BoxDecoration(
                color: white,
                border: Border(
                    top: BorderSide(width: 3, color: black.withOpacity(.1)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(3, (p) {
                List items = [
                  {"icon": Icons.image_outlined, "title": "Gallery"},
                  {"icon": Icons.attach_file, "title": "Send File"},
                  {"icon": Icons.camera_alt_outlined, "title": "Camera"},
                  // {"icon": Icons.location_on_outlined, "title": "Add Location"},
                ];
                return Flexible(
                  child: GestureDetector(
                    onTap: () {
                      showEmoji = !showEmoji;
                      setState(() {});
                      if (p == 0) pickImages();
                      if (p == 1) postChatDoc();
                      if (p == 2) handleCameraPicking();
                      // if (p == 3) handleTagLocation();
                    },
                    child: Container(
                      padding: EdgeInsets.all(6),
                      width: 100,
                      color: transparent,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(items[p]["icon"]),
                          addSpace(4),
                          Text(
                            items[p]["title"],
                            style: textStyle(false, 12, black),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
          )
        /*if (showEmoji)
          EmojiPicker(
            onEmojiSelected: (emoji, category) {
              String text = messageController.text;
              StringBuffer sb = StringBuffer();
              sb.write(text);
              sb.write(emoji.emoji);
              messageController.text = sb.toString();
//              hasMessage=false
              setState(() {});
            },
            //recommendKeywords: ["happy", "love"],
          )*/
      ],
    );
  }

  bool canCancelRecording() {
    return xPosition < (getScreenWidth(context) / 1.5);
  }

  int getItemCount() {
    return chatList.length < shownChatsCount
        ? chatList.length
        : shownChatsCount;
  }

  postChatText() {
    String text = messageController.text.trim();
    if (text.trim().isEmpty) {
      showError("Type a message");
      return;
    }

    final String id = getRandomId();
    final BaseModel model = new BaseModel();
    model.put(CHAT_ID, chatId);

    //showError(sDate.toString());
//    model.put(
//        PARTIES, [userModel.getObjectId(), otherUser.getObjectId()]);
    model.put(MESSAGE, text);
    model.put(TYPE, CHAT_TYPE_TEXT);
    model.put(OBJECT_ID, id);

    if (replyModel != null) {
      model.put(REPLY_DATA, replyModel.items);
      replyModel = null;
    }

    model.saveItem(CHAT_BASE, true, document: id, onComplete: () {
      pushChat(text);
    });

    messageController.text = "";
    hasMessage = false;
    sendIcon = Icons.mic;
    addChatToList(model);
    setState(() {});
    scrollToBottom();
    updateTyping(false);
  }

  pushChat(String content) {
    pushChatToParties(List.from(chatSetup.getList(PARTIES)), content);
  }

  pushChatToParties(List parties, String content) async {
    if (parties.isEmpty) return;
    print("Pushing Chat $content");
    String id = parties[0];

    DocumentSnapshot doc = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(id)
        .get(GetOptions(source: Source.server))
        .catchError((e) async {
      await Future.delayed(Duration(milliseconds: 2000));
      pushChatToParties(parties, content);
    });
    if (doc == null) return;
    if (!doc.exists) {
      parties.removeAt(0);
      pushChatToParties(parties, content);
      return;
    }
    List muted = doc.data()[MUTED] ?? [];
    if (muted.contains(chatId) ||
        doc.id == userModel.getObjectId() ||
        doc.data()[DONT_PUSH] == true) {
      parties.removeAt(0);
      pushChatToParties(parties, content);
      return;
    }

    String name = userModel.getString(NAME);

    Map data = Map();
    data[TYPE] = PUSH_TYPE_CHAT;
    data[OBJECT_ID] = chatId;

    String title = "";
    String message = "";

    if (supportMode &&
        appSettingsModel.getString(SUPPORT_ID) == userModel.getObjectId()) {
      name = "Support Team";
    }

    title = "New Message";
    message = "$name: $content";

    NotificationService.sendPush(
        topic: doc.documentID,
        title: title,
        body: message,
        tag: '${userModel.getObjectId()}chat',
        data: data);

    print("Pushed Chat to ${doc.data()[NAME]}");
    parties.removeAt(0);
    pushChatToParties(parties, content);
  }

  handleTagLocation() {
    pushAndResult(context, SearchPlace(), result: (BaseModel _) {
      if (_ == null) return;
      String taggedPlace = _.getString(PLACE_NAME);
      double taggedPlaceLat = _.getDouble(LATITUDE);
      double taggedPlaceLong = _.getDouble(LONGITUDE);

      final String id = getRandomId();
      final BaseModel model = new BaseModel();
      model.put(CHAT_ID, chatId);

      //showError(sDate.toString());
//    model.put(
//        PARTIES, [userModel.getObjectId(), otherUser.getObjectId()]);
      model.put(PLACE_NAME, taggedPlace);
      model.put(LATITUDE, taggedPlaceLat);
      model.put(LONGITUDE, taggedPlaceLong);
      model.put(TYPE, CHAT_TYPE_LOCATION);
      model.put(OBJECT_ID, id);

      if (replyModel != null) {
        model.put(REPLY_DATA, replyModel.items);
        replyModel = null;
      }

      model.saveItem(CHAT_BASE, true, document: id, onComplete: () {
        pushChat("Sent you a location");
      });

      messageController.text = "";
      hasMessage = false;
      sendIcon = Icons.mic;
      addChatToList(model);
      setState(() {});
      scrollToBottom();
      updateTyping(false);
    });
  }

  handleCameraPicking() {
    pushAndResult(context, CameraMain(), result: (List<BaseModel> _) {
      if (_ == null) return;
      pushAndResult(
          context,
          PreviewPostImages(
            _,
            [],
            edittable: true,
            indexOf: 0,
            heroTag: getRandomId(),
          ), result: (List<BaseModel> _) {
        if (_ == null || _.isEmpty) return;

        for (var m in _) {
          bool isVideo = m.isVideo;
          String path = m.getString(IMAGE_PATH);
          String thumbnail = m.getString(THUMBNAIL_PATH);
          int duration = m.getInt(VIDEO_LENGTH);
          if (!isVideo) {
            postChatImage(path);
          } else {
            postChatVideo(path, duration, thumbnail);
          }
//          break;
        }
        setState(() {});
      });
    });
  }

  pickImages() async {
    PhotoPicker.openPicker(
      mediaType: 'any', // image | video | any
      multiple: true,
      //selectedAssets: selectedPhotos,
    ).then((value) {
      if (null == value) return;
      final photos = value.map((e) {
        print("found url ${e.url.replaceAll("file://", "")}");

        BaseModel model = BaseModel();
        model.put(IMAGE_PATH, e.url.replaceAll("file://", ""));
        model.put(THUMBNAIL_PATH, e.thumbnailUrl.replaceAll("file://", ""));
        model.put(IS_VIDEO, e.type == "video");
        return model;
      }).toList();

      pushAndResult(
          context,
          PreviewPostImages(
            photos,
            [],
            edittable: true,
            indexOf: 0,
            heroTag: getRandomId(),
          ), result: (List<BaseModel> _) {
        if (_ == null || _.isEmpty) return;

        for (var m in _) {
          bool isVideo = m.isVideo;
          String path = m.getString(IMAGE_PATH);
          String thumbnail = m.getString(THUMBNAIL_PATH);
          int duration = m.getInt(VIDEO_LENGTH);
          if (!isVideo) {
            postChatImage(path);
          } else {
            postChatVideo(path, duration, thumbnail);
          }
//          break;
        }
        setState(() {});
      });
    }).catchError((e) {
      logError(e.code, e.bookmarkList);
    });

    /* pushAndResult(
        context,
        pre_gallery(
          maxSelection: 6,
          maxVideoDurationInSeconds: 600,
//      singleMode: true,
        ), result: (_) async {
      await Future.delayed(Duration(milliseconds: 500));
      pushAndResult(
          context,
          ViewChatMedia(
            _,
            0,
            edittable: true,
            maxSelection: 6,
          ), result: (_) async {
        for (Map m in _) {
          bool isVideo = m[IS_VIDEO];
          String path = m[PATH];
          String thumbnail = m[THUMBNAIL_PATH];
          int duration = m[VIDEO_LENGTH];
          if (!isVideo) {
            postChatImage(path);
          } else {
            postChatVideo(path, duration, thumbnail);
          }
//          break;
        }
      });
    });*/
  }

  postChatImage(String path) async {
    final String id = getRandomId();
    final BaseModel model = new BaseModel();
    model.put(CHAT_ID, chatId);
//    model.put(
//        PARTIES,
//        [userModel.getObjectId(), otherUser.getObjectId()]);
    model.put(TYPE, CHAT_TYPE_IMAGE);
    model.put(IMAGE_PATH, path);
    model.put(OBJECT_ID, id);
    model.put(DATABASE_NAME, CHAT_BASE);

    //chatList.insert(0, model);

    upOrDown.add(model.getObjectId());

    if (replyModel != null) {
      model.put(REPLY_DATA, replyModel.items);
      replyModel = null;
    }
    model.saveItem(CHAT_BASE, true, document: id, onComplete: () {
      saveChatFile(model, IMAGE_PATH, IMAGE_URL, () {
        addChatToList(model);
        setState(() {});
        pushChat('sent a photo');
      });
    });

    addChatToList(model);
    setState(() {});
    print("setting State...");
    scrollToBottom();
  }
/*

  Future<String> getImageForWeb() {
    final completer = new Completer<String>();
    final html.InputElement input = html.document.createElement('input');
    input
      ..type = 'file'
      ..accept = 'image/*';
    input.onChange.listen((e) async {
      final List<html.File> files = input.files;
      final reader = new html.FileReader();
      reader.readAsDataUrl(files[0]);
      reader.onError.listen((error) => completer.completeError(error));
      await reader.onLoad.first;
      completer.complete(reader.result as String);
    });
    input.click();
    return completer.future;
  }

 selectImageWeb(onSelected(Map selectedItem))async{
    String fileItem = await getImageForWeb();
    if(fileItem==null)return;
    if(fileItem.isEmpty)return;

    List parts = fileItem.split(";base64,");
    String a = parts[0].toString();
    String extension = a.substring(a.indexOf("/")+1,a.length);
    String fileText = parts[1];
    print("File Ext: $extension");
    print("File Item: $fileItem");

    int n=fileText.length;
    double y=fileText.endsWith("==")?2:1;
    double x = (n * (3/4)) - y;
//    x = x/~1000;
    String fileSize = filesize(x);
    print("File Size: $fileSize");

    onSelected({
      FILE_EXTENSION:extension,
      FILE_SIZE:fileSize,
      FILE_DATA:fileText
    });
    return;

//    Base64Decoder base64decoder = Base64Decoder();
//    base64decoder.convert(input).

  }

  uploadImageWeb(Map data,onComplete(String url,error))async{
    String extension = data[FILE_EXTENSION];
    String fileSize = data[FILE_SIZE];
    String fileData = data[FILE_DATA];
    fb.StorageReference ref = fb
        .app()
        .storage()
        .refFromURL('gs://splitam-b3664.appspot.com')
        .child("images/${getRandomIdShort()}.$extension");

    fb.UploadTask uploadTask = ref.putString(fileData,"base64");

    fb.UploadTaskSnapshot taskSnapshot = await uploadTask.future;
    taskSnapshot.ref.getDownloadURL().then((_) {
      BaseModel model = new BaseModel();
      model.put(FILE_URL, _.toString());
      model.put(REFERENCE, ref.fullPath);
      model.saveItem(REFERENCE_BASE, false);

      print("Url: ${_.toString()}");
      onComplete(_.toString(),null);
    }, onError: (error) {
      print("Error: $error");
      onComplete(null,error);
    });
  }
*/
*/

  postChatDoc() async {
    final path = await FlutterDocumentPicker.openDocument();

    var file = File(path);
    if ((await file.length()) > 30000000) {
      showErrorDialog(context, "Document should not be more than 30MB");
      return;
    }

    String ext = Path.extension(file.path);
    String fileName = Path.basename(file.path);
    String fileSize = filesize(await file.length());

    yesNoDialog(context, "Send File",
        "Are you sure you want to send this file \"$fileName\"", () {
      final String id = getRandomId();
      final BaseModel model = new BaseModel();
      model.put(CHAT_ID, chatId);
      model.put(TYPE, CHAT_TYPE_DOC);

//          model.put(
//              PARTIES,
//              [userModel.getObjectId(), otherUser.getObjectId()]);

      model.put(FILE_PATH, file.path);
      model.put(FILE_EXTENSION, ext.toLowerCase());
      model.put(FILE_SIZE, fileSize);
      model.put(FILE_NAME, fileName);

      model.put(OBJECT_ID, id);
      model.put(DATABASE_NAME, CHAT_BASE);

      //chatList.insert(0, model);

      upOrDown.add(model.getObjectId());

      if (replyModel != null) {
        model.put(REPLY_DATA, replyModel.items);
        replyModel = null;
      }
      model.saveItem(CHAT_BASE, true, document: id, onComplete: () {
        saveChatFile(model, FILE_PATH, FILE_URL, () {
          addChatToList(model);
          setState(() {});
          pushChat('sent a document');
        });
      });

      addChatToList(model);
      setState(() {});
      scrollToBottom();
    });
  }

  postChatVideo(String path, int duration, String thumb) async {
    final String id = getRandomId();
    final BaseModel model = new BaseModel();
    model.put(CHAT_ID, chatId);
//      model.put(
//          PARTIES,
//          [userModel.getObjectId(), otherUser.getObjectId()]);

    model.put(TYPE, CHAT_TYPE_VIDEO);
    model.put(THUMBNAIL_PATH, thumb);
    model.put(IMAGE_PATH, path);
    String length = getTimerText(duration);

    print("video duration $length");

    model.put(VIDEO_LENGTH, length);
    model.put(OBJECT_ID, id);
    model.put(DATABASE_NAME, CHAT_BASE);

    //chatList.insert(0, model);

    upOrDown.add(model.getObjectId());

    if (replyModel != null) {
      model.put(REPLY_DATA, replyModel.items);
      replyModel = null;
    }
    model.saveItem(CHAT_BASE, true, document: id, onComplete: () {
      saveChatVideo(model, () {
        addChatToList(model);
        setState(() {});
        pushChat('sent a video');
      });
    });

    addChatToList(model);
    setState(() {});
    scrollToBottom();
  }

  postChatAudio() async {
    String path = await localPath;
    File newFile = await File(recordingFilePath).copy(
        "$path/${DateTime.now().millisecondsSinceEpoch}${pathLib.basename(recordingFilePath)}");

    print("Old Path: ${recordingFilePath}");
    print("New Path: ${newFile.path}");
    final String id = getRandomId();
    BaseModel chat = BaseModel();
    chat.put(CHAT_ID, chatId);
//      chat.put(PARTIES, [userModel.getUserId(), otherUser.getObjectId()]);
    chat.put(TYPE, CHAT_TYPE_REC);
    chat.put(AUDIO_LENGTH, recordTimerText);
    chat.put(AUDIO_PATH, newFile.path);
    chat.put(OBJECT_ID, id);

    if (replyModel != null) {
      chat.put(REPLY_DATA, replyModel.items);
      replyModel = null;
    }
    chat.saveItem(CHAT_BASE, true, document: id, onComplete: () {
      pushChat("a voice note");
    });
    handleAudio(chat);
    addChatToList(chat);
    setState(() {});
    scrollToBottom();
  }

  scrollToBottom() async {
    if (chatList.length < 2) return;
    await Future.delayed(Duration(seconds: 1));
    try {
      messageListController.jumpTo(
        index: 0, /*duration: Duration(milliseconds: 500),curve: Curves.ease*/
      );
      if (unreadPosition != -1) {
        unreadPosition = -1;
        setState(() {});
      }
    } catch (e) {}
  }

  updateTyping(bool typing) {
    userModel
      ..put(REC_ID, null)
      ..put(TYPING_ID, typing ? chatId : null)
      ..updateItems();
  }

  updateRecording(bool recording) {
    userModel
      ..put(TYPING_ID, null)
      ..put(REC_ID, recording ? chatId : null)
      ..updateItems();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    if (state == AppLifecycleState.paused) {
      visibleChatId = "";
      updateTyping(false);
      try {
        typingSoundController.pause();
        messageSoundController.pause();
      } catch (e) {}
    }
    if (state == AppLifecycleState.resumed) {
      visibleChatId = chatId;
    }

    super.didChangeAppLifecycleState(state);
  }

  prepareRecording(u) async {
    if (recording) return;
    recording = true;
    xPosition = u.globalPosition.dx;
    yPosition = u.globalPosition.dy;
    cancelled = false;
    stopUpdatingPosition = false;
    recordTimer = 0;
    recordTimerText = "00:00";
    recordingOpacity = 1;
    createRecordTimer();
    startRecorder();
    updateRecording(true);
    setState(() {});
  }

  stopRecording() {
    if (!recording) return;
    recording = false;
    xPosition = -1;
    yPosition = -1;
    setState(() {});
    stopRecorder();
    updateRecording(false);
    recordTimer = 0;
    setState(() {});
    if (!cancelled && !tooShort) {
      postChatAudio();
    }
  }

  double bSize = 100;
//  double cSize = 0;
  //double cSize1 = 0;
//  double holdOpacity = 0;
  String recLength = "";
  String recordingFilePath = "";
  FlutterSound flutterSound;
  VideoPlayerController audioController;
  bool timerCounting = false;
  int timerCount = 4;
  double timerOpacity = 0;
  bool cancelled = false;
  bool stopUpdatingPosition = false;
  bool recording = false;
  int recordTimer = 0;
  int MAX_ABOUT_ME_LENGTH = 30;
  String recordTimerText = "00:00";
  double recordingOpacity = 1;
  createRecordTimer() {
    Future.delayed(Duration(seconds: 1), () {
      if (!recording) {
        return;
      }
      recordTimer++;
      if (recordTimer > 30) {
        stopRecording();
        return;
      }
      recordingOpacity = recordingOpacity == 1 ? 0 : 1;

      int min = recordTimer ~/ 60;
      int sec = recordTimer % 60;

      String m = min.toString();
      String s = sec.toString();

      String ms = m.length == 1 ? "0$m" : m;
      String ss = s.length == 1 ? "0$s" : s;

      recordTimerText = "$ms:$ss";

      setState(() {});
      createRecordTimer();
    });
  }

  void startRecorder() async {
    try {
      Directory tempDir = await getTemporaryDirectory();
      t_CODEC _codec = t_CODEC.CODEC_AAC;
      recordingFilePath = await flutterSound.startRecorder(
        uri: '${tempDir.path}/sound.aac',
        codec: _codec,
      );

      print("Rec at $recordingFilePath");
    } catch (err) {
      print('startRecorder error: ${err.toString()}');
    }
  }

  bool tooShort = false;
  void stopRecorder() async {
    recLength = recordTimerText;
    if (recordTimer < 1) {
      tooShort = true;
      setState(() {});
    }
    try {
      await flutterSound.stopRecorder();
    } catch (err) {
      print('stopRecorder error: $err');
    }
    if (tooShort || cancelled) {
      File(recordingFilePath).delete();
      Vibration.vibrate(duration: 100);
    }

    Future.delayed(Duration(seconds: 1), () {
      tooShort = false;
      cancelled = false;
      setState(() {});
    });
  }

  loadRecorder() async {
    flutterSound = new FlutterSound();
    flutterSound.setSubscriptionDuration(0.01);
    flutterSound.setDbPeakLevelUpdate(0.8);
    flutterSound.setDbLevelEnabled(true);

    print("Loaded Recorder");
//    flutterSound = new FlutterSound();
//    flutterSound.setSubscriptionDuration(0.01);
//    flutterSound.setDbPeakLevelUpdate(0.8);
//    flutterSound.setDbLevelEnabled(true);
//    flutterSoundRecorder = await FlutterSoundRecorder().initialize();
  }

  holdToRecord(u) {
    tappingDown = true;
//    FocusScope.of(context).requestFocus(FocusNode());
    Future.delayed(Duration(milliseconds: 500), () {
      if (!tappingDown) {
        tooShort = true;
        Vibration.vibrate(duration: 100);
        setState(() {});
        Future.delayed(Duration(seconds: 1), () {
          tooShort = false;
          setState(() {});
        });
        return;
      }
      prepareRecording(u);
    });
  }

  scrollToMessage(String id) {
    if (!mounted) return;
    showProgress(true, context);
    int position = getMessagePosition(id);

    print("The position: $position");
    if (position != -1) {
      Future.delayed(Duration(milliseconds: 1000), () {
        showProgress(false, context);

        Future.delayed(Duration(seconds: 1), () {
          messageListController.jumpTo(
            index: position,
          );

          Future.delayed(Duration(milliseconds: 500), () {
            blinkPosition = position;
            setState(() {});
            Future.delayed(Duration(seconds: 1), () {
              blinkPosition = -1;
              setState(() {});
            });
          });
        });
      });
      return;
    }
    if (shownChatsCount >= chatList.length) {
      print("Ignoring...");
      Future.delayed(Duration(milliseconds: 500), () {
        showProgress(false, context);
      });
      return;
    }

    loadMorePrev();
    Future.delayed(Duration(milliseconds: 1000), () {
      scrollToMessage(id);
    });
  }

  getMessagePosition(String id) {
    print("Finding Position...");
//      if (shownChatsCount >= chatList.length) return -1;

    for (int i = 0; i < shownChatsCount; i++) {
      BaseModel bm = chatList[i];
      if (chatRemoved(bm)) continue;
      if (bm.getObjectId() == id) {
        return i;
      }
    }

    return -1;
  }

  loadMorePrev() async {
    shownChatsCount = shownChatsCount + countIncrement;
    shownChatsCount =
        shownChatsCount >= chatList.length ? chatList.length : shownChatsCount;
    setState(() {});
  }

  incomingChatText(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }

    String message = chat.getString(MESSAGE);

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        new GestureDetector(
          onLongPress: () {
            showChatOptions(context, chat);
          },
          child: Container(
              margin: EdgeInsets.fromLTRB(60, 0, 60, 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  if (replyData.items.isNotEmpty)
                    chatReplyWidget(
                      replyData,
                    ),
                  Card(
                    clipBehavior: Clip.antiAlias,
                    color: white,
                    /*shadowColor: black.withOpacity(.3),*/
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: new Container(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      decoration: BoxDecoration(
//                    color: white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            message,
                            textAlign: TextAlign.end,
                            style: textStyle(false, 17, black),
                          ),
                          addSpace(3),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (splitMode)
                                Flexible(
                                  child: Text(
                                    getChatSender(chat),
//                                "John John John John John John John John John John John ",
                                    style: textStyle(true, 12, light_green3),
                                    maxLines: 1,
                                  ),
                                ),
                              if (splitMode) addSpaceWidth(5),
                              Text(
                                getChatTime(chat.getInt(TIME)),
                                style:
                                    textStyle(false, 12, black.withOpacity(.3)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ),
        chatUserImage(chat: chat)
      ],
    );
  }

  incomingChatDeleted(context, BaseModel chat) {
    return Container();
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    return new Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        GestureDetector(
          onLongPress: () {
            showChatOptions(context, chat, deletedChat: true);
          },
          child: Container(
              margin: EdgeInsets.fromLTRB(60, 0, 60, 15),
              child: Card(
                clipBehavior: Clip.antiAlias,
                color: white,
                elevation: 5,
                /*shadowColor: black.withOpacity(.3),*/
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(0),
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                )),
                child: new Container(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  decoration: BoxDecoration(
//                    color: white,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Deleted",
                            style: textStyle(false, 15, black),
                          ),
                          addSpaceWidth(5),
                          Icon(
                            Icons.info,
                            color: red0,
                            size: 17,
                          )
                        ],
                      ),
                      /*addSpace(3),
                Text(
                  */ /*timeAgo.format(
                        DateTime.fromMillisecondsSinceEpoch(
                            chat.getTime()),
                        locale: "en_short")*/ /*
                  getChatTime(chat.getInt(TIME)),
                  style: textStyle(false, 12, black.withOpacity(.3)),
                ),*/
                    ],
                  ),
                ),
              )),
        ),
        chatUserImage(chat: chat)
      ],
    );
  }

  incomingChatTyping(context, {@required bool typing}) {
    return new Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        Container(
            height: 40,
            margin: EdgeInsets.fromLTRB(60, 0, 60, 15),
            child: Column(
              children: [
                Card(
                  clipBehavior: Clip.antiAlias, color: white,
                  elevation: 0, //shadowColor: black.withOpacity(.3),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(0),
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  )),
                  child: new Container(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    decoration: BoxDecoration(
//                  color: white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 5,
                              height: 5,
                              decoration: BoxDecoration(
                                  color: light_green3, shape: BoxShape.circle),
                            ),
                            addSpaceWidth(5),
                            Container(
                              width: 4,
                              height: 4,
                              decoration: BoxDecoration(
                                  color: light_green3, shape: BoxShape.circle),
                            ),
                            addSpaceWidth(5),
                            Container(
                              width: 3,
                              height: 3,
                              decoration: BoxDecoration(
                                  color: light_green3, shape: BoxShape.circle),
                            ),
                            addSpaceWidth(5),
                            if (!typing)
                              Icon(
                                Icons.mic,
                                color: light_green3,
                                size: 12,
                              )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )),
        chatUserImage()
      ],
    );
  }

  outgoingChatText(context, BaseModel chat, bool firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String message = chat.getString(MESSAGE);

    bool read = chat.getList(READ_BY).contains(getTheOtherId(chatSetup));

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        showChatOptions(context, chat);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(60, 0, 15, 15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (replyData.items.isNotEmpty)
              chatReplyWidget(
                replyData,
              ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Card(
                    elevation: 12,
                    color: appColor,
                    shadowColor: black.withOpacity(.3),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(0),
                      topLeft: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: new Container(
                      margin: const EdgeInsets.fromLTRB(15, 10, 20, 10),
                      child: Text(
                        message,
                        style: textStyle(false, 17, white),
                      ),
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: light_green3,
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }

  incomingChatDoc(context, BaseModel chat, bool exists, onComplete) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }

    String fileUrl = chat.getString(FILE_URL);
    String fileName = chat.getString(FILE_NAME);
    String size = chat.getString(FILE_SIZE);
    String ext = chat.getString(FILE_EXTENSION);
    bool downloading = upOrDown.contains(chat.getObjectId());
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//  return Container();
    return new Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        Opacity(
          opacity: fileUrl.isEmpty ? (.4) : 1,
          child: new GestureDetector(
            onLongPress: () {
              showChatOptions(context, chat);
            },
            onTap: () async {
              if (fileUrl.isEmpty || !exists) return;

              /*if (!exists) {
              downloadChatFile(chat, false, onComplete);
              return;
            }*/

              String fileName =
                  "${chat.getObjectId()}.${chat.getString(FILE_EXTENSION)}";
              File file = await getDirFile(fileName);
              await OpenFile.open(file.path);
            },
            child: Container(
              width: 200,
              margin: EdgeInsets.fromLTRB(60, 0, 0, 15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  if (replyData.items.isNotEmpty)
                    chatReplyWidget(
                      replyData,
                    ),
                  new Card(
                    clipBehavior: Clip.antiAlias,
                    color: white,
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                          width: 250,
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
//                        color: black.withOpacity(.2),
                              borderRadius: BorderRadius.circular(5)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              if (!exists || fileUrl.isNotEmpty)
                                Container(
                                  width: 27,
                                  height: 27,
                                  margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: appColor,
                                      border: Border.all(
                                          width: 1, color: appColor)),
                                  child: GestureDetector(
                                    onTap: () {
                                      if (!downloading) {
                                        downloadChatFile(chat, onComplete);
                                      }
                                    },
                                    child: (!downloading)
                                        ? Container(
                                            width: 27,
                                            height: 27,
                                            child: Icon(
                                              Icons.arrow_downward,
                                              color: white,
                                              size: 15,
                                            ),
                                          )
                                        : Container(
                                            margin: EdgeInsets.all(3),
                                            child: CircularProgressIndicator(
                                              //value: 20,
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      white),
                                              strokeWidth: 2,
                                            ),
                                          ),
                                  ),
                                ),
                              addSpaceWidth(5),
                              //addSpaceWidth(10),
                              Image.asset(
                                getExtImage(ext),
                                width: 20,
                                height: 20,
                              ),
                              addSpaceWidth(10),
                              new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      fileName,
                                      maxLines: 1,
                                      //overflow: TextOverflow.ellipsis,
                                      style: textStyle(false, 14, black),
                                    ),
                                    addSpace(3),
                                    Text(
                                      size,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: textStyle(
                                          false, 12, black.withOpacity(.5)),
                                    ),
                                  ],
                                ),
                              ),
                              //addSpaceWidth(5),
                            ],
                          ),
                        ),

//                  addSpace(3),
//                  Padding(
//                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
//                    child: Text(
//                      getChatTime(chat.getInt(TIME)),
//                      style: textStyle(false, 12, black.withOpacity(.3)),
//                    ),
//                  ),
                      ],
                    ),
                  ),
                  addSpace(3),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (splitMode)
                        Flexible(
                          child: Text(
                            getChatSender(chat),
//                                "John John John John John John John John John John John ",
                            style: textStyle(true, 12, light_green3),
                            maxLines: 1,
                          ),
                        ),
                      if (splitMode) addSpaceWidth(5),
                      Text(
                        getChatTime(chat.getInt(TIME)),
                        style: textStyle(false, 12, black.withOpacity(.3)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        chatUserImage(chat: chat)
      ],
    );
  }

  outgoingChatDoc(context, BaseModel chat, onSaved, bool firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String fileName = chat.getString(FILE_NAME);
    String size = chat.getString(FILE_SIZE);
    String ext = chat.getString(FILE_EXTENSION);
    bool uploading = upOrDown.contains(chat.getObjectId());
    String filePath = chat.getString(FILE_PATH);
    String fileUrl = chat.getString(FILE_URL);

    bool read = chat.getList(READ_BY).contains(getTheOtherId(chatSetup));
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        //long pressed chat...
        showChatOptions(context, chat);
      },
      onTap: () async {
        if (fileUrl.isEmpty) return;

        await OpenFile.open(filePath);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 15, 15),
        width: 220,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 200,
                  child: new Card(
                    //color: read ? appColor.withOpacity(read?1:0.7) : light_green3,
                    color: appColor.withOpacity(read ? 1 : 0.7),
                    clipBehavior: Clip.antiAlias,
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    shape: RoundedRectangleBorder(
                        borderRadius: chat.myItem()
                            ? BorderRadius.only(
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25),
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(0),
                              )
                            : BorderRadius.only(
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25),
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(0),
                              )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                          width: 250,
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
//                  color: black.withOpacity(.2),
                              borderRadius: BorderRadius.circular(5)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              //addSpaceWidth(10),
                              fileUrl.isNotEmpty
                                  ? Container()
                                  : new Container(
                                      width: 27,
                                      height: 27,
                                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: appColor.withOpacity(.3),
                                          border: Border.all(
                                              width: 1, color: appColor)),
                                      child: GestureDetector(
                                        onTap: () {
                                          if (!uploading) {
                                            saveChatFile(chat, FILE_PATH,
                                                FILE_URL, onSaved);
                                            onSaved();
                                          }
                                        },
                                        child: uploading
                                            ? Container(
                                                margin: EdgeInsets.all(3),
                                                child:
                                                    CircularProgressIndicator(
                                                  //value: 20,
                                                  valueColor:
                                                      AlwaysStoppedAnimation<
                                                          Color>(white),
                                                  strokeWidth: 2,
                                                ),
                                              )
                                            : Container(
                                                width: 27,
                                                height: 27,
                                                child: Icon(
                                                  Icons.arrow_upward,
                                                  color: white,
                                                  size: 15,
                                                ),
                                              ),
                                      )),

                              new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      fileName,
                                      maxLines: 1,
                                      //overflow: TextOverflow.ellipsis,
                                      style: textStyle(true, 14, white),
                                    ),
                                    addSpace(3),
                                    Text(
                                      size,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: textStyle(
                                          false, 12, white.withOpacity(.5)),
                                    ),
                                  ],
                                ),
                              ),
                              //addSpaceWidth(5),

                              addSpaceWidth(10),
                              Image.asset(
                                getExtImage(ext),
                                width: 20,
                                height: 20,
                              ),

                              //addSpaceWidth(5),
                            ],
                          ),
                        ),
                        //addSpace(3),
//            Padding(
//              padding: const EdgeInsets.fromLTRB(15, 0, 10, 10),
//              child: Text(
//                getChatTime(chat.getInt(TIME)),
//                style: textStyle(false, 12, white.withOpacity(.3)),
//              ),
//            ),
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: light_green3,
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }

  loadVideo() async {
    await Future.delayed(Duration(seconds: 1));
    print("loading chat videos...");
    for (int i = 0; i < chatList.length; i++) {
      BaseModel model = chatList[i];
      if (model.getInt(TYPE) == CHAT_TYPE_VIDEO) {
        VideoController.loadVideo(model, () {
          if (mounted) setState(() {});
        });
      }
    }
  }

  viewMedia(BaseModel chat) {
    List photoList = [];
    for (int i = 0; i < chatList.length; i++) {
      BaseModel model = chatList[i];
      if (model.getInt(TYPE) == CHAT_TYPE_IMAGE ||
          model.getInt(TYPE) == CHAT_TYPE_VIDEO) {
        photoList.add(model);
      }
    }

    int defPosition = 0;
    List mediaList = [];
    for (int i = 0; i < photoList.length; i++) {
      BaseModel model = photoList[i];
      if (model.getObjectId() == chat.getObjectId()) {
        defPosition = i;
      }
      if (model.getInt(TYPE) == CHAT_TYPE_IMAGE) {
        mediaList.add({IMAGE_URL: model.getString(IMAGE_URL)});
      }
      if (model.getInt(TYPE) == CHAT_TYPE_VIDEO) {
        String path = model.getString(IMAGE_URL);
        String thumbnail = model.getString(THUMBNAIL_URL);
        mediaList.add({
          OBJECT_ID: model.getObjectId(),
          IMAGE_URL: path,
          THUMBNAIL_URL: thumbnail,
          IS_VIDEO: true
        });
      }
    }
    mediaList = List.from(mediaList.reversed);

    print(mediaList);

    pushAndResult(
        context,
        PreviewPostImages(
          mediaList.map((e) => BaseModel(items: e)).toList(),
          [],
          indexOf: mediaList.length - (defPosition + 1),
          heroTag: getRandomId(),
        ));
  }

  incomingChatImage(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    //List images = chat.getList(IMAGES);
    String firstImage = chat.getString(IMAGE_URL);

//  return Container();
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        new GestureDetector(
          onLongPress: () {
            //long pressed chat...
            showChatOptions(context, chat);
          },
          onTap: () {
            if (firstImage.isEmpty) return;
            viewMedia(chat);
          },
          child: Container(
            width: 250,
//            height: 200,
            margin: EdgeInsets.fromLTRB(65, 0, 40, 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                if (replyData != null)
                  chatReplyWidget(
                    replyData,
                  ),
                Container(
                  width: 250,
                  height: 200,
                  child: new Card(
                    clipBehavior: Clip.antiAlias,
                    color: white,
                    /*shadowColor: black.withOpacity(.3),*/
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        CachedNetworkImage(
                            imageUrl: firstImage,
                            width: 200,
                            height: 200,
                            fit: BoxFit.cover,
                            placeholder: (c, p) {
                              return placeHolder(200, width: double.infinity);
                            }),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: gradientLine(height: 40),
                        ),
                        Align(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  if (splitMode)
                                    Flexible(
                                      child: Text(
                                        getChatSender(chat),
//                                "John John John John John John John John John John John ",
                                        style: textStyle(true, 12, white),
                                        maxLines: 1,
                                      ),
                                    ),
                                  if (splitMode) addSpaceWidth(5),
                                  Text(
                                    getChatTime(chat.getInt(TIME)),
                                    style: textStyle(
                                        false, 12, white.withOpacity(.3)),
                                  ),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        chatUserImage(chat: chat)
      ],
    );
  }

  incomingChatLocation(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    //List images = chat.getList(IMAGES);
    String firstImage = chat.getString(IMAGE_URL);
    String taggedPlace = chat.getString(PLACE_NAME);
    double taggedPlaceLat = chat.getDouble(LATITUDE);
    double taggedPlaceLong = chat.getDouble(LONGITUDE);

//  return Container();
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        new GestureDetector(
          onLongPress: () {
            //long pressed chat...
            showChatOptions(context, chat);
          },
          onTap: () {
            openMap(taggedPlaceLat, taggedPlaceLong);
          },
          child: Container(
            width: 250,
//            height: 200,
            margin: EdgeInsets.fromLTRB(65, 0, 40, 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                if (replyData != null)
                  chatReplyWidget(
                    replyData,
                  ),
                Container(
                  width: 250,
                  height: 100,
                  child: new Card(
                    clipBehavior: Clip.antiAlias,
                    color: white,
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Image.asset(
                          google_map,
                          width: 200,
                          height: 100,
                          fit: BoxFit.cover,
                        ),
                        /* CachedNetworkImage(
                            imageUrl: firstImage,
                            width: 200,
                            height: 200,
                            fit: BoxFit.cover,
                            placeholder: (c, p) {
                              return placeHolder(200, width: double.infinity);
                            }),*/
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: gradientLine(height: 100),
                        ),
                        Align(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  if (splitMode)
                                    Flexible(
                                      child: Text(
                                        getChatSender(chat),
//                                "John John John John John John John John John John John ",
                                        style: textStyle(true, 12, white),
                                        maxLines: 1,
                                      ),
                                    ),
                                  if (splitMode) addSpaceWidth(5),
                                  Text(
                                    getChatTime(chat.getInt(TIME)),
                                    style: textStyle(
                                        false, 12, white.withOpacity(.3)),
                                  ),
                                ],
                              ),
                            )),
                        Align(
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Icon(
                                  Icons.location_on,
                                  size: 24,
                                  color: white,
                                ),
                                Flexible(
                                  child: Text(
                                    taggedPlace,
                                    style: textStyle(
                                        false, 12, white.withOpacity(.8)),
                                  ),
                                )
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        chatUserImage(chat: chat)
      ],
    );
  }

  outgoingChatImage(context, BaseModel chat, onSaved, firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String imageUrl = chat.getString(IMAGE_URL);
    String imagePath = chat.getString(IMAGE_PATH);
    bool uploading = upOrDown.contains(chat.getObjectId());

    bool read = chat.getList(READ_BY).contains(getTheOtherId(chatSetup));

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        //long pressed chat...
        showChatOptions(context, chat);
      },
      onTap: () {
        if (imageUrl.isEmpty) return;
        viewMedia(chat);
//        pushAndResult(context, ViewImage([imageUrl], 0));
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 15, 15),
        width: 270,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: 230,
                  height: 200,
                  child: new Card(
                    color: light_green3,
                    clipBehavior: Clip.antiAlias,
//                  margin: EdgeInsets.all(0),
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(0),
                    )),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        imageUrl.isEmpty
                            ? Image.file(
                                File(imagePath),
                                fit: BoxFit.cover,
                              )
                            : CachedNetworkImage(
                                imageUrl: imageUrl,
                                fit: BoxFit.cover,
                                placeholder: (c, p) {
                                  return placeHolder(200,
                                      width: double.infinity);
                                }),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: gradientLine(height: 40),
                        ),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                getChatTime(chat.getInt(TIME)),
                                style:
                                    textStyle(false, 12, white.withOpacity(.3)),
                              ),
                            )),
                        imageUrl.isNotEmpty
                            ? Container()
                            : Align(
                                alignment: Alignment.center,
                                child: GestureDetector(
                                  onTap: () {
                                    if (!uploading) {
                                      saveChatFile(
                                          chat, IMAGE_PATH, IMAGE_URL, onSaved);
                                      onSaved();
                                    }
                                  },
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        color: black.withOpacity(.9),
                                        border:
                                            Border.all(color: white, width: 1),
                                        shape: BoxShape.circle),
                                    child: uploading
                                        ? Container(
                                            margin: EdgeInsets.all(5),
                                            child: CircularProgressIndicator(
                                              //value: 20,
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      white),
                                              strokeWidth: 2,
                                            ),
                                          )
                                        : Center(
                                            child: Icon(
                                              Icons.arrow_upward,
                                              color: white,
                                              size: 20,
                                            ),
                                          ),
                                  ),
                                ),
                              )
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: light_green3,
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }

  outgoingChatLocation(context, BaseModel chat, onSaved, firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String imageUrl = chat.getString(IMAGE_URL);
    String taggedPlace = chat.getString(PLACE_NAME);
    double taggedPlaceLat = chat.getDouble(LATITUDE);
    double taggedPlaceLong = chat.getDouble(LONGITUDE);

    String imagePath = chat.getString(IMAGE_PATH);
    bool uploading = upOrDown.contains(chat.getObjectId());

    bool read = chat.getList(READ_BY).contains(getTheOtherId(chatSetup));

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        //long pressed chat...
        showChatOptions(context, chat);
      },
      onTap: () {
        openMap(taggedPlaceLat, taggedPlaceLong);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 15, 15),
        width: 270,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: 230,
                  height: 100,
                  child: new Card(
                    color: light_green3,
                    clipBehavior: Clip.antiAlias,
//                  margin: EdgeInsets.all(0),
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(0),
                    )),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Image.asset(
                          google_map,
                          width: 200,
                          height: 100,
                          fit: BoxFit.cover,
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: gradientLine(height: 100, alpha: .8),
                        ),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                getChatTime(chat.getInt(TIME)),
                                style:
                                    textStyle(false, 12, white.withOpacity(.3)),
                              ),
                            )),
                        Align(
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Icon(
                                  Icons.location_on,
                                  size: 24,
                                  color: white,
                                ),
                                Flexible(
                                  child: Text(
                                    taggedPlace,
                                    style: textStyle(
                                        false, 12, white.withOpacity(.8)),
                                  ),
                                )
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: light_green3,
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }

  incomingChatVideo(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }

    String videoUrl = chat.getString(IMAGE_URL);
    String thumb = chat.getString(THUMBNAIL_URL);
    String videoLenght = chat.getString(VIDEO_LENGTH);
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      alignment: Alignment.centerLeft,
      children: <Widget>[
        Opacity(
          opacity: videoUrl.isEmpty ? (.4) : 1,
          child: new GestureDetector(
            onLongPress: () {
              showChatOptions(context, chat);
              //long pressed chat...
            },
            child: Container(
              width: 250,
              margin: EdgeInsets.fromLTRB(65, 0, 40, 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  if (replyData != null)
                    chatReplyWidget(
                      replyData,
                    ),
                  new Card(
                    clipBehavior: Clip.antiAlias,
                    color: white,
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                            color: light_green3.withOpacity(.1),
                            width: 250,
                            height: 150,
                            child: new GestureDetector(
                                onTap: () {
                                  if (videoUrl.isEmpty) return;

                                  viewMedia(chat);

//                                  pushAndResult(context,
//                                      PlayVideo(chat.getObjectId(), videoUrl));
                                },
                                child: new Stack(
                                  children: <Widget>[
                                    thumb.isEmpty
                                        ? Container()
                                        : CachedNetworkImage(
                                            imageUrl: thumb,
                                            fit: BoxFit.cover,
                                            width: double.infinity,
                                            height: 250,
                                          ),
                                    Center(
                                      child: new Container(
                                        width: 40,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: black.withOpacity(.9),
                                            border: Border.all(
                                                color: white, width: 1),
                                            shape: BoxShape.circle),
                                        child: videoUrl.isNotEmpty
                                            ? Center(
                                                child: Icon(
                                                  Icons.play_arrow,
                                                  color: white,
                                                  size: 20,
                                                ),
                                              )
                                            : Container(),
                                      ),
                                    ),
                                    new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Expanded(flex: 1, child: Container()),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      8, 0, 0, 8),
                                              child: Text(
                                                getChatTime(chat.getInt(TIME)),
                                                style: textStyle(false, 12,
                                                    white.withOpacity(.3)),
                                              ),
                                            ),
                                            Flexible(
                                                flex: 1, child: Container()),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                  color: black.withOpacity(.9),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        8, 4, 8, 4),
                                                child: Text(videoLenght,
                                                    style: textStyle(
                                                        false, 12, white)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ))),
                        /* addSpace(3),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 5, 15, 10),
                        child: Text(
                          getChatTime(chat.getInt(TIME)),
                          style: textStyle(false, 12, black.withOpacity(.3)),
                        ),
                      ),*/
                      ],
                    ),
                  ),
                  addSpace(3),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (splitMode)
                        Flexible(
                          child: Text(
                            getChatSender(chat),
//                                "John John John John John John John John John John John ",
                            style: textStyle(true, 12, light_green3),
                            maxLines: 1,
                          ),
                        ),
                      if (splitMode) addSpaceWidth(5),
                      Text(
                        getChatTime(chat.getInt(TIME)),
                        style: textStyle(false, 12, black.withOpacity(.3)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        chatUserImage(chat: chat)
      ],
    );
  }

  outgoingChatVideo(context, BaseModel chat, onSaved, firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String videoUrl = chat.getString(IMAGE_URL);
    String videoPath = chat.getString(VIDEO_PATH);
    String thumbPath = chat.getString(THUMBNAIL_PATH);
    String thumb = chat.getString(THUMBNAIL_URL);
    String videoLength = chat.getString(VIDEO_LENGTH);
    bool uploading = upOrDown.contains(chat.getObjectId());

    bool read = chat.getList(READ_BY).contains(getTheOtherId(chatSetup));

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        showChatOptions(context, chat);
        //long pressed chat...
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 20, 15),
        width: 270,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 250,
                  child: new Card(
                    color: light_green3,
                    clipBehavior: Clip.antiAlias,
                    elevation: 12,
                    shadowColor: black.withOpacity(.3),
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(0),
                    )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                            color: light_green3.withOpacity(.1),
                            width: 250,
                            height: 150,
                            child: new GestureDetector(
                                onTap: () {
                                  if (videoUrl.isNotEmpty) {
                                    viewMedia(chat);
                                  } else {
                                    if (!uploading) {
                                      saveChatVideo(chat, onSaved);
                                      onSaved();
                                    }
                                  }
                                },
                                child: new Stack(
                                  fit: StackFit.expand,
                                  children: <Widget>[
                                    thumb.isEmpty
                                        ? Image.file(
                                            File(thumbPath),
                                            fit: BoxFit.cover,
                                          )
                                        : CachedNetworkImage(
                                            imageUrl: thumb,
                                            fit: BoxFit.cover,
//                                        placeholder: (c, p) {
//                                          return placeHolder(250,
//                                              width: double.infinity);
//                                        }
                                          ),
                                    /*
                                  thumb.isEmpty
                                      ? Container()
                                      : CachedNetworkImage(
                                          imageUrl: thumb,
                                          fit: BoxFit.cover,
                                          width: double.infinity,
                                          height: 250,
                                        ),*/
                                    Center(
                                      child: new Container(
                                        width: 40,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: black.withOpacity(.9),
                                            border: Border.all(
                                                color: white, width: 1),
                                            shape: BoxShape.circle),
                                        child: videoUrl.isNotEmpty
                                            ? Center(
                                                child: Icon(
                                                  Icons.play_arrow,
                                                  color: white,
                                                  size: 20,
                                                ),
                                              )
                                            : (!uploading)
                                                ? Center(
                                                    child: Icon(
                                                      Icons.arrow_upward,
                                                      color: white,
                                                      size: 20,
                                                    ),
                                                  )
                                                : Container(
                                                    margin: EdgeInsets.all(5),
                                                    child:
                                                        CircularProgressIndicator(
                                                      //value: 20,
                                                      valueColor:
                                                          AlwaysStoppedAnimation<
                                                              Color>(white),
                                                      strokeWidth: 2,
                                                    ),
                                                  ),
                                      ),
                                    ),
                                    new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Expanded(flex: 1, child: Container()),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                  color: black.withOpacity(.9),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        8, 4, 8, 4),
                                                child: Text(videoLength,
                                                    style: textStyle(
                                                        false, 12, white)),
                                              ),
                                            ),
                                            Flexible(
                                                flex: 1, child: Container()),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 0, 8, 8),
                                              child: Text(
                                                getChatTime(chat.getInt(TIME)),
                                                style: textStyle(false, 12,
                                                    white.withOpacity(.3)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ))),
                        //addSpace(3),
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: light_green3,
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }

  chatUserImage({BaseModel chat}) {
    String id = "";
    String image = "";

    if (chat != null) {
      id = chat.getUserId();
      image = chat.getString(USER_IMAGE);
    } else {
      id = otherUser.getObjectId();
      image = otherUser.getString(USER_IMAGE);
    }

    return Align(
      alignment: Alignment.bottomLeft,
      child: new GestureDetector(
        onTap: () {
          /* if (isFarm && !supportMode) {
            if (farmModel.items.isNotEmpty) {
              pushAndResult(
                  context,
                  FarmMain(
                    farmModel: farmModel,
                    */ /*fromChat: true,*/ /*
                  ));
            }
          }*/
//        pushAndResult(
//            context,
//            MyProfile(
//              fromChat: true,personModel: otherPerson,
//            ),opaque: false);
        },
        child: new Container(
          decoration: BoxDecoration(
            border: Border.all(width: 2, color: white),
            shape: BoxShape.circle,
          ),
          margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
          width: 40,
          height: 40,
          child: Stack(
            children: <Widget>[
              Card(
                margin: EdgeInsets.all(0),
                shape: CircleBorder(),
                clipBehavior: Clip.antiAlias,
                color: transparent,
                elevation: .5,
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 40,
                      height: 40,
                      color: light_green3,
                      child: Center(
                          child: Icon(
                        Icons.person,
                        color: white,
                        size: 15,
                      )),
                    ),
                    CachedNetworkImage(
                      width: 40,
                      height: 40,
                      imageUrl: image,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              if (isOnline)
                Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: white, width: 2),
                      color: red0,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  String getChatDate(int milli) {
    final formatter = DateFormat("MMM d 'AT' h:mm a");
    DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
    return formatter.format(date);
  }

  static String getChatTime(int milli) {
    final formatter = DateFormat("h:mm a");
    DateTime date = DateTime.fromMillisecondsSinceEpoch(milli);
    return formatter.format(date);
  }

  saveChatFile(BaseModel model, String pathKey, String urlKey, onSaved) {
    upOrDown.add(model.getObjectId());
    String path = model.getString(pathKey);
    uploadFile(
      File(path),
      (_, error) {
        upOrDown.removeWhere((s) => s == model.getObjectId());
        if (error != null) {
          return;
        }
        model.put(urlKey, _);
        model.updateItems();
        onSaved();
      },
    );
  }

  saveChatVideo(BaseModel model, onSaved) {
    String thumb = model.getString(THUMBNAIL_PATH);
    String videoPath = model.getString(IMAGE_PATH);
    String thumbUrl = model.getString(THUMBNAIL_URL);
    String videoUrl = model.getString(IMAGE_URL);

    print("url $videoUrl");
    print("path $videoPath");
    print("thumb $thumb");
    print("thumbUrl $thumb");

    bool uploadingThumb = thumbUrl.isEmpty;

    if (videoUrl.isNotEmpty) {
      onSaved();
      return;
    }

    upOrDown.add(model.getObjectId());

    print("hello $uploadingThumb");
    uploadFile(File(uploadingThumb ? thumb : videoPath), (_, error) {
      upOrDown.removeWhere((s) => s == model.getObjectId());
      if (error != null) {
        return;
      }
      model.put(uploadingThumb ? THUMBNAIL_URL : IMAGE_URL, _);
      model.updateItems();
      saveChatVideo(model, onSaved);
    });
  }

  downloadChatFile(BaseModel model, onComplete) async {
    String fileName =
        "${model.getObjectId()}.${model.getString(FILE_EXTENSION)}";
    File file = await getDirFile(fileName);
    upOrDown.add(model.getObjectId());
    onComplete();

    QuerySnapshot shots = await Firestore.instance
        .collection(REFERENCE_BASE)
        .where(FILE_URL, isEqualTo: model.getString(FILE_URL))
        .limit(1)
        .getDocuments();
    if (shots.documents.isEmpty) {
      upOrDown.removeWhere((s) => s == model.getObjectId());
      onComplete();
    } else {
      for (DocumentSnapshot doc in shots.docs) {
        if (!doc.exists || doc.data().isEmpty) continue;
        BaseModel model = BaseModel(doc: doc);
        String ref = model.getString(REFERENCE);
        Reference storageReference =
            FirebaseStorage.instance.ref().child(ref);
        storageReference.writeToFile(file).then((_) {
          //showError("Download Complete");
          upOrDown.removeWhere((s) => s == model.getObjectId());
          onComplete();
        }, onError: (error) {
          upOrDown.removeWhere((s) => s == model.getObjectId());
          onComplete();
        }).catchError((error) {
          upOrDown.removeWhere((s) => s == model.getObjectId());
          onComplete();
        });

        break;
      }
    }
  }

  showChatOptions(context, BaseModel chat, {bool deletedChat = false}) {
    int type = chat.getInt(TYPE);
    pushAndResult(
        context,
        listDialog(
          type == CHAT_TYPE_TEXT && !deletedChat
              ? ["Copy", "Delete"]
              : ["Delete"],
          //usePosition: false,
        ),
        //opaque: false,
        result: (_) {
      if (_ == "Copy") {
        //ClipboardManager.copyToClipBoard(chat.getString(MESSAGE));
      }
      if (_ == "Delete") {
        if (chat.myItem()) {
          chat.put(DELETED, true);
          chat.updateItems();
        } else {
          List hidden = List.from(chat.getList(HIDDEN));
          hidden.add(userModel.getObjectId());
          chat.put(HIDDEN, hidden);
          chat.updateItems();
        }
        refreshChatDates(force: true);
      }
    });
  }

  VideoPlayerController recAudioController;
  String currentPlayingAudio;

  bool recPlayEnded = false;
  List noFileFound = [];
  getChatAudioWidget(
      context, BaseModel chat, onEdited(bool removed), bool firstChat) {
    if (chat.getBoolean(DELETED)) {
      if (!chat.myItem()) {
        return incomingChatDeleted(context, chat);
      }
      return Container();
    }
//    return Container();
    bool read = chat.getList(READ_BY).contains(getTheOtherId(chatSetup));
    String audioUrl = chat.getString(AUDIO_URL);
    String audioPath = chat.getString(AUDIO_PATH);
    String audioLength = chat.getString(AUDIO_LENGTH);
    bool uploading = upOrDown.contains(chat.getObjectId());
    bool noFile = noFileFound.contains(chat.getObjectId());
    bool currentPlay = currentPlayingAudio == chat.getObjectId();
    bool isPlaying = recAudioController != null &&
        recAudioController.value.initialized &&
        recAudioController.value.isPlaying;
    var parts = audioPath.split("/");
    String baseFileName = parts[parts.length - 1];
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return Container(
//    height: chat.myItem()?null:40,
      width: chat.myItem() ? null : 270,
      margin: EdgeInsets.fromLTRB(0, 0, 0, chat.myItem() ? 15 : 10),
      child: Stack(
        children: [
          Align(
            alignment:
                !chat.myItem() ? Alignment.centerLeft : Alignment.centerRight,
            child: Opacity(
              opacity: audioUrl.isEmpty && !chat.myItem() ? (.4) : 1,
              child: new GestureDetector(
                onTap: () async {
                  if (audioUrl.isEmpty && !chat.myItem()) return;
                  if (uploading) return;
                  if (noFile) {
                    showMessage(context, Icons.error, red0, "File not found",
                        "This file no longer exist on your device");
                    return;
                  }

                  if (!chat.myItem()) {
                    String path = await localPath;
                    File file =
                        File("$path/${chat.getObjectId()}$baseFileName");
                    print("File Path: ${file.path}");
                    bool exists = await file.exists();
                    if (!exists) {
                      upOrDown.add(chat.getObjectId());
                      onEdited(false);
                      downloadFile(file, audioUrl, (e) {
                        upOrDown.removeWhere(
                            (element) => element == chat.getObjectId());
                        fileThatExists.add(chat.getObjectId());
                        onEdited(false);
                      });
                      return;
                    } else {
                      audioPath = file.path;
                    }
                  }

                  if (currentPlayingAudio == chat.getObjectId()) {
                    if (recAudioController != null &&
                        recAudioController.value.initialized) {
                      if (recAudioController.value.isPlaying) {
                        recAudioController.pause();
                      } else {
                        currentPlayingAudio = chat.getObjectId();
                        recAudioController.play();
                        recPlayEnded = false;
                        onEdited(false);
                      }
                    }
                  } else {
                    /*if (recAudioController != null) {
                    await recAudioController.pause();
                    await recAudioController.dispose();
                    recAudioController = null;
                  }*/
                    try {
                      await recAudioController.pause();
                    } catch (e) {}
                    try {
                      await recAudioController.dispose();
                    } catch (e) {}
                    recAudioController = null;

                    recAudioController =
                        VideoPlayerController.file(File(audioPath));
                    recAudioController.addListener(() async {
                      if (recAudioController != null) {
                        int currentTime =
                            recAudioController.value.position.inSeconds;
                        int fullTime = getSeconds(chat.getString(AUDIO_LENGTH));
                        print("FullTime $fullTime CurrentTime $currentTime");
                        if (currentTime >= fullTime - 1 && currentTime != 0) {
                          if (recPlayEnded) return;
                          recPlayEnded = true;
                          await recAudioController.pause();
                          await recAudioController.seekTo(Duration(seconds: 0));
                          print("Play Finished");
                          onEdited(false);
                          /* Future.delayed(Duration(milliseconds: 200),()async{
                            currentPlayingAudio="";
                            recAudioController=null;
                          });*/
                        }
                      }
                    });
                    recAudioController.initialize().then((value) {
                      currentPlayingAudio = chat.getObjectId();
                      recAudioController.play();
                      recPlayEnded = false;
                      onEdited(false);
                    }).catchError((e) {
                      showMessage(context, Icons.error, red0, "Audio Error",
                          "This audio recording is corrupted");
                    });
                  }
                },
                onLongPress: () {
                  showChatOptions(context, chat);
                },
                child: Container(
                  width: 200,
                  margin: EdgeInsets.fromLTRB(
                      chat.myItem() ? 0 : 70, 0, chat.myItem() ? 20 : 0, 0),
                  child: Column(
                    crossAxisAlignment: chat.myItem()
                        ? (CrossAxisAlignment.start)
                        : CrossAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (replyData != null)
                        chatReplyWidget(
                          replyData,
                        ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new Container(
                            height: 30,
                            width: 170,
                            color: transparent,
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 12,
                              shadowColor: black.withOpacity(.3),
                              shape: RoundedRectangleBorder(
                                  borderRadius: chat.myItem()
                                      ? BorderRadius.only(
                                          bottomLeft: Radius.circular(25),
                                          bottomRight: Radius.circular(25),
                                          topLeft: Radius.circular(25),
                                          topRight: Radius.circular(0),
                                        )
                                      : BorderRadius.only(
                                          bottomLeft: Radius.circular(25),
                                          bottomRight: Radius.circular(25),
                                          topLeft: Radius.circular(0),
                                          topRight: Radius.circular(25),
                                        )),
                              margin: EdgeInsets.all(0),
                              color: isPlaying && currentPlay
                                  ? appColor
                                  : appColor.withOpacity(.5),
                              child: Stack(
                                fit: StackFit.expand,
                                children: [
                                  /*LinearProgressIndicator(
                                    value:currentPlay?(playPosition / 100):0,
                                    backgroundColor: transparent,
                                    valueColor:
                                    AlwaysStoppedAnimation<Color>(black.withOpacity(.7)),
                                  ),*/
                                  Row(
                                    children: [
                                      addSpaceWidth(10),
                                      if (uploading)
                                        Container(
                                          width: 14,
                                          height: 14,
                                          child: CircularProgressIndicator(
                                            //value: 20,
                                            valueColor:
                                                AlwaysStoppedAnimation<Color>(
                                                    white),
                                            strokeWidth: 2,
                                          ),
                                        ),
                                      if (!uploading)
                                        chat.myItem()
                                            ? (Icon(
                                                currentPlay && isPlaying
                                                    ? (Icons.pause)
                                                    : Icons.play_circle_filled,
                                                color: white,
                                              ))
                                            : (Icon(
                                                !fileThatExists.contains(
                                                        chat.getObjectId())
                                                    ? Icons.file_download
                                                    : currentPlay && isPlaying
                                                        ? (Icons.pause)
                                                        : Icons
                                                            .play_circle_filled,
                                                color: white,
                                              )) /*FutureBuilder(
                                      builder: (c,d){
                                        if(!d.hasData)return Container();
                                        bool exists = d.data;
                                        return Icon(
                                          !exists?Icons.file_download:currentPlay && isPlaying?
                                          (Icons.pause):Icons.play_circle_filled,
                                          color: white,
                                        );
                                      },future: checkLocalFile("${chat.getObjectId()}$baseFileName"),
                                    )*/
                                      ,
                                      Flexible(
                                        fit: FlexFit.tight,
                                        child: Container(
                                          height: 2,
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              color: white,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                        ),
                                      ),
                                      Container(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 2, 5, 2),
                                        decoration: BoxDecoration(
                                            color: white,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(25))),
                                        child: Text(
                                          audioLength,
                                          style: textStyle(false, 12, appColor),
                                        ),
                                      ),
                                      addSpaceWidth(5),
                                      if (noFile)
                                        Container(
                                            padding: EdgeInsets.all(1),
                                            decoration: BoxDecoration(
                                                color: white,
                                                shape: BoxShape.circle),
                                            child: Icon(
                                              Icons.error,
                                              color: red0,
                                              size: 18,
                                            )),
                                      addSpaceWidth(10),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          if (chat.myItem())
                            if (read && firstChat)
                              Icon(
                                Icons.remove_red_eye,
                                size: 12,
                                color: light_green3,
                              ),
                        ],
                      ),
                      if (!chat.myItem() && splitMode)
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(35, 5, 0, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                if (splitMode)
                                  Flexible(
                                    child: Text(
                                      getChatSender(chat),
//                                "John John John John John John John John John John John ",
                                      style: textStyle(true, 12, light_green3),
                                      maxLines: 1,
                                    ),
                                  ),
                                if (splitMode) addSpaceWidth(5),
                                Text(
                                  getChatTime(chat.getInt(TIME)),
                                  style: textStyle(
                                      false, 12, black.withOpacity(.3)),
                                ),
                              ],
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (!chat.myItem()) chatUserImage(chat: chat)
        ],
      ),
    );
  }

  chatReplyWidget(BaseModel chat, {onRemoved, bool fitScreen = false}) {
    if (chat.items.isEmpty) return Container();
    String text = chat.getString(MESSAGE);
    int type = chat.getType();
    if (type == CHAT_TYPE_DOC) text = "Document";
    if (type == CHAT_TYPE_IMAGE) text = "Photo";
    if (type == CHAT_TYPE_VIDEO)
      text = "Video (${chat.getString(VIDEO_LENGTH)})";
    if (type == CHAT_TYPE_REC)
      text = "Voice Message (${chat.getString(AUDIO_LENGTH)})";
    var icon;
    if (type == CHAT_TYPE_DOC) icon = Icons.assignment;
    if (type == CHAT_TYPE_IMAGE) icon = Icons.photo;
    if (type == CHAT_TYPE_VIDEO) icon = Icons.videocam;
    if (type == CHAT_TYPE_REC) icon = Icons.mic;

    String image = "";
    if (type == CHAT_TYPE_IMAGE) image = chat.getString(IMAGE_URL);
    if (type == CHAT_TYPE_VIDEO) image = chat.getString(THUMBNAIL_URL);

    return GestureDetector(
      onTap: () {
        scrollToMessage(chat.getObjectId());
      },
      child: Container(
//    width: 100,
          width: fitScreen ? double.infinity : null,
          child: Card(
            clipBehavior: Clip.antiAlias,
            color: default_white,
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                side: BorderSide(color: black.withOpacity(.1), width: .5)),
            child: Container(
              decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: appColor, width: 5))),
              child: Row(
                mainAxisSize: fitScreen ? MainAxisSize.max : MainAxisSize.min,
                children: [
                  Flexible(
                    fit: fitScreen ? FlexFit.tight : FlexFit.loose,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(
                          10,
                          onRemoved == null ? 10 : 0,
                          onRemoved == null ? 10 : 0,
                          image.isEmpty ? 10 : 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              if (onRemoved != null /*&& image.isEmpty*/)
                                Container(
                                  width: 25,
                                  height: 25,
                                  child: FlatButton(
                                      padding: EdgeInsets.all(0),
                                      onPressed: () {
                                        onRemoved();
                                      },
                                      color: appColor,
                                      child: Icon(
                                        Icons.close,
                                        size: 16,
                                        color: white.withOpacity(.8),
                                      )),
                                ),
                              addSpaceWidth(10),
                              Flexible(
                                  fit: !fitScreen
                                      ? FlexFit.loose
                                      : FlexFit.tight,
                                  child: Text(
                                    chat.myItem() ? "YOU" : getChatSender(chat),
                                    style: textStyle(
                                        true, 12, black.withOpacity(.5)),
                                  )),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (icon != null)
                                Icon(
                                  icon,
                                  size: 14,
                                  color: black.withOpacity(.3),
                                ),
                              addSpaceWidth(3),
                              Flexible(
                                child: Text(
                                  text,
                                  style: textStyle(false, 14, black),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              addSpaceWidth(10),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (image.isNotEmpty)
                    Container(
                      height: 50,
                      width: 50,
                      color: blue3,
                      child: CachedNetworkImage(
                        imageUrl: image,
                        fit: BoxFit.cover,
                      ),
                    )
                  /*if (image.isNotEmpty)
                    Container(
                      width: 50,
                      height: 50,
                      child: Stack(
                        fit: StackFit.expand,
                        children: [
                          CachedNetworkImage(
                            imageUrl: image,
                            fit: BoxFit.cover,
                          ),
                          if (onRemoved != null)
                            */ /*  Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                width: 16,
                                height: 16,
                                margin: EdgeInsets.all(2),
                                child: FlatButton(
                                    padding: EdgeInsets.all(0),
                                    onPressed: () {
                                      onRemoved();
                                    },
                                    shape: CircleBorder(),
                                    color: white.withOpacity(.5),
                                    child: Icon(
                                      Icons.close,
                                      size: 12,
                                      color: black.withOpacity(.5),
                                    )),
                              ),
                            ),*/ /*
                            Container(
                              width: 25,
                              height: 25,
                              child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  onPressed: () {
                                    onRemoved();
                                  },
                                  color: Colors.red,
                                  child: Icon(
                                    Icons.close,
                                    size: 16,
                                    color: white.withOpacity(.8),
                                  )),
                            )
                        ],
                      ),
                    )*/
                ],
              ),
            ),
          )),
    );
  }

  String getChatSender(BaseModel chat) {
    // if (chat.getUserId() == chatSetup.getString(FARM_ADMIN))
    //   return chatSetup.getString(FARM_NAME);

    return chat.getString(NAME);
  }

  String errorText = "";
  showError(String text) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      errorText = "";
      setState(() {});
    });
  }

  clickedBack() async {
    if (showEmoji) {
      showEmoji = false;
      setState(() {});
      return;
    }
    visibleChatId = "";
    updateTyping(false);

    SharedPreferences shed = await SharedPreferences.getInstance();
    String text = messageController.text;
    shed.setString(CHAT_DRAFT, text);
    updateChatToRead();
    Navigator.pop(context);
  }
}
