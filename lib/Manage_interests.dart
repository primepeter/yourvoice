import 'package:acclaim/app/app.dart';
import 'package:acclaim/create_interest.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ManageInterests extends StatefulWidget {
  final List selections;
  ManageInterests(this.selections);
  @override
  _ManageInterestsState createState() => _ManageInterestsState();
}

class _ManageInterestsState extends State<ManageInterests> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();

  bool setup = false;
  bool showCancel = false;
  FocusNode focusSearch = FocusNode();
  List allItems = [];
  List items = [];
  List selections;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selections = widget.selections;
    allItems = appSettingsModel.getList(APP_INTERESTS);
    allItems.sort((m1, m2) => m1[OBJECT_ID].compareTo(m2[OBJECT_ID]));
    reload();
  }

  reload() {
    String search = searchController.text.trim().toLowerCase();
    items.clear();
    for (Map item in allItems) {
      if (search.isNotEmpty) {
        List values = item.values.toList();
        bool exist = false;
        for (var a in values) {
          if (a.toString().toLowerCase().contains(search)) {
            exist = true;
            break;
          }
        }
        if (!exist) continue;
      }
      items.add(item);
    }

    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //print(items.toList());

    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, selections);
        return;
      },
      child: Scaffold(
        body: page(),
        backgroundColor: white,
        key: _scaffoldKey,
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        addSpace(40),
        Row(
          children: <Widget>[
            InkWell(
                onTap: () {
                  Navigator.pop(context, selections);
                },
                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                      child: Icon(
                    Icons.keyboard_backspace,
                    color: black,
                    size: 25,
                  )),
                )),
            Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Text(
                  "Interests Manager",
                  style: textStyle(true, 20, black),
                )),
            addSpaceWidth(10),
            FlatButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                color: red0,
                onPressed: () {
                  pushAndResult(context, CreateInterest(), result: (_) {
                    items = appSettingsModel.getList(APP_INTERESTS);
                    allItems = appSettingsModel.getList(APP_INTERESTS);
                    setState(() {});
                  });
                },
                child: Text(
                  "Create",
                  style: textStyle(true, 14, white),
                )),
            addSpaceWidth(20),
          ],
        ),
        addSpace(5),
        Container(
          height: 45,
          margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
          decoration: BoxDecoration(
              color: white.withOpacity(.8),
              borderRadius: BorderRadius.circular(25),
              border: Border.all(color: black.withOpacity(.1), width: 1)),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              addSpaceWidth(10),
              Icon(
                Icons.search,
                color: blue3.withOpacity(.5),
                size: 17,
              ),
              addSpaceWidth(10),
              new Flexible(
                flex: 1,
                child: new TextField(
                  textInputAction: TextInputAction.search,
                  textCapitalization: TextCapitalization.sentences,
                  autofocus: false,
                  onSubmitted: (_) {
                    //reload();
                  },
                  decoration: InputDecoration(
                      hintText: "Search",
                      hintStyle: textStyle(
                        false,
                        18,
                        blue3.withOpacity(.5),
                      ),
                      border: InputBorder.none,
                      isDense: true),
                  style: textStyle(false, 16, black),
                  controller: searchController,
                  cursorColor: black,
                  cursorWidth: 1,
                  focusNode: focusSearch,
                  keyboardType: TextInputType.text,
                  onChanged: (s) {
                    showCancel = s.trim().isNotEmpty;
                    setState(() {});
                    reload();
                  },
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    focusSearch.unfocus();
                    showCancel = false;
                    searchController.text = "";
                  });
                  reload();
                },
                child: showCancel
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                        child: Icon(
                          Icons.close,
                          color: black,
                          size: 20,
                        ),
                      )
                    : new Container(),
              )
            ],
          ),
        ),
        Expanded(
            child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, crossAxisSpacing: 5, mainAxisSpacing: 5

              //childAspectRatio: 2.5
              ),
          itemBuilder: (ctx, p) {
            Map item = items[p];
            BaseModel model = BaseModel(items: item);
            String langKey = model.getObjectId();
            Map translations = model.getMap(TRANSLATIONS);
            String english = translations['english'] ?? "";
            bool selected = selections.contains(langKey);
            return GestureDetector(
              onLongPress: () {
                showListDialog(context, ["Edit", "Delete"],   (_) {
                  if (_ == "Edit") {
                    pushAndResult(
                        context,
                        CreateInterest(
                          model: model,
                        ), result: (_) {
                      print("....$_");

                      items = appSettingsModel.getList(APP_INTERESTS);
                      allItems = appSettingsModel.getList(APP_INTERESTS);
                      setState(() {});
                    });
                  }

                  if (_ == "Delete") {
                    yesNoDialog(context, "Delete?",
                        "Are you sure you want to delete this interest item?",
                        () {
                      final appInterests =
                          appSettingsModel.getList(APP_INTERESTS);
                      appInterests.remove(item);
                      appSettingsModel
                        ..put(APP_INTERESTS, appInterests)
                        ..updateItems();
                      setState(() {});
                    });
                  }
                }, returnIndex: false);
              },
              child: Card(
                child: Column(
                  children: [
                    Expanded(
                      child: CachedNetworkImage(
                        imageUrl: model.getString(ICON),
                        fit: BoxFit.cover,
                        width: double.infinity,
                        height: double.infinity,
                        placeholder: (s, c) {
                          return placeHolder(80);
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(5),
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: List.generate(translations.values.length,
                              (index) {
                            String key = translations.keys.toList()[index];
                            String value = translations.values.toList()[index];
                            if (value.isEmpty) value = "No Translation";

                            return Row(
                              //mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  key.toUpperCase(),
                                  style: textStyle(
                                      false, 12, black.withOpacity(.6)),
                                ),
                                addSpaceWidth(5),
                                Expanded(
                                  child: Text(
                                    value,
                                    maxLines: 1,
                                    style: textStyle(false, 12, black),
                                  ),
                                ),
                              ],
                            );
                          })),
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: items.length,
          padding: 5.padAll(),
          //physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
        )),
      ],
    );
  }
}
