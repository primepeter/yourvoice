import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_photo_picker/photo_picker.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'LangMain.dart';
import 'app/app.dart';
import 'app_config.dart';
import 'app_index.dart';
import 'preview_post_images.dart';

class AddNote extends StatefulWidget {
  final BaseModel model;

  const AddNote({Key key, this.model}) : super(key: key);
  @override
  _AddNoteState createState() => _AddNoteState();
}

class _AddNoteState extends State<AddNote> {
  final titleController = TextEditingController();
  final messageController = TextEditingController();
  bool imagesEdited = false;
  List<BaseModel> photos = [];
  final scaffoldKey = GlobalKey<ScaffoldState>();

  String id = getRandomId();
  BaseModel model = BaseModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.model != null) {
      model = widget.model;
      id = widget.model.getObjectId();
      titleController.text = model.getString(TITLE);
      messageController.text = model.getString(MESSAGE);
      photos = model.getListModel(IMAGES);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: white,
      body: pageBody(),
    );
  }

  pageBody() {
    return Container(
      margin: 20.padAt(t: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.centerRight,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context, "");
              },
              shape: CircleBorder(),
              padding: EdgeInsets.all(20),
              child: Icon(Icons.close),
            ),
          ),
          Container(
            padding: EdgeInsets.all(14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        Language.value(LangKey.addaNote),
                        textAlign: TextAlign.start,
                        style: textStyle(true, 25, Colors.black),
                      ),
                      addSpace(4),
                      Text(
                        Language.value(LangKey.noteSub),
                        style:
                            textStyle(false, 14, Colors.black.withOpacity(.7)),
                      )
                    ],
                  ),
                ),
                /*Image.asset(
                  "assets/images/suggestions.png",
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                )*/
              ],
            ),
          ),
          //addSpace(15),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            width: double.infinity,
            height: errorText.isEmpty ? 0 : 40,
            color: red0,
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Center(
                child: Text(
              errorText,
              style: textStyle(true, 16, white),
            )),
          ),

          Flexible(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(5),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    textCapitalization: TextCapitalization.sentences,
                    textAlignVertical: TextAlignVertical.top,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        //counter: null,
                        //labelText: "title",
                        labelStyle: textStyle(false, 18, black.withOpacity(.3)),
                        hintText: Language.value(LangKey.noteTitle)),
                    style: textStyle(
                      false,
                      18,
                      black,
                    ),
                    //focusNode: focusNode,
                    controller: titleController,
                    cursorColor: black,
                    cursorWidth: 1,
                    keyboardType: TextInputType.multiline,
                    inputFormatters: [],
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: blue09,
                      borderRadius: BorderRadius.circular(5),
                      border:
                          Border.all(color: black.withOpacity(.1), width: .5)),
                  child: new TextField(
                    textCapitalization: TextCapitalization.sentences,
                    textAlignVertical: TextAlignVertical.top,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        //counter: null,
                        //labelText: "title",
                        labelStyle: textStyle(false, 18, black.withOpacity(.3)),
                        hintText: Language.value(LangKey.noteDescription)),
                    style: textStyle(
                      false,
                      18,
                      black,
                    ),
                    //focusNode: focusNode,
                    controller: messageController,
                    cursorColor: black,
                    cursorWidth: 1,
                    maxLines: 6,
                    keyboardType: TextInputType.multiline,
                    inputFormatters: [],
                    scrollPadding: EdgeInsets.all(0),
                  ),
                ),
                addSpace(10),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          PhotoPicker.openPicker(
                            mediaType: 'image', // image | video | any
                            multiple: true,
                            limit: 6,
                          ).then((value) {
                            if (null == value) return;
                            photos = value.map((e) {
                              BaseModel model = BaseModel();
                              bool isVideo = e.type == "video";
                              if (isVideo) {
                                model.put(THUMBNAIL_PATH,
                                    e.thumbnailUrl.replaceAll("file://", ""));
                                model.put(VIDEO_PATH,
                                    e.thumbnailUrl.replaceAll("file://", ""));
                                model.put(IS_VIDEO, true);
                              } else {
                                model.put(IMAGE_PATH,
                                    e.url.replaceAll("file://", ""));
                              }
                              return model;
                            }).toList();
                            setState(() {});
                          }).catchError((e) {
                            logError(e.code, e.bookmarkList);
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.all(5),
                          height: photos.isEmpty ? 100 : 50,
                          width: photos.isEmpty ? 100 : 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: black.withOpacity(0.09)),
                          child: Center(
                              child: Icon(
                            Icons.add_circle_outline,
                            size: 20,
                          )),
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Language.value(LangKey.addPhotosAndVideos),
                              style: textStyle(true, 18, black),
                            ),
                            Text(
                              Language.value(LangKey.optional),
                              style:
                                  textStyle(false, 13, black.withOpacity(.5)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                addSpace(10),
                imagesLayout(),
                /* Row(
                  children: [
                    Flexible(
                      child: SingleChildScrollView(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: List.generate(photos.length, (index) {
                            BaseModel model = photos[index];
                            String imagePath = model.getString(IMAGE_PATH);
                            String imageUrl = model.getString(IMAGE_URL);
                            bool online = imageUrl.startsWith("http");
                            bool active = 1 == index;
                            print(imageUrl);
                            return GestureDetector(
                              onTap: () {
                                setState(() {});
                              },
                              child: Container(
                                padding: EdgeInsets.all(1),
                                margin: EdgeInsets.all(5),
                                height: 160,
                                width: 160,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: black.withOpacity(.1)),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Stack(
                                      children: [
                                        if (online)
                                          CachedNetworkImage(
                                            imageUrl: imageUrl,
                                            height: 160,
                                            width: 160,
                                            fit: BoxFit.cover,
                                          )
                                        else
                                          Image.file(
                                            File(imagePath),
                                            height: 160,
                                            width: 160,
                                            fit: BoxFit.cover,
                                          ),
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Flexible(
                                                child: FlatButton(
                                                  onPressed: () {},
                                                  child: Icon(
                                                    Icons.visibility,
                                                    color: white,
                                                    size: 15,
                                                  ),
                                                  color: AppConfig.appColor,
                                                  padding: EdgeInsets.all(0),
                                                  materialTapTargetSize:
                                                      MaterialTapTargetSize
                                                          .shrinkWrap,
                                                  */ /*shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),*/ /*
                                                ),
                                              ),
                                              Flexible(
                                                child: FlatButton(
                                                  onPressed: () {
                                                    photos.remove(model);
                                                    setState(() {});
                                                  },
                                                  child: Icon(
                                                    Icons.delete,
                                                    color: white,
                                                    size: 15,
                                                  ),
                                                  color: red0,
                                                  padding: EdgeInsets.all(0),
                                                  materialTapTargetSize:
                                                      MaterialTapTargetSize
                                                          .shrinkWrap,
                                                  */ /*shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topRight: Radius
                                                                  .circular(5),
                                                              bottomRight:
                                                                  Radius
                                                                      .circular(
                                                                          5))),*/ /*
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )),
                              ),
                            );
                          }),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        PhotoPicker.openPicker(
                          mediaType: 'image', // image | video | any
                          multiple: true,
                          limit: 6,
                        ).then((value) {
                          if (null == value) return;
                          photos = value.map((e) {
                            BaseModel model = BaseModel();
                            bool isVideo = e.type == "video";
                            if (isVideo) {
                              model.put(THUMBNAIL_PATH,
                                  e.thumbnailUrl.replaceAll("file://", ""));
                              model.put(VIDEO_PATH,
                                  e.thumbnailUrl.replaceAll("file://", ""));
                              model.put(IS_VIDEO, true);
                            } else {
                              model.put(
                                  IMAGE_PATH, e.url.replaceAll("file://", ""));
                            }
                            return model;
                          }).toList();
                          setState(() {});
                        }).catchError((e) {
                          logError(e.code, e.message);
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.all(5),
                        height: photos.isEmpty ? 100 : 50,
                        width: photos.isEmpty ? 100 : 50,
                        decoration: BoxDecoration(
                            shape: photos.isEmpty
                                ? BoxShape.rectangle
                                : BoxShape.circle,
                            borderRadius: photos.isEmpty
                                ? BorderRadius.circular(10)
                                : null,
                            color: black.withOpacity(0.09)),
                        child: Center(
                            child: Icon(
                          Icons.add_circle_outline,
                          size: 20,
                        )),
                      ),
                    )
                  ],
                ),*/
                addSpace(10),
                Container(
                  padding: EdgeInsets.all(14),
                  child: FlatButton(
                    onPressed: () {
                      onSavePressed();
                    },
                    color: AppConfig.appColor,
                    padding: EdgeInsets.all(16),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: Center(
                      child: Text(
                        Language.value(LangKey.save),
                        style: textStyle(true, 16, white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  imagesLayout() {
    //CollageType.LeftBig 3
    //CollageType.VSplit 2

    int size = photos.length;
    CollageType type1 = size == 1
        ? CollageType.FourLeftBig
        : size == 2
            ? CollageType.VSplit
            : CollageType.LeftBig;
    CollageType type2 = size == 1
        ? CollageType.NineSquare
        : size == 2
            ? CollageType.VSplit
            : CollageType.LeftBig;
    return StaggeredGridView.countBuilder(
        shrinkWrap: true,
        itemCount: size > 3 ? 3 : size,
        padding: EdgeInsets.all(10),
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: getCrossAxisCount(type1),
        //primary: true,
        itemBuilder: (BuildContext context, int p) {
          BaseModel bm = photos[p];
          String imageUrl = bm.getString(IMAGE_URL);
          String imagePath = bm.getString(IMAGE_PATH);
          String thumbPath = bm.getString(THUMBNAIL_PATH);
          String thumbUrl = bm.getString(THUMBNAIL_URL);
          bool onlineImage = imageUrl.startsWith("http");
          bool isVideo = bm.isVideo;
          String heroTag = "image$p";

          return GestureDetector(
            onTap: () {
              pushAndResult(
                  context,
                  PreviewPostImages(
                    photos,
                    [],
                    edittable: true,
                    indexOf: p,
                    heroTag: heroTag,
                  ), result: (_) {
                if (photos.length != _.length) {
                  model
                    ..put(IMAGES, photos.map((e) => e.items).toList())
                    ..updateItems();
                  print("updating....");
                }
                photos = _;

                setState(() {});
              });
            },
            child: Stack(
              fit: StackFit.passthrough,
              children: <Widget>[
                if (onlineImage)
                  Container(
                    margin: EdgeInsets.all(1.2),
                    child: CachedNetworkImage(
                      imageUrl: isVideo ? thumbUrl : imageUrl,
                      fit: BoxFit.cover,
                      placeholder: (_, s) {
                        return placeHolder(250, width: double.infinity);
                      },
                    ),
                  )
                else
                  Container(
                    margin: EdgeInsets.all(1.2),
                    child: Image.file(
                      File(isVideo ? thumbPath : imagePath),
                      fit: BoxFit.cover,
                    ),
                  ),
                if (isVideo)
                  Container(
                    height: 250,
                    child: Center(
                      child: Container(
                        height: 30,
                        width: 30,
                        padding: 3.padAll(),
                        margin: 6.padAll(),
                        decoration: BoxDecoration(
                          color: black.withOpacity(.5),
                          border: Border.all(color: white, width: 2),
                          shape: BoxShape.circle,
                        ),
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.play_arrow,
                          color: white,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                if (size > 3 && p == 2)
                  Container(
                    color: AppConfig.appColor.withOpacity(.7),
                    margin: EdgeInsets.all(1.2),
                    alignment: Alignment.center,
                    child: Text(
                      "+${size - 3}",
                      style: textStyle(false, 18, white),
                    ),
                  ),
              ],
            ),
          );
        },
        staggeredTileBuilder: (int index) => StaggeredTile.count(
            getCellCount(index: index, isForCrossAxis: true, type: type2),
            getCellCount(index: index, isForCrossAxis: false, type: type2)));
  }

  onSavePressed() async {
    bool hasInternet = await isConnected();

    if (!hasInternet) {
      showError(Language.value(LangKey.noInternet));
      return;
    }

    if (titleController.text.isEmpty) {
      showError(Language.value(LangKey.noteTitleRequired));
      return;
    }

    if (messageController.text.isEmpty) {
      showError(Language.value(LangKey.noteDescRequired));
      return;
    }

    model.put(OBJECT_ID, id);
    model.put(TITLE, titleController.text);
    model.put(MESSAGE, messageController.text);
    //model.put(STATUS, PENDING);
    model.put(IMAGES, photos.map((e) => e.items).toList());
    int p = myNotes.indexWhere((e) => e.getObjectId() == id);
    if (p == -1)
      myNotes.add(model);
    else
      myNotes[p] = model;
    noteController.add(model);
    Navigator.pop(context, "");
  }

  String errorText = "";
  showError(String text, {bool wasLoading = false}) {
    if (wasLoading) showProgress(false, context);
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 3), () {
      errorText = "";
      setState(() {});
    });
  }
}
