import 'package:acclaim/app/app.dart';
import 'package:acclaim/main_screens/show_person.dart';
import 'package:acclaim/people.dart';
import 'package:flutter/material.dart';

import 'LangMain.dart';
import 'app_config.dart';

class ShowFollowers extends StatefulWidget {
  final List<BaseModel> items;

  const ShowFollowers({Key key, this.items}) : super(key: key);
  @override
  _ShowFollowersState createState() => _ShowFollowersState();
}

class _ShowFollowersState extends State<ShowFollowers> {
  List<BaseModel> items = followers;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.items != null) items = widget.items;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  page() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Row(
            children: <Widget>[
              InkWell(
                  onTap: () {
                    Navigator.pop(context, "");
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.keyboard_backspace,
                      color: black,
                      size: 25,
                    )),
                  )),
              Expanded(
                  child: Text(
                LangKey.followers.toAppLanguage,
                style: textStyle(true, 20, black),
              )),
              InkWell(
                  onTap: () {
                    pushAndResult(
                        context,
                        People(
                          selections: followers,
                        ), result: (List<BaseModel> _) {
                      for (var d in _) {
                        userModel.putInList(
                            MY_FOLLOWER_IDS, d.getObjectId(), true);
                      }
                      userModel.updateItems();
                      setState(() {});
                    });
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    child: Center(
                        child: Icon(
                      Icons.search,
                      color: black,
                      size: 25,
                    )),
                  )),
            ],
          ),
        ),
        Expanded(
          child: Builder(
            builder: (c) {
              if (items.isEmpty)
                return emptyLayout(
                    Icons.people,
                    LangKey.noFollowers.toAppLanguage,
                    LangKey.noFollowersMsg.toAppLanguage);

              return ListView.builder(
                  itemCount: items.length,
                  padding: 0.padAll(),
                  itemBuilder: (ctx, p) {
                    BaseModel thisUser = items[p];
                    bool followed = followers.contains(thisUser.getUserId());

                    return InkWell(
                      onTap: () {
                        //print("hello");
                        pushAndResult(
                            context,
                            ShowPerson(
                              model: thisUser,
                            ));
                      },
                      child: Container(
                        padding: 8.padAll(),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom:
                                    BorderSide(color: black.withOpacity(.02)))),
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: Row(
                                children: <Widget>[
                                  imageHolder(50, thisUser.userImage),
                                  10.spaceWidth(),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        thisUser.fullName,
                                        style: textStyle(true, 14, black),
                                      ),
                                      5.spaceHeight(),
                                      Text(
                                        '@${thisUser.nickName}',
                                        style: textStyle(
                                            false, 12, black.withOpacity(.7)),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            5.spaceWidth(),
                            FlatButton(
                              onPressed: () {
                                followThisUser(thisUser, followed,
                                    onComplete: () {
                                  setState(() {});
                                });
                              },
                              color: AppConfig.appColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                  side: BorderSide(color: AppConfig.appColor)),
                              child: Center(
                                  child: Text(
                                followed ? "Unfollow" : "Follow",
                                style: textStyle(true, 12, white),
                              )),
                            ),
                          ],
                        ),
                      ),
                    );
                  });
            },
          ),
        )
      ],
    );
  }
}
